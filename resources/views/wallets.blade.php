<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trade any Coin - Deposits & Withdrawals</title>
<link rel="icon" type="image/png" href="https://tradeanycoin.com/assets/images/logo.png">
<!-- Bootstrap CSS CDN -->
<link rel="stylesheet" href="https://tradeanycoin.com/assets/bootstrap/css/bootstrap.min.css" >
<!-- Our Custom CSS -->
<link rel="stylesheet" href="https://tradeanycoin.com/assets/css/style.css">
<!-- Scrollbar Custom CSS -->
<link rel="stylesheet" href="https://tradeanycoin.com/assets/css/jquery.mCustomScrollbar.min.css">

<!-- Font Awesome JS -->
<script defer src="https://tradeanycoin.com/assets/js/solid.js"></script>
<script defer src="https://tradeanycoin.com/assets/js/fontawesome.js"></script>
</head>

<body>
<div class="wrapper"> 
  <?php
    $full_btc_usd  = json_decode($full_btc_usd = App\Models\Liveprice::where('key','full_btc_usd')->first()->value,true);
    $full_bch_usd  = json_decode($full_bch_usd = App\Models\Liveprice::where('key','full_bch_usd')->first()->value,true);
    $full_eth_usd  = json_decode($full_eth_usd = App\Models\Liveprice::where('key','full_eth_usd')->first()->value,true);
    $full_ltc_usd  = json_decode($full_ltc_usd = App\Models\Liveprice::where('key','full_ltc_usd')->first()->value,true);
    $full_neo_usd  = json_decode($full_neo_usd = App\Models\Liveprice::where('key','full_neo_usd')->first()->value,true);
  ?>
  <!-- Sidebar  -->
  <nav id="sidebar">
    <div class="sidebar-header">
      <h3><img src="https://tradeanycoin.com/assets/images/logo.jpg" alt="logo" /></h3>
    </div>
    <ul class="list-unstyled components">
      <li class="dashboard active"><a href="{{url('dashboard')}}">Dashboard</a></li>
      <li class="wallet"> <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Wallet</a>
        <ul class="collapse list-unstyled" id="homeSubmenu">
          <li><a href="{{ url('wallets/data' )}}">Deposit & Withdraws</a></li>
          <li><a href="{{url('wallets/history')}}">History</a></li>
        </ul>
      </li>
      <li class="exchange"> <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Exchange</a>
        <ul class="collapse list-unstyled" id="pageSubmenu">
          <li> <a href="{{url('exchange/data')}}">Exchange</a> </li>
          <li> <a href="{{url('exchange/orders')}}">My Open Orders</a> </li>
          <li> <a href="{{url('exchange/trade_history')}}">My Trade History</a> </li>
        </ul>
      </li>
      <li class="transaction"><a href="{{url('transaction')}}"l>Transaction</a></li>
      <li class="settings"> <a href="#settingsSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Settings</a>
        <ul class="collapse list-unstyled" id="settingsSubmenu">
          <li> <a href="{{url('setting/profile')}}">My Profile</a> </li>
          <li> <a href="{{url('setting/security')}}">Security</a> </li>
        </ul>
      </li>
      <li class="affiliate"> <a href="#affiliateSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Affiliate</a>
        <ul class="collapse list-unstyled" id="affiliateSubmenu">
          <li> <a href="{{url('affiliate')}}">Members</a> </li>
        </ul>
      </li>
      <li class="support"> <a href="{{url('support')}}">Support and Ticket</a> </li>
    </ul>
  </nav>
  
  <!-- Page Content  -->
  <div id="content">
    <nav id="headnev" class="navbar navbar-expand-lg navbar-light">
      <div class="container-fluid">
        <button type="button" id="sidebarCollapse" class="btn btn-info"> <i class="fas fa-align-left"></i> </button>
        <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <i class="fas fa-align-justify"></i> </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="nav navbar-nav ml-auto">
            <li class="nav-item active"> 1 BTC = <?php echo number_format($full_btc_usd['price'], 2)?> </li>
            <li class="nav-item"> 1 ETH = <?php echo number_format($full_eth_usd['price'], 2)?> </li>
            <li class="nav-item"> NAPIERIRF </li>
            <li class="nav-item"> <a class="nav-link" href="{{ url('logout') }}">Sign out</a> </li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="inner-content">
      <div class="col-md-12"> 
        <!--- Deposits Withdrawals Section Start --->
        <div id="deposits-withdrawals-tables">
          <h1>Deposits & Withdrawals</h1>
          <p>The maximum withdrawal in the USD per day: &pound;1000</p>
          <table class="table table-hover">
            <thead>
              <tr>
                <th>Coin</th>
                <th>Name</th>
                <th>Total Coin</th>
                <th>24H Exchange</th>
                <th>24H Volume</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
             <tr>
                <td data-title="Coin"><img src="https://tradeanycoin.com/assets/images/deposit-AC.png" alt="AC" />AC </td>
                <td data-title="Name"> Autocoin </td>
                <td data-title="Total Coin"> 170.34 </td>
                <td data-title="24H Exchange"> +2.11% </td>
                <td data-title="24H Volume"> 27,900, 011 POUND </td>
                <td data-title="Actions"><button class="btn btn-custom mb-2">Deposit</button>
                  <button class="btn btn-custom mb-2">Withdraw</button></td>
              </tr>
              <tr>
                <td data-title="Coin"><img src="https://tradeanycoin.com/assets/images/deposit-bitcoin.png" alt="bitcoin" />BCT </td>
                <td data-title="Name"> Bitcoin </td>
                <td data-title="Total Coin"> <?php echo number_format($full_btc_usd['total_supply'], 4)?> </td>
                <td data-title="24H Exchange"> <?php echo number_format($full_btc_usd['exchange'], 2);?>% </td>
                <td data-title="24H Volume"> <?php echo number_format($full_btc_usd['volume'], 2);?> POUND </td>
                <td data-title="Actions"><button class="btn btn-custom mb-2">Deposit</button>
                  <button class="btn btn-custom mb-2">Withdraw</button></td>
              </tr>
              <tr>
                <td data-title="Coin"><img src="https://tradeanycoin.com/assets/images/deposit-bitcoin.png" alt="bitcoin" />BCH </td>
                <td data-title="Name"> Bitcash </td>
                <td data-title="Total Coin"> <?php echo number_format($full_bch_usd['total_supply'], 4)?> </td>
                <td data-title="24H Exchange"> <?php echo number_format($full_bch_usd['exchange'], 2);?>% </td>
                <td data-title="24H Volume"> <?php echo number_format($full_bch_usd['volume'], 2);?> POUND </td>
                <td data-title="Actions"><button class="btn btn-custom mb-2">Deposit</button>
                  <button class="btn btn-custom mb-2">Withdraw</button></td>
              </tr>
              <tr>
                <td data-title="Coin"><img src="https://tradeanycoin.com/assets/images/deposit-ETH.png" alt="ETH" />ETH </td>
                <td data-title="Name"> Ethereum </td>
                <td data-title="Total Coin"> <?php echo number_format($full_eth_usd['total_supply'], 4)?> </td>
                <td data-title="24H Exchange"> <?php echo number_format($full_eth_usd['exchange'], 2);?>% </td>
                <td data-title="24H Volume"> <?php echo number_format($full_eth_usd['volume'], 2);?> POUND </td>
                <td data-title="Actions"><button class="btn btn-custom mb-2">Deposit</button>
                  <button class="btn btn-custom mb-2">Withdraw</button></td>
              </tr>
              <tr>
                <td data-title="Coin"><img src="https://tradeanycoin.com/assets/images/deposit-litecoin.png" alt="OMG" />LTC </td>
                <td data-title="Name"> Litecoin </td>
                <td data-title="Total Coin"> <?php echo number_format($full_ltc_usd['total_supply'], 4)?> </td>
                <td data-title="24H Exchange"> <?php echo number_format($full_ltc_usd['exchange'], 2);?>% </td>
                <td data-title="24H Volume"> <?php echo number_format($full_ltc_usd['volume'], 2);?> POUND </td>
                <td data-title="Actions"><button class="btn btn-custom mb-2">Deposit</button>
                  <button class="btn btn-custom mb-2">Withdraw</button></td>
              </tr>
              <tr>
                <td data-title="Coin"><img src="https://tradeanycoin.com/assets/images/deposit-NE.png" alt="NE" />NE </td>
                <td data-title="Name"> Neo </td>
                <td data-title="Total Coin"> <?php echo number_format($full_neo_usd['total_supply'], 4)?> </td>
                <td data-title="24H Exchange"> <?php echo number_format($full_neo_usd['exchange'], 2);?>% </td>
                <td data-title="24H Volume"> <?php echo number_format($full_neo_usd['volume'], 2);?> POUND </td>
                <td data-title="Actions"><button class="btn btn-custom mb-2">Deposit</button>
                  <button class="btn btn-custom mb-2">Withdraw</button></td>
              </tr>
            </tbody>
          </table>
        </div>
        
        <!--- Deposits Withdrawals Section End ---> 
      </div>
    </div>
  </div>
</div>
<!-- jQuery CDN - Slim version (=without AJAX) --> 
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> 
<!-- Bootstrap JS --> 
<script src="https://tradeanycoin.com/assets/bootstrap/js/bootstrap.min.js"></script> 
<!-- jQuery Custom Scroller CDN --> 
<script src="https://tradeanycoin.com/assets/js/jquery.mCustomScrollbar.concat.min.js"></script> 
<script src="https://tradeanycoin.com/assets/js/custom.js"></script>
</body>
</html>