<!doctype html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
	<head>
		<title>Open a Demat Account Online - Demat Account Opening at Upstox</title>

		<meta charset="utf-8">
		<meta name="description" content="Open a Demat Account - Avail hassle free paperless demat account opening online at Upstox with cross trading platform, technology one can trust, easy on the pocket and paperless account opening. Visit now to open demat account Instantly & Trade at lowest flat fee.">
		<meta name="keywords" content="demat account, open demat account, trading account, demat account procedures, trading account procedures, upstox"/>
		<script type='text/javascript'>window.mod_pagespeed_start = Number(new Date());</script><link rel="canonical" href="index.html"/>
		<link rel="amphtml" href="amp/index.html">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<link rel="apple-touch-icon-precomposed" sizes="57x57" href="https://cf.upstox.com/favicon/xapple-touch-icon-57x57.png.pagespeed.ic.PWN5ngBG2x.png"/>
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="https://cf.upstox.com/favicon/xapple-touch-icon-114x114.png.pagespeed.ic.P5fSoPRTRg.png"/>
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="https://cf.upstox.com/favicon/xapple-touch-icon-72x72.png.pagespeed.ic.dPr12OoDoO.png"/>
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="https://cf.upstox.com/favicon/xapple-touch-icon-144x144.png.pagespeed.ic.Cc5pAZNBGv.png"/>
		<link rel="apple-touch-icon-precomposed" sizes="60x60" href="https://cf.upstox.com/favicon/xapple-touch-icon-60x60.png.pagespeed.ic.B6TyfMw8sd.png"/>
		<link rel="apple-touch-icon-precomposed" sizes="120x120" href="https://cf.upstox.com/favicon/xapple-touch-icon-120x120.png.pagespeed.ic.VbdjAOh2gj.png"/>
		<link rel="apple-touch-icon-precomposed" sizes="76x76" href="https://cf.upstox.com/favicon/xapple-touch-icon-76x76.png.pagespeed.ic.5utlhsXYlm.png"/>
		<link rel="apple-touch-icon-precomposed" sizes="152x152" href="https://cf.upstox.com/favicon/xapple-touch-icon-152x152.png.pagespeed.ic.8ym6UzTz-q.png"/>
		<link rel="icon" type="image/png" href="https://cf.upstox.com/favicon/xfavicon-196x196.png.pagespeed.ic.m7rN5cB4c5.png" sizes="196x196"/>
		<link rel="icon" type="image/png" href="https://cf.upstox.com/favicon/xfavicon-96x96.png.pagespeed.ic.LO1nSCoxjW.png" sizes="96x96"/>
		<link rel="icon" type="image/png" href="https://cf.upstox.com/favicon/xfavicon-32x32.png.pagespeed.ic.TpxbBQzO8L.png" sizes="32x32"/>
		<link rel="icon" type="image/png" href="https://upstox.com/favicon/favicon-16x16.png.pagespeed.ce.hpgvXZabxg.png" sizes="16x16"/>
		<link rel="icon" type="image/png" href="https://cf.upstox.com/favicon/xfavicon-128.png.pagespeed.ic.wB80g_2Gse.png" sizes="128x128"/>

		<meta name="application-name" content="/favicon/&nbsp;"/>
		<meta name="msapplication-TileColor" content="/favicon/#FFFFFF"/>
		<meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png"/>
		<meta name="msapplication-square70x70logo" content="/favicon/mstile-70x70.png"/>
		<meta name="msapplication-square150x150logo" content="/favicon/mstile-150x150.png"/>
		<meta name="msapplication-wide310x150logo" content="/favicon/mstile-310x150.png"/>
		<meta name="msapplication-square310x310logo" content="/favicon/mstile-310x310.png"/>

					
		
		 <!-- Clevertap Code-->

	    <script data-pagespeed-orig-type="text/javascript" type="text/psajs" data-pagespeed-orig-index="0">var clevertap={event:[],profile:[],account:[],onUserLogin:[],notifications:[]};if(location.hostname=="uat.upstox.com"){clevertap.account.push({"id":"TEST-5W7-R7R-KR5Z"});}else if(location.hostname=="upstox.com"){clevertap.account.push({"id":"4W7-R7R-KR5Z"});}else{clevertap.account.push({"id":"TEST-5W7-R7R-KR5Z"});}(function(){var wzrk=document.createElement('script');wzrk.type='text/javascript';wzrk.async=true;wzrk.src=('https:'==document.location.protocol?'https://d2r1yp2w7bby2u.cloudfront.net':'http://static.clevertap.com')+'/js/a.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(wzrk,s);})();</script>

	     <!-- Clevertap Code Ends here-->
		

		<!-- Google Tag Manager -->
		<script type="text/psajs" data-pagespeed-orig-index="1">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-T99SMW');</script>
		<!-- End Google Tag Manager -->

		<!-- Loading Lato font here, optimize this-->
		<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">

		<!-- First fold CSS -->
		<style type="text/css">*{box-sizing:border-box}body{font-family:'Lato',sans-serif;margin:0}header.demat-header{width:100%}header.demat-header .upstox-logo{margin:50px;display:inline-block}header.demat-header .demat-help-button{display:inline-block;position:absolute;right:0;margin:50px;height:50px;width:80px;cursor:pointer;color:#0046bb}header.demat-header .demat-help-button span{}header.demat-header .demat-help-button span.demat-help-bubble{background:url(https://cf.upstox.com/open-demat-account/images/xhelp_bubble.png.pagespeed.ic.vY5wtg7fNg.png) no-repeat;background-size:contain;margin-right:10px;display:inline-block;width:20px;height:20px;position:relative;top:7px}main.first-fold{background:url(https://cf.upstox.com/open-demat-account/images/xoda_bg.png.pagespeed.ic.aZvTPJSpH4.png) no-repeat;background-size:85% 100%;width:100%;padding-bottom:60px}@media screen and (max-width:1370px){main.first-fold{background-size:1000px}.account-container section.demat-details{margin-top:70px}}.first-fold .account-container{display:flex;flex-direction:row;justify-content:space-evenly}.account-container .demat-details{color:#fffefe;flex:0 0 auto;margin-top:100px}.account-container .demat-details h1{font-size:50px;font-weight:300}.account-container .demat-details p{font-size:22px;font-weight:300}.account-container .demat-form,.account-container .signinbox{width:400px;background-color:#fff;box-shadow:0 0 19.3px 15.8px rgba(0,0,0,.05);margin-bottom:20px;margin-top:20px;padding:25px}.account-container .demat-form p{font-size:18px;letter-spacing:.8px;text-align:left;font-weight:bold;color:#aaa;margin-top:0;margin-bottom:20px;text-transform:uppercase;letter-spacing:1.5px}.account-container .demat-form input{opacity:.75;height:50px;width:100%;background-color:#fff;border:solid 1px #ccc;border-radius:0;padding:10px 20px;font-size:16px;margin-bottom:20px;font-family:Lato;font-size:16px;font-weight:400;letter-spacing:.8px;text-align:left}.demat-form #txtcontact{width:calc(100% - 49px)}.account-container .demat-form .input-group-addon{display:inline-block;line-height:50px;height:50px;position:relative;top:1px;color:#000;text-align:center;border:1px solid #ced4da;border-right:none;padding:0 10px}.demat-form #submitsignup{width:100%;height:50px;border:none;background:#0046bb;color:#fff;font-size:16px;font-weight:700;letter-spacing:1px;text-transform:uppercase;cursor:pointer}.demat-form .form-divider{position:relative;margin-top:30px;left:-25px;width: calc(100% + 50px);border-bottom:1px solid rgba(204,204,204,.75)}.demat-form #referral_section{margin-top:20px}.demat-form #referral_section label{font-size:14px;display:block;color:#666}.demat-form #referral_section .ref-wrapper{display:flex;flex-direction:row;margin-top:20px;margin-bottom:20px}button:focus{outline:0}.demat-form #referral_section .ref-wrapper button{border-radius:4px;border:solid 1px #ccc;padding:10px!important;background:transparent;font-size:16px;letter-spacing:.6px;text-align:center;color:#ccc;height:40px;line-height:14px;width:48%}.ref-yes{margin-right:20px}.demat-form #referral_section .ref-wrapper button.selected{border:solid 1px #0046bb;background:#e8f1ff;color:#0046bb}.account-container .signinbox p{margin-top:0;font-size:14px;text-transform:uppercase;color:#666;letter-spacing:1.5px}.account-container .signinbox a{font-size:18px;font-weight:700;color:#0046bb;letter-spacing:1.5px;border-bottom:2px solid #0046bb;padding-bottom:3px;text-decoration:none!important}small{display:none}@media screen and (max-width:1150px){header.demat-header .demat-help-button{color:#fff}.first-fold .account-container{flex-direction:column}.account-container section.demat-details{margin-top:0}.account-container .demat-details h1{text-align:center;font-size:36px;margin-top:0}.account-container .demat-details div{display:none}.account-container .form-container{display:flex;align-items:center;flex-direction:column}.account-container .demat-form .input-group-addon{top:0}}@media screen and (max-width:450px){.account-container .demat-details h1{font-size:22px}.account-container .demat-form p{font-size:14px}header.demat-header .demat-help-button{margin:15px 20px;font-size:12px;width:62px}header.demat-header .upstox-logo{margin:20px}header.demat-header .upstox-logo img{height:20px}.account-container .demat-form,.account-container .signinbox{width:calc(100% - 30px);min-width:200px;margin-bottom:20px;margin-top:20px;padding:15px}.account-container .demat-form input{height:40px;font-size:14px;padding:15px}.account-container .demat-form .input-group-addon{line-height:40px;height:40px;top:1px;font-size:14px}.account-container .signinbox{padding-bottom:20px;margin-top:5px}.account-container .signinbox p{font-size:12px;margin-bottom:5px}.account-container .signinbox a{font-size:14px;margin-bottom:5px}main.first-fold{padding-bottom:20px;margin-bottom:20px}.demat-form .form-divider{width: calc(100% + 30px);left:-15px}}</style>
		<!-- End First fold CSS -->
	</head>


	<body><noscript><meta HTTP-EQUIV="refresh" content="0; URL=https://upstox.com/open-demat-account/'https://upstox.com/open-demat-account/?PageSpeed=noscript'" /><style><!--table,div,span,font,p{display:none} --></style><div style="display:block">Please click <a href="index.html?PageSpeed=noscript">here</a> if you are not redirected within a few seconds.</div></noscript>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T99SMW" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->

		<main class="first-fold">
			<header class="demat-header">
				<div class="upstox-logo">
					<a href="../kb-tags/futures/index.html"><img src="img/bitex-white-2.png"></a>
				</div>
				<div class="demat-help-button" onclick="toggleSidebar(true)"><span class="demat-help-bubble"></span><span>HELP</span></div>
			</header>
			<div class="account-container">
				<section class="demat-details">
					<h1>Create an account</h1>
					<div>
						<p style="line-height: 40px; letter-spacing: 0.8px;">
							Upstox has everything you need to <br> 
							take your trade skills to the next level. <br> Open a demat & trading account for an<br> unparalleled experience.
						</p>
					</div>
				</section>

				<div class="form-container">
					<section class="demat-form">
					@if(Session::has('flash_notification.message'))
                        <script>toastr.{{ Session::get('flash_notification.level') }}('{{ Session::get("flash_notification.message") }}', 'Response Status')</script>
                    @endif
                    @if (isset($msg))
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ $msg }}
                        </div>
                    @endif
                    @if (isset($error))
                        <div class="alert alert-error">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ $error }}
                        </div>
                    @endif
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
						<p>Sign Up</p>
						{!! Form::open(array('url' => url('register'), 'method' => 'post', 'name' => 'form','class'=>'register-form')) !!}
							<div id="emailGroup" class="form-group" style="display: inline-table;">
								{!! Form::text('first_name', null, array('class' => 'form-control', 'placeholder'=>trans_choice('general.first_name',1),'required'=>'required','id'=>'first_name')) !!}
							</div>
							<div id="emailGroup" class="form-group" style="display: inline-table;">
								{!! Form::text('last_name', null, array('class' => 'form-control', 'placeholder'=>trans_choice('general.last_name',1),'required'=>'required','id'=>'last_name')) !!}
							</div>

							<div id="emailGroup" class="form-group">
								{!! Form::email('email', null, array('class' => 'form-control', 'placeholder'=>trans_choice('general.email',1),'required'=>'required','id'=>'email')) !!}
								
							</div>
							<div id="passwordGroup" class="form-group">
								{!! Form::password('password', array('class' => 'form-control', 'placeholder'=>trans('general.password'),'required'=>'required','id'=>'password')) !!}
								
							</div>
							<div id="passwordGroup" class="form-group">
								{!! Form::password('repeat_password', array('class' => 'form-control', 'placeholder'=>trans('general.repeat_password'),'required'=>'required','id'=>'repeat_password')) !!}
								
							</div>
							<div id="mobileGroup" class="input-group">
								<div class="input-group-addon">{!! Form::select('code',array(''=>'Country Code','91'=>'IN (+91)','971' => 'UAE (+971) ','1'=>'USA (+1)',),old('code'),array('class' => 'select form-control','required'=>'required','id'=>'code')) !!}
								</div>
								{!! Form::text('phone', null, array('class' => 'form-control', 'placeholder'=>trans_choice('general.phone',1),'required'=>'required','id'=>'phone')) !!}
								
							</div>
							<div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="newsletter" class="styled" checked="checked"
                                       id="newsletter">
                                {{ trans('general.subscribe_to_newsletter') }}
                            </label>
                        </div>

                        <div class="checkbox">
                            <label class="checkbox-inline">
                                <input type="checkbox" name="terms" class="styled" checked="checked" id="terms"
                                       required>
                                {{ trans('general.accept_terms') }}
                            </label>
                        </div>
                    
							<div class="form-group">
								<button id="submitsignup" type="submit" class="btn-primary">{{ trans('general.sign_up') }}</button>
							</div>
							{!! Form::close() !!}
							<div class="form-divider"></div>
							<div id="referral_section" class="input-group">
								<label>Have you been referred by someone you know?</label>
								<div class="ref-wrapper">
									<button id="refYesBtn" onclick="showRefCodeInput(this)" type="button" class="ref-yes" clevertapevent="Referral Yes">Yes</button>
									<button id="refNoBtn" onclick="hideRefCodeInput(this)" type="button" class="ref-no float-right selected" clevertapevent="Referral No">No</button>
								</div>
							</div>
							<div id="iccodeGroup" class="input-group">
								<!-- <label>Introducer's Client Code</label> -->
								<input id="id_iccode" onkeypress="return isNumber(event)" maxlength="6" class="form-control" placeholder="Introducer's Client Code" style="display:none;" clevertapevent="Referral ID Text Box">
								<small class="validation-text">Introducer code must be 6 digits</small>
							</div>
						</form>
					</section>

					<section class="signinbox">
						<p>Or continue your application</p>
													<a href="account" id="ac-sign-in" clevertapevent="Sign In">SIGN IN</a>
											</section>
				</div>
			</div>			
		</main>
		<!-- End of First-Fold -->

		<!-- JS File -->
		<script data-pagespeed-orig-type="text/javascript" src="https://cf.upstox.com/open-demat-account/app.js.pagespeed.jm.pLWohfiWgz.js" defer type="text/psajs" data-pagespeed-orig-index="2"></script>

		<style>.no-select{-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-o-user-select:none;user-select:none}section.demat-help{position:fixed;height:100vh;width:100vw;top:0;overflow:hidden;z-index:2}.demat-help .help-overlay{background-color:rgba(0,0,0,.5);width:100%;height:100%}.help-sidebar{width:430px;height:100%;position:absolute;right:-400px;top:0;display:block;background-color:#fff}.help-sidebar h6{font-size:18px;margin:25px 30px}.help-sidebar h6 span{position:absolute;right:50px;cursor:pointer;font-size:12px;top:30px}section.demat-help{}.demat-help .help-overlay{}.demat-help .help-sidebar{transition:all .2s ease-out}.help-sidebar h2{font-size:18px}.help-sidebar .help-topics-container{padding:0 30px;height:80%;overflow-y:auto}.help-sidebar .help-topic{border:1px solid rgba(0,0,0,.125);border-radius:.25rem;margin-bottom:30px}.help-topic .topic-name{background:#ececec;padding:20px;font-weight:700;font-size:16px;cursor:pointer;margin:0}.help-topic .topic-dropdown{font-size:14px;line-height:1.43;letter-spacing:.7px;text-align:left;color:#4c4c4c;max-height:0;overflow:hidden;-webkit-transition:max-height .2s ease-out;-moz-transition:max-height .2s ease-out;-o-transition:max-height .2s ease-out;transition:max-height .2s ease-out}.help-topic .topic-dropdown.show-faq{max-height:300px;padding:20px}.topic-dropdown div{}.topic-dropdown ul{padding:15px;margin-top:0}.topic-dropdown li{}main.second-fold{background:url(https://cf.upstox.com/open-demat-account/images/grey-bottom.svg) no-repeat;width:100%;background-position:right top 6px}section.demat-benefits{width:100%;margin-bottom:60px}section.demat-benefits .benefits-container{display:flex;width:900px;flex-wrap:wrap;justify-content:space-between;margin:0 auto}.benefits-container .benefits{width:320px;text-align:center;margin-bottom:50px}.benefits-container .benefits h2{font-size:22px;margin:20px 0}.benefits-container .benefits p{font-size:18px;line-height:35px;letter-spacing:.5px}.benefits .benefits-img{margin:0 auto;height:65px;width:65px;background:url(https://cf.upstox.com/open-demat-account/images/xdemat_sprite.png.pagespeed.ic._cYAE8glE8.png) no-repeat}.benefits-img.cross-image{background-position:-3px 5px}.benefits-img.tech-image{background-position:-73px 6px}.benefits-img.cost-image{background-position:-142px 5px}.benefits-img.paper-image{background-position:-207px -2px}section.demat-howto .howto-container{display:flex;width:1000px;flex-wrap:wrap;justify-content:space-between;margin:0 auto}.howto-container .howto{width:320px}.howto-container .howto p{display:inline-block;width:220px;margin:0;margin-left:20px;font-size:16px;line-height:30px;letter-spacing:.5px}.howto .howto-img{margin:0 auto;height:180px;width:235px;background:url(https://cf.upstox.com/open-demat-account/images/xdemat_sprite.png.pagespeed.ic._cYAE8glE8.png) no-repeat;margin-bottom:30px}.howto .howto-num{height:80px;width:50px;display:inline-block;background:url(https://cf.upstox.com/open-demat-account/images/xdemat_sprite.png.pagespeed.ic._cYAE8glE8.png) no-repeat}.howto-img.form-image{background-position:-2px -94px;margin-top:30px}.howto-img.scan-image{width:200px;height:210px;background-position:-250px -78px}.howto-img.welcome-image{height:205px;width:250px;background-position:-460px -79px}.howto-num.one-image{background-position:-328px 0}.howto-num.two-image{background-position:-462px 0}.howto-num.three-image{background-position:-662px 0}section.demat-faq{background-color:#0046bb;margin-top:80px;padding-top:5px}section.demat-faq .faq-container{display:flex;flex-wrap:wrap;width:1000px;margin:0 auto;justify-content:space-between;padding-bottom:60px}section.demat-faq .faq{flex:0 1 460px}section.demat-faq .faq a{color:#fff}section.demat-faq .faq h2.show-on-desktop{display:block}section.demat-faq .faq h2.show-on-mobile{display:none}.carousal-container .carousal-left{display:none}.carousal-container .carousal-right{display:none}@media screen and (max-width:1150px){body{background:#efefef}main.second-fold{background:#efefef}.carousal-container{display:flex;flex-direction:row;width:400px;margin:0 auto}.carousal-container .carousal-window{overflow:hidden;box-shadow:0 0 20px 0 rgba(0,0,0,.1)}.carousal-container .carousal{display:flex;height:100%;overflow:hidden;position:relative;-webkit-transition:left .2s ease-out;-moz-transition:left .2s ease-out;-o-transition:left .2s ease-out;transition:left .2s ease-out}.carousal-container .carousal-left{flex:0 0 40px;display:block;cursor:pointer;font-size:50px;text-align:center;line-height:340px}.carousal-container .carousal-right{flex:0 0 40px;display:block;cursor:pointer;font-size:50px;text-align:center;line-height:340px}.demat-benefits .carousal-container .carousal-window{height:320px}section.demat-benefits .benefits-container{width:1280px}.benefits-container .benefits{width:320px;height:100%;text-align:center;margin-bottom:50px;display:inline-block;background-color:#fff;padding:20px}section.demat-howto .howto-container{display:flex;width:100%;flex-direction:column;align-items:center}.howto-container .howto{margin-bottom:50px}section.demat-faq .faq-container{display:flex;flex-wrap:wrap;width:100%;padding:0 60px 60px 60px}section.demat-faq .faq{flex:0 1 100%}section.demat-faq .faq h2{cursor:pointer;-webkit-touch-callout:none;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}section.demat-faq .faq p{max-height:0;overflow:hidden;-webkit-transition:max-height .2s ease-out;-moz-transition:max-height .2s ease-out;-o-transition:max-height .2s ease-out;transition:max-height .2s ease-out}section.demat-faq .faq h2.show-on-desktop{display:none}section.demat-faq .faq h2.show-on-mobile{display:block}section.demat-feedback .carousal-container .feedback-container{font-size:16px;justify-content:left;width:1060px}section.demat-feedback .feedback-container .user-feedback{font-size:14px}section.demat-feedback .feedback-container .feedback{flex:0 0 300px;background:#fff;padding:20px}.demat-feedback .carousal-container{width:380px}.demat-feedback .carousal-container .carousal-window{height:340px}.second-fold section.demat-feedback h6{width:86%}.second-fold section.demat-account-link button{width:100%;padding:30px}body footer{padding:10px}}section.demat-faq h2{font-size:22px;font-weight:900;letter-spacing:1.1px;text-align:left;color:#fff;margin-bottom:20px}.faq p{font-size:16px;font-weight:300;text-align:left;color:#e0e0e0;letter-spacing:.6px;line-height:30px}section.demat-feedback{margin-top:60px;margin-bottom:60px}section.demat-feedback h6{font-size:22px;line-height:1.36;letter-spacing:.5px;text-align:center;color:#4c4c4c;width:500px;margin:0 auto;font-weight:400;margin-bottom:50px}section.demat-feedback .feedback-container{display:flex;flex-wrap:wrap;justify-content:space-around;width:1000px;margin:0 auto}.feedback-container .feedback{flex:1 1;text-align:center;padding:30px;position:relative}.feedback-container .feedback:hover{box-shadow:0 0 20px 0 rgba(0,0,0,.1)}.feedback .user-img{width:115px;height:115px;margin-bottom:30px;margin:0 auto;background-color:#d4d4d4;border-radius:50%;line-height:113px;font-size:70px;font-weight:900;color:#fff}.feedback .user-img.one{}.feedback .user-feedback{font-size:18px;line-height:1.39;letter-spacing:.4px;text-align:center;color:#2a2a2a;margin-bottom:30px;margin-top:40px}.feedback .user-name{font-weight:900;position:absolute;bottom:0;width:calc(100% - 60px)}section.demat-account-link{background-color:#0046bb;display:flex;flex-direction:column;align-items:center}section.demat-account-link h6{text-align:center;margin:0;font-size:22px;color:#fff;font-weight:400;margin-bottom:80px}section.demat-account-link button{background-color:#fff;box-shadow:0 14px 21px 0 rgba(0,0,0,.28);padding:27px 90px;font-size:18px;font-weight:700;letter-spacing:.9px;width:auto;text-align:center;color:#0046bb;text-transform:uppercase;border:none;margin-bottom:80px;cursor:pointer}footer{background:#000;font-size:20px;line-height:2;letter-spacing:.3px;color:#445a70!important;padding:60px 150px 30px 150px;display:flex;flex-direction:column}footer p,footer #footer_readmore{font-size:12px}footer p:not(.made-in-india){margin-top:0}footer h2{font-size:14px}footer h3{font-size:13px;margin-bottom:0}.custom-container{padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}@media (min-width:768px){.custom-container{width:750px}}@media (min-width:992px){.custom-container{width:970px}}@media (min-width:1200px){.custom-container{width:1000px}}footer #footer_readmore{color:#0046bb;cursor:pointer}footer a{text-decoration:none;color:#0046bb}.validation-text{display:none}#iccodeGroup .validation-text{bottom:-26px}.validation-text.invalid_email,.validation-text.invalid_mobile,.validation-text.zero_start{display:none}.validation-text.duplicate_email,.validation-text.duplicate_mobile,.validation-text.empty_password,.validation-text.short_password{display:none}.benefit-demat-ac{font-size:50px;font-weight:300;text-align:center;color:#001a46;width:100%;margin-bottom:50px}.subheading{font-size:22px;font-weight:900;line-height:1;letter-spacing:1.1px;text-align:center;color:#000}.how-to-open-demat-ac{font-size:50px;font-weight:300;text-align:center;color:#001a46;width:100%}.faq-title{font-size:50px;font-weight:300;text-align:center;color:#fff;width:100%;margin-top:60px;margin-bottom:60px}.testimonial-title{font-size:50px;font-weight:300;line-height:1;text-align:center;color:#001a46;width:100%;margin:60px 0 40px 0}.c2a-title{font-size:50px;font-weight:300;line-height:1;text-align:center;width:100%;margin-bottom:50px;margin-top:50px;color:#fff}@media screen and (max-width:450px){.help-sidebar{width:calc(100% - 40px)}.help-topic .topic-dropdown.show-faq{max-height:340px}.carousal-container{width:300px}.carousal-container .carousal-left,.carousal-container .carousal-right{flex:0 0 20px;line-height:300px}.demat-benefits .carousal-container .carousal-window{height:300px}.benefits-container .benefits{width:260px}.demat-feedback .carousal-container{width:300px}section.demat-feedback .feedback-container .feedback{flex:0 0 260px}.benefits-container .benefits p{font-size:14px;line-height:20px}.howto .howto-num{margin-left:23px}section.demat-faq .faq h2{font-size:16px}section.demat-faq .faq-container{padding:0 30px 20px}section.demat-faq .faq h2.show-on-mobile{text-align:center}section.demat-benefits .benefits-container{width:1160px}.help-topic .topic-name{overflow:hidden;word-wrap:break-word}.benefit-demat-ac,.how-to-open-demat-ac,.faq-title,.testimonial-title,.c2a-title{font-size:30px;font-weight:400}}</style>

		<section class="demat-help" style="display:none;">
			<div class="help-overlay" onclick="toggleSidebar(false)"></div>
			<div class="help-sidebar">
				<h6>Help<span onclick="toggleSidebar(false)">Close</span></h6>
				<div class="help-topics-container">
					<div class="help-topic">
						<h2 class="topic-name no-select" onclick="showFAQ(event, 'class', 'topic-dropdown')">Charges for opening a demat account with Upstox</h2>
						<div class="topic-dropdown show-faq">
							<ul>
								<li>Equity trading Rs. 300 (includes AMC) </li>
								<li>Commodity trading Rs. 150 </li>
							</ul>
							<div>A/C opening charges are non-refundable and exclusive of taxes.</div>
						</div>
					</div>
					<div class="help-topic" id="help-topic">
						<h2 class="topic-name no-select" onclick="showFAQ(event, 'class', 'topic-dropdown')">Opening a HUF/Corporate/Partnership/NRI account</h2>
						<div class="topic-dropdown">
							
							<div>You can download the physical account opening forms from our website to open non-individual or NRI accounts and send the completed forms to our mailing address on the forms. </div>
						</div>
					</div>
				</div>
			</div>
		</section>
		

		<main class="second-fold">
			<section class="demat-benefits">
				<h2 class="benefit-demat-ac">Benefits of opening a demat account</h2>
				<div class="carousal-container">
					<div class="carousal-left" onclick="carousalSlide(event, 'left')">&lsaquo;</div>
					<div class="carousal-window" data-carousal-width=320 data-carousal-count=4>
						<div class="benefits-container carousal" style="left: 0px;">
							<div class="benefits">
								<div class="benefits-img cross-image"></div>
								<h2 class="subheading">Cross-platform trading</h2>
								<p>Innovative, cutting-edge trading software on web, mobile and desktop.<br>Take your pick!</p>
							</div>
							
							<div class="benefits">
								<div class="benefits-img tech-image"></div>
								<h2 class="subheading">Tech you can trust</h2>
								<p>Ups and downs are a part of life &amp; share market. Don’t worry, Upstox platforms have got your back!</p>
							</div>

							<div class="benefits">
								<div class="benefits-img cost-image"></div>
								<h3 class="subheading">Easy on the pocket</h3>
								<p>Free equity deliveries, Rs. 20/intraday order. Your profits are yours. Why spend on brokerage?</p>
							</div>

							<div class="benefits">
								<div class="benefits-img paper-image"></div>
								<h2 class="subheading">Paperless account opening</h2>
								<p>Have an Aadhaar card? Forget the hassle of physical forms. Just fill out the forms online.</p>
							</div>
						</div>
					</div>
					<div class="carousal-right" onclick="carousalSlide(event, 'right')">&rsaquo;</div>
				</div>
			</section>

			<section class="demat-howto">
				<h2 class="how-to-open-demat-ac">How to open a demat account</h2>
				<div class="howto-container">
					<div class="howto">
						<div class="howto-img form-image"></div>
						<div class="howto-num one-image"></div>
						<p>Fill up the form, paper or paperless &amp; submit for verification.</p>
					</div>
					
					<div class="howto">
						<div class="howto-img scan-image"></div>
						<div class="howto-num two-image"></div>
						<p>All your details are scanned and verified for opening your account.</p>
					</div>

					<div class="howto">
						<div class="howto-img welcome-image"></div>
						<div class="howto-num three-image"></div>
						<p>Once verified, you receive a unique ID &amp; voila, you are ready to trade.</p>
					</div>
				</div>
			</section>

			<section class="demat-faq">
				<p class="faq-title">Frequently asked questions</p>
				<div class="faq-container">
					<div class="faq">
						<h2 class="show-on-desktop">How much time does it take to open a demat account?</h2>
						<h2 class="show-on-mobile" onclick="showFAQ(event, 'tag', 'p')">How much time does it take to open a demat account?</h2>
						<p>Once you submit your complete account opening form and upload all the required documents for verification, it takes about 12-24 hours to open an account.</p>
					</div>
					
					<div class="faq">
						<h2 class="show-on-desktop">What documents are required to open a demat account?</h2>
						<h2 class="show-on-mobile" onclick="showFAQ(event, 'tag', 'p')">What documents are required to open a demat account?</h2>
						<p>Opening a demat account is Aadhaar-based. You just have to keep these documents handy to fasttrack your application: Aadhaar Card, PAN Card, Canceled Cheque and the Latest Bank Statement with IFSC/MICR code.</p>
					</div>

					<div class="faq">
						<h2 class="show-on-desktop">What kind of margins does Upstox provide?</h2>
						<h2 class="show-on-mobile" onclick="showFAQ(event, 'tag', 'p')">What kind of margins does Upstox provide?</h2>
						<p>Varying levels of margins are available on intraday orders for equities, F&O, commodities and currency futures—you can check your <a href="https://www.dropbox.com/sh/ajvp6wh8w3v57w6/AAAbsqUpoAY8kIUzJ98MwrJEa?dl=0" target="_blank" rel="nofollow">daily margin limits</a>. If you would like to trade with better leverage, sign up for our Priority Packs after you open a demat account. </p>
					</div>

					<div class="faq">
						<h2 class="show-on-desktop">Why does Upstox require a Power of Attorney (POA) to open a demat account?</h2>
						<h2 class="show-on-mobile" onclick="showFAQ(event, 'tag', 'p')">Why does Upstox require a Power of Attorney (POA) to open a demat account?</h2>
						<p>You have to submit a physical POA to be able to sell and transfer shares from your demat account, as per the rules set by the stock exchanges you wish to trade on.</p>
					</div>
				</div>
			</section>
			<section class="demat-feedback">
				<p class="testimonial-title">Big community of people like you</p>
				<h6>We’re proud of our products, and we’re really excited when we get feedback from our users.</h6>
				<div class="carousal-container">
					<div class="carousal-left" onclick="carousalSlide(event, 'left')">&lsaquo;</div>
					<div class="carousal-window" data-carousal-width=300 data-carousal-count=3>
						<div class="feedback-container carousal" style="left: 0px;">
							<div class="feedback">
								<div class="user-img one">B</div>
								<p class="user-feedback">My experience with Upstox has been great! Before using Upstox, I used three other major stock trading services but Upstox stands far from those.</p>
								<p class="user-name">Bala Swarna Rao</p>
							</div>

							<div class="feedback">
								<div class="user-img two">Y</div>
								<p class="user-feedback">Support at Upstox is fabulous! It’s quick, effortless and efficient.</p>
								<p class="user-name">Yusuf Mansawala</p>
							</div>

							<div class="feedback">
								<div class="user-img three">N</div>
								<p class="user-feedback">It’s better to trade through Upstox to earn more money without paying huge amounts in brokerage.</p>
								<p class="user-name">Nilu Rout</p>
							</div>
						</div>
					</div>
					<div class="carousal-right" onclick="carousalSlide(event, 'right')">&rsaquo;</div>
				</div>
			</section>
			<section class="demat-account-link">
				<div class="c2a-title">Join the Upstox family</div>
				<h6>Just place trades. Let our tech do the rest.<br>Trade Faster. Trade Smarter.</h6>
				<a href="index.html#"><button>Open Demat Account</button></a>
			</section>
		</main>
		
		<footer>
			<div class="custom-container">
				<p>
					RKSV Securities: NSE CM: INB231394231 | NSE F&O: INF231394231 | NSE CDS: INE231394231 | CDSL: IN-DP-CDSL- 00282534 | NSDL: IN-DP-NSDL-11496819 | BSE CM: INB011394237 | BSE F&O: INF 011394237 | CDSL: IN-DP-CDSL- 00283831 | NSDL: IN-DP-NSDL-11497282 | RKSV Commodities MCX Member Code: 46510 | FMC Regn. No. MCX: MCX/TM/CORP/2034 | Registered Address: RKSV/Upstox, 30th Floor, Sunshine Tower, Senapati Bapat Marg, Dadar (W), Mumbai, Maharashtra 400013. For any complaints email at complaints@rksv.in | Please ensure you carefully read the <a href="../index.html?p=27" target="_blank">Risk Disclosure Document as prescribed by SEBI</a>.
				</p>
				<div id="hidden_footer" style="display:none">
					<p>
						The cost-effective brokerage plans make Upstox a trustworthy and reliable <a href="../kb-tags/futures/index.html">online discount stockbroker</a>. Available on both the web and mobile, it offers unmatched convenience to traders. If you are considering opening a demat account online, then Upstox is just the right place for you.
					</p>
					<h2>What is a Demat Account?</h2>
					<p>
						A <a href="../index.html?p=21">demat account</a> is where all your securities, purchased in dematerialized or electronic form are saved. In simpler words, it is analogous to a bank account. Just like a bank account holds your money, a demat account holds your shares.
					</p>

					<h2>Features of Online Demat Account.</h2>
					<h3>Pocket-friendly</h3>
					<p>
						The equity deliveries are free and everything you earn remains all yours. You do not end up spending your hard-earned profit on brokerage.
					</p>

					<h3>Paperless account opening</h3>
					<p>
						All you need is an Aadhar Card to open demat account. There are no physical documents or forms required! Just fill the form online and you can easily submit the documents on the website.
					</p>

					<h3>Trustworthy Tech</h3>
					<p>
						Share markets are full of surprises as a result of which there are a lot of ups and downs. But don’t worry, with our trustworthy technology, you’ll always remain online and on top of market fluctuations.
					</p>

					<h3>Cross-platform trading</h3>
					<p>
						Opening a demat account online with Upstox provides you with an option to trade on any other platform. Available on web, desktop and mobile, it proves to be the best choice for trading online!
					</p>

					<h2>Documents required to open a demat account</h2>
					<p>
						You only need a small number of documents for opening the online demat account.

					-    Aadhar Card

					-    PAN Card

					-    Latest bank statement with IFSC code

					-    Cancelled Cheque
					</p>

					<h2>Opening demat account</h2>
					<p>
						The demat account opening process is fairly easy and hassle-free. To open a demat account online, follow the simple steps given below.
					</p>

					<h3>Step 1: Fill the form</h3>
					<p>
						Fill the form on the website that is required for opening an online demat account. Once  the form is submitted, it will be sent for verification. It takes about 12-24 hours to open a demat account.
					</p>

					<h3>Step 2: Verification</h3>
					<p>
						After you submit the required documents along with the account opening form, they will be verified. In case of any problems, you will be contacted using the contact information that you provide.
					</p>

					<h3>Step 3: Post-verification</h3>
					<p>
						Once the verification is successful, you will get an unique ID  After this you will be all set to enter the world of trading with your free demat account!
					</p>
				</div>
				<div id="footer_readmore" onclick="readmoreFooter();">
					Read More
				</div>
				<p class="made-in-india">
					Made with love in India | Copyrights © 2017, <a href="../kb-tags/futures/index.html" target="_blank">Upstox</a>
				</p>
			</div>
		</footer>
	<script data-pagespeed-no-defer>(function(){function f(b){var a=window;if(a.addEventListener)a.addEventListener("load",b,!1);else if(a.attachEvent)a.attachEvent("onload",b);else{var c=a.onload;a.onload=function(){b.call(this);c&&c.call(this)}}};window.pagespeed=window.pagespeed||{};var k=window.pagespeed;function l(b,a,c,g,h){this.h=b;this.i=a;this.l=c;this.j=g;this.b=h;this.c=[];this.a=0}l.prototype.f=function(b){for(var a=0;250>a&&this.a<this.b.length;++a,++this.a)try{document.querySelector(this.b[this.a])&&this.c.push(this.b[this.a])}catch(c){}this.a<this.b.length?window.setTimeout(this.f.bind(this),0,b):b()};
k.g=function(b,a,c,g,h){if(document.querySelector&&Function.prototype.bind){var d=new l(b,a,c,g,h);f(function(){window.setTimeout(function(){d.f(function(){for(var a="oh="+d.l+"&n="+d.j,a=a+"&cs=",b=0;b<d.c.length;++b){var c=0<b?",":"",c=c+encodeURIComponent(d.c[b]);if(131072<a.length+c.length)break;a+=c}k.criticalCssBeaconData=a;var b=d.h,c=d.i,e;if(window.XMLHttpRequest)e=new XMLHttpRequest;else if(window.ActiveXObject)try{e=new ActiveXObject("Msxml2.XMLHTTP")}catch(m){try{e=new ActiveXObject("Microsoft.XMLHTTP")}catch(n){}}e&&
(e.open("POST",b+(-1==b.indexOf("?")?"?":"&")+"url="+encodeURIComponent(c)),e.setRequestHeader("Content-Type","application/x-www-form-urlencoded"),e.send(a))})},0)})}};k.criticalCssBeaconInit=k.g;})();
pagespeed.selectors=["#iccodeGroup .validation-text","*",".account-container .demat-details",".account-container .demat-details div",".account-container .demat-details h1",".account-container .demat-details p",".account-container .demat-details strong",".account-container .demat-details ul",".account-container .demat-form",".account-container .demat-form .error-msg input",".account-container .demat-form .input-group-addon",".account-container .demat-form input",".account-container .demat-form p",".account-container .form-container",".account-container .signinbox",".account-container .signinbox a",".account-container .signinbox p",".account-container section.demat-details",".benefit-demat-ac",".benefits .benefits-img",".benefits-container .benefits",".benefits-container .benefits h2",".benefits-container .benefits img",".benefits-container .benefits p",".benefits-img.cost-image",".benefits-img.cross-image",".benefits-img.paper-image",".benefits-img.tech-image",".c2a-title",".carousal-container",".carousal-container .carousal",".carousal-container .carousal-left",".carousal-container .carousal-right",".carousal-container .carousal-window",".custom-container",".demat-benefits .carousal-container .carousal-window",".demat-feedback .carousal-container",".demat-feedback .carousal-container .carousal-window",".demat-form #referral_section",".demat-form #referral_section .ref-wrapper",".demat-form #referral_section .ref-wrapper button",".demat-form #referral_section .ref-wrapper button.selected",".demat-form #referral_section label",".demat-form #submitsignup",".demat-form #txtcontact",".demat-form .form-divider",".demat-help .help-overlay",".demat-help .help-sidebar",".error-msg",".error-msg .validation-text",".error-msg .validation-text.empty_password",".error-msg .validation-text.short_password",".error-msg.duplicate-email .validation-text.duplicate_email",".error-msg.duplicate-mobile .validation-text.duplicate_mobile",".error-msg.invalid-email .validation-text.invalid_email",".error-msg.invalid-mobile .validation-text.invalid_mobile",".error-msg.zero-start .validation-text.zero_start",".faq p",".faq-title",".feedback .user-feedback",".feedback .user-img",".feedback .user-img.one",".feedback .user-name",".feedback-container .feedback",".first-fold .account-container",".help-sidebar",".help-sidebar .help-topic",".help-sidebar .help-topics-container",".help-sidebar h2",".help-sidebar h6",".help-sidebar h6 span",".help-topic .topic-dropdown",".help-topic .topic-dropdown.show-faq",".help-topic .topic-name",".how-to-open-demat-ac",".howto .howto-img",".howto .howto-num",".howto-container .howto",".howto-container .howto p",".howto-img.form-image",".howto-img.scan-image",".howto-img.welcome-image",".howto-num.one-image",".howto-num.three-image",".howto-num.two-image",".no-select",".ref-yes",".second-fold section.demat-account-link button",".second-fold section.demat-feedback h2",".second-fold section.demat-feedback h6",".subheading",".testimonial-title",".topic-dropdown div",".topic-dropdown li",".topic-dropdown ul",".validation-text",".validation-text.duplicate_email",".validation-text.duplicate_mobile",".validation-text.empty_password",".validation-text.invalid_email",".validation-text.invalid_mobile",".validation-text.short_password",".validation-text.zero_start","body","body footer","button","footer","footer #footer_readmore","footer .hide","footer .show","footer a","footer h1","footer h2","footer h3","footer p","header.demat-header","header.demat-header .demat-help-button","header.demat-header .demat-help-button span","header.demat-header .demat-help-button span.demat-help-bubble","header.demat-header .upstox-logo","header.demat-header .upstox-logo img","main.first-fold","main.second-fold","section.demat-account-link","section.demat-account-link button","section.demat-account-link h1","section.demat-account-link h6","section.demat-benefits","section.demat-benefits .benefits-container","section.demat-benefits h1","section.demat-faq","section.demat-faq .faq","section.demat-faq .faq a","section.demat-faq .faq h2","section.demat-faq .faq h2.show-on-desktop","section.demat-faq .faq h2.show-on-mobile","section.demat-faq .faq p","section.demat-faq .faq p.show-faq","section.demat-faq .faq-container","section.demat-faq h1","section.demat-faq h2","section.demat-feedback","section.demat-feedback .carousal-container .feedback-container","section.demat-feedback .feedback-container","section.demat-feedback .feedback-container .feedback","section.demat-feedback .feedback-container .user-feedback","section.demat-feedback h1","section.demat-feedback h6","section.demat-help","section.demat-howto .howto-container","section.demat-howto h1","small"];pagespeed.criticalCssBeaconInit('/ngx_pagespeed_beacon','https://upstox.com/open-demat-account/','M-RtU4slx-','kq_uw1AUTtg',pagespeed.selectors);</script><noscript class="psa_add_styles"><link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet"><style type="text/css">*{box-sizing:border-box}body{font-family:'Lato',sans-serif;margin:0}header.demat-header{width:100%}header.demat-header .upstox-logo{margin:50px;display:inline-block}header.demat-header .demat-help-button{display:inline-block;position:absolute;right:0;margin:50px;height:50px;width:80px;cursor:pointer;color:#0046bb}header.demat-header .demat-help-button span{}header.demat-header .demat-help-button span.demat-help-bubble{background:url(https://cf.upstox.com/open-demat-account/images/xhelp_bubble.png.pagespeed.ic.vY5wtg7fNg.png) no-repeat;background-size:contain;margin-right:10px;display:inline-block;width:20px;height:20px;position:relative;top:7px}main.first-fold{background:url(https://cf.upstox.com/open-demat-account/images/xoda_bg.png.pagespeed.ic.aZvTPJSpH4.png) no-repeat;background-size:85% 100%;width:100%;padding-bottom:60px}@media screen and (max-width:1370px){main.first-fold{background-size:1000px}.account-container section.demat-details{margin-top:70px}}.first-fold .account-container{display:flex;flex-direction:row;justify-content:space-evenly}.account-container .demat-details{color:#fffefe;flex:0 0 auto;margin-top:100px}.account-container .demat-details h1{font-size:50px;font-weight:300}.account-container .demat-details p{font-size:22px;font-weight:300}.account-container .demat-details ul{-webkit-padding-start:18px}.account-container .demat-details strong{font-size:23px;font-weight:700}.account-container .demat-form,.account-container .signinbox{width:400px;background-color:#fff;box-shadow:0 0 19.3px 15.8px rgba(0,0,0,.05);margin-bottom:20px;margin-top:20px;padding:25px}.account-container .demat-form p{font-size:18px;letter-spacing:.8px;text-align:left;font-weight:bold;color:#aaa;margin-top:0;margin-bottom:20px;text-transform:uppercase;letter-spacing:1.5px}.account-container .demat-form input{opacity:.75;height:50px;width:100%;background-color:#fff;border:solid 1px #ccc;border-radius:0;padding:10px 20px;font-size:16px;margin-bottom:20px;font-family:Lato;font-size:16px;font-weight:400;letter-spacing:.8px;text-align:left}.demat-form #txtcontact{width:calc(100% - 49px)}.account-container .demat-form .input-group-addon{display:inline-block;line-height:50px;height:50px;position:relative;top:1px;color:#000;text-align:center;border:1px solid #ced4da;border-right:none;padding:0 10px}.demat-form #submitsignup{width:100%;height:50px;border:none;background:#0046bb;color:#fff;font-size:16px;font-weight:700;letter-spacing:1px;text-transform:uppercase;cursor:pointer}.demat-form .form-divider{position:relative;margin-top:30px;left:-25px;width: calc(100% + 50px);border-bottom:1px solid rgba(204,204,204,.75)}.demat-form #referral_section{margin-top:20px}.demat-form #referral_section label{font-size:14px;display:block;color:#666}.demat-form #referral_section .ref-wrapper{display:flex;flex-direction:row;margin-top:20px;margin-bottom:20px}button:focus{outline:0}.demat-form #referral_section .ref-wrapper button{border-radius:4px;border:solid 1px #ccc;padding:10px!important;background:transparent;font-size:16px;letter-spacing:.6px;text-align:center;color:#ccc;height:40px;line-height:14px;width:48%}.ref-yes{margin-right:20px}.demat-form #referral_section .ref-wrapper button.selected{border:solid 1px #0046bb;background:#e8f1ff;color:#0046bb}.account-container .signinbox p{margin-top:0;font-size:14px;text-transform:uppercase;color:#666;letter-spacing:1.5px}.account-container .signinbox a{font-size:18px;font-weight:700;color:#0046bb;letter-spacing:1.5px;border-bottom:2px solid #0046bb;padding-bottom:3px;text-decoration:none!important}small{display:none}@media screen and (max-width:1150px){header.demat-header .demat-help-button{color:#fff}.first-fold .account-container{flex-direction:column}.account-container section.demat-details{margin-top:0}.account-container .demat-details h1{text-align:center;font-size:36px;margin-top:0}.account-container .demat-details div{display:none}.account-container .form-container{display:flex;align-items:center;flex-direction:column}.account-container .demat-form .input-group-addon{top:0}}@media screen and (max-width:450px){.account-container .demat-details h1{font-size:22px}.account-container .demat-form p{font-size:14px}header.demat-header .demat-help-button{margin:15px 20px;font-size:12px;width:62px}header.demat-header .upstox-logo{margin:20px}header.demat-header .upstox-logo img{height:20px}.account-container .demat-form,.account-container .signinbox{width:calc(100% - 30px);min-width:200px;margin-bottom:20px;margin-top:20px;padding:15px}.account-container .demat-form input{height:40px;font-size:14px;padding:15px}.account-container .demat-form .input-group-addon{line-height:40px;height:40px;top:1px;font-size:14px}.account-container .signinbox{padding-bottom:20px;margin-top:5px}.account-container .signinbox p{font-size:12px;margin-bottom:5px}.account-container .signinbox a{font-size:14px;margin-bottom:5px}main.first-fold{padding-bottom:20px;margin-bottom:20px}.demat-form .form-divider{width: calc(100% + 30px);left:-15px}}</style><link rel="stylesheet" type="text/css" href="https://cf.upstox.com/open-demat-account/A.styles.css.pagespeed.cf.FSXd7k2ET0.css"></noscript><script data-pagespeed-no-defer>(function(){function b(){var a=window,c=e;if(a.addEventListener)a.addEventListener("load",c,!1);else if(a.attachEvent)a.attachEvent("onload",c);else{var d=a.onload;a.onload=function(){c.call(this);d&&d.call(this)}}};var f=!1;function e(){if(!f){f=!0;for(var a=document.getElementsByClassName("psa_add_styles"),c=0,d;d=a[c];++c)if("NOSCRIPT"==d.nodeName){var k=document.createElement("div");k.innerHTML=d.textContent;document.body.appendChild(k)}}}function g(){var a=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequestAnimationFrame||window.msRequestAnimationFrame||null;a?a(function(){window.setTimeout(e,0)}):b()}
var h=["pagespeed","CriticalCssLoader","Run"],l=this;h[0]in l||!l.execScript||l.execScript("var "+h[0]);for(var m;h.length&&(m=h.shift());)h.length||void 0===g?l[m]?l=l[m]:l=l[m]={}:l[m]=g;})();
pagespeed.CriticalCssLoader.Run();</script><script data-pagespeed-no-defer>(function(){window.pagespeed=window.pagespeed||{};var f=window.pagespeed;function h(c,a,e,b){this.f=c;this.a=a;this.b=e;this.g=b}f.beaconUrl="";
function k(c){var a=c.f,e=window.mod_pagespeed_start,b=Number(new Date)-e,a=a+(-1==a.indexOf("?")?"?":"&"),a=a+"ets="+("load"==c.a?"load:":"unload:");if("beforeunload"!=c.a||!window.mod_pagespeed_loaded){a=a+b+("&r"+c.a+"=");if(window.performance){var b=window.performance.timing,d=b.navigationStart,g=b.requestStart,a=a+(b[c.a+"EventStart"]-d),a=a+("&nav="+(b.fetchStart-d)),a=a+("&dns="+(b.domainLookupEnd-b.domainLookupStart)),a=a+("&connect="+(b.connectEnd-b.connectStart)),a=a+("&req_start="+(g-d))+
("&ttfb="+(b.responseStart-g)),a=a+("&dwld="+(b.responseEnd-b.responseStart)),a=a+("&dom_c="+(b.domContentLoadedEventStart-d));window.performance.navigation&&(a+="&nt="+window.performance.navigation.type);d=-1;b.msFirstPaint?d=b.msFirstPaint:window.chrome&&window.chrome.loadTimes&&(d=Math.floor(1E3*window.chrome.loadTimes().firstPaintTime));d-=g;0<=d&&(a+="&fp="+d)}else a+=b;f.getResourceTimingData&&window.parent==window&&(a+=f.getResourceTimingData());a+=window.parent!=window?"&ifr=1":"&ifr=0";"load"==
c.a&&(window.mod_pagespeed_loaded=!0,(b=window.mod_pagespeed_num_resources_prefetched)&&(a+="&nrp="+b),(b=window.mod_pagespeed_prefetch_start)&&(a+="&htmlAt="+(e-b)));f.criticalCss&&(e=f.criticalCss,a+="&ccis="+e.total_critical_inlined_size+"&cces="+e.total_original_external_size+"&ccos="+e.total_overhead_size+"&ccrl="+e.num_replaced_links+"&ccul="+e.num_unreplaced_links);a+="&dpr="+window.devicePixelRatio;""!=c.b&&(a+=c.b);document.referrer&&(a+="&ref="+encodeURIComponent(document.referrer));a+=
"&url="+encodeURIComponent(c.g);f.beaconUrl=a;(new Image).src=a}}f.c=function(c,a,e,b){var d=new h(c,a,e,b);window.addEventListener?window.addEventListener(a,function(){k(d)},!1):window.attachEvent("on"+a,function(){k(d)})};f.addInstrumentationInit=f.c;})();

pagespeed.addInstrumentationInit('/ngx_pagespeed_beacon', 'load', '', 'https://upstox.com/open-demat-account/');</script><script type="text/javascript" src="https://cf.upstox.com/pagespeed_static/js_defer.I4cHjq6EEP.js"></script></body>
</html>