<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trade any Coin - Exchange Portal</title>
<link rel="icon" type="image/png" href="https://tradeanycoin.com/assets/images/logo.png">
<!-- Bootstrap CSS CDN -->
<link rel="stylesheet" href="https://tradeanycoin.com/assets/bootstrap/css/bootstrap.min.css" >
<!-- Our Custom CSS -->
<link rel="stylesheet" href="https://tradeanycoin.com/assets/css/style.css">
<!-- <link rel="stylesheet" href="https://tradeanycoin.com/assets/style2.css"> -->
<!-- Scrollbar Custom CSS -->
<link rel="stylesheet" href="https://tradeanycoin.com/assets/css/jquery.mCustomScrollbar.min.css">
<link rel="stylesheet" href="{{ asset('assets/themes/limitless/css/style.css') }}">
<!-- Font Awesome JS -->
<script defer src="https://tradeanycoin.com/assets/js/solid.js"></script>
<script defer src="https://tradeanycoin.com/assets/js/fontawesome.js"></script>
</head>

<body">
    <div class="wrapper"> 
        @php
            $usd = \App\Models\TradeCurrency::where('default_currency', 1)->first();
            $btc = \App\Models\TradeCurrency::where('network', "bitcoin")->first();
            $bch = \App\Models\TradeCurrency::where('network', "bitcash")->first();
            $jaagcoin = \App\Models\TradeCurrency::where('network', "jaagcoin")->first();
            $ltc = \App\Models\TradeCurrency::where('network', "litecoin")->first();
            $neo = \App\Models\TradeCurrency::where('network', "neo")->first();
            $eth = \App\Models\TradeCurrency::where('network', "ethereum")->first();
            //BTC
            $full_btc_usd  = json_decode($full_btc_usd = App\Models\Liveprice::where('key','full_btc_usd')->first()->value,true);
            $full_bch_usd  = json_decode($full_bch_usd = App\Models\Liveprice::where('key','full_bch_usd')->first()->value,true);
            $full_eth_usd  = json_decode($full_eth_usd = App\Models\Liveprice::where('key','full_eth_usd')->first()->value,true);
            $full_ltc_usd  = json_decode($full_ltc_usd = App\Models\Liveprice::where('key','full_ltc_usd')->first()->value,true);
            $full_neo_usd  = json_decode($full_neo_usd = App\Models\Liveprice::where('key','full_neo_usd')->first()->value,true);

            $eth_balance = Sentinel::getuser()->ethereum_balance;
            $btc_balance = Sentinel::getuser()->bitcoin_balance;
            $bch_balance = Sentinel::getuser()->bitcash_balance;
            $aed_balance = Sentinel::getuser()->default_balance;
            $neo_balance = Sentinel::getuser()->neo_balance;
            $ltc_balance = Sentinel::getuser()->litecoin_balance;
            $jaagcoin_balance = Sentinel::getuser()->jaagcoin_balance;
        @endphp
        <!-- Sidebar  -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>
                    <img src="https://tradeanycoin.com/assets/images/logo.jpg" alt="logo" />
                </h3>
            </div>
            <ul class="list-unstyled components">
                <li class="dashboard active">
                    <a href="{{url('dashboard')}}">Dashboard</a>
                </li>
                <li class="wallet"> 
                    <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Wallet</a>
                    <ul class="collapse list-unstyled" id="homeSubmenu">
                        <li>
                            <a href="{{ url('wallets/data' )}}">Deposit & Withdraws</a>
                        </li>
                        <li>
                            <a href="{{url('wallets/history')}}">History</a>
                        </li>
                    </ul>
                </li>
                <li class="exchange"> 
                    <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Exchange</a>
                    <ul class="collapse list-unstyled" id="pageSubmenu">
                      <li> <a href="{{url('exchange/data')}}">Exchange</a> </li>
                      <li> <a href="{{url('exchange/orders')}}">My Open Orders</a> </li>
                      <li> <a href="{{url('exchange/trade_history')}}">My Trade History</a> </li>
                    </ul>
                </li>
                <li class="transaction">
                    <a href="{{url('transaction')}}"l>Transaction</a>
                </li>
                <li class="settings"> 
                    <a href="#settingsSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Settings</a>
                    <ul class="collapse list-unstyled" id="settingsSubmenu">
                        <li> 
                            <a href="{{url('setting/profile')}}">My Profile</a> 
                        </li>
                        <li> 
                            <a href="{{url('setting/security')}}">Security</a> 
                        </li>
                    </ul>
                </li>
                <li class="affiliate"> 
                    <a href="#affiliateSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Affiliate</a>
                    <ul class="collapse list-unstyled" id="affiliateSubmenu">
                        <li>
                            <a href="{{url('affiliate')}}">Members</a> 
                        </li>
                    </ul>
                </li>
                <li class="support">
                    <a href="{{url('support')}}">Support and Ticket</a> 
                </li>
            </ul>
        </nav>
        <!-- Page Content  -->
        <div id="content">
            <nav id="headnev" class="navbar navbar-expand-lg navbar-light">
                <div class="container-fluid">
                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fas fa-align-left"></i> 
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> 
                        <i class="fas fa-align-justify"></i> 
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item active"> 1 BTC = <?php echo number_format($full_btc_usd['price'], 2)?> </li>
                            <li class="nav-item"> 1 ETH = <?php echo number_format($full_eth_usd['price'], 2) ?> </li>
                            <li class="nav-item"> NAPIERIRF </li>
                            <li class="nav-item"> 
                                <a class="nav-link" href="{{ url('logout') }}">Sign out</a> 
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="content" style="padding-left: 20px; padding-right: 20px;">
                <section class="">
                    <section>
                        <div class="pageContent">
                            <div class="container" >
                                <div class="topInfoCards">
                                    @php
                                        $usd = \App\Models\TradeCurrency::where('default_currency', 1)->first();
                                        $btc = \App\Models\TradeCurrency::where('network', "bitcoin")->first();
                                        $bch = \App\Models\TradeCurrency::where('network', "bitcash")->first();
                                        $jaagcoin = \App\Models\TradeCurrency::where('network', "jaagcoin")->first();
                                        $ltc = \App\Models\TradeCurrency::where('network', "litecoin")->first();
                                        $neo = \App\Models\TradeCurrency::where('network', "neo")->first();
                                        $eth = \App\Models\TradeCurrency::where('network', "ethereum")->first();

                                        $btc_volume = (float)$full_btc_usd['volume'];
                                        $btc_volume = floatval($btc_volume);
                                        $btc_volume_str = $btc_volume;

                                        $bch_volume = (float)$full_bch_usd['volume'];
                                        $bch_volume = floatval($bch_volume);
                                        $bch_volume_str = number_format($bch_volume, 2);

                                        $ltc_volume = (float)$full_ltc_usd['volume'];
                                        $ltc_volume = floatval($ltc_volume);
                                        $ltc_volume_str = number_format($ltc_volume, 2);

                                        $eth_volume = (float)$full_eth_usd['volume'];
                                        $eth_volume = floatval($eth_volume);
                                        $eth_volume_str = number_format($eth_volume, 2);

                                        $neo_volume = (float)$full_eth_usd['volume'];
                                        $neo_volume = floatval($neo_volume);
                                        $neo_volume_str = number_format($neo_volume, 2);
                                    @endphp
                                    <div id="main-content-container" class="bars-above">
                                        <div id="dashboard-page">
                                            <!-- Waluty -->
                                            <!--<div id="currencies"></div>-->
                                            <div class="row page-margin">
                                                <div class="row ">
                                                    <div class="row ">
                                                        <!-- Portfele -->
                                                        <div class="col-xs-12 col-md-4 col-lg-4">
                                                            <div id="wallets" class="card">
                                                                <div class="wallets-container">
                                                                    <div class="small-header horizontal layout center">
                                                                        <div class="left">
                                                                            <h4>Wallets</h4>
                                                                        </div>
                                                                        <div class="right flex">
                                                                            <value>
                                                                                <number id="wallets-value">≈ {{number_format(\App\Helpers\GeneralHelper::user_aed_available(Sentinel::getUser()->id),4)}}</number>
                                                                                    <currency id="wallets-currency">
                                                                                        <span class="currency-name-holder">USD</span>
                                                                                    </currency>
                                                                            </value>
                                                                        </div>
                                                                    </div>
                                                                    <div class="items layout vertical">
                                                                        <div class="item layout horizontal center waves-effect" data-currency="btc">
                                                                            <div class="left layout horizontal center">
                                                                                <i class="currency-icon big mdi-currency-bitcoin" style="background: #6d5170;"></i>
                                                                                <span class="currency-name" style="color:#6d5170;">ac</span>
                                                                            </div>
                                                                            <div class="right flex">
                                                                                <div class="row">
                                                                                    <div class="col xs12 s12 m6 l6 xl6 offset-xs0 offset-s0 offset-m0 offset-l0 offset-xl0 wallet-value">
                                                                                        <value>
                                                                                            <number style="color: #6d5170;"><?php echo number_format($jaagcoin_balance, 4) ?></number>
                                                                                        </value>
                                                                                        <div class="blocked layout horizontal center hidden">
                                                                                            <i class="mdi-lock"></i>
                                                                                            <number>0.00000000</number>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col xs12 s12 m6 l6 xl6 about-value layout horizontal center hidden">-</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item layout horizontal center waves-effect" data-currency="btc">
                                                                            <div class="left layout horizontal center">
                                                                                <i class="currency-icon big mdi-currency-bitcoin" style="background: #ffb400;"></i>
                                                                                <span class="currency-name" style="color:#ffb400;">btc</span>
                                                                            </div>
                                                                            <div class="right flex">
                                                                                <div class="row">
                                                                                    <div class="col xs12 s12 m6 l6 xl6 offset-xs0 offset-s0 offset-m0 offset-l0 offset-xl0 wallet-value">
                                                                                        <value>
                                                                                            <number style="color: #ffb400;"><?php echo number_format(Sentinel::getuser()->bitcoin_balance,4); ?></number>
                                                                                        </value>
                                                                                        <div class="blocked layout horizontal center hidden">
                                                                                            <i class="mdi-lock"></i>
                                                                                            <number>0.00000000</number>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col xs12 s12 m6 l6 xl6 about-value layout horizontal center hidden">-</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item layout horizontal center waves-effect" data-currency="btc">
                                                                            <div class="left layout horizontal center">
                                                                                <i class="currency-icon big mdi-currency-bitcoin" style="background: #ffb400;"></i>
                                                                                <span class="currency-name" style="color:#ffb400;">bch</span>
                                                                            </div>
                                                                            <div class="right flex">
                                                                                <div class="row">
                                                                                    <div class="col xs12 s12 m6 l6 xl6 offset-xs0 offset-s0 offset-m0 offset-l0 offset-xl0 wallet-value">
                                                                                        <value>
                                                                                            <number style="color: #37c866;"><?php echo number_format(Sentinel::getuser()->bitcash_balance,4); ?></number>
                                                                                        </value>
                                                                                        <div class="blocked layout horizontal center hidden">
                                                                                            <i class="mdi-lock"></i>
                                                                                            <number>0.00000000</number>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col xs12 s12 m6 l6 xl6 about-value layout horizontal center hidden">-</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item layout horizontal center waves-effect" data-currency="game">
                                                                            <div class="left layout horizontal center">
                                                                                <i class="currency-icon big mdi-currency-gamecredit" style="background: #98c01f;"></i>
                                                                                <span class="currency-name" style="color:#98c01f;">ltc</span>
                                                                            </div>
                                                                            <div class="right flex">
                                                                                <div class="row">
                                                                                    <div class="col xs12 s12 m6 l6 xl6 offset-xs0 offset-s0 offset-m0 offset-l0 offset-xl0 wallet-value">
                                                                                        <value>
                                                                                            <number style="color: #98c01f;"><?php echo number_format(Sentinel::getuser()->litecoin_balance,4); ?></number>
                                                                                        </value>
                                                                                        <div class="blocked layout horizontal center hidden">
                                                                                            <i class="mdi-lock"></i>
                                                                                            <number>0.00000000</number>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col xs12 s12 m6 l6 xl6 about-value layout horizontal center hidden">-</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item layout horizontal center waves-effect" data-currency="eth">
                                                                            <div class="left layout horizontal center">
                                                                                <i class="currency-icon big mdi-currency-ethereum" style="background: #4a90e2;"></i>
                                                                                <span class="currency-name" style="color:#4a90e2;">eth</span>
                                                                            </div>
                                                                            <div class="right flex">
                                                                                <div class="row">
                                                                                    <div class="col xs12 s12 m6 l6 xl6 offset-xs0 offset-s0 offset-m0 offset-l0 offset-xl0 wallet-value">
                                                                                        <value>
                                                                                            <number style="color: #4a90e2;"><?php echo number_format(Sentinel::getuser()->ethereum_balance,4); ?></number>
                                                                                        </value>
                                                                                        <div class="blocked layout horizontal center hidden">
                                                                                            <i class="mdi-lock"></i>
                                                                                            <number>0.00000000</number>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col xs12 s12 m6 l6 xl6 about-value layout horizontal center hidden">-</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="item layout horizontal center waves-effect" data-currency="bcc">
                                                                            <div class="left layout horizontal center">
                                                                                <i class="currency-icon big mdi-currency-bitcoin-cash" style="background: #ff9639;"></i>
                                                                                <span class="currency-name" style="color:#ff9639;">neo</span>
                                                                            </div>
                                                                            <div class="right flex">
                                                                                <div class="row">
                                                                                    <div class="col xs12 s12 m6 l6 xl6 offset-xs0 offset-s0 offset-m0 offset-l0 offset-xl0 wallet-value">
                                                                                        <value>
                                                                                            <number style="color: #ff9639;"><?php echo number_format(Sentinel::getuser()->neo_balance,4); ?></number>
                                                                                        </value>
                                                                                        <div class="blocked layout horizontal center hidden">
                                                                                            <i class="mdi-lock"></i>
                                                                                            <number>0.00000000</number>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col xs12 s12 m6 l6 xl6 about-value layout horizontal center hidden">-</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!-- karta jako ostatni - 5ty item -->
                                                                        <!--<div class="item layout horizontal center" id="card-item"></div>-->
                                                                    </div>
                                                                    <div class="layout horizontal center-justified goto-link-container">
                                                                        <a href="{{url('wallet/btc')}}" title="" id="go-to-history" class="layout horizontal center">
                                                                            <h6 style="color: #6d5170">See Wallets</h6>
                                                                            <i class="mdi-chevron-right"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Rynki -->
                                                        <div class="col-xs-12 col-md-4 col-lg-5">
                                                            <div id="markets" class="card">
                                                                <div class="markets-container">
                                                                    <div class="small-header horizontal layout center">
                                                                        <div class="left"><br>
                                                                            <h4>USD Markets</h4><hr>
                                                                        </div>
                                                                    </div>
                                                                    <div id="markets-list">
                                                                        <div id="header" class="layout horizontal center">
                                                                            <div class="space"></div>
                                                                            <div class="header row no-padding-vertical">
                                                                                <div class="col s6 xxxl7 layout horizontal center">
                                                                                    <div>Rate</div>
                                                                                </div>
                                                                                <div class="col s6 xxxl5 layout horizontal center">
                                                                                    <div>Volume</div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="empty"></div>
                                                                        </div>
                                                                        <div class="items">
                                                                        <!-- tutaj pojawia sie rynki z danej waluty glownej -->
                                                                            <div class="item layout horizontal center active" data-market="BTC-USD" data-first-currency="BTC" data-second-currency="USD"><!-- klasa "active" -->
                                                                                <div class="layout horizontal center currency-pair">
                                                                                    <i class="currency-icon big mdi-currency-bitcoin" style="background-color: #ffb400;"></i>
                                                                                    <span>BTC</span>
                                                                                </div>
                                                                                <div class="overlay" style="background-color: #ffb400;"></div>
                                                                                <div class="horizontal layout center row no-margin">
                                                                                    <div class="col xxs12 s6 xxxl7 value-and-change">
                                                                                        <div class="layout horizontal center rate-value">
                                                                                            <value><?php  echo number_format($full_btc_usd['price'],2); ?></value>
                                                                                        </div>
                                                                                        <div class="layout horizontal center change-value">
                                                                                            <value>
                                                                                                @if((float)$full_btc_usd["exchange"] > 0)
                                                                                                <difference class="layout horizontal center up" style="display: flex;">
                                                                                                    <i class="mdi-chevron-up"></i>
                                                                                                    <i class="mdi-equal"></i>
                                                                                                    <span id="difference-value"><?php echo number_format($full_btc_usd['exchange'], 2); ?>%</span>
                                                                                                </difference>
                                                                                                @else
                                                                                                <difference class="layout horizontal center down" style="display: flex;">
                                                                                                    <i class="mdi-chevron-up"></i>
                                                                                                    <i class="mdi-equal"></i>
                                                                                                    <span id="difference-value"><?php echo number_format($full_btc_usd['exchange'], 2); ?>%</span>
                                                                                                </difference>
                                                                                                @endif
                                                                                            </value>
                                                                                            <!--<div class="transaction-text">Sell</div>-->
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col xxs12 s6 xxxl5 layout horizontal center">
                                                                                        <div class="layout horizontal center volume-value">
                                                                                            <value><?php echo $neo_volume_str ?></value>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="item layout horizontal center active" data-market="BCH-USD" data-first-currency="BCH" data-second-currency="USD"><!-- klasa "active" -->
                                                                                <div class="layout horizontal center currency-pair">
                                                                                    <i class="currency-icon big mdi-currency-bitcoin-cash" style="background-color: #ff9639;"></i>
                                                                                    <span>BCH</span>
                                                                                </div>
                                                                                <div class="overlay" style="background-color: #ff9639;"></div>
                                                                                <div class="horizontal layout center row no-margin">
                                                                                    <div class="col xxs12 s6 xxxl7 value-and-change">
                                                                                        <div class="layout horizontal center rate-value">
                                                                                            <value><?php  echo number_format($full_bch_usd['price'],2); ?></value>
                                                                                        </div>
                                                                                        <div class="layout horizontal center change-value">
                                                                                            <value>
                                                                                                @if((float)$full_bch_usd['exchange'] > 0)
                                                                                                <difference class="layout horizontal center up" style="display: flex;">
                                                                                                    <i class="mdi-chevron-up"></i>
                                                                                                    <i class="mdi-equal"></i>
                                                                                                    <span id="difference-value"><?php echo number_format($full_bch_usd['exchange'], 2)?>%</span>
                                                                                                </difference>
                                                                                                @else
                                                                                                <difference class="layout horizontal center down" style="display: flex;">
                                                                                                    <i class="mdi-chevron-up"></i>
                                                                                                    <i class="mdi-equal"></i>
                                                                                                    <span id="difference-value"><?php echo number_format($full_bch_usd['exchange'], 2);?>%</span>
                                                                                                </difference>
                                                                                                @endif
                                                                                            </value>
                                                                                            <!--<div class="transaction-text">Sell</div>-->
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col xxs12 s6 xxxl5 layout horizontal center">
                                                                                        <div class="layout horizontal center volume-value">
                                                                                            <value><?php echo $bch_volume_str; ?></value>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="item layout horizontal center active" data-market="LTC-USD" data-first-currency="LTC" data-second-currency="USD"><!-- klasa "active" -->
                                                                                <div class="layout horizontal center currency-pair">
                                                                                    <i class="currency-icon big mdi-currency-bitcoin-cash" style="background-color: #ff9639;"></i>
                                                                                    <span>LTC</span>
                                                                                </div>
                                                                                <div class="overlay" style="background-color: #ff9639;"></div>
                                                                                <div class="horizontal layout center row no-margin">
                                                                                    <div class="col xxs12 s6 xxxl7 value-and-change">
                                                                                        <div class="layout horizontal center rate-value">
                                                                                            <value><?php  echo number_format($full_ltc_usd['price'],2); ?></value>
                                                                                        </div>
                                                                                        <div class="layout horizontal center change-value">
                                                                                            <value>
                                                                                                @if((float)$full_ltc_usd['exchange'] > 0)
                                                                                                <difference class="layout horizontal center down" style="display: flex;">
                                                                                                    <i class="mdi-chevron-up"></i>
                                                                                                    <i class="mdi-equal"></i>
                                                                                                    <span id="difference-value"><?php echo number_format($full_ltc_usd['exchange'], 2);?>%</span>
                                                                                                </difference>
                                                                                                @else
                                                                                                <difference class="layout horizontal center down" style="display: flex;">
                                                                                                    <i class="mdi-chevron-up"></i>
                                                                                                    <i class="mdi-equal"></i>
                                                                                                    <span id="difference-value"><?php echo number_format($full_ltc_usd['exchange'], 2);?>%</span>
                                                                                                </difference>
                                                                                                @endif
                                                                                            </value>
                                                                                            <!--<div class="transaction-text">Sell</div>-->
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col xxs12 s6 xxxl5 layout horizontal center">
                                                                                        <div class="layout horizontal center volume-value">
                                                                                            <value><?php echo $ltc_volume_str; ?></value>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="item layout horizontal center active" data-market="ETH-USD" data-first-currency="ETH" data-second-currency="USD"><!-- klasa "active" -->
                                                                                <div class="layout horizontal center currency-pair">
                                                                                    <i class="currency-icon big mdi-currency-ethereum" style="background-color: #4a90e2;"></i>
                                                                                    <span>ETH</span>
                                                                                </div>
                                                                                <div class="overlay" style="background-color: #4a90e2;"></div>
                                                                                <div class="horizontal layout center row no-margin">
                                                                                    <div class="col xxs12 s6 xxxl7 value-and-change">
                                                                                        <div class="layout horizontal center rate-value">
                                                                                            <value><?php  echo number_format($full_eth_usd['price'],2); ?></value>
                                                                                        </div>
                                                                                        <div class="layout horizontal center change-value">
                                                                                            <value>
                                                                                                @if((float)$full_eth_usd['exchange'] > 0)
                                                                                                <difference class="layout horizontal center up" style="display: flex;">
                                                                                                    <i class="mdi-chevron-up"></i>
                                                                                                    <i class="mdi-equal"></i>
                                                                                                    <span id="difference-value"><?php echo number_format($full_eth_usd['exchange'], 2); ?>%</span>
                                                                                                </difference>
                                                                                                @else
                                                                                                <difference class="layout horizontal center up" style="display: flex;">
                                                                                                    <i class="mdi-chevron-down"></i>
                                                                                                    <i class="mdi-equal"></i>
                                                                                                    <span id="difference-value"><?php echo number_format($full_eth_usd['exchange'], 2); ?>%</span>
                                                                                                </difference>
                                                                                                @endif
                                                                                            </value>
                                                                                            <!--<div class="transaction-text">Sell</div>-->
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col xxs12 s6 xxxl5 layout horizontal center">
                                                                                        <div class="layout horizontal center volume-value">
                                                                                            <value><?php echo $eth_volume_str; ?></value>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="border-bottom: 0px;" class="item layout horizontal center active" data-market="LSK-USD" data-first-currency="LSK" data-second-currency="USD"><!-- klasa "active" -->
                                                                                <div class="layout horizontal center currency-pair">
                                                                                    <i class="currency-icon big mdi-currency-lisk" style="background-color: #1170a4;"></i>
                                                                                    <span>NEO</span>
                                                                                </div>
                                                                                <div class="overlay" style="background-color: #1170a4;"></div>
                                                                                <div class="horizontal layout center row no-margin">
                                                                                    <div class="col xxs12 s6 xxxl7 value-and-change">
                                                                                        <div class="layout horizontal center rate-value">  
                                                                                            <value><?php  echo number_format($full_neo_usd['price'],2); ?></value>  
                                                                                        </div>
                                                                                        <div class="layout horizontal center change-value">
                                                                                            <value>
                                                                                                @if((float)$full_neo_usd['exchange'] > 0)
                                                                                                <difference class="layout horizontal center up" style="display: flex;">
                                                                                                    <i class="mdi-chevron-up"></i>
                                                                                                    <i class="mdi-equal"></i>
                                                                                                    <span id="difference-value"><?php echo number_format($full_neo_usd['exchange'], 2) ?>%</span>
                                                                                                </difference>
                                                                                                @else
                                                                                                <difference class="layout horizontal center down" style="display: flex;">
                                                                                                    <i class="mdi-chevron-up"></i>
                                                                                                    <i class="mdi-equal"></i>
                                                                                                    <span id="difference-value"><?php echo number_format($full_neo_usd['exchange'], 2) ?>%</span>
                                                                                                </difference>
                                                                                                @endif
                                                                                            </value>
                                                                                            <!--<div class="transaction-text">Sell</div>-->
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col xxs12 s6 xxxl5 layout horizontal center">
                                                                                        <div class="layout horizontal center volume-value">
                                                                                            <value><?php echo $neo_volume_str; ?></value>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="layout horizontal center-justified goto-link-container">
                                                                            <a href="{{url('wallet/btc')}}" title="" id="go-to-history" class="layout horizontal center">
                                                                                <h6 style="color: #6d5170">See Market</h6>
                                                                                <i class="mdi-chevron-right"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="layout horizontal center-justified goto-link-container">
                                                                    </div>
                                                                    <!--div class="loader-container" style="background: #fff">
                                                                    <paper-spinner-lite active></paper-spinner-lite>
                                                                    </div-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-md-4 col-lg-3">
                                                            <div id="wallets" class="card">
                                                                <div class="wallets-container">
                                                                    <div class="small-header horizontal layout center">
                                                                        <div class="left"><br>
                                                                            <h4>Account Status</h4><hr>
                                                                        </div>
                                                                    </div>
                                                                    @if(Sentinel::getUser()->email_verified==1)
                                                                    <div class="item layout horizontal center waves-effect">
                                                                        <div class="left layout horizontal center">
                                                                            <h5 class="currency-name">1. Email Verified</h5>
                                                                        </div>
                                                                        <div class="right flex" style="font-size: 3rem; color: green; margin-left: 53%;">
                                                                            <i class="mdi-verified"></i>
                                                                        </div>
                                                                    </div>
                                                                    @else
                                                                    <div class="item layout horizontal center waves-effect">
                                                                        <div class="left layout horizontal center">
                                                                            <h5 class="currency-name">
                                                                                <a style="color:black;" href="{{url('setting/data')}}">1. Email Verified</a>
                                                                            </h5>
                                                                        </div>
                                                                        <div class="right flex" style="font-size: 3rem; color: red; margin-left: 53%">
                                                                            <i class="mdi-close-circle"></i>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                    @if(Sentinel::getUser()->documents_verified==1)
                                                                    <div class="item layout horizontal center waves-effect">
                                                                        <div class="left layout horizontal center">
                                                                            <h5 class="currency-name">2. Documents Verified</h5>
                                                                        </div>
                                                                        <div class=" flex" style="font-size: 3rem; color: green; margin-left: 40%">
                                                                            <i class="mdi-verified"></i>
                                                                        </div>
                                                                    </div>
                                                                    @else
                                                                    <div class="item layout horizontal center waves-effect">
                                                                        <div class="left layout horizontal center">
                                                                            <h5 class="currency-name">
                                                                                <a style="color:black;" href="{{url('setting/data')}}">3. Documents Verified</a>
                                                                            </h5>
                                                                        </div>
                                                                        <div class="right flex" style="font-size: 3rem; color: red; margin-left: 40%">
                                                                            <i class="mdi-close-circle"></i>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                </div>
                                                                <br>
                                                                <div class="layout horizontal center-justified goto-link-container">
                                                                    <a href="{{url('setting/data')}}" title="" id="go-to-history" class="layout horizontal center">
                                                                        <h6 style="color: #6d5170">Go to Settings</h6>
                                                                        <i class="mdi-chevron-right"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-7 col-md-6 col-sm-12 col-xs-12">
                                            <div class="panel panel-body panelStyle" style="height:400px; border: 2px solid #edeff1; background: #ffffff">
                                                <div class="small-header horizontal layout center ">
                                                    <div class="left ">
                                                        <h4 style="font-size: 24px; color: #284274">History</h4>
                                                        <hr>
                                                    </div>
                                                </div>
                                                <div class="table-responsive">
                                                    <table id="order_history" class="table tableStyle">
                                                        <thead>
                                                            <tr>
                                                                <th>{{trans_choice('general.type',1)}}</th>
                                                                <th>{{trans_choice('general.status',1)}}</th>
                                                                <th>{{trans_choice('general.market',1)}}</th>
                                                                <th>{{trans_choice('general.price',1)}}</th>
                                                                <th>{{trans_choice('general.volume',1)}}</th>
                                                                <th>{{trans_choice('general.time',1)}}</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach(\App\Models\OrderBook::where('user_id', Sentinel::getUser()->id)->orderBy('created_at','desc')->limit(5)->get() as $key)
                                                            <?php
                                                                $trade_currency = \App\Models\TradeCurrency::find($key->trade_currency_id);
                                                                $default = \App\Models\TradeCurrency::where('default_currency', 1)->first();
                                                            ?>
                                                            <tr>
                                                                <td class="typeTd">
                                                                    @if($key->order_type=="ask")
                                                                        {{trans_choice('general.ask',1)}}
                                                                    @endif
                                                                    @if($key->order_type=="bid")
                                                                        {{trans_choice('general.bid',1)}}
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if($key->status=="pending")
                                                                        {{trans_choice('general.pending',1)}}
                                                                    @endif
                                                                    @if($key->status=="processing")
                                                                        {{trans_choice('general.processing',1)}}
                                                                    @endif
                                                                    @if($key->status=="cancelled")
                                                                        {{trans_choice('general.cancelled',1)}}
                                                                    @endif
                                                                    @if($key->status=="done")
                                                                        {{trans_choice('general.done',1)}}
                                                                    @endif
                                                                    @if($key->status=="accepted")
                                                                        {{trans_choice('general.accepted',1)}}
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if(!empty($trade_currency))
                                                                        {{$trade_currency->xml_code}}
                                                                    @endif
                                                                    @if(!empty($default))
                                                                        {{$default->xml_code}}
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    {{ round($key->amount,6) }}
                                                                </td>
                                                                <td>
                                                                    {{round( $key->volume,6) }}
                                                                </td>
                                                                <td>
                                                                    {{ $key->created_at }}
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12" style="padding-bottom: 20px;">
                                            <div class="panel panel-body panelStyle" style="border: 2px solid #edeff1; background: #ffffff">
                                                <div class="small-header horizontal layout center">
                                                    <div class="left">
                                                        <h4 style="font-size: 24px; color: #284274">How does it work?</h4><hr>
                                                        <div class="left flex" style="font-size: 3rem; color: #1170a4;">
                                                            <i class="mdi-numeric-1-box"></i>   
                                                        </div>
                                                        <div class=" layout horizontal center">
                                                            <h5 class="currency-name">Deposit USD via <b>Wire Transfer</b>, <b>Credit/Debit card</b> or <b>Cash pick-up service</b>.</h5>
                                                        </div>
                                                        <br>
                                                        <div class="left flex" style="font-size: 3rem; color: #37c866;">
                                                            <i class="mdi-numeric-2-box"></i>   
                                                        </div>
                                                        <div class=" layout horizontal center">
                                                            <h5 class="currency-name">Buy digital currencies like <b>Bitcoin, Bitcash, Litecoin, Ethereum and Neo</b> using USD.</h5>
                                                        </div>
                                                        <br>
                                                        <div class="left flex" style="font-size: 3rem; color: #6d5170;">
                                                            <i class="mdi-numeric-3-box"></i>   
                                                        </div>
                                                        <div class=" layout horizontal center">
                                                            <h5 class="currency-name">Start trading cryptocurrencies on our Professional <b>Trading platform</b>.
                                                            </h5>
                                                        </div>
                                                        <br>
                                                        <div class="layout horizontal center-justified goto-link-container">
                                                            <a href="{{url('setting/data')}}" title="" id="go-to-history" class="layout horizontal center">
                                                                <h6 style="color: #6d5170">Go to FAQs</h6>
                                                                <i class="mdi-chevron-right"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </section>
                </section>
            </div>
        </div>
    </div>        <!-- jQuery CDN - Slim version (=without AJAX) --> 
    <script src="{{ asset('assets/plugins/amcharts/amcharts.js') }}"
                type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/amcharts/serial.js') }}"
        type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/amcharts/pie.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/amcharts/themes/light.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/amcharts/plugins/export/export.min.js') }}"
            type="text/javascript"></script>
    <script>
            $('#order_history').DataTable({
                
                "language": {
                    "emptyTable":     "No deposts yet"
                }
            });
    </script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> 
    <!-- Bootstrap JS --> 
    <script src="https://tradeanycoin.com/assets/bootstrap/js/bootstrap.min.js"></script> 
    <!-- jQuery Custom Scroller CDN --> 
    <script src="https://tradeanycoin.com/assets/js/jquery.mCustomScrollbar.concat.min.js"></script> 
    <script src="https://tradeanycoin.com/assets/js/custom.js"></script>
    </body>
</html>