<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trade any Coin - My Open Orders</title>
<link rel="icon" type="image/png" href="https://tradeanycoin.com/assets/images/logo.png">
<!-- Bootstrap CSS CDN -->
<link rel="stylesheet" href="https://tradeanycoin.com/assets/bootstrap/css/bootstrap.min.css" >
<!-- Our Custom CSS -->
<link rel="stylesheet" href="https://tradeanycoin.com/assets/css/style.css">
<!-- Scrollbar Custom CSS -->
<link rel="stylesheet" href="https://tradeanycoin.com/assets/css/jquery.mCustomScrollbar.min.css">

<!-- Font Awesome JS -->
<script defer src="https://tradeanycoin.com/assets/js/solid.js"></script>
<script defer src="https://tradeanycoin.com/assets/js/fontawesome.js"></script>
</head>

<body>
<div class="wrapper"> 
  <!-- Sidebar  -->
  <nav id="sidebar">
    <div class="sidebar-header">
      <h3><img src="https://tradeanycoin.com/assets/images/logo.jpg" alt="logo" /></h3>
    </div>
    <ul class="list-unstyled components">
      <li class="dashboard active"><a href="{{url('dashboard')}}">Dashboard</a></li>
      <li class="wallet"> <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Wallet</a>
        <ul class="collapse list-unstyled" id="homeSubmenu">
          <li><a href="{{ url('wallets/data' )}}">Deposit & Withdraws</a></li>
          <li><a href="{{url('wallets/history')}}">History</a></li>
        </ul>
      </li>
      <li class="exchange"> <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Exchange</a>
        <ul class="collapse list-unstyled" id="pageSubmenu">
          <li> <a href="{{url('exchange/data')}}">Exchange</a> </li>
          <li> <a href="{{url('exchange/orders')}}">My Open Orders</a> </li>
          <li> <a href="{{url('exchange/trade_history')}}">My Trade History</a> </li>
        </ul>
      </li>
      <li class="transaction"><a href="{{url('transaction')}}"l>Transaction</a></li>
      <li class="settings"> <a href="#settingsSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Settings</a>
        <ul class="collapse list-unstyled" id="settingsSubmenu">
          <li> <a href="{{url('setting/profile')}}">My Profile</a> </li>
          <li> <a href="{{url('setting/security')}}">Security</a> </li>
        </ul>
      </li>
      <li class="affiliate"> <a href="#affiliateSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Affiliate</a>
        <ul class="collapse list-unstyled" id="affiliateSubmenu">
          <li> <a href="{{url('affiliate')}}">Members</a> </li>
        </ul>
      </li>
      <li class="support"> <a href="{{url('support')}}">Support and Ticket</a> </li>
    </ul>
  </nav>
  
  <!-- Page Content  -->
  <div id="content">
    <nav id="headnev" class="navbar navbar-expand-lg navbar-light">
      <div class="container-fluid">
        <button type="button" id="sidebarCollapse" class="btn btn-info"> <i class="fas fa-align-left"></i> </button>
        <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <i class="fas fa-align-justify"></i> </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="nav navbar-nav ml-auto">
            <li class="nav-item active"> 1 BTC = 8180.28 </li>
            <li class="nav-item"> 1 ETC = 469.07 </li>
            <li class="nav-item"> NAPIERIRF </li>
            <li class="nav-item"> <a class="nav-link" href="#">Sign out</a> </li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="inner-content">
      <div class="col-md-12"> 
        
        <!--- Transactions Section Start --->
        <div class="transactions">
          <h2 class="title">Transactions</h2>
          
          <!--- Transactions Currencies Section Start --->
          <div class="transaction-currencies mb-4">
            <div class="row auto-cols">
              <div class="col-sm-12 col-md-12 col-lg-3">
                <div class="float-left"><img src="https://tradeanycoin.com/assets/images/transaction-AC.png" alt="transaction-AC" class="float-left mr-sm-3"></div>
                <div class="float-right">Autocoin <span>0.00000000</span></div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-3">
                <div class="float-left"><img src="https://tradeanycoin.com/assets/images/transaction-bitcoin.png" alt="transaction-bitcoin" class="float-left mr-sm-3"></div>
                <div class="float-right">Bitcoin <span>0.00000000</span></div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-3">
                <div class="float-left"><img src="https://tradeanycoin.com/assets/images/transaction-ETH.png" alt="transaction-ETH" class="float-left mr-sm-3"></div>
                <div class="float-right">ETH <span>0.00000000</span></div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-3">
                <div class="float-left"><img src="https://tradeanycoin.com/assets/images/transaction-USD.png" alt="transaction-USD" class="float-left mr-sm-3"></div>
                <div class="float-right">USD <span>0.00000000</span></div>
              </div>
            </div>
          </div>
          <!--- Transactions Currencies Section End ---> 
          
          <!--- Transactions Table Section Start --->
          <div class="transactions-table">
            <form class="pt-4 pb-4">
              <select name="lending-profit">
                <option value="Lending Profit">Lending Profit</option>
                <option value="Lending Profit">Lending Profit</option>
                <option value="Lending Profit">Lending Profit</option>
                <option value="Lending Profit">Lending Profit</option>
              </select>
            </form>
            <table class="table">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Received Dollar</th>
                  <th>Description</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td data-title="Date">24.06.2018</td>
                  <td data-title="Received Dollar">233,038 USD</td>
                  <td data-title="Description">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, 
                    sed diam nonumy eirmod tempor.</td>
                </tr>
              </tbody>
            </table>
          </div>
          <!--- Transactions Table Section End ---> 
          
        </div>
        <!--- Transactions Section End ---> 
      </div>
    </div>
  </div>
</div>
<!-- jQuery CDN - Slim version (=without AJAX) --> 
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> 
<!-- Bootstrap JS --> 
<script src="https://tradeanycoin.com/assets/bootstrap/js/bootstrap.min.js"></script> 
<!-- jQuery Custom Scroller CDN --> 
<script src="https://tradeanycoin.com/assets/js/jquery.mCustomScrollbar.concat.min.js"></script> 
<script src="https://tradeanycoin.com/assets/js/custom.js"></script>
</body>
</html>