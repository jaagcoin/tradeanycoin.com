<!doctype html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
    <head>
        <title>Verify - Bitex UAE</title>

        <meta charset="utf-8">
        <meta name="description" content="Login to Bitex UAE to start trading digital assets like Bitcoin, Ethereum, Litecoin and Ripple using AED (Dirham) currency in the United Arab Emirates.">
        <meta name="keywords" content="login, create an account, register, bitex uae, bitex, bitcoin, ethereum, ripple, litecoin, uae, united arab emirates, cryptocurrency, exchange, trading"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"/>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        
        <!-- Loading Lato font here, optimize this-->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">

        <!-- First fold CSS -->
        <style type="text/css">*{box-sizing:border-box}body{font-family:'Lato',sans-serif;margin:0}header.demat-header{width:100%}header.demat-header .upstox-logo{margin:50px;display:inline-block}header.demat-header .demat-help-button{display:inline-block;position:absolute;right:0;margin:50px;height:50px;width:80px;cursor:pointer;color:#0046bb}header.demat-header .demat-help-button span{}header.demat-header .demat-help-button span.demat-help-bubble{background:url(https://cf.upstox.com/open-demat-account/images/xhelp_bubble.png.pagespeed.ic.vY5wtg7fNg.png) no-repeat;background-size:contain;margin-right:10px;display:inline-block;width:20px;height:20px;position:relative;top:7px}main.first-fold{background:url(https://tradeanycoin.com/assets/themes/limitless/images/register-background.png) no-repeat;background-size:85% 100%;width:100%;padding-bottom:60px}@media screen and (max-width:1370px){main.first-fold{background-size:1000px}.account-container section.demat-details{margin-top:70px}}.first-fold .account-container{display:flex;flex-direction:row;justify-content:space-evenly}.account-container .demat-details{color:#fffefe;flex:0 0 auto;margin-top:100px}.account-container .demat-details h1{font-size:50px;font-weight:300}.account-container .demat-details p{font-size:22px;font-weight:300}.account-container .demat-form,.account-container .signinbox{width:400px;background-color:#fff;box-shadow:0 0 19.3px 15.8px rgba(0,0,0,.05);margin-bottom:20px;margin-top:20px;padding:25px}.account-container .demat-form p{font-size:18px;letter-spacing:.8px;text-align:left;font-weight:bold;color:#aaa;margin-top:0;margin-bottom:20px;text-transform:uppercase;letter-spacing:1.5px}.account-container .demat-form input{opacity:.75;height:50px;width:100%;background-color:#fff;border:solid 1px #ccc;border-radius:0;padding:10px 20px;font-size:16px;margin-bottom:20px;font-family:Lato;font-size:16px;font-weight:400;letter-spacing:.8px;text-align:left}.demat-form #txtcontact{width:calc(100% - 49px)}.account-container .demat-form .input-group-addon{display:inline-block;line-height:50px;height:50px;position:relative;top:1px;color:#000;text-align:center;border:1px solid #ced4da;border-right:none;padding:0 10px}.demat-form #submitsignup{width:100%;height:50px;border:none;background:#17294e;color:#fff;font-size:16px;font-weight:700;letter-spacing:1px;text-transform:uppercase;cursor:pointer}.demat-form .form-divider{position:relative;margin-top:30px;left:-25px;width: calc(100% + 50px);border-bottom:1px solid rgba(204,204,204,.75)}.demat-form #referral_section{margin-top:20px}.demat-form #referral_section label{font-size:14px;display:block;color:#666}.demat-form #referral_section .ref-wrapper{display:flex;flex-direction:row;margin-top:20px;margin-bottom:20px}button:focus{outline:0}.demat-form #referral_section .ref-wrapper button{border-radius:4px;border:solid 1px #ccc;padding:10px!important;background:transparent;font-size:16px;letter-spacing:.6px;text-align:center;color:#ccc;height:40px;line-height:14px;width:48%}.ref-yes{margin-right:20px}.demat-form #referral_section .ref-wrapper button.selected{border:solid 1px #0046bb;background:#e8f1ff;color:#0046bb}.account-container .signinbox p{margin-top:0;font-size:14px;text-transform:uppercase;color:#666;letter-spacing:1.5px}.account-container .signinbox a{font-size:18px;font-weight:700;color:#0046bb;letter-spacing:1.5px;border-bottom:2px solid #0046bb;padding-bottom:3px;text-decoration:none!important}small{display:none}@media screen and (max-width:1150px){header.demat-header .demat-help-button{color:#fff}.first-fold .account-container{flex-direction:column}.account-container section.demat-details{margin-top:0}.account-container .demat-details h1{text-align:center;font-size:36px;margin-top:0}.account-container .demat-details div{display:none}.account-container .form-container{display:flex;align-items:center;flex-direction:column}.account-container .demat-form .input-group-addon{top:0}}@media screen and (max-width:450px){.account-container .demat-details h1{font-size:22px}.account-container .demat-form p{font-size:14px}header.demat-header .demat-help-button{margin:15px 20px;font-size:12px;width:62px}header.demat-header .upstox-logo{margin:20px}header.demat-header .upstox-logo img{height:100%}.account-container .demat-form,.account-container .signinbox{width:calc(100% - 30px);min-width:200px;margin-bottom:20px;margin-top:20px;padding:15px}.account-container .demat-form input{height:40px;font-size:14px;padding:15px}.account-container .demat-form .input-group-addon{line-height:40px;height:40px;top:1px;font-size:14px}.account-container .signinbox{padding-bottom:20px;margin-top:5px}.account-container .signinbox p{font-size:12px;margin-bottom:5px}.account-container .signinbox a{font-size:14px;margin-bottom:5px}main.first-fold{padding-bottom:20px;margin-bottom:20px}.demat-form .form-divider{width: calc(100% + 30px);left:-15px}}</style>
        <!-- End First fold CSS -->
    </head>


    <body>

        <main class="first-fold">
            <header class="demat-header">
                <div class="upstox-logo">
                    <a href="https://www.bitexuae.com"><img src="{{ asset('assets/themes/limitless/images/bitex-white-2.png') }}" width="200"></a>
                </div>
                <div class="demat-help-button"><a href="{{url('logout')}}" style="text-decoration: none; color: #17294e; border: 1px solid #d0d0d0; padding: 10px;">LOGOUT</a></div>
            </header>
            <div class="account-container">
                <section class="demat-details">
                    <h1>Complete verification</h1>
                    <div>
                        <p style="line-height: 40px; letter-spacing: 0.8px;">
                            To Buy/Sell Digital Assets<br> 
                            like Bitcoin, Ethereum, Litecoin and Ripple.<br> Use the professional trading platform today.
                        </p>
                    </div>
                    
                </section>

                <div class="form-container">
                    <section class="demat-form">
                    @if(empty(Sentinel::getUser()->email))
                        <div class="alert alert-primary alert-bordered">
                            <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                        class="sr-only">Close</span></button>
                            {{trans('general.verify_email_msg')}}
                        </div>
                        <div class="alert bg-danger alert-bordered">
                            <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                        class="sr-only">Close</span></button>
                            {{trans('general.email_not_set')}}
                        </div>
                    @else
                        @if(Sentinel::getUser()->email_verified==1)
                            <div class="alert bg-success alert-bordered">
                                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                            class="sr-only">Close</span></button>
                                {{trans('general.email_verified')}}
                            </div>
                        @else
                        <div style="text-align: center;">
                        <span>1. Email</span><div style="display: inline-block;width: 80px;height: 2px;margin: 9px 10px 2px;background-color: #d0d0d0;"></div>
                        <span style="color: #d0d0d0">2. Phone</span>
                        </div><br><br>
                            @if(!isset($_COOKIE['otpresend']))
                            <div style="text-align: center;">
                            <button type="submit" class="btn btn-success btn-sm" style="width: 50%;height: 50px;border: none;background: #8296a5;color: #fff;font-size: 16px;font-weight: 500;letter-spacing: 1px;cursor: pointer;" 
                                    id="requestOtp">{{trans_choice('general.request',1)}} {{trans_choice('general.otp',1)}}</button><br>
                                </div>
                            @endif

                        
                        {!! Form::open(array('url' => 'user/verifyemail/checkotp','class'=>'',"enctype" => "multipart/form-data")) !!}
                            
                            

                            <div id="emailGroup" class="form-group">
                                {!!  Form::label('otp',trans('general.otp'),array('class'=>'control-label')) !!}
                                {!! Form::number('otp','',array('class'=>'form-control','required'=>'required','id'=>'otp')) !!}
                                
                            </div>

                            <div class="form-group">
                                <button id="submitsignup" type="submit" class="btn-primary">{{trans_choice('general.verify',1)}}</button>
                            </div>
                            {!! Form::close() !!}
                        @endif
            @endif
                            
                        </form>
                    </section>

                    <section class="signinbox">
                        <p>Don't have an account?</p>
                                                    <a href="{{url('register')}}" id="ac-sign-in" clevertapevent="Sign In">REGISTER</a>
                                            </section>
                </div>
            </div>          
        </main>

        <script>
        $("#requestOtp").click(function (e) {
            $.ajax({
                type: 'GET',
                url: '{{url('user/verify_email/send_otp?id='.Sentinel::getUser()->id)}}',
                dataType: 'json',
                success: function (data) {
                    if (data.success === 1) {
                        swal({
                            title: '{{trans_choice('general.success',1)}}',
                            html: 'Email Sent',
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: '{{trans_choice('general.ok',1)}}',
                        });
                        $('#requestOtp').hide();
                    } else {
                        swal({
                            title: '{{trans_choice('general.error',1)}}',
                            text: '{{trans_choice('general.error_occurred',1)}}',
                            type: 'warning',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: '{{trans_choice('general.ok',1)}}',
                            cancelButtonText: '{{trans_choice('general.cancel',1)}}'
                        })
                    }
                }
                ,
                error: function (e) {
                    swal({
                        title: '{{trans_choice('general.error',1)}}',
                        text: '{{trans_choice('general.error_occurred',1)}}',
                        type: 'warning',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: '{{trans_choice('general.ok',1)}}',
                        cancelButtonText: '{{trans_choice('general.cancel',1)}}'
                    })
                }
            });
        })
    </script>
        
    </body>
</html>