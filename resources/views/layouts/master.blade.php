<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title> @yield('title')</title>
<link rel="icon" type="image/png" href="https://tradeanycoin.com/assets/images/logo.png">
<!-- Bootstrap CSS CDN -->
<link rel="stylesheet" href="https://tradeanycoin.com/assets/bootstrap/css/bootstrap.min.css" >
<!-- Our Custom CSS -->
<link rel="stylesheet" href="https://tradeanycoin.com/assets/css/style.css">
<!-- Scrollbar Custom CSS -->
<link rel="stylesheet" href="https://tradeanycoin.com/assets/css/jquery.mCustomScrollbar.min.css">

<!-- Font Awesome JS -->
<script defer src="https://tradeanycoin.com/assets/js/solid.js"></script>
<script defer src="https://tradeanycoin.com/assets/js/fontawesome.js"></script>

<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
<link rel="stylesheet" href="{{ asset('assets/themes/limitless/css/icons/icomoon/styles.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<link rel="stylesheet" href="{{ asset('assets/themes/limitless/css/bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('assets/themes/limitless/css/core.css') }}">
<link rel="stylesheet" href="{{ asset('assets/themes/limitless/css/components.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/themes/limitless/css/colors.css') }}">
    <!--  New added css -->
    <link rel="stylesheet" href="{{ asset('assets/themes/limitless/css/jquery.mCustomScrollbar.css') }}">

    <!-- End new Added CSS -->
    <link rel="stylesheet" href="{{ asset('assets/themes/limitless/css/style.css') }}">

    <link rel="stylesheet" href="{{asset('assets/font/materialdesignicons-webfont.woff')}}">
    <link rel="stylesheet" href="{{asset('assets/font/materialdesignicons-webfont.woff2')}}">
    <link rel="stylesheet" href="{{asset('assets/font/materialdesignicons-webfont.ttf')}}">

    <link rel="stylesheet" href="{{asset('font/materialdesignicons-webfont.woff')}}">
    <link rel="stylesheet" href="{{asset('font/materialdesignicons-webfont.woff2')}}">
    <link rel="stylesheet" href="{{asset('font/materialdesignicons-webfont.ttf')}}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/2.4.85/fonts/materialdesignicons-webfont.woff">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/2.4.85/fonts/materialdesignicons-webfont.woff2">



    <link href="{{ asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/bootstrap-toastr/toastr.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/bootstrap-touchspin/bootstrap.touchspin.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/fullcalendar/fullcalendar.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/fancybox/jquery.fancybox.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/amcharts/plugins/export/export.css') }}"
          rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}"
          rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/plugins/datepicker/bootstrap-datepicker3.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="text/png" href="{{ asset('assets/themes/limitless/images/favicon.png') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->
</head>

<body>
<div class="wrapper"> 
  <!-- Sidebar  -->
  <nav id="sidebar">
    <div class="sidebar-header">
      <h3><img src="https://tradeanycoin.com/assets/images/logo.jpg" alt="logo" /></h3>
    </div>
    <ul class="list-unstyled components">
      <li class="dashboard active"><a href="{{url('dashboard')}}">Dashboard</a></li>
      <li class="wallet"> <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Wallet</a>
        <ul class="collapse list-unstyled" id="homeSubmenu">
          <li><a href="{{ url('wallets/data' )}}">Deposit & Withdraws</a></li>
          <li><a href="{{url('wallets/history')}}">History</a></li>
        </ul>
      </li>
      <li class="exchange"> <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Exchange</a>
        <ul class="collapse list-unstyled" id="pageSubmenu">
          <li> <a href="{{url('exchange/data')}}">Exchange</a> </li>
          <li> <a href="{{url('exchange/orders')}}">My Open Orders</a> </li>
          <li> <a href="{{url('trade_history')}}">My Trade History</a> </li>
        </ul>
      </li>
      <li class="transaction"><a href="{{url('transaction')}}"l>Transaction</a></li>
      <li class="settings"> <a href="#settingsSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Settings</a>
        <ul class="collapse list-unstyled" id="settingsSubmenu">
          <li> <a href="{{url('setting/profile')}}">My Profile</a> </li>
          <li> <a href="{{url('setting/security')}}">Security</a> </li>
        </ul>
      </li>
      <li class="affiliate"> <a href="#affiliateSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Affiliate</a>
        <ul class="collapse list-unstyled" id="affiliateSubmenu">
          <li> <a href="{{url('affiliate')}}">Members</a> </li>
        </ul>
      </li>
      <li class="support"> <a href="{{url('support')}}">Support and Ticket</a> </li>
    </ul>
  </nav>
  
  <!-- Page Content  -->
  <div id="content">
    <nav id="headnev" class="navbar navbar-expand-lg navbar-light">
      <div class="container-fluid">
        <button type="button" id="sidebarCollapse" class="btn btn-info" style="border-width: 1px; padding: .375rem .75rem"> <i class="fas fa-align-left"></i> </button>
        <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <i class="fas fa-align-justify"></i> </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="nav navbar-nav ml-auto" style="margin-left: auto!important; float: right; align-items:center;">
            <li class="nav-item active"> 1 BTC = 8180.28 </li>
            <li class="nav-item"> 1 ETC = 469.07 </li>
            <li class="nav-item"> NAPIERIRF </li>
            <li class="nav-item"> <a class="nav-link" href="#" style="padding: 0">Sign out</a> </li>
          </ul>
        </div>
      </div>
    </nav>
    <section class=""  style="margin: 3.0rem">
                    @if(Session::has('flash_notification.message'))
                        <script>toastr.{{ Session::get('flash_notification.level') }}('{{ Session::get("flash_notification.message") }}', 'Response Status')</script>
                    @endif
                    @if (isset($msg))
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ $msg }}
                        </div>
                    @endif
                    @if (isset($error))
                        <div class="alert alert-error">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ $error }}
                        </div>
                    @endif
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
<!--- Add content -->
    @yield('content')
  </section>
  </div>
</div>
<!-- jQuery CDN - Slim version (=without AJAX) --> 
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> 
<!-- Bootstrap JS --> 
<script src="https://tradeanycoin.com/assets/bootstrap/js/bootstrap.min.js"></script> 
<!-- jQuery Custom Scroller CDN --> 
<script src="https://tradeanycoin.com/assets/js/jquery.mCustomScrollbar.concat.min.js"></script> 
<script src="https://tradeanycoin.com/assets/js/custom.js"></script>
</body>
</html>