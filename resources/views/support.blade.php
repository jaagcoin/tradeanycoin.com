<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trade any Coin - Support & Ticket</title>
<link rel="icon" type="image/png" href="https://tradeanycoin.com/assets/images/logo.png">
<!-- Bootstrap CSS CDN -->
<link rel="stylesheet" href="https://tradeanycoin.com/assets/bootstrap/css/bootstrap.min.css" >
<!-- Our Custom CSS -->
<link rel="stylesheet" href="https://tradeanycoin.com/assets/css/style.css">
<!-- Scrollbar Custom CSS -->
<link rel="stylesheet" href="https://tradeanycoin.com/assets/css/jquery.mCustomScrollbar.min.css">

<!-- Font Awesome JS -->
<script defer src="https://tradeanycoin.com/assets/js/solid.js"></script>
<script defer src="https://tradeanycoin.com/assets/js/fontawesome.js"></script>
</head>

<body>
<div class="wrapper"> 
  <!-- Sidebar  -->
  <nav id="sidebar">
    <div class="sidebar-header">
      <h3><img src="https://tradeanycoin.com/assets/images/logo.jpg" alt="logo" /></h3>
    </div>
    <ul class="list-unstyled components">
      <li class="dashboard active"><a href="{{url('dashboard')}}">Dashboard</a></li>
      <li class="wallet"> <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Wallet</a>
        <ul class="collapse list-unstyled" id="homeSubmenu">
          <li><a href="{{ url('wallets/data' )}}">Deposit & Withdraws</a></li>
          <li><a href="{{url('wallets/history')}}">History</a></li>
        </ul>
      </li>
      <li class="exchange"> <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Exchange</a>
        <ul class="collapse list-unstyled" id="pageSubmenu">
          <li> <a href="{{url('exchange/data')}}">Exchange</a> </li>
          <li> <a href="{{url('exchange/orders')}}">My Open Orders</a> </li>
          <li> <a href="{{url('trade_history')}}">My Trade History</a> </li>
        </ul>
      </li>
      <li class="transaction"><a href="{{url('transaction')}}"l>Transaction</a></li>
      <li class="settings"> <a href="#settingsSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Settings</a>
        <ul class="collapse list-unstyled" id="settingsSubmenu">
          <li> <a href="{{url('setting/profile')}}">My Profile</a> </li>
          <li> <a href="{{url('setting/security')}}">Security</a> </li>
        </ul>
      </li>
      <li class="affiliate"> <a href="#affiliateSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Affiliate</a>
        <ul class="collapse list-unstyled" id="affiliateSubmenu">
          <li> <a href="{{url('affiliate')}}">Members</a> </li>
        </ul>
      </li>
      <li class="support"> <a href="{{url('support')}}">Support and Ticket</a> </li>
    </ul>
  </nav>
  
  <!-- Page Content  -->
  <div id="content">
    <nav id="headnev" class="navbar navbar-expand-lg navbar-light">
      <div class="container-fluid">
        <button type="button" id="sidebarCollapse" class="btn btn-info"> <i class="fas fa-align-left"></i> </button>
        <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <i class="fas fa-align-justify"></i> </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="nav navbar-nav ml-auto">
            <li class="nav-item active"> 1 BTC = 8180.28 </li>
            <li class="nav-item"> 1 ETC = 469.07 </li>
            <li class="nav-item"> NAPIERIRF </li>
            <li class="nav-item"> <a class="nav-link" href="#">Sign out</a> </li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="inner-content">
      <div class="col-md-12"> 
        
        <!--- Support Section Start --->
        <div class="support-ticket">
          <h1 class="title">Support and Ticket</h1>
          <p>We are always available and accessible to support you at anytime you need us. Here is our support email address link: <a href="mailto:support@theautoblock.com">support@theautoblock.com</a> In case you need a technical support, it would be more convenient if you could create a ticket.</p>
          
          <!--- Support Table Section Start --->
          <div class="support-ticket-table">
            <div class="support-new-ticket mb-4">
              <button type="button" class="btn btn-custom">New support Ticket</button>
            </div>
            <div class="support-list-ticket-table">
              <h5 class="support-list-ticket-title">List Ticket</h5>
              <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Category</th>
                    <th>Content</th>
                    <th>Created date</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td data-title="#"></td>
                    <td data-title="Title"></td>
                    <td data-title="Category"></td>
                    <td data-title="Content"></td>
                    <td data-title="Created date">You have no ticket!</td>
                    <td data-title="Status"></td>
                    <td data-title="Action"></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <!--- Support Table Section End ---> 
          
        </div>
        <!--- Support Section End ---> 
        
      </div>
    </div>
  </div>
</div>
<!-- jQuery CDN - Slim version (=without AJAX) --> 
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> 
<!-- Bootstrap JS --> 
<script src="https://tradeanycoin.com/assets/bootstrap/js/bootstrap.min.js"></script> 
<!-- jQuery Custom Scroller CDN --> 
<script src="https://tradeanycoin.com/assets/js/jquery.mCustomScrollbar.concat.min.js"></script> 
<script src="https://tradeanycoin.com/assets/js/custom.js"></script>
</body>
</html>