<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trade any Coin - Exchange</title>
<link rel="icon" type="image/png" href="https://tradeanycoin.com/assets/images/logo.png">
<!-- Bootstrap CSS CDN -->
<link rel="stylesheet" href="https://tradeanycoin.com/assets/bootstrap/css/bootstrap.min.css" >
<!-- Our Custom CSS -->
<link rel="stylesheet" href="https://tradeanycoin.com/assets/css/style.css">
<!-- Scrollbar Custom CSS -->
<link rel="stylesheet" href="https://tradeanycoin.com/assets/css/jquery.mCustomScrollbar.min.css">

<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<!-- Font Awesome JS -->
<script defer src="https://tradeanycoin.com/assets/js/solid.js"></script>
<script defer src="https://tradeanycoin.com/assets/js/fontawesome.js"></script>
</head>

<body  class="login">
<div class="page-wrapper flex-row align-items-center">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-5 no-padding-right">
        <div class="card p-4">
          <div class="card-body ">
            <form class="login-form" action="#" method="post">
              <div id="login">
                <div class="h2"> Sign In to your account </div>
                <div class="form-group ">
                    @if(Session::has('flash_notification.message'))
                        <script>toastr.{{ Session::get('flash_notification.level') }}('{{ Session::get("flash_notification.message") }}', 'Response Status')</script>
                    @endif
                    @if (isset($msg))
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ $msg }}
                        </div>
                    @endif
                    @if (isset($error))
                        <div class="alert alert-error">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ $error }}
                        </div>
                    @endif
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                  {!! Form::email('email', null, array('class' => 'form-control', 'placeholder'=>trans_choice('general.email',1),'required'=>'required')) !!}
                                
                </div>
                <div class="form-group">
                  {!! Form::password('password', array('class' => 'form-control', 'placeholder'=>trans('login.password'),'required'=>'required')) !!}
                </div>
                <div class="form-group">
                  <div class="g-recaptcha form-field" data-sitekey="6LcENG4UAAAAAP4HQJTuonIEv-AkuDPd_y3vysrr"></div>
                </dir>
                <div class="row">
                  <div class="col-6">
                    <div class="custom-control custom-checkbox mt-2">
                      <input class="custom-control-input" id="keep" name="keep" type="checkbox">
                      <label class="custom-control-label" for="keep"> Remember me</label>
                    </div>
                  </div>
                  <div class="col-6 text-right"> <a href="{{url('reset')}}" id="btnForgotPassword" class="btn btn-link ">{{ trans('general.forgot_password') }}</a></div>
                </div>
                <div class="row">
                  <div class="col-12">
                    <button type="submit" name="login" id="login" class="btn btn-orange btn-block  ">{{ trans('general.login') }}</button>
                                            {!! Form::open(array('url' => url('login'), 'method' => 'post', 'name' => 'form','class'=>'f-login-form')) !!}

                  </div>
                </div>
                <div class="row mt-3">
                  <div class="col-12"> Don’t have an account? <a href="{{url('register')}}" id="btnCreateAccount" class="btn btn-link">Create an account</a> <br>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-md-5 no-padding-left bg-blue">
        <div class="bg-blue "> </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=<?php echo 'en'; ?>"></script>
<!-- jQuery CDN - Slim version (=without AJAX) --> 
<script src="https://code.jquery.com/jquery-3.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> 
<!-- Bootstrap JS --> 
<script src="https://tradeanycoin.com/assets/bootstrap/js/bootstrap.min.js"></script> 
<!-- jQuery Custom Scroller CDN --> 
<script src="https://tradeanycoin.com/assets/js/jquery.mCustomScrollbar.concat.min.js"></script> 
<script src="https://tradeanycoin.com/assets/js/custom.js"></script>
</body>
</html>
