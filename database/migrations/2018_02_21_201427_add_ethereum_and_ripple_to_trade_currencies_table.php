<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEthereumAndRippleToTradeCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE withdrawals CHANGE COLUMN network network ENUM('usd','bitcoin','bitcash','ethereum','neo','litecoin','jaagcoin') DEFAULT 'usd'");
        DB::statement("ALTER TABLE deposits CHANGE COLUMN network network ENUM('usd','bitcoin','bitcash','ethereum','neo','litecoin','jaagcoin') DEFAULT 'usd'");
        DB::statement("ALTER TABLE order_book CHANGE COLUMN network network ENUM('usd','bitcoin','bitcash','ethereum','neo','litecoin','jaagcoin') DEFAULT 'usd'");
        \Illuminate\Support\Facades\DB::table('trade_currencies')->insert([
            [

                'name' => 'Ethereum',
                'network' => 'ethereum',
                'xml_code' => 'ETH',
            ],
            [

                'name' => 'Bitcoin',
                'network' => 'bitcoin',
                'xml_code' => 'BCH',
            ],
            [

                'name' => 'Bitcash',
                'network' => 'bitcash',
                'xml_code' => 'BCH',
            ],
            [

                'name' => 'Litecoin',
                'network' => 'litecoin',
                'xml_code' => 'ETH',
            ],
            [

                'name' => 'Neo',
                'network' => 'neo',
                'xml_code' => 'NEO',
            ],
            [

                'name' => 'Jaagcoin',
                'network' => 'jaagcoin',
                'xml_code' => 'JAAG',
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        App\Models\TradeCurrency::where('network', 'ethereum')
            ->delete();
    }
}
