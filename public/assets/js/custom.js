/* ------------------ Static navbar --------------*/
$(document).ready(function () {
            $("#sidebar").mCustomScrollbar({
                theme: "minimal"
            });

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar, #content').toggleClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });
			
			
			/// Add class for loadding page smoothly
			
        $('.wrapper').addClass('container-loaded');
});

// Script for top Navigation Menu
   /* jQuery(window).bind('scroll', function () {
      if (jQuery(window).scrollTop() > 100) {
        jQuery('#headnev').addClass('navbar-fixed-top').removeClass('topnavbar');
        jQuery('body').addClass('bodytopmargin').removeClass('bodynomargin');
      } else {
        jQuery('#headnev').removeClass('navbar-fixed-top').addClass('topnavbar');
        jQuery('body').removeClass('bodytopmargin').addClass('bodynomargin');
      }
    });*/
	
	// Equal height function
	matchColumns=function(){ 
     var divs,contDivs,maxHeight,divHeight,d; 
     divs=document.getElementsByTagName('div'); 
     contDivs=[]; 
     maxHeight=0;  
     for(var i=0;i<divs.length;i++){  
          // make collection with <div> elements with class attribute "equal"
          if(/\bequal\b/.test(divs[i].className)){ 
                d=divs[i]; 
                contDivs[contDivs.length]=d;  
                if(d.offsetHeight){ 
                     divHeight=d.offsetHeight; 					
                } 
                else if(d.style.pixelHeight){ 
                     divHeight=d.style.pixelHeight;					 
                } 
                maxHeight=Math.max(maxHeight,divHeight); 
          } 
     } 
     for(var i=0;i<contDivs.length;i++){ 
          contDivs[i].style.height=maxHeight + "px"; 
     } 
}  
window.onload=function(){ 
     if(document.getElementsByTagName){ 
          matchColumns();			 
     } 
} 