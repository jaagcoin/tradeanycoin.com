<?php

namespace App\Http\Controllers;
use App\Models\WalletAddress;
use App\Models\TradeCurrency;
use Illuminate\Http\Request;
use App\Models\Exchange;
use App\Models\Setting;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use App\cexapi;
use GuzzleHttp\Client;

class AffiliateController extends Controller
{
    public function __construct()
    {
      $walletId = Sentinel::check()->eth_wallet_id;
        $this->middleware(['sentinel','verify_requirements']);
    }

    public function index()
    {
    	return view('afiliate');
    }
}
