<?php

namespace App\Http\Controllers;
use App\Models\WalletAddress;
use App\Models\TradeCurrency;
use Illuminate\Http\Request;
use App\Models\Exchange;
use App\Models\Setting;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use App\cexapi;
use GuzzleHttp\Client;

class DashboardController extends Controller
{
    public function __construct()
    {
      $walletId = Sentinel::check()->eth_wallet_id;
                    $client = new Client;
                    $jaagcoin = TradeCurrency::where('network', "jaagcoin")->first();
                    $contractAddress = $jaagcoin->address;
                     $response = $client->request('POST', 'http://178.128.222.5:8000/wallet/balances/', [
                        'auth' => ['admin', 'admin'], 
                        'headers' => ['Accept' => 'application/x-www-form-urlencoded'],
                        'form_params' => [
                            'id' => $walletId,
                            'contract' => $contractAddress
                        ]
                     ]);

                     if ($response->getStatusCode() == "200") {
                        $body = json_decode($response->getBody());
                        if ($body->status) {
                          Sentinel::getUser()->dodgecoin_balance = $body->data; 
                        }
                     }
                    $client = new Client;
                    $response = $client->request('POST', 'http://178.128.222.5:8000/wallet/balances/', [
                        'auth' => ['admin', 'admin'], 
                        'headers' => ['Accept' => 'application/x-www-form-urlencoded'],
                        'form_params' => [
                            'id' => $walletId
                        ]
                     ]);

                     if ($response->getStatusCode() == "200") {
                        $body = json_decode($response->getBody());
                        if ($body->status) {
                            Sentinel::getUser()->ethereum_balance = $body->data; 
                        }
                     }

                     Sentinel::getUser()->save();
        $this->middleware(['sentinel','verify_requirements']);
    }

    public function index($coinuse='BTC')
    {
        $marks = array('BTC', 'ETH', 'XRP','LTC');

        if (!in_array($coinuse, $marks))
            return redirect()->to("dashboard-exchange/BTC");
        Sentinel::getUser()->do;

      $user_id = Sentinel::getUser()->id;
      $trade_cur = WalletAddress::where('user_id',$user_id)->get();

      $buy_fees = Setting::where('setting_key','buy_percentage')->first()->setting_value;
      $sell_fees =Setting::where('setting_key','sell_percentage')->first()->setting_value;
      $aed_rate =Setting::where('setting_key','aed_rate')->first()->setting_value;
      $exchange_data = Exchange::where('user_id',$user_id)->orderby('id','desc')->get();

    	return view('exchange.dashboard',compact('buy_fees','sell_fees','trade_cur','exchange_data','aed_rate',"coinuse"));
    }
}
