
<?php $__env->startSection('title'); ?>
    <?php echo e(trans_choice('general.add',1)); ?> <?php echo e(trans_choice('general.user',1)); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="pageContent">
    <div class="container">
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title"><?php echo e(trans_choice('general.add',1)); ?> <?php echo e(trans_choice('general.user',1)); ?></h6>

            <div class="heading-elements">

            </div>
        </div>
        <?php echo Form::open(array('url' => 'user/store','class'=>'',"enctype" => "multipart/form-data")); ?>

        <div class="panel-body">
            <div class="form-group">
                <?php echo Form::label(trans('general.first_name'),null,array('class'=>'control-label')); ?>

                <?php echo Form::text('first_name','',array('class'=>'form-control','required'=>'required')); ?>

            </div>
            <div class="form-group">
                <?php echo Form::label(trans('general.last_name'),null,array('class'=>'control-label')); ?>

                <?php echo Form::text('last_name','',array('class'=>'form-control','required'=>'required')); ?>

            </div>
            <div class="form-group">
                <?php echo Form::label(trans('general.gender'),null,array('class'=>' control-label')); ?>

                <?php echo Form::select('gender', array('Male' =>trans('general.male'), 'Female' => trans('general.female')),null,array('class'=>'form-control')); ?>

            </div>
            <div class="form-group">
                <?php echo Form::label(trans_choice('general.phone',1),null,array('class'=>'control-label')); ?>

                <?php echo Form::text('phone',null,array('class'=>'form-control')); ?>

            </div>
            <div class="form-group ">
                <?php echo Form::label(trans_choice('general.email',1),null,array('class'=>'control-label')); ?>

                <?php echo Form::email('email','',array('class'=>'form-control','required'=>'required')); ?>

            </div>
            <div class="form-group">
                <?php echo Form::label(trans('general.password'),null,array('class'=>'control-label')); ?>

                <?php echo Form::password('password',array('class'=>'form-control','required'=>'required')); ?>

            </div>
            <div class="form-group">
                <?php echo Form::label(trans('general.repeat_password'),null,array('class'=>'control-label')); ?>

                <?php echo Form::password('rpassword',array('class'=>'form-control','required'=>'required')); ?>

            </div>
            <div class="form-group">
                <?php echo Form::label(trans_choice('general.role',1),null,array('class'=>' control-label')); ?>

                <?php echo Form::select('role', $role,null,array('class'=>'form-control')); ?>

            </div>
            <div class="form-group">
                <?php echo Form::label(trans('general.address'),null,array('class'=>'control-label')); ?>

                <?php echo Form::textarea('address','',array('class'=>'form-control wysihtml5','rows'=>'3')); ?>

            </div>
            <div class="form-group">
                <?php echo Form::label(trans_choice('general.note',2),null,array('class'=>'control-label')); ?>


                <?php echo Form::textarea('notes','',array('class'=>'form-control wysihtml5','rows'=>'3')); ?>

            </div>
        </div>
        <!-- /.panel-body -->
        <div class="panel-footer">
            <div class="heading-elements">
                <button type="submit" class="btn btn-primary pull-right"><?php echo e(trans_choice('general.save',1)); ?></button>
            </div>
        </div>
        <?php echo Form::close(); ?>

    </div>
</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer-scripts'); ?>
    <script src="<?php echo e(asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>