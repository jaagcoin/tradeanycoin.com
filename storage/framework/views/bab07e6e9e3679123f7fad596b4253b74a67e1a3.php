
<?php $__env->startSection('title'); ?>
    <?php echo e(trans_choice('general.payment',1)); ?> <?php echo e(trans_choice('general.gateway',2)); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="pageContent">
    <div class="container">
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">  <?php echo e(trans_choice('general.payment',1)); ?> <?php echo e(trans_choice('general.gateway',2)); ?></h6>

            <div class="heading-elements">
                <?php if(Sentinel::hasAccess('deposits.create')): ?>
                    <a href="<?php echo e(url('payment_gateway/create')); ?>" class="btn btn-info btn-xs">
                        <?php echo e(trans_choice('general.add',1)); ?> <?php echo e(trans_choice('general.gateway',1)); ?>

                    </a>
                <?php endif; ?>
            </div>
        </div>
        <div class="panel-body ">
            <div class="table-responsive">
                <table id="data-table" class="table table-striped table-condensed table-hover">
                    <thead>
                    <tr>
                        <th><?php echo e(trans_choice('general.name',1)); ?></th>
                        <th><?php echo e(trans_choice('general.logo',1)); ?></th>
                        
                        <th><?php echo e(trans_choice('general.type',1)); ?></th>
                        <th><?php echo e(trans_choice('general.note',1)); ?></th>
                        
                        <th><?php echo e(trans_choice('general.action',1)); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($key->name); ?></td>
                            <td>
                                <?php if(empty($key->logo)): ?>
                                    <img src="<?php echo e(asset('uploads/no_image.png')); ?>" width="33">
                                <?php else: ?>
                                    <img src="<?php echo e(asset('uploads/'.$key->logo)); ?>" width="33">
                                <?php endif; ?>
                            </td>
                            

                            
                            <td>
                                <?php echo e($key->type); ?>

                            </td>
                            <td>
                                <?php echo e($key->notes); ?>

                            </td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                            <?php if(Sentinel::hasAccess('deposits.update')): ?>
                                                <li><a href="<?php echo e(url('payment_gateway/'.$key->id.'/edit')); ?>"><i
                                                                class="fa fa-edit"></i> <?php echo e(trans('general.edit')); ?> </a>
                                                </li>
                                            <?php endif; ?>
                                            <?php if($key->active==0): ?>
                                                <?php if(Sentinel::hasAccess('deposits.delete')): ?>
                                                    <li><a href="<?php echo e(url('payment_gateway/'.$key->id.'/delete')); ?>"
                                                           class="delete"><i
                                                                    class="fa fa-trash"></i> <?php echo e(trans('general.delete')); ?>

                                                        </a>
                                                    </li>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.panel-body -->
    </div>
</div>
</div>
    <!-- /.box -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer-scripts'); ?>
    <script>
        $('#data-table').DataTable({
            "order": [[0, "asc"]],
            "columnDefs": [
                {"orderable": false, "targets": [3]}
            ],
            "language": {
                "lengthMenu": "<?php echo e(trans('general.lengthMenu')); ?>",
                "zeroRecords": "<?php echo e(trans('general.zeroRecords')); ?>",
                "info": "<?php echo e(trans('general.info')); ?>",
                "infoEmpty": "<?php echo e(trans('general.infoEmpty')); ?>",
                "search": "<?php echo e(trans('general.search')); ?>",
                "infoFiltered": "<?php echo e(trans('general.infoFiltered')); ?>",
                "paginate": {
                    "first": "<?php echo e(trans('general.first')); ?>",
                    "last": "<?php echo e(trans('general.last')); ?>",
                    "next": "<?php echo e(trans('general.next')); ?>",
                    "previous": "<?php echo e(trans('general.previous')); ?>"
                }
            },
            responsive: false
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>