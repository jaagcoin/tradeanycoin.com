<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Trading Exchange</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo e(asset('assets/themes/limitless/css/bootstrap.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/themes/limitless/css/icons/icomoon/styles.css')); ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo e(asset('assets/themes/limitless/css/core.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/themes/limitless/css/components.css')); ?>">

    <!--  New added css -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/themes/limitless/css/jquery.mCustomScrollbar.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/themes/limitless/css/style.css')); ?>">
    <!-- End new Added CSS -->

    <link href="<?php echo e(asset('assets/plugins/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo e(asset('assets/plugins/bootstrap-touchspin/bootstrap.touchspin.min.css')); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo e(asset('assets/plugins/fullcalendar/fullcalendar.css')); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo e(asset('assets/plugins/sweetalert2/sweetalert2.min.css')); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo e(asset('assets/plugins/toastr/toastr.min.css')); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo e(asset('assets/plugins/fancybox/jquery.fancybox.css')); ?>"
          rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="<?php echo e(asset('assets/themes/limitless/css/jquery.gridly.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/themes/limitless/css/jquery.gridster.min.css')); ?>">

          
    <link href="<?php echo e(asset('assets/plugins/amcharts/plugins/export/export.css')); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo e(asset('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo e(asset('assets/plugins/datepicker/bootstrap-datepicker3.min.css')); ?>" rel="stylesheet" type="text/css"/>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    

    
    <style type="text/css">
    .row { position: relative;}
    .example {
  position: relative;   }
 
.ticker-wrapper {
      width: 0;
      min-width: 600px;
      max-width: 700px;
      height: 100%;
      background: transparent;
      float: right;
    }

    .ticker-wrapper-header {
      font-size: 13px;

    }

    .ticker-wrapper-body {
     
      box-sizing: border-box;
      border-radius: 3px;
      overflow: hidden;
    }

    .ticker-container {
      display: table;
      position: relative;
      width: 100%;
      height: 70px;
      overflow-y: hidden;
      table-layout: fixed;
     
    }

    .ticker-item {
      display: table-cell;
      position: relative;
      padding: 10px 23px;
      height: 50px;
      border-left: 1px solid transparent;
      background-clip: padding-box;
      vertical-align: top;
      color: #4a4a4a;
      -webkit-transform: translateZ(0);
    }

    .ticker-item:first-child {
      border-left: none;
    }

    .ticker-item:after {
      content: "";
    position: absolute;
    top: 10px;
    bottom: 10px;
    left: -1px;
    width: 1px;
    background-color: #e4e6ea;
    }

    a {
    background-color: transparent;
    text-decoration: none;
    }

    .ticker-item-head {
      display: flex;
      height: 20px;
      width: 100%;
      flex-direction: row;
      justify-content: space-between;
      align-items: stretch;
      font-size: 12px;
    }

    .ticker-item-title {
      display: inline-block;
      flex-shrink: 1;
      -webkit-user-select: text;
      -moz-user-select: text;
      -ms-user-select: text;
      user-select: text;
      cursor: pointer;
      overflow: hidden;
      white-space: nowrap;
      text-overflow: ellipsis;
    }

    .ticker-item-last {
      flex-grow: 1;
      overflow: visible;
      white-space: nowrap;
      -webkit-user-select: text;
      -moz-user-select: text;
      -ms-user-select: text;
      user-select: text;
      text-align: right;
    }

    .ticker-item-body {
      width: 100%;
      white-space: nowrap;
      text-overflow: ellipsis;
      font-size: 22px;
      line-height: 26px;
    }

    .ticker-item-body-up {
      color: #3cbc98;
    }

    .ticker-item-body-down {
      color: #ff4a68;
    }

    .ticker-item-change-direction {
      display: inline-block;
      vertical-align: top;
      width: 12px;
      height: 8px;
    }

    .ticker-item-change {
      font-size: 12px;
      margin: 0 0 0 8px;
    }

  .example .brick {
    opacity: 1;
    cursor: pointer;
    position: relative;
    -webkit-box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.36);
    -moz-box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.36);
    box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.36);
   }

    .example .brick .delete {
      display: block;
      color: #000;
      background: rgba(255, 255, 255, 0.2);
      width: 40px;
      font-size: 25px;
      height: 40px;
      top: 0;
      right: 0;
      position: absolute;
      text-align: center;
      line-height: 40px;
      z-index: 100 }
    .example .brick.small {
      width: 180px;
      height: 180px;
      overflow: hidden;
      background: #fafafa; }
    .example .brick.large {
      width: 180px;
      height: 180px;overflow: hidden; }
    .example .brick.dragging {
      opacity: 0.8; }
    .example .brick {}
.example {
    text-transform: uppercase;
    
}
.example h3 { padding: 0; margin: 0; padding: 0 10px; font-size: 13px; font-weight: 700;}
.example h3:first-of-type { margin-top: 10px;}
.example > div { padding-left: 10px; padding-right: 10px;}
.abbr-wrap:first-of-type { margin-top: 8px;}
.buy-sell { margin-top: 8px}
.abbr-wrap .smbl, .buy-sell > div { padding-left: 20px; padding-right: 20px; }
.buy-sell > div {color: #fff; font-weight: 700; position: relative; }
.buy-sell .sell-itm,.buy-sell .buy-itm { position: absolute; bottom: 8px; width: 59%; }
.buy-sell .value-blk { width: 65px; height: 26px; line-height: 26px; font-size: 16px; color: #000; background: #fff; text-align: center; position: absolute; z-index: 5; margin-left: -32px; left: 50%; top: 50%; margin-top: -13px; border-radius: 3px;}
.example .grey-sell,
.example .green-buy {
    background: #b4b4b4;
    height: 80px;
}
.example .green-buy {
    background: #24a473;
}
        .card.full-card {
            position: fixed;
            top: 80px;
            z-index: 99999;
            -webkit-box-shadow: none;
            box-shadow: none;
            right: 0;
            border-radius: 0;
            border: 1px solid #ddd;
            width: calc(100vw - 287px);
            height: calc(100vh - 80px);
        }
        .loader {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif') 50% 50% no-repeat rgb(249,249,249);
            opacity: .8;
        }
   
        tr:hover {
            background-color: #d9d9d9;
        }

        .intro {
            background-color: #2f2c2c1f;
        }
        .onhover-dropdown  a li {
            color: #00000082;
        }
/* Set height of body and the document to 100% */

@import  url('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700');

body, html {
    height: 100%;
    margin: 0;
    font-family: 'Source Sans Pro', sans-serif;
}
h1,h2,h3,h4,h5,h6,p,a, th, td {font-family: 'Source Sans Pro', sans-serif; }
table.theme-tbl thead th, table.theme-tbl tbody td { text-align: left;}
table.theme-tbl thead th { color: #9b9b9b; font-weight: 500;}
table.theme-tbl tbody td { padding: 8px 8px;font-size: 14px;}
.navbar-avatar-dd>.dropdown-toggle { padding: 15px 0 15px 25px;}
/* Style tab links */
.bottomtabbar{
    position:fixed; bottom:0; z-index: 999999; width: 100%;background: #fff;-webkit-box-shadow: 0px -1px 5px -1px rgba(0,0,0,0.27);
-moz-box-shadow: 0px -1px 5px -1px rgba(0,0,0,0.27);
box-shadow: 0px -1px 5px -1px rgba(0,0,0,0.27);
}
.txt-xl { font-size: 13px;}
.tablink {
    background-color: white;
    color: black;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 8px 16px;
    font-size: 17px;
    width: 120px;
    position: relative;
    text-align: left;
}

.selectL { color: #fff;}

.tablink:hover {
    background-color: #0046bb;
    color: #fff;
}

.tablink:after {
  content: "";
  width: 0; 
  height: 0; 
  border-left: 7px solid transparent;
  border-right: 7px solid transparent;
  border-top: 7px solid #9b9b9b;
  position: absolute;
      right: 10px;
    top: 18px;
}

.tabSelected:after {
  border-top: 7px solid #fff;
}

/* Style the tab content (and add height:100% for full page content) */
.tabcontent {
   height: 0;
    overflow-y: hidden;
    padding: 0;
}

#Home {background-color: #f5f7fa; overflow-y: auto;}
#chartsTab {background-color: #f5f7fa; overflow-y: auto; margin-top: 70px;}
#quotesTab {background-color: #f5f7fa; overflow-y: auto; margin-top: 0px; height: 100%;}
#quotesTab > .row, #quotesTab > .row > div,
#quotesTab #quotesContent,
#quotesTab > .row #quotesContent ul li,
#quotesTab > .row #quotesContent ul li .gridster-box,
#quotesTab > .row #quotesContent ul li .gridster-box .theme-widget,
#books ul li,
#books ul li .gridster-box,
#books ul li .gridster-box .theme-widget,
#chartsContent ul li,
#chartsContent ul li .gridster-box,
#chartsContent ul li .gridster-box .theme-widget { height: 100%; }
#quotesTab > .row #quotesContent ul li .gridster-box .theme-widget,
#books ul li .gridster-box .theme-widget { overflow: hidden;}
#quotesTab > .row #quotesContent ul li .gridster-box .theme-widget .chart-div { margin-bottom: 50px;}
#quotesTab > .row #quotesContent ul li .gridster-box .theme-widget .widget-body,
#books ul li .gridster-box .theme-widget .widget-body,
#books ul li .gridster-box .theme-widget .widget-body .table-responsive{ height: 90%;}
#quotesTab > .row #quotesContent ul,#books ul, #chartsContent ul { height: 43%; }
#books ul li .gridster-box .theme-widget .widget-body #price_ticker { height: auto;}


#quotesTab > .row #quotesContent ul li .gridster-box,
#chartsContent ul li .gridster-box,
#books ul li .gridster-box { padding: 10px 0;}



#moreTab {background-color: #f5f7fa; overflow-y: auto; margin-top: 80px;}


  
.icon-bar {
    width: 180px;
    margin-top: 15px;
}

.icon-bar a {
    float: left;
    width: 38px;
    height: 38px;
    line-height: 38px;
    border-radius: 50%;
    text-align: center;
    padding: 0;
    transition: all 0.3s ease;
    color: #4a4a4a;
    font-size: 26px;
    background: #f5f7fa;
    margin-right: 20px;
}

.icon-bar #grayButton {
  font-size: 22px;
}



.icon-bar a:hover {
    background-color: #f5f7fa;
}
.icon-bar .aed-iconbar,
.icon-bar .aed-iconbar:hover {
    line-height: 32px;
}

.icon-bar .aed-iconbar img { width: 50%; }


.theme-widget { padding: 0;-webkit-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.78);
-moz-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.78);
box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.78);}
.theme-widget .widget-header,.theme-widget .widget-body { padding: 8px 15px;}
.theme-widget.open .widget-body { height: 250px; overflow-y: auto;}
.theme-widget .widget-header{-webkit-border-top-left-radius: 5px; cursor: all-scroll;
-webkit-border-top-right-radius: 5px;
-moz-border-radius-topleft: 5px;
-moz-border-radius-topright: 5px;
border-top-left-radius: 5px;
border-top-right-radius: 5px;}

table.theme-tbl thead th,
table.theme-tbl thead td { padding: 10px 10px;}


.widget-header { background: #f4f4f4;}

.overFlowH{ overflow: hidden;}
.heightH{ height: 100%}

#balance-show {display:none;position:absolute; width: 250px;top:40px}
.order-form input { border: 1px solid #000;}
.order-form label { text-transform: uppercase;}
.order-form .form-sm .form-control.cust_fld { border: 1px solid #000; background: none;}
.order-form .form-sm button, .order-form .form-sm input, .order-form .form-sm select { border-radius: 0;}
.d-flex-select>div { width: 100%; flex-shrink: 0; padding-right: 6px;}
.order-form .modal-header {-webkit-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.25);-moz-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.25);
box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.25); }
.modal.in .modal-dialog { max-width: 400px;}
.order-form .modal-header .close { top: 15px;}
.order-form .abbr-wrap { margin-bottom: 20px; }
.order-form .abbr-wrap .smbl { padding-left: 10px; font-size: 18px; font-weight: 700;}
.order-form .abbr-wrap .smbl-cls{ color: #24a473;}
.order-form .modal-title { text-transform: uppercase; font-size: 15px;}
.close { font-size: 25px;}
        /*.ltcdisabled
        {
            pointer-events: none;

            !* for "disabled" effect *!
            opacity: 0.5;
            background: #CCC;
        }*/

.table-scroll .mCSB_container_wrapper { margin-right: 10px;}
.table-scroll .mCSB_container_wrapper>.mCSB_container { padding-right: 0;}

  .gray { color: #96969b }
  .gray, .gray #Home, .gray #chartsTab, .gray #quotesTab { background: #111d29;}



  .gray .theme-widget { background: #1a2f42; color: #96969b}
  .gray .headerTopSection{ background: #142535;box-shadow: 0 1px 1px #000;}
  .gray .theme-widget .widget-header { background: #142535; color: #f1f1f1 ;  }
  .gray .navbar-avatar-dd>.dropdown-toggle { border-left: 1px solid #000}
  .gray .theme-widget.open .widget-body { background: #1a2f42}
  .gray .icon-bar a { background: #1f364c; color: #d8d8d8;}
  .gray .icon-bar a:hover { background: #2b4b6a; color: #fff;}
  
  .gridster li .remove { position: absolute; top: 10px; right: 15px;}
  .gray .example .brick.small,.gray .example .brick.large{ background: #1f364c;}
  .gray .theme-dt th { background: #152738;color: #96969b; border-bottom: 0;}
  .gray .theme-dt.bg-success { background: #1f364c; color: #dee0e2}
  .gray .ticker-item {color: #dee0e2}
  .gray .ticker-wrapper-body, .gray .ticker-container { background: none; border: 0;}
  .gray .bottomtabbar{ background: #142535}
  .gray .tablink { background: #20303f; color: #484d4b}
  .gray tr:hover {
    background-color: #031425;
   }
   .table-striped>tbody>tr:nth-child(odd)>td,
   .table-striped>tbody>tr:nth-child(odd)>th {
      background-color: #f4f4f4;
    }
   
    .table-striped>tbody>tr:hover,
    .table-striped>tbody>tr:hover:nth-child(odd)>td {
      background-color: #d9d9d9;
    }
    .gray .table-striped>tbody>tr:nth-child(odd)>td,
    .gray .table-striped>tbody>tr:nth-child(odd)>th {
      background-color: #1f364c;
    } 
    .gray .table-striped>tbody>tr:hover,
    .gray .table-striped>tbody>tr:hover:nth-child(odd)>td {
      background-color: #031425;
     }
     .gray table.theme-tbl tbody td { color: #fff}

    
    </style>



</head>


<body class="dashboard overFlowH" >
<?php
//BTC
$full_btc_usd  = json_decode($full_btc_usd = App\Models\Liveprice::where('key','full_btc_usd')->first()->value,true);
$full_eth_usd  = json_decode($full_eth_usd = App\Models\Liveprice::where('key','full_eth_usd')->first()->value,true);
$full_ltc_usd  = json_decode($full_ltc_usd = App\Models\Liveprice::where('key','full_ltc_usd')->first()->value,true);
$full_xrp_usd  = json_decode($full_xrp_usd = App\Models\Liveprice::where('key','full_xrp_usd')->first()->value,true);


$btc_aed_last = $full_btc_usd['last'] * $aed_rate;
$btc_aed_high = $full_btc_usd['high'] * $aed_rate;
$btc_aed_low =  $full_btc_usd['low']* $aed_rate;
$btc_aed_open = $full_btc_usd['open'] * $aed_rate;
$btc_aed_bid = $full_btc_usd['bid'] * $aed_rate;
$btc_aed_ask = $full_btc_usd['ask'] * $aed_rate;
$btc_aed_24hr = ($btc_aed_last - $btc_aed_open)/$btc_aed_open * 100;
$btc_aed_change = ($btc_aed_last - $btc_aed_open);


$eth_aed_last = $full_eth_usd['last'] * $aed_rate;
$eth_aed_high = $full_eth_usd['high'] * $aed_rate;
$eth_aed_low =  $full_eth_usd['low']* $aed_rate;
$eth_aed_open = $full_eth_usd['open'] * $aed_rate;
$eth_aed_bid = $full_eth_usd['bid'] * $aed_rate;
$eth_aed_ask = $full_eth_usd['ask'] * $aed_rate;
$eth_aed_24hr = ($eth_aed_last - $eth_aed_open)/$eth_aed_open * 100;
$eth_aed_change = ($eth_aed_last - $eth_aed_open);

$ltc_aed_last = $full_ltc_usd['last'] * $aed_rate;
$ltc_aed_high = $full_ltc_usd['high'] * $aed_rate;
$ltc_aed_low =  $full_ltc_usd['low']* $aed_rate;
$ltc_aed_open = $full_ltc_usd['open'] * $aed_rate;
$ltc_aed_bid = $full_ltc_usd['bid'] * $aed_rate;
$ltc_aed_ask = $full_ltc_usd['ask'] * $aed_rate;
$ltc_aed_24hr = ($ltc_aed_last - $ltc_aed_open)/$ltc_aed_open * 100;
$ltc_aed_change = ($ltc_aed_last - $ltc_aed_open);

$xrp_aed_last = $full_xrp_usd['last'] * $aed_rate;
$xrp_aed_high = $full_xrp_usd['high'] * $aed_rate;
$xrp_aed_low =  $full_xrp_usd['low']* $aed_rate;
$xrp_aed_open = $full_xrp_usd['open'] * $aed_rate;
$xrp_aed_bid = $full_xrp_usd['bid'] * $aed_rate;
$xrp_aed_ask = $full_xrp_usd['ask'] * $aed_rate;
$xrp_aed_24hr = ($xrp_aed_last - $xrp_aed_open)/$xrp_aed_open * 100;
$xrp_aed_change = ($xrp_aed_last - $xrp_aed_open);

$full_array = json_decode($full_array = App\Models\Liveprice::where('key','full_array')->first()->value,true);


$eth_last_btc = $full_array['RAW']['BTC']['ETH']['PRICE'];
$eth_vol_btc =  $full_array['RAW']['BTC']['ETH']['VOLUME24HOUR'];
$eth_24hr_btc = $full_array['RAW']['BTC']['ETH']['CHANGE24HOUR'];
$eth_low_btc =  $full_array['RAW']['BTC']['ETH']['LOW24HOUR'];
$eth_high_btc = $full_array['RAW']['BTC']['ETH']['HIGH24HOUR'];

$ltc_last_btc = $full_array['RAW']['BTC']['LTC']['PRICE'];
$ltc_vol_btc =  $full_array['RAW']['BTC']['LTC']['VOLUME24HOUR'];
$ltc_24hr_btc = $full_array['RAW']['BTC']['LTC']['CHANGE24HOUR'];
$ltc_low_btc =  $full_array['RAW']['BTC']['LTC']['LOW24HOUR'];
$ltc_high_btc = $full_array['RAW']['BTC']['LTC']['HIGH24HOUR'];

$xrp_last_btc = $full_array['RAW']['BTC']['XRP']['PRICE'];
$xrp_vol_btc =  $full_array['RAW']['BTC']['XRP']['VOLUME24HOUR'];
$xrp_24hr_btc = $full_array['RAW']['BTC']['XRP']['CHANGE24HOUR'];
$xrp_low_btc =  $full_array['RAW']['BTC']['XRP']['LOW24HOUR'];
$xrp_high_btc = $full_array['RAW']['BTC']['XRP']['HIGH24HOUR'];

$aud_last_btc = $full_array['RAW']['BTC']['AED']['PRICE'];
$aud_vol_btc =  $full_array['RAW']['BTC']['AED']['VOLUME24HOUR'];
$aud_24hr_btc = $full_array['RAW']['BTC']['AED']['CHANGE24HOUR'];
$aud_low_btc =  $full_array['RAW']['BTC']['AED']['LOW24HOUR'];
$aud_high_btc = $full_array['RAW']['BTC']['AED']['HIGH24HOUR'];

//  echo $xrp_last_btc.'||||'.$xrp_vol_btc.'||||'.$xrp_24hr_btc.'||||'.$xrp_high_btc;


//LTC
$full_array_ltc = json_decode($full_array_ltc = App\Models\Liveprice::where('key','full_array_ltc')->first()->value,true);

$eth_last_ltc = $full_array_ltc['RAW']['LTC']['ETH']['PRICE'];
$eth_vol_ltc  = $full_array_ltc['RAW']['LTC']['ETH']['VOLUME24HOUR'];
$eth_24hr_ltc = $full_array_ltc['RAW']['LTC']['ETH']['CHANGE24HOUR'];
$eth_low_ltc  = $full_array_ltc['RAW']['LTC']['ETH']['LOW24HOUR'];
$eth_high_ltc = $full_array_ltc['RAW']['LTC']['ETH']['HIGH24HOUR'];

$btc_last_ltc = $full_array_ltc['RAW']['LTC']['BTC']['PRICE'];
$btc_vol_ltc  = $full_array_ltc['RAW']['LTC']['BTC']['VOLUME24HOUR'];
$btc_24hr_ltc = $full_array_ltc['RAW']['LTC']['BTC']['CHANGE24HOUR'];
$btc_low_ltc  = $full_array_ltc['RAW']['LTC']['BTC']['LOW24HOUR'];
$btc_high_ltc = $full_array_ltc['RAW']['LTC']['BTC']['HIGH24HOUR'];

$xrp_last_ltc = $full_array_ltc['RAW']['LTC']['XRP']['PRICE'];
$xrp_vol_ltc  = $full_array_ltc['RAW']['LTC']['XRP']['VOLUME24HOUR'];
$xrp_24hr_ltc = $full_array_ltc['RAW']['LTC']['XRP']['CHANGE24HOUR'];
$xrp_low_ltc  = $full_array_ltc['RAW']['LTC']['XRP']['LOW24HOUR'];
$xrp_high_ltc = $full_array_ltc['RAW']['LTC']['XRP']['HIGH24HOUR'];

$aud_last_ltc = $full_array_ltc['RAW']['LTC']['AED']['PRICE'];
$aud_vol_ltc  = $full_array_ltc['RAW']['LTC']['AED']['VOLUME24HOUR'];
$aud_24hr_ltc = $full_array_ltc['RAW']['LTC']['AED']['CHANGE24HOUR'];
$aud_low_ltc  = $full_array_ltc['RAW']['LTC']['AED']['LOW24HOUR'];
$aud_high_ltc = $full_array_ltc['RAW']['LTC']['AED']['HIGH24HOUR'];




$full_array_xrp = json_decode($full_array_xrp = App\Models\Liveprice::where('key', 'full_array_xrp')->first()->value, true);

$eth_last_xrp = $full_array_xrp['RAW']['XRP']['ETH']['PRICE'];
$eth_vol_xrp  = $full_array_xrp['RAW']['XRP']['ETH']['VOLUME24HOUR'];
$eth_24hr_xrp = $full_array_xrp['RAW']['XRP']['ETH']['CHANGE24HOUR'];
$eth_low_xrp  = $full_array_xrp['RAW']['XRP']['ETH']['LOW24HOUR'];
$eth_high_xrp = $full_array_xrp['RAW']['XRP']['ETH']['HIGH24HOUR'];

$btc_last_xrp = $full_array_xrp['RAW']['XRP']['BTC']['PRICE'];
$btc_vol_xrp  = $full_array_xrp['RAW']['XRP']['BTC']['VOLUME24HOUR'];
$btc_24hr_xrp = $full_array_xrp['RAW']['XRP']['BTC']['CHANGE24HOUR'];
$btc_low_xrp  = $full_array_xrp['RAW']['XRP']['BTC']['LOW24HOUR'];
$btc_high_xrp = $full_array_xrp['RAW']['XRP']['BTC']['HIGH24HOUR'];

$ltc_last_xrp = $full_array_xrp['RAW']['XRP']['LTC']['PRICE'];
$ltc_vol_xrp  = $full_array_xrp['RAW']['XRP']['LTC']['VOLUME24HOUR'];
$ltc_24hr_xrp = $full_array_xrp['RAW']['XRP']['LTC']['CHANGE24HOUR'];
$ltc_low_xrp  = $full_array_xrp['RAW']['XRP']['LTC']['LOW24HOUR'];
$ltc_high_xrp = $full_array_xrp['RAW']['XRP']['LTC']['HIGH24HOUR'];

$aud_last_xrp = $full_array_xrp['RAW']['XRP']['AED']['PRICE'];
$aud_vol_xrp  = $full_array_xrp['RAW']['XRP']['AED']['VOLUME24HOUR'];
$aud_24hr_xrp = $full_array_xrp['RAW']['XRP']['AED']['CHANGE24HOUR'];
$aud_low_xrp  = $full_array_xrp['RAW']['XRP']['AED']['LOW24HOUR'];
$aud_high_xrp = $full_array_xrp['RAW']['XRP']['AED']['HIGH24HOUR'];

$full_array_eth = json_decode($full_array_eth = App\Models\Liveprice::where('key', 'full_array_eth')->first()->value, true);

$xrp_last_eth = $full_array_eth['RAW']['ETH']['XRP']['PRICE'];
$xrp_vol_eth  = $full_array_eth['RAW']['ETH']['XRP']['VOLUME24HOUR'];
$xrp_24hr_eth = $full_array_eth['RAW']['ETH']['XRP']['CHANGE24HOUR'];
$xrp_low_eth  = $full_array_eth['RAW']['ETH']['XRP']['LOW24HOUR'];
$xrp_high_eth = $full_array_eth['RAW']['ETH']['XRP']['HIGH24HOUR'];

$btc_last_eth = $full_array_eth['RAW']['ETH']['BTC']['PRICE'];
$btc_vol_eth  = $full_array_eth['RAW']['ETH']['BTC']['VOLUME24HOUR'];
$btc_24hr_eth = $full_array_eth['RAW']['ETH']['BTC']['CHANGE24HOUR'];
$btc_low_eth  = $full_array_eth['RAW']['ETH']['BTC']['LOW24HOUR'];
$btc_high_eth = $full_array_eth['RAW']['ETH']['BTC']['HIGH24HOUR'];

$ltc_last_eth = $full_array_eth['RAW']['ETH']['LTC']['PRICE'];
$ltc_vol_eth  = $full_array_eth['RAW']['ETH']['LTC']['VOLUME24HOUR'];
$ltc_24hr_eth = $full_array_eth['RAW']['ETH']['LTC']['CHANGE24HOUR'];
$ltc_low_eth  = $full_array_eth['RAW']['ETH']['LTC']['LOW24HOUR'];
$ltc_high_eth = $full_array_eth['RAW']['ETH']['LTC']['HIGH24HOUR'];

$aud_last_eth = $full_array_eth['RAW']['ETH']['AED']['PRICE'];
$aud_vol_eth  = $full_array_eth['RAW']['ETH']['AED']['VOLUME24HOUR'];
$aud_24hr_eth = $full_array_eth['RAW']['ETH']['AED']['CHANGE24HOUR'];
$aud_low_eth  = $full_array_eth['RAW']['ETH']['AED']['LOW24HOUR'];
$aud_high_eth = $full_array_eth['RAW']['ETH']['AED']['HIGH24HOUR'];

//Get AED basic price
$basic_price = json_decode($basic_price = App\Models\Liveprice::where('key', 'basic_price')->first()->value, true);

$basic = $basic_price['RAW']['USD']['AED']['PRICE'];
//   $volume = $basic_price->RAW->USD->AED->VOLUME24HOUR;
$chnage_24 = $basic_price['RAW']['USD']['AED']['CHANGE24HOUR'];
$low =       $basic_price['RAW']['USD']['AED']['LOW24HOUR'];
$high =      $basic_price['RAW']['USD']['AED']['HIGH24HOUR'];

$price_aed = 1 / $basic;
//  $volume_aed = 1/$volume;
$chnage_aed = 1 / $chnage_24;
$low_aed = 1 / $low;
$high_aed = 1 / $high;

$btc_last_aed = ($basic_price['RAW']['USD']['BTC']['PRICE']) / $price_aed;
$btc_24hr_aed = ($basic_price['RAW']['USD']['BTC']['CHANGE24HOUR']) / $chnage_aed;
$btc_low_aed  = ($basic_price['RAW']['USD']['BTC']['LOW24HOUR']) / $low_aed;
$btc_high_aed = ($basic_price['RAW']['USD']['BTC']['HIGH24HOUR']) / $high_aed;

$ltc_last_aed = ($basic_price['RAW']['USD']['LTC']['PRICE']) / $price_aed;
$ltc_24hr_aed = ($basic_price['RAW']['USD']['LTC']['CHANGE24HOUR']) / $chnage_aed;
$ltc_low_aed  = ($basic_price['RAW']['USD']['LTC']['LOW24HOUR']) / $low_aed;
$ltc_high_aed = ($basic_price['RAW']['USD']['LTC']['HIGH24HOUR']) / $high_aed;

$xrp_last_aed = ($basic_price['RAW']['USD']['XRP']['PRICE']) / $price_aed;
$xrp_24hr_aed = ($basic_price['RAW']['USD']['XRP']['CHANGE24HOUR']) / $chnage_aed;
$xrp_low_aed  = ($basic_price['RAW']['USD']['XRP']['LOW24HOUR']) / $low_aed;
$xrp_high_aed = ($basic_price['RAW']['USD']['XRP']['HIGH24HOUR']) / $high_aed;

$eth_last_aed = ($basic_price['RAW']['USD']['ETH']['PRICE']) / $price_aed;
$eth_24hr_aed = ($basic_price['RAW']['USD']['ETH']['CHANGE24HOUR']) / $chnage_aed;
$eth_low_aed  = ($basic_price['RAW']['USD']['ETH']['LOW24HOUR']) / $low_aed;
$eth_high_aed = ($basic_price['RAW']['USD']['ETH']['HIGH24HOUR']) / $high_aed;

?>

<?php

$eth_balance = Sentinel::getuser()->ethereum_balance;
$btc_balance = Sentinel::getuser()->bitcoin_balance;
$aed_balance = Sentinel::getuser()->aed_balance;
$xrp_balance = Sentinel::getuser()->ripple_balance;
$ltc_balance = Sentinel::getuser()->litecoin_balance;

?>
<!-- Modal 
<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal">Full Book</button>-->
<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content order-form">
      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Order Form</h4>
        <div class="abbr-wrap row">
          <div class="smbl text-left col-xs-12">MTM</div>
          <div class="smbl text-left col-xs-12 smbl-cls">228.65</div>
        </div>         
      </div>
      <div class="modal-body">
        
       
        <form class="form-sm">
            <div class="row d-flex d-flex-select" style="margin-bottom: 8px;">
                <div>
                    <select id="order_type" name="order_type" class="form-control cust_fld">

                        <option value="limit">Limits</option>
                        <option value="market">Market</option>

                    </select>
                </div>
                <div>
                </div>
            </div>

            <input type="hidden" name="coin_use" id="coin_use" value="BTC">

            <div class="row d-flex">
                <div>
                    <div class="input-group">
                        <label> Amount AED</label> <span id="approx_usd" style="float: right;"> ≈  </span>
                        <input onkeypress="return isNumber(event)" v-model="counter" type="text" class="form-control cust_fld" onkeyup="calculate_price()" name="usd_amount" id="usd_amount" aria-describedby="sizing-addon1">
                    </div>
                </div>
                <div>
                    <div class="input-group">
                        <label id="coinname"> AMOUNT BTC</label>
                        <input  onkeypress="return isNumber(event)"  v-model="counter1" type="text" name="coin_amount" id="coin_amount"  onkeyup="calculate_price()" class="form-control cust_fld"  aria-describedby="sizing-addon1">
                    </div>
                </div>
            </div>
            <div class="row d-flex">
                <div>
                    <div class="input-group">
                        <label> Fees On <b>AED</b> </label>
                        <input  v-model="feebuy" type="text" class="form-control cust_fld" readonly id="buy_fees" aria-describedby="sizing-addon1">
                    </div>
                </div>
                <div>
                    <div class="input-group">
                        <label> Fees On <b>AED</b></label>
                        <input v-model="feesell" type="text"  id="sell_fees" readonly class="form-control cust_fld"  aria-describedby="sizing-addon1">
                    </div>
                </div>
            </div>
            <input type="hidden" id="buy_fees_per" name="" value="<?php echo e($buy_fees); ?>">
            <input type="hidden" id="sell_fees_per" name="" value="<?php echo e($sell_fees); ?>">
            <input type="hidden" id="final_aed" name="" value="0">

            <div class="row d-flex">
                <div>
                    <label style="color: green;">Pay :<span  id="pay_div"></span> </label><br>
                    <label style="color: green;">Buy Fees : <b ><?php echo e($buy_fees); ?>%</b></label>
                    <button id="buy_button"  onclick="buy_order()"    type="button"  class="btn btn-success btn-sm btn-block">Exchange Buy</button>
                </div>
                <div>
                    <label style="color: green;">Receive :<span  id="rec_div"></span></label><br>
                    <label style="color: green;">Sell Fees: <b><?php echo e($sell_fees); ?>%</b></span></label>
                    <button id="sell_button" onclick="sell_order()"   type="button"  class="btn btn-danger btn-sm btn-block">Exchange Sell</button>
                </div>
            </div>
        </form>        
        
      </div>

    </div>
  </div>
</div>
                      
<div class="container-fluid overFlowH heightH color-switch" id="app">
    <div class="row heightH">
        <div class="col-sm-12 heightH">
            <header class="headerSection">
                <div class="headerTopSection">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="logo" style="position:absolute; left:0; top: 0">
                                <a href="<?php echo e(url('dashboard-exchange')); ?>"><img class="logo-white" src="<?php echo e(asset('assets/themes/limitless/images/bitex-logo.png')); ?>" width="130px" height="45px"></a>
                            </div>
                            <div class="col-xs-6 col-sm-8" style="padding-left: 150px;">
                                
                                <div class="ticker-wrapper">
                                    <div class="ticker-wrapper-header">
                                      <div class="ticker-wrapper-body" style="height: 100%">
                                        <div class="ticker-container">
                                          <div style="display: table-row;">

                                            <a class="ticker-item" href="<?php echo e(url('dashboard-exchange',"BTC")); ?>">
                                              <div class="ticker-item-head">
                                                <span class="ticker-item-title">
                                                  BITCOIN
                                                </span>
                                                <span class="ticker-item-last">
                                                  <?php  echo number_format($btc_aed_last,2); ?>
                                                </span>
                                              </div>
                                              <div class="ticker-item-body ticker-item-body-down">
                                              <span class="ticker-item-change-direction">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 8"><path fill="none" stroke="currentcolor" stroke-linecap="round" stroke-width="2" d="m1 6 5-4 5 4"></path></svg>
                                              </span>
                                              <span style="user-select:text"><?php  echo number_format($btc_aed_24hr,2); ?>%</span>
                                              <span class="ticker-item-change"><?php  echo number_format($btc_aed_change,2); ?></span>
                                              </div>
                                            </a>


                                            <a class="ticker-item" href="<?php echo e(url('dashboard-exchange',"ETH")); ?>">
                                              <div class="ticker-item-head">
                                                <span class="ticker-item-title">
                                                  ETHEREUM
                                                </span>
                                                <span class="ticker-item-last">
                                                  <?php  echo number_format($eth_aed_last,2); ?>
                                                </span>
                                              </div>
                                              <div class="ticker-item-body ticker-item-body-up">
                                              <span class="ticker-item-change-direction">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 8"><path fill="none" stroke="currentcolor" stroke-linecap="round" stroke-width="2" d="m1 6 5-4 5 4"></path></svg>
                                              </span>
                                              <span style="user-select:text"><?php  echo number_format($eth_aed_24hr,2); ?>%</span>
                                              <span class="ticker-item-change"><?php  echo number_format($eth_aed_change,2); ?></span>
                                              </div>
                                            </a>


                                            <a class="ticker-item" href="<?php echo e(url('dashboard-exchange',"LTC")); ?>">
                                              <div class="ticker-item-head">
                                                <span class="ticker-item-title">
                                                  LITECOIN
                                                </span>
                                                <span class="ticker-item-last">
                                                  <?php  echo number_format($ltc_aed_last,2); ?>
                                                </span>
                                              </div>
                                              <div class="ticker-item-body ticker-item-body-down">
                                              <span class="ticker-item-change-direction">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 8"><path fill="none" stroke="currentcolor" stroke-linecap="round" stroke-width="2" d="m1 6 5-4 5 4"></path></svg>
                                              </span>
                                              <span style="user-select:text"><?php  echo number_format($ltc_aed_24hr,2); ?>%</span>
                                              <span class="ticker-item-change"><?php  echo number_format($ltc_aed_change,2); ?></span>
                                              </div>
                                            </a>


                                            <a class="ticker-item" href="<?php echo e(url('dashboard-exchange',"XRP")); ?>">
                                              <div class="ticker-item-head">
                                                <span class="ticker-item-title">
                                                  RIPPLE
                                                </span>
                                                <span class="ticker-item-last">
                                                  <?php  echo number_format($xrp_aed_last,2); ?>
                                                </span>
                                              </div>
                                              <div class="ticker-item-body ticker-item-body-up">
                                              <span class="ticker-item-change-direction">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 8"><path fill="none" stroke="currentcolor" stroke-linecap="round" stroke-width="2" d="m1 6 5-4 5 4"></path></svg>
                                              </span>
                                              <span style="user-select:text"><?php  echo number_format($xrp_aed_24hr,2); ?>%</span>
                                              <span class="ticker-item-change"><?php  echo number_format($xrp_aed_change,2); ?></span>
                                              </div>
                                            </a>

                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                

                            </div>

                            <div class="col-xs-6 col-sm-4">

                                <div class="headerTopRightCol pull-right">
                                    <div class="navbar-avatar-dd dropdown">
                                        <span class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                          <span class="userName"><?php echo e(Sentinel::getUser()->first_name); ?> <?php echo e(Sentinel::getUser()->last_name); ?></span>
                                          <span class="avtarImg">
                                            <?php if(Sentinel::getUser()->profile_pic==''): ?>
                                                  <img data-name="<?php echo e(Sentinel::getUser()->first_name); ?>" class="profile_image" style="width: 38px; height: 38px;">
                                              <?php else: ?>
                                                  <img src="<?php echo e(asset('assets/profile/'.Sentinel::getUser()->profile_pic)); ?>" alt="..." class="img-preview" style="width: 38px; height: 38px;">
                                              <?php endif; ?>
                                          </span>
                                          <span class="rightTglIcon">
                                            <span class="tglLine"></span>
                                            <span class="tglLine"></span>
                                            <span class="tglLine"></span>
                                          </span>
                                        </span>
                                        <ul class="dropdown-menu dropdown-menu-right ddStle" aria-labelledby="dropdownMenu1">
                                            <li><a href="<?php echo e(url('user/profile')); ?>"><span class="ddIcon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="16px">
                        <path fill-rule="evenodd"  fill="rgb(206, 206, 206)" d="M11.999,13.902 C12.000,13.652 12.001,13.866 11.999,13.902 ZM11.999,14.194 C11.999,14.194 11.115,16.000 6.000,16.000 C0.884,16.000 0.001,14.194 0.001,14.194 C0.001,13.984 0.000,13.864 0.000,13.799 C0.001,13.834 0.004,13.818 0.009,13.519 C0.077,9.853 0.591,8.797 4.260,8.125 C4.260,8.125 4.783,8.800 6.000,8.800 C7.217,8.800 7.739,8.125 7.739,8.125 C11.449,8.804 11.933,9.877 11.992,13.641 C11.996,13.882 11.998,13.923 11.999,13.902 C11.999,13.966 11.999,14.060 11.999,14.194 ZM6.000,7.825 C4.245,7.825 2.823,6.073 2.823,3.913 C2.823,1.752 3.290,-0.000 6.000,-0.000 C8.709,-0.000 9.176,1.752 9.176,3.913 C9.176,6.073 7.754,7.825 6.000,7.825 Z"/>
                      </svg></span> <span class="ddText"><?php echo e(trans_choice('general.profile',1)); ?></span></a></li>
                                            <li><a href="javascript:void(0)"><span class="ddIcon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="15px" height="16px">
                        <path fill-rule="evenodd"  fill="rgb(206, 206, 206)" d="M12.546,8.637 L12.546,12.736 C12.546,13.116 12.243,13.438 11.867,13.424 C11.492,13.424 11.189,13.116 11.189,12.736 L11.189,8.637 C11.189,8.256 11.492,7.949 11.867,7.949 C12.243,7.949 12.546,8.256 12.546,8.637 ZM14.899,10.950 C14.884,10.964 14.870,10.979 14.856,10.994 C14.798,11.521 14.552,12.004 14.191,12.370 C14.177,12.384 14.148,12.413 14.134,12.428 L14.134,12.487 L14.134,14.199 C14.134,14.434 14.119,14.639 14.061,14.814 C14.004,14.990 13.932,15.136 13.816,15.268 C13.701,15.400 13.556,15.502 13.397,15.561 C13.282,15.605 13.152,15.634 13.008,15.634 L12.863,15.634 L8.965,15.634 C8.850,15.854 8.633,16.000 8.373,16.000 L6.670,16.000 C6.294,16.000 5.991,15.693 5.991,15.312 C5.991,14.931 6.294,14.624 6.670,14.624 L8.388,14.624 C8.633,14.624 8.864,14.770 8.980,14.990 L12.878,14.990 L12.892,14.990 C12.993,15.005 13.094,14.990 13.166,14.961 C13.239,14.931 13.296,14.887 13.340,14.844 C13.397,14.785 13.426,14.712 13.455,14.609 C13.484,14.492 13.498,14.360 13.498,14.199 L13.498,12.867 C13.397,12.911 13.282,12.955 13.181,12.984 C13.195,12.897 13.210,12.823 13.210,12.736 L13.210,8.637 C13.210,8.549 13.195,8.461 13.181,8.388 C13.571,8.505 13.932,8.725 14.206,9.003 C14.235,9.032 14.264,9.061 14.292,9.091 L14.292,7.861 C14.292,7.451 14.264,7.056 14.191,6.661 C14.119,6.265 14.018,5.885 13.888,5.504 C13.744,5.138 13.585,4.772 13.383,4.421 C13.181,4.069 12.964,3.747 12.704,3.440 C12.676,3.411 12.661,3.381 12.647,3.352 C12.473,3.381 12.286,3.323 12.141,3.177 C10.871,1.888 9.196,1.230 7.522,1.230 C5.847,1.230 4.172,1.888 2.902,3.177 C2.757,3.323 2.570,3.381 2.368,3.352 C2.353,3.381 2.324,3.411 2.310,3.440 C2.050,3.747 1.833,4.069 1.631,4.421 C1.429,4.758 1.256,5.123 1.126,5.504 C0.996,5.870 0.895,6.265 0.823,6.661 C0.751,7.056 0.722,7.466 0.722,7.861 L0.722,9.076 C0.751,9.047 0.780,9.017 0.808,8.988 C1.097,8.695 1.444,8.490 1.833,8.373 C1.819,8.461 1.805,8.534 1.805,8.622 L1.805,12.721 C1.805,12.809 1.819,12.897 1.833,12.970 C1.444,12.853 1.083,12.633 0.808,12.355 C0.462,11.989 0.217,11.521 0.144,10.979 C0.130,10.964 0.115,10.950 0.101,10.935 C0.043,10.862 -0.000,10.774 -0.000,10.672 L-0.000,7.846 C-0.000,7.407 0.043,6.968 0.115,6.529 C0.202,6.090 0.318,5.665 0.462,5.241 C0.621,4.831 0.808,4.421 1.025,4.040 C1.242,3.660 1.487,3.294 1.776,2.957 C1.805,2.913 1.848,2.884 1.877,2.869 C1.833,2.664 1.877,2.459 2.036,2.298 C3.551,0.761 5.544,-0.000 7.522,-0.000 C9.499,-0.000 11.492,0.776 13.008,2.313 C13.152,2.474 13.210,2.679 13.166,2.884 C13.210,2.913 13.239,2.942 13.267,2.972 C13.542,3.308 13.802,3.674 14.018,4.055 C14.249,4.435 14.437,4.831 14.581,5.255 C14.726,5.665 14.841,6.104 14.884,6.543 C14.957,6.968 15.000,7.422 15.000,7.861 L15.000,10.686 C15.000,10.789 14.971,10.876 14.899,10.950 ZM3.176,7.963 C3.551,7.963 3.855,8.271 3.855,8.651 L3.855,12.750 C3.855,13.131 3.551,13.438 3.176,13.438 C2.801,13.438 2.498,13.116 2.498,12.750 L2.498,8.651 C2.498,8.271 2.801,7.963 3.176,7.963 Z"/>
                      </svg></span> <span class="ddText">Contact Support</span></a></li>
                                            <li><a href="<?php echo e(url('logout')); ?>"><span class="ddIcon"><svg  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="22px" height="16px">
                        <path fill-rule="evenodd"  fill="rgb(206, 206, 206)" d="M13.820,16.000 C11.098,16.000 8.563,14.682 7.037,12.473 C6.728,12.026 6.849,11.418 7.306,11.116 C7.764,10.813 8.385,10.931 8.695,11.379 C9.848,13.047 11.764,14.044 13.820,14.044 C17.228,14.044 20.000,11.333 20.000,8.000 C20.000,4.667 17.228,1.956 13.820,1.956 C11.758,1.956 9.838,2.957 8.686,4.633 C8.378,5.082 7.757,5.201 7.299,4.900 C6.840,4.598 6.718,3.991 7.026,3.542 C8.551,1.324 11.090,-0.000 13.820,-0.000 C18.330,-0.000 22.000,3.589 22.000,8.000 C22.000,12.411 18.330,16.000 13.820,16.000 ZM3.414,7.022 L15.000,7.022 C15.552,7.022 16.000,7.460 16.000,8.000 C16.000,8.540 15.552,8.978 15.000,8.978 L3.414,8.978 L4.707,10.242 C5.097,10.624 5.097,11.243 4.707,11.625 C4.512,11.816 4.256,11.912 4.000,11.912 C3.744,11.912 3.488,11.816 3.293,11.625 L0.293,8.692 C0.270,8.669 0.248,8.645 0.227,8.621 C0.222,8.615 0.218,8.608 0.213,8.602 C0.198,8.583 0.183,8.564 0.169,8.544 C0.165,8.538 0.162,8.532 0.158,8.526 C0.144,8.505 0.131,8.484 0.118,8.461 C0.116,8.457 0.114,8.453 0.112,8.448 C0.099,8.424 0.087,8.400 0.076,8.375 C0.075,8.372 0.074,8.369 0.073,8.366 C0.062,8.339 0.052,8.312 0.043,8.285 C0.042,8.282 0.042,8.278 0.041,8.275 C0.033,8.248 0.025,8.221 0.020,8.192 C0.018,8.184 0.017,8.176 0.016,8.168 C0.012,8.145 0.007,8.121 0.005,8.098 C0.002,8.065 -0.000,8.033 -0.000,8.000 C-0.000,7.967 0.002,7.935 0.005,7.902 C0.007,7.879 0.011,7.856 0.016,7.833 C0.017,7.824 0.018,7.816 0.020,7.807 C0.025,7.780 0.033,7.752 0.041,7.725 C0.042,7.722 0.042,7.718 0.043,7.715 C0.052,7.687 0.062,7.661 0.073,7.634 C0.074,7.631 0.075,7.628 0.076,7.625 C0.087,7.600 0.099,7.576 0.112,7.552 C0.114,7.547 0.116,7.543 0.118,7.538 C0.130,7.516 0.144,7.495 0.158,7.474 C0.162,7.468 0.165,7.462 0.169,7.456 C0.183,7.436 0.198,7.417 0.213,7.398 C0.218,7.392 0.222,7.385 0.227,7.379 C0.248,7.355 0.270,7.331 0.293,7.308 L3.293,4.374 C3.683,3.992 4.317,3.992 4.707,4.374 C5.098,4.756 5.098,5.376 4.707,5.758 L3.414,7.022 Z"/>
                      </svg></span> <span class="ddText"><?php echo e(trans_choice('general.logout',1)); ?></span></a></li>
                                        </ul>
                                    </div>
                                </div>


                                <div class="icon-bar pull-right">
                                      <a class="active" href="#" id="grayButton"><i class="fa fa-moon-o"></i></a> 
                            
                                      <a href="javascript" id="addWidgetButton"><i class="fa">&#43;</i></a>
                                      <div role="presentation" id="balance-check" style="position:relative;float: left">

                                        
                                            <a href="#" class="aed-iconbar">
                                                  <img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMS4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDMzNC44NzcgMzM0Ljg3NyIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMzM0Ljg3NyAzMzQuODc3OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjMycHgiIGhlaWdodD0iMzJweCI+CjxnPgoJPHBhdGggZD0iTTMzMy4xOTYsMTU1Ljk5OWgtMTYuMDY3VjgyLjA5YzAtMTcuNzE5LTE0LjQxNS0zMi4xMzQtMzIuMTM0LTMyLjEzNGgtMjEuNzYxTDI0MC45NjUsOS45MTcgICBDMjM3LjU3MSwzLjc5OCwyMzEuMTEyLDAsMjI0LjEwNywwYy0zLjI2NSwwLTYuNTA0LDAuODQyLTkuMzY0LDIuNDI5bC04NS40NjQsNDcuNTI2SDMzLjgxNSAgIGMtMTcuNzE5LDAtMzIuMTM0LDE0LjQxNS0zMi4xMzQsMzIuMTM0djIyMC42NTNjMCwxNy43MTksMTQuNDE1LDMyLjEzNCwzMi4xMzQsMzIuMTM0aDI1MS4xOCAgIGMxNy43MTksMCwzMi4xMzQtMTQuNDE1LDMyLjEzNC0zMi4xMzR2LTY0LjgwMmgxNi4wNjdWMTU1Ljk5OXogTTI4NC45OTUsNjIuODA5YzkuODk3LDAsMTcuOTgyLDcuNTE5LDE5LjA2OCwxNy4xNGgtMjQuMTUyICAgbC05LjUyNS0xNy4xNEgyODQuOTk1eiBNMjIwLjk5NiwxMy42NjNjMy4wMTQtMS42OSw3LjA3LTAuNTA4LDguNzM0LDIuNDk0bDM1LjQ3Niw2My43ODZIMTAxLjc5OEwyMjAuOTk2LDEzLjY2M3ogICAgTTMwNC4yNzUsMzAyLjc0MmMwLDEwLjYzLTguNjUxLDE5LjI4MS0xOS4yODEsMTkuMjgxSDMzLjgxNWMtMTAuNjMsMC0xOS4yODEtOC42NTEtMTkuMjgxLTE5LjI4MVY4Mi4wOSAgIGMwLTEwLjYzLDguNjUxLTE5LjI4MSwxOS4yODEtMTkuMjgxaDcyLjM1M0w3NS4zNDUsNzkuOTVIMzcuODMyYy0zLjU1NCwwLTYuNDI3LDIuODc5LTYuNDI3LDYuNDI3czIuODczLDYuNDI3LDYuNDI3LDYuNDI3aDE0LjM5NiAgIGgyMzQuODNoMTcuMjE3djYzLjIwMWgtNDYuOTk5Yy0yMS44MjYsMC0zOS41ODksMTcuNzY0LTM5LjU4OSwzOS41ODl2Mi43NjRjMCwyMS44MjYsMTcuNzY0LDM5LjU4OSwzOS41ODksMzkuNTg5aDQ2Ljk5OVYzMDIuNzQyeiAgICBNMzIwLjM0MiwyMjUuMDg3aC0zLjIxM2gtNTkuODUzYy0xNC43NDMsMC0yNi43MzYtMTEuOTkyLTI2LjczNi0yNi43MzZ2LTIuNzY0YzAtMTQuNzQzLDExLjk5Mi0yNi43MzYsMjYuNzM2LTI2LjczNmg1OS44NTMgICBoMy4yMTNWMjI1LjA4N3ogTTI3Ni45NjEsMTk3LjQ5N2MwLDcuODQxLTYuMzUsMTQuMTktMTQuMTksMTQuMTljLTcuODQxLDAtMTQuMTktNi4zNS0xNC4xOS0xNC4xOXM2LjM1LTE0LjE5LDE0LjE5LTE0LjE5ICAgQzI3MC42MTIsMTgzLjMwNiwyNzYuOTYxLDE4OS42NjIsMjc2Ljk2MSwxOTcuNDk3eiIgZmlsbD0iIzAwMDAwMCIvPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" />
                                            </a>
                                            
                                            <div class="theme-widget balance-widget open" id="balance-show">
                                                <div class="widget-header acr">
                                                    <h5> <i class="fa fa-chevron-right" aria-hidden="true"></i>Balances</h5>
                                                </div>
                                                <div class="widget-body ">
                                                    <div class="table-responsive">
                                                        <table class="theme-tbl" width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th>Exchange</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <th scope="row">  <img src="<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/btc.png" alt=""> BTC</th>
                                                                <td id="btc_bal_set"><?php  echo number_format(Sentinel::getuser()->bitcoin_balance,6); ?></td>
                                                            </tr>
                                                            </tbody>
                                                            <tbody>
                                                            <tr>
                                                                <th scope="row">  <img src="<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/eth.png" alt=""> ETH</th>
                                                                <td id="eth_bal_set" ><?php echo number_format(Sentinel::getuser()->ethereum_balance,4); ?></td>
                                                            </tr>
                                                            </tbody>
                                                            <tbody>
                                                            <tr>
                                                                <th scope="row">  <img src="<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/xrp.png" alt=""> XRP</th>
                                                                <td id="xrp_bal_set" ><?php echo number_format(Sentinel::getuser()->ripple_balance,4); ?></td>
                                                            </tr>
                                                            </tbody>
                                                            <tbody class="last">
                                                            <tr>
                                                                <th scope="row">  <img src="<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/ltc.png" alt=""> LTC</th>
                                                                <td id="ltc_bal_set" ><?php echo number_format(Sentinel::getuser()->litecoin_balance,4); ?></td>
                                                            </tr>

                                                            </tbody>
                                                            <tbody class="last">
                                                            <tr>
                                                                <th scope="row">  <img src="https://cdn3.iconfinder.com/data/icons/currency-2/460/Arab-Emirates-Dirham-512.png" alt=""> AED</th>
                                                                <td id="aed_bal_set" ><?php echo e(number_format(\App\Helpers\GeneralHelper::user_aed_available(Sentinel::getUser()->id),4)); ?></td>
                                                            </tr>

                                                            </tbody>

                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        
                                      </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="navSection" style="display: none;">
                    <nav class="navbar navbar-default">
                        <div class="container">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header navbar-toggle-left">
                                <button type="button" class="navbar-toggle navTglStyle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar top-bar"></span>
                                    <span class="icon-bar middle-bar"></span>
                                    <span class="icon-bar bottom-bar"></span>
                                </button>
                                <span class="navMenuTag">MENU</span>
                            </div>
                        <?php
                        $usd = \App\Models\TradeCurrency::where('default_currency', 1)->first();
                        $btc = \App\Models\TradeCurrency::where('network', "bitcoin")->first();
                        $dogecoin = \App\Models\TradeCurrency::where('network', "dogecoin")->first();
                        $ltc = \App\Models\TradeCurrency::where('network', "litecoin")->first();
                        $xrp = \App\Models\TradeCurrency::where('network', "ripple")->first();
                        $eth = \App\Models\TradeCurrency::where('network', "ethereum")->first();
                        ?>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav navStyle active">
                                    <?php if(Sentinel::inRole('client')): ?>
                                        <?php echo $__env->make('left_menu.client', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                    <?php else: ?>
                                        <?php echo $__env->make('left_menu.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                    <?php endif; ?>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </div><!-- /.container -->
                    </nav>
                </div>

            </header>


            <div class="loader"></div>
            <div id="Home" class="tabcontent">
              <div style="margin-top: 73px;" class="heightH">
                  <div class="row heightH">
                      <div class="col-sm-12 overFlowH heightH">
                          <div class="dashboard-header row" style="display:none;">
                              <div class="dashboard-header-sidebar col-sm-2">
                                  <div>
                                      <a href="<?php echo e(url('/dashboard')); ?>"><svg  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="136px" height="18px">
                                              <path fill-rule="evenodd"  fill="#fff"  d="M135.341,13.079 L135.341,16.000 L125.459,16.000 L125.459,1.837 L134.981,1.837 L134.981,4.738 L128.760,4.738 L128.760,7.398 L134.641,7.398 L134.641,10.139 L128.760,10.139 L128.760,13.079 L135.341,13.079 ZM115.260,16.000 L109.979,16.000 L109.979,1.837 L115.100,1.837 C119.121,1.837 123.202,3.517 123.202,8.878 C123.202,13.860 119.161,16.000 115.260,16.000 ZM115.020,4.758 L113.320,4.758 L113.320,13.039 L114.940,13.039 C117.381,13.039 119.641,12.039 119.641,8.878 C119.641,5.698 117.381,4.758 115.020,4.758 ZM103.901,13.219 L98.420,13.219 L97.380,16.000 L93.722,16.000 L93.659,16.000 L89.721,16.000 L86.661,10.379 L85.500,10.379 L85.500,16.000 L82.140,16.000 L82.140,1.837 L87.541,1.837 C90.261,1.837 92.962,2.877 92.962,6.118 C92.962,8.018 91.842,9.379 90.021,9.939 L93.685,15.939 L99.600,1.837 L102.921,1.837 L108.802,16.000 L105.002,16.000 L103.901,13.219 ZM87.301,4.598 L85.480,4.598 L85.480,7.898 L87.101,7.898 C88.201,7.898 89.581,7.618 89.581,6.178 C89.581,4.858 88.321,4.598 87.301,4.598 ZM101.201,5.618 L99.400,10.479 L102.961,10.479 L101.201,5.618 ZM76.581,16.000 L73.160,16.000 L73.160,4.758 L69.160,4.758 L69.160,2.292 L64.021,9.999 L64.021,16.000 L60.601,16.000 L60.601,9.999 L55.280,1.837 L59.420,1.837 L62.441,7.078 L65.462,1.837 L69.160,1.837 L69.463,1.837 L80.582,1.837 L80.582,4.758 L76.581,4.758 L76.581,16.000 ZM53.362,16.000 L52.262,13.219 L46.781,13.219 L45.741,16.000 L42.020,16.000 L47.961,1.837 L51.282,1.837 L57.163,16.000 L53.362,16.000 ZM49.561,5.618 L47.761,10.479 L51.322,10.479 L49.561,5.618 ZM41.503,11.879 C41.503,14.960 38.802,16.000 36.102,16.000 L30.460,16.000 L30.460,1.837 L36.102,1.837 C38.302,1.837 40.923,2.617 40.923,5.478 C40.923,7.038 39.982,8.078 38.602,8.538 L38.602,8.578 C40.222,8.858 41.503,10.019 41.503,11.879 ZM35.441,4.558 L33.761,4.558 L33.761,7.478 L35.601,7.478 C36.882,7.478 37.622,6.938 37.622,5.938 C37.622,4.978 36.882,4.558 35.441,4.558 ZM35.661,9.999 L33.761,9.999 L33.761,13.239 L35.681,13.239 C36.762,13.239 38.082,12.939 38.082,11.559 C38.082,10.379 37.122,9.999 35.661,9.999 ZM18.188,17.576 C17.911,17.875 17.595,18.000 17.288,18.000 C16.278,18.000 15.365,16.647 16.272,15.668 L16.589,15.327 C15.259,15.233 13.979,14.896 12.796,14.130 C11.704,13.423 10.824,12.468 10.048,11.436 C9.776,11.074 9.516,10.703 9.264,10.329 C8.973,9.894 8.694,9.451 8.412,9.009 C7.819,8.078 7.214,7.157 6.446,6.352 C5.198,5.044 3.605,4.604 1.874,4.604 C1.689,4.604 1.503,4.609 1.315,4.619 C1.287,4.620 1.259,4.621 1.232,4.621 C-0.431,4.621 -0.395,2.008 1.315,1.921 C1.510,1.911 1.702,1.906 1.891,1.906 C5.500,1.906 8.028,3.695 10.049,6.581 C10.137,6.707 10.225,6.832 10.311,6.962 C10.762,7.641 11.187,8.336 11.640,9.009 C12.108,9.704 12.607,10.376 13.198,10.983 C14.237,12.051 15.422,12.490 16.721,12.614 C16.639,12.515 16.559,12.414 16.476,12.316 C16.001,11.754 15.921,10.962 16.476,10.408 C16.729,10.157 17.106,10.015 17.477,10.015 C17.822,10.015 18.162,10.137 18.391,10.408 C19.124,11.276 19.824,12.169 20.557,13.037 C21.034,13.515 21.217,14.309 20.626,14.946 L18.188,17.576 ZM18.391,7.609 C17.915,8.172 16.964,8.096 16.475,7.609 C15.921,7.056 16.001,6.264 16.475,5.702 C16.559,5.603 16.639,5.502 16.721,5.404 C15.421,5.528 14.237,5.967 13.198,7.035 C12.787,7.458 12.421,7.913 12.078,8.385 C11.891,8.101 11.705,7.812 11.517,7.518 C11.319,7.208 11.114,6.886 10.904,6.571 C10.774,6.374 10.641,6.185 10.508,5.999 C11.170,5.194 11.913,4.459 12.796,3.887 C13.979,3.122 15.259,2.785 16.589,2.691 L16.272,2.349 C15.090,1.073 17.001,-0.839 18.188,0.442 L20.626,3.072 C21.217,3.709 21.034,4.503 20.557,4.981 C19.824,5.848 19.124,6.742 18.391,7.609 ZM1.315,13.399 C3.261,13.497 5.064,13.113 6.446,11.665 C7.032,11.051 7.524,10.368 7.988,9.665 C8.210,10.013 8.438,10.371 8.673,10.722 C8.960,11.149 9.273,11.592 9.611,12.032 C7.554,14.709 4.971,16.281 1.315,16.096 C-0.423,16.008 -0.432,13.310 1.315,13.399 Z"/>
                                          </svg>
                                      </a>
                                  </div>
                              </div>
                              <div class="col-sm-4">
                                  <input v-model="currency" value="BTC"  type="hidden" class="form-control">
                                  <ul class="header-left">
                                      <li>
                                          <div class="onhover-dropdown" >
                                              <a> Trading <i class="fa fa-caret-down"></i></a>
                                              <ul class="onhover-show-div" id="price_ticker">
                                                  <a class="teading_a" href="<?php echo e(url('dashboard-exchange',"BTC")); ?>"><li><img src="<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/btc.png" alt="">BTC</li></a>
                                                  <a class="teading_a" href="<?php echo e(url('dashboard-exchange',"ETH")); ?>"><li><img src="<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/eth.png" alt="">ETH</li></a>
                                                  <a class="teading_a" href="<?php echo e(url('dashboard-exchange',"LTC")); ?>"><li><img src="<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/ltc.png" alt="">LTC</li></a>
                                                  <a class="teading_a" href="<?php echo e(url('dashboard-exchange',"XRP")); ?>"><li><img src="<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/xrp.png" alt="">XRP</li></a>

                                              </ul>
                                          </div>
                                      </li>

                                      <li class="onhover-dropdown">
                                          <div class="onhover-dropdown">
                                              <a class="mr-10"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="12px" height="16px">
                                                      <path fill-rule="evenodd" fill="#fff" d="M11.999,13.902 C12.000,13.652 12.001,13.866 11.999,13.902 ZM11.999,14.194 C11.999,14.194 11.115,16.000 6.000,16.000 C0.884,16.000 0.001,14.194 0.001,14.194 C0.001,13.984 0.000,13.864 0.000,13.799 C0.001,13.834 0.004,13.818 0.009,13.519 C0.077,9.853 0.591,8.797 4.260,8.125 C4.260,8.125 4.783,8.800 6.000,8.800 C7.217,8.800 7.739,8.125 7.739,8.125 C11.449,8.804 11.933,9.877 11.992,13.641 C11.996,13.882 11.998,13.923 11.999,13.902 C11.999,13.966 11.999,14.060 11.999,14.194 ZM6.000,7.825 C4.245,7.825 2.823,6.073 2.823,3.913 C2.823,1.752 3.290,-0.000 6.000,-0.000 C8.709,-0.000 9.176,1.752 9.176,3.913 C9.176,6.073 7.754,7.825 6.000,7.825 Z"></path>
                                                  </svg> My account <i class="fa fa-caret-down"></i></a>
                                              <ul class="onhover-show-div">
                                                  <?php if(Sentinel::check()): ?>
                                                      <a href="<?php echo e(url('logout')); ?>"><li class="text-primary"><i class="fa fa-sign-out mr-10"></i>Logout</li></a>
                                                  <?php else: ?>
                                                      <a href="<?php echo e(url('login')); ?>"><li class="text-primary" ><i class="fa fa-sign-in mr-10"></i>Login</li></a>
                                                      <a href="<?php echo e(url('register')); ?>"><li class="text-primary"><i class="fa fa-plus-square-o mr-10"></i>Register</li></a>
                                                  <?php endif; ?>
                                              </ul>
                                          </div>
                                      </li>
                                  </ul>
                              </div>

                              <!-- <div class="col-sm-6 text-right">
                                   <ul class="header-right">
                                       <li>
                                           <form class="form-inline search-form">
                                               <div class="form-group">
                                                   <label class="sr-only">Email</label>
                                                   <input type="search" class="form-control-plaintext" placeholder="Search..">
                                               </div>
                                           </form>
                                       </li>
                                       <li><button type="button" class="btn btn-xs btn-info theme-btn">Tour</button></li>
                                   </ul>
                               </div>-->
                          </div>

                          <div class="row heightH">
                              
                              <div class="col-sm-12 heightH">
                                  
                                  <div id="books" class="gridster heightH">
                                      <ul class="clearfix">
                                          <li data-sizey="1" data-sizex="6" data-col="1" data-row="1" class="col-sm-12">
                                            <div class="gridster-box">
                                                <div class="theme-widget balance-widget open">
                                                  <div class="widget-header acr">
                                                      <h5> <i class="fa fa-chevron-right" aria-hidden="true"></i>SELECT CURRENCY <span id="slash_coin" style="color: #17294e">BTC/AED</span></h5>
                                                  </div>
                                                  <div class="widget-body">
                                                      <ul id="price_ticker">
                                                          <a class="teading_a" href="<?php echo e(url('dashboard-exchange',"BTC")); ?>"><li><img src="<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/btc.png" alt="" width="30px" height="30px">&nbsp;&nbsp;Bitcoin (BTC)</li></a>
                                                          <a class="teading_a" href="<?php echo e(url('dashboard-exchange',"ETH")); ?>"><li><img src="<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/eth.png" alt="" width="30px" height="30px">&nbsp;&nbsp;Ethereum (ETH)</li></a>
                                                          <a class="teading_a" href="<?php echo e(url('dashboard-exchange',"LTC")); ?>"><li><img src="<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/ltc.png" alt="" width="30px" height="30px">&nbsp;&nbsp;Litecoin (LTC)</li></a>
                                                          <a class="teading_a" href="<?php echo e(url('dashboard-exchange',"XRP")); ?>"><li><img src="<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/xrp.png" alt="" width="30px" height="30px">&nbsp;&nbsp;Ripple (XRP)</li></a>


                                                      </ul>
                                                  </div>
                                              </div>
                                            </div>                                            
                                          </li>
                                      </ul>
                                      <ul>
                                          <li data-sizey="1" data-sizex="6" data-col="1" data-row="2" class="col-sm-6">
                                            <div class="gridster-box">
                                              <div class="theme-widget open">
                                                  <div class="widget-header">
                                                      <h5 class="acr1"> <i class="fa fa-chevron-right" aria-hidden="true"></i>Order history</h5>

                                                      <span style="float: right;margin-top: -25px;">
                                                      <button onclick="order_full('bid')" class="btn btn-sm btn-default">Bid</button>
                                                      <button onclick="order_full('ask')"  class="btn btn-sm btn-default">Ask</button>
                                                          <select onchange="call_order_full(this.value)">
                                                              <!-- <option value="ALL">All</option> -->
                                                              <option <?php if('BTC' == $coinuse): ?> selected=" " <?php endif; ?> value="BTC">BTC/AED</option>
                                                              <option <?php if('LTC' == $coinuse): ?> selected=" " <?php endif; ?> value="LTC">LTC/AED</option>
                                                              <option <?php if('ETH' == $coinuse): ?> selected=" " <?php endif; ?> value="ETH">ETH/AED</option>
                                                              <option <?php if('XRP' == $coinuse): ?> selected=" " <?php endif; ?> value="XRP">XRP/AED</option>
                                                          </select>
                                                      </span>

                                                  </div>
                                                  <div class="widget-body">
                                                      <div class="table-responsive">
                                                          <table id="order-his-tbl" class="display" style="width:100%">
                                                              <thead>
                                                              <tr>
                                                                  <th>#</th>
                                                                  <th>Side</th>
                                                                  <th>order_id</th>
                                                                  <th>Pair</th>
                                                                  
                                                                  <th>Price</th>
                                                                  <th>Amount</th>
                                                                  <th>Fee</th>
                                                                  <th>TOTAL</th>
                                                                  <th>Status</th>
                                                              </tr>
                                                              </thead>
                                                              <tbody>
                                                              </tbody>
                                                          </table>
                                                      </div>
                                                  </div>
                                              </div>  
                                            </div>  
                                          </li>
                                          <li  data-sizey="1" data-sizex="6" data-col="1" data-row="3" class="col-sm-6">
                                            <div class="gridster-box">
                                              <div class="theme-widget balance-widget open">
                                                <div class="widget-header">
                                                    <h5 class="acr1"> <i class="fa fa-chevron-right" aria-hidden="true"></i> Open Orders </h5>
                                                    <span style="float: right;margin-top: -25px;">
                                                        <button onclick="order_middle('bid')" class="btn btn-sm btn-default">Bid</button>
                                                         <button onclick="order_middle('ask')"  class="btn btn-sm btn-default">Ask</button>
                                                            <select id="both_side" onchange="call_order(this.value)">
                                                                <!-- <option value="ALL">All</option> -->
                                                                <option <?php if('BTC' == $coinuse): ?> selected=" " <?php endif; ?> value="BTC">BTC/AED</option>
                                                                <option <?php if('LTC' == $coinuse): ?> selected=" " <?php endif; ?> value="LTC">LTC/AED</option>
                                                                <option <?php if('ETH' == $coinuse): ?> selected=" " <?php endif; ?> value="ETH">ETH/AED</option>
                                                                <option <?php if('XRP' == $coinuse): ?> selected=" " <?php endif; ?> value="XRP">XRP/AED</option>
                                                            </select>
                                                    </span>
                                                </div>


                                                <div class="widget-body">
                                                    <div class="table-responsive">
                                                        <table id="example12" class="display" style="width:100%">
                                                            <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Side</th>
                                                                <th>order_id</th>
                                                                <th>Pair</th>
                                                                <th>Price</th>
                                                                <th>Amount</th>
                                                                <th>Fee</th>
                                                                <th>TOTAL</th>
                                                                <th>Action</th>
                                                                <th>Status</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>
                                          </li>

                                      </ul>
                                  </div>                                        
                              </div>
                          </div> 

                        
                      
                  </div>
              </div>

              </div>
            </div>



<div id="chartsTab" class="tabcontent heightH">
  <div class="row heightH">
    <div class="col-sm-12 heightH">
  
    <div id="chartsContent" class="gridster heightH">
      <ul class="clearfix">
          <li data-sizey="1" data-sizex="3" data-col="1" data-row="1" class="col-sm-6">
              <div class="gridster-box">
                <div class="theme-widget balance-widget open">
                    <div class="widget-header">
                        <h5 class="acr1"> <i class="fa fa-chevron-right" aria-hidden="true"></i> Chart One </h5>
                       
                    </div>


                </div>
                  <div class="handle-resize"></div>
              </div>
              
          </li>
          <li data-sizey="1" data-sizex="3" data-col="2" data-row="1" class="col-sm-6">
              <div class="gridster-box">
                <div class="theme-widget balance-widget open">
                    <div class="widget-header">
                        <h5 class="acr1"> <i class="fa fa-chevron-right" aria-hidden="true"></i>  Chart Two </h5>
                       
                    </div>



                    
                </div>
              
          </li> 
        </ul>
        <ul>  
          <li data-sizey="1" data-sizex="3" data-col="4" data-row="3" class="col-sm-6">
              <div class="gridster-box">
                <div class="theme-widget balance-widget open">
                    <div class="widget-header">
                        <h5 class="acr1"> <i class="fa fa-chevron-right" aria-hidden="true"></i> Chart Three </h5>
                       
                    </div>

                </div>
              
          </li>  
          <li data-sizey="1" data-sizex="3" data-col="5" data-row="3" class="col-sm-6">
              <div class="gridster-box">
                <div class="theme-widget balance-widget open">
                    <div class="widget-header">
                        <h5 class="acr1"> <i class="fa fa-chevron-right" aria-hidden="true"></i>  Chart Four </h5>
                       
                    </div>



                </div>
              
          </li>                      
      </ul>
    </div>     
    </div>
  </div>
                                     
</div>

<div id="quotesTab" class="tabcontent">
  <div class="row">
    <div class="col-sm-12">
      <div id="quotesContent" class="gridster">
        <ul class="clearfix"> 
          <li data-sizey="1" data-sizex="3" data-col="1" data-row="1" class="col-sm-6">
              <div class="gridster-box">
              
                  <div class="theme-widget tickers-widget open">
                      <div class="widget-header acr">
                          <h5> <i class="fa fa-chevron-right" aria-hidden="true"></i>Tickers</h5>
                      </div>
                      <div class="widget-body cstmscroll">
                          <div class="table-responsive">
                              <table width="100%" class="theme-tbl table-striped">
                                  <thead>
                                  <tr>
                                      <th width="20%" class="text-left">Symbol</th>
                                      <th>Bid</th>
                                      <th>Ask</th>
                                      <th>High</th>
                                      <th>Low</th>
                                      <th>Open</th>
                                      <th>Daily Change</th>
                                  </tr>
                                  </thead>

                                  <tbody>
                                  <tr onmouseover="call_btc_aed_new()" id="call_btc_aed_new">
                                      <td><img src="<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/btc.png" />BTC</td>
                                      <td><?php  echo number_format($btc_aed_bid,2); ?></td>
                                      <td><?php  echo number_format($btc_aed_ask,2); ?></td>
                                      <td><?php  echo number_format($btc_aed_high,2); ?></td>
                                      <td><?php  echo number_format($btc_aed_low,2); ?></td>
                                      <td><?php  echo number_format($btc_aed_open,2); ?></td>
                                      <td><span class="up"><?php  echo number_format($btc_aed_24hr,2); ?>%</span></td>
                                      
                                  </tr>
                                 <?php /*<tr  onmouseover="call_xrp_btc()">
                                      <td><?php  echo number_format($xrp_last_btc,2); ?><span>XRP</span></td>
                                      <td><span class="up"><?php  echo number_format($xrp_24hr_btc,2); ?>%</span></td>
                                      <td><?php  echo number_format($xrp_vol_btc,2); ?></td>
                                  </tr>

                                  <tr  onmouseover="call_ltc_btc()">
                                      <td><?php  echo number_format($ltc_last_btc,2); ?><span>LTC</span></td>
                                      <td><span class="up"><?php  echo number_format($ltc_24hr_btc,2); ?>%</span></td>
                                      <td><?php  echo number_format($ltc_vol_btc,2); ?></td>
                                  </tr>
                                  <tr  onmouseover="call_aed_btc()">
                                      <td><?php  echo number_format($btc_aed_last,2); ?><span>AED</span></td>
                                      <td><span class="up"><?php  echo number_format($eth_24hr_btc,2); ?>%</span></td>
                                      <td><?php  echo number_format($aud_vol_btc,2); ?></td>
                                  </tr> */?>
                                  
                                  <tr onmouseover="call_eth_ltc()" >
                                      <td> <img src="<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/ltc.png" /> LTC</td>
                                      <td><?php  echo number_format($ltc_aed_bid,2); ?></td>
                                      <td><?php  echo number_format($ltc_aed_ask,2); ?></td>
                                      <td><?php  echo number_format($ltc_aed_high,2); ?></td>
                                      <td><?php  echo number_format($ltc_aed_low,2); ?></td>
                                      <td><?php  echo number_format($ltc_aed_open,2); ?></td>
                                      <td><span class="up"><?php  echo number_format($ltc_aed_24hr,2); ?>%</span></td>
                                  </tr>
                                  <?php /*<tr  onmouseover="call_btc_ltc()" >
                                      <td><?php  echo number_format($btc_last_ltc,2); ?><span>BTC</span></td>
                                      <td><span class="up"><?php  echo number_format($btc_24hr_ltc,2); ?>%</span></td>
                                      <td><?php  echo number_format($btc_vol_ltc,2); ?></td>
                                  </tr>
                                  <tr  onmouseover="call_xrp_ltc()" >
                                      <td><?php  echo number_format($xrp_last_ltc,2); ?><span>XRP</span></td>
                                      <td><span class="up"><?php  echo number_format($xrp_24hr_ltc,2); ?>%</span></td>
                                      <td><?php  echo number_format($xrp_vol_ltc,2); ?></td>
                                  </tr>
                                  <tr  onmouseover="call_aed_ltc()" >
                                      <td><?php  echo number_format($aud_last_ltc,2); ?><span>AED</span></td>
                                      <td><span class="up"><?php  echo number_format($aud_24hr_ltc,2); ?>%</span></td>
                                      <td><?php  echo number_format($aud_vol_ltc,2); ?></td>
                                  </tr>*/?>
                                 
                                  <tr onmouseover="call_eth_xrp()" >
                                      <td> <img src="<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/xrp.png" /> XRP</td>
                                      <td><?php  echo number_format($xrp_aed_bid,2); ?></td>
                                      <td><?php  echo number_format($xrp_aed_ask,2); ?></td>
                                      <td><?php  echo number_format($xrp_aed_high,2); ?></td>
                                      <td><?php  echo number_format($xrp_aed_low,2); ?></td>
                                      <td><?php  echo number_format($xrp_aed_open,2); ?></td>
                                      <td><span class="up"><?php  echo number_format($xrp_aed_24hr,2); ?>%</span></td>
                                  </tr>
                                <?php /*  <tr  onmouseover="call_ltc_xrp()" >
                                      <td><?php  echo number_format($ltc_last_xrp,2); ?><span>LTC</span></td>
                                      <td><span class="up"><?php  echo number_format($ltc_24hr_xrp,2); ?>%</span></td>
                                      <td><?php  echo number_format($ltc_vol_xrp,2); ?></td>
                                  </tr>
                                  <tr  onmouseover="call_btc_xrp()" >
                                      <td><?php  echo number_format($btc_last_xrp,2); ?><span>BTC</span></td>
                                      <td><span class="up"><?php  echo number_format($btc_24hr_xrp,2); ?>%</span></td>
                                      <td><?php  echo number_format($btc_vol_xrp,2); ?></td>
                                  </tr>
                                  <tr  onmouseover="call_aed_xrp()" >
                                      <td><?php  echo number_format($aud_last_xrp,2); ?><span>AED</span></td>
                                      <td><span class="up"><?php  echo number_format($aud_24hr_xrp,2); ?>%</span></td>
                                      <td><?php  echo number_format($aud_vol_xrp,2); ?></td>
                                  </tr>*/?>
                                 
                                  <tr onmouseover="call_xrp_eth()" >
                                      <td scope="row" > <img src="<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/eth.png" /> ETH</td>
                                      <td><?php  echo number_format($eth_aed_bid,2); ?></td>
                                      <td><?php  echo number_format($eth_aed_ask,2); ?></td>
                                      <td><?php  echo number_format($eth_aed_high,2); ?></td>
                                      <td><?php  echo number_format($eth_aed_low,2); ?></td>
                                      <td><?php  echo number_format($eth_aed_open,2); ?></td>
                                      <td><span class="up"><?php  echo number_format($eth_aed_24hr,2); ?>%</span></td>
                                  </tr>
                                  <?php /*<tr  onmouseover="call_ltc_eth()" >
                                      <td><?php  echo number_format($ltc_last_eth,2); ?><span>LTC</span></td>
                                      <td><span class="up"><?php  echo number_format($ltc_24hr_eth,2); ?>%</span></td>
                                      <td><?php  echo number_format($ltc_vol_eth,2); ?></td>
                                  </tr>
                                  <tr  onmouseover="call_btc_eth()" >
                                      <td><?php  echo number_format($btc_last_eth,2); ?><span>BTC</span></td>
                                      <td><span class="up"><?php  echo number_format($btc_24hr_eth,2); ?>%</span></td>
                                      <td><?php  echo number_format($btc_vol_eth,2); ?></td>
                                  </tr>
                                  <tr  onmouseover="call_aed_eth()" >
                                      <td><?php  echo number_format($aud_last_eth,2); ?><span>AED</span></td>
                                      <td><span class="up"><?php  echo number_format($aud_24hr_eth,2); ?>%</span></td>
                                      <td><?php  echo number_format($aud_vol_eth,2); ?></td>
                                  </tr>*/?>
                                  <tr onmouseover="call_eth_ltc()" >
                                      <td> <img src="<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/glc.png" /> GLC</td>
                                      <td>3.70</td>
                                      <td>3.70</td>
                                      <td>3.70</td>
                                      <td>3.70</td>
                                      <td>3.70</td>
                                      <td><span class="up">0%</span></td>
                                  </tr>
                                  </tbody>

                              </table>
                          </div>
                      </div>
                  </div>
                              
                  <div class="handle-resize"></div>
              </div>
              <span class="remove removeWidget">X</span>
          </li>
          <li data-sizey="1" data-sizex="3" data-col="2" data-row="1" class="col-sm-6">
              <div class="gridster-box">
                  
                      <div  class="theme-widget balance-widget expand-chart open"  >
                          <div class="widget-header acr">
                              <h5> <i class="fa fa-chevron-right" aria-hidden="true"></i>CHART <span id="slash_coin">BTC/AED</span></h5>
                          </div>
                          <div class="widget-body cstmscroll">
                              <div class="chart-div">
                                  <section class="market-depth-sec">
                                      <div class="container">
                                          <div class="row">
                                              <div class="col-lg-12">
                                                  <div class="market-depth-content">
                                                      <button class="btn btn-primary expand-chart-button" onclick="toggleFullScreen();" style="display: none;">Expand</button>
                                                      <div id="chartcontainer"></div>
                                                      <div id="loader" align="center">
                                                          <img src="<?php echo e(URL::asset('loading.gif')); ?>">
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </section>
                                  <input type="hidden" id="coin" value="BTC">
                                  <input type="hidden" id="currency" value="">

                              </div>
                          </div>
                      </div>
                                  
                  <div class="handle-resize"></div>
              </div>
              <span class="remove removeWidget">X</span>
          </li>
        </ul>
        <ul class="clearfix">
          <li data-sizey="1" data-sizex="2" data-col="4" data-row="2" class="col-sm-4">
              <div class="gridster-box">
                  <div class="theme-widget balance-widget open">
                      <div class="widget-header">
                          <h5 class="acr1"> <i class="fa fa-chevron-right" aria-hidden="true"></i>Quote Panel </h5>
                      </div>
                      <div class="widget-body cstmscroll" style="position: relative">
                          <section class='example' style="font-family: 'Source Sans Pro">
                              <div class='gridly'>
                                <div class='brick small' data-toggle="modal" data-target="#myModal">
                                  <h3>BITCOIN</h3>
                                  <h3 style="font-weight: 500;">BTC/AED</h3>
                                  <div class="abbr-wrap row">
                                      <div class="smbl text-left col-xs-6">Locked</div>
                                      <div class="smbl text-right col-xs-6">Available</div>
                                  </div>
                                  <div class="abbr-wrap row">
                                      <div class="smbl text-left col-xs-6">0.0000</div>
                                      <div class="smbl text-right col-xs-6"><?php  echo number_format(Sentinel::getuser()->bitcoin_balance,6); ?></div>
                                  </div>  
                                  <div class="buy-sell row">
                                      <div class="value-blk"> 1</div>
                                      <div class="grey-sell text-left col-xs-6">
                                          <div style="margin-top: 10px;" id="btc_sell_quote"></div>
                                          <div class="sell-itm">Sell</div>
                                      </div>
                                      <div class="green-buy text-right col-xs-6">
                                          <div style="margin-top: 10px;" id="btc_buy_quote"></div>
                                          <div class="buy-itm">Buy</div>
                                      </div>
                                  </div>                                                                               
                                  <a class='delete' href='#'>&times;</a>
                                </div>
                                <div class='brick small' data-toggle="modal" data-target="#myModal">
                                  <h3>ETHEREUM</h3>
                                  <h3 style="font-weight: 500;">ETH/AED</h3>
                                  <div class="abbr-wrap row">
                                      <div class="smbl text-left col-xs-6">Locked</div>
                                      <div class="smbl text-right col-xs-6">Available</div>
                                  </div>
                                  <div class="abbr-wrap row">
                                      <div class="smbl text-left col-xs-6">0.0000</div>
                                      <div class="smbl text-right col-xs-6"><?php  echo number_format(Sentinel::getuser()->ethereum_balance,6); ?></div>
                                  </div>  
                                  <div class="buy-sell row">
                                      <div class="value-blk"> 1</div>
                                      <div class="grey-sell text-left col-xs-6">
                                          <div style="margin-top: 10px;">0</div>
                                          <div class="sell-itm">Sell</div>
                                      </div>
                                      <div class="green-buy text-right col-xs-6">
                                          <div style="margin-top: 10px;">228.65</div>
                                          <div class="buy-itm">Buy</div>
                                      </div>
                                  </div>                                                                               
                                  <a class='delete' href='#'>&times;</a>
                                </div>
                                <div class='brick small' data-toggle="modal" data-target="#myModal">
                                  <h3>LITECOIN</h3>
                                  <h3 style="font-weight: 500">LTC/AED</h3>
                                  <div class="abbr-wrap row">
                                      <div class="smbl text-left col-xs-6">Locked</div>
                                      <div class="smbl text-right col-xs-6">Available</div>
                                  </div>
                                  <div class="abbr-wrap row">
                                      <div class="smbl text-left col-xs-6">0.0000</div>
                                      <div class="smbl text-right col-xs-6"><?php  echo number_format(Sentinel::getuser()->litecoin_balance,6); ?></div>
                                  </div>  
                                  <div class="buy-sell row">
                                      <div class="value-blk"> 1</div>
                                      <div class="grey-sell text-left col-xs-6">
                                          <div style="margin-top: 10px;">0</div>
                                          <div class="sell-itm">Sell</div>
                                      </div>
                                      <div class="green-buy text-right col-xs-6">
                                          <div style="margin-top: 10px;">228.65</div>
                                          <div class="buy-itm">Buy</div>
                                      </div>
                                  </div>                                                                               
                                  <a class='delete' href='#'>&times;</a>
                                </div>
                                <div class='brick small' data-toggle="modal" data-target="#myModal">
                                  <h3>AMBUJACEM</h3>
                                  <h3>NSE EQ</h3>
                                  <div class="abbr-wrap row">
                                      <div class="smbl text-left col-xs-6">MTM</div>
                                      <div class="smbl text-right col-xs-6">228.65</div>
                                  </div>
                                  <div class="abbr-wrap row">
                                      <div class="smbl text-left col-xs-6">0</div>
                                      <div class="smbl text-right col-xs-6">0</div>
                                  </div>  
                                  <div class="buy-sell row">
                                      <div class="value-blk"> 1</div>
                                      <div class="grey-sell text-left col-xs-6">
                                          <div style="margin-top: 10px;">0</div>
                                          <div class="sell-itm">Sell</div>
                                      </div>
                                      <div class="green-buy text-right col-xs-6">
                                          <div style="margin-top: 10px;">228.65</div>
                                          <div class="buy-itm">Buy</div>
                                      </div>
                                  </div>                                                                               
                                  <a class='delete' href='#'>&times;</a>
                                </div>
                                <div class='brick small' data-toggle="modal" data-target="#myModal">
                                  <h3>AMBUJACEM</h3>
                                  <h3>NSE EQ</h3>
                                  <div class="abbr-wrap row">
                                      <div class="smbl text-left col-xs-6">MTM</div>
                                      <div class="smbl text-right col-xs-6">228.65</div>
                                  </div>
                                  <div class="abbr-wrap row">
                                      <div class="smbl text-left col-xs-6">0</div>
                                      <div class="smbl text-right col-xs-6">0</div>
                                  </div>  
                                  <div class="buy-sell row">
                                      <div class="value-blk"> 1</div>
                                      <div class="grey-sell text-left col-xs-6">
                                          <div style="margin-top: 10px;">0</div>
                                          <div class="sell-itm">Sell</div>
                                      </div>
                                      <div class="green-buy text-right col-xs-6">
                                          <div style="margin-top: 10px;">228.65</div>
                                          <div class="buy-itm">Buy</div>
                                      </div>
                                  </div>                                                                               
                                  <a class='delete' href='#'>&times;</a>
                                </div>
                                <div class='brick small' data-toggle="modal" data-target="#myModal">
                                  <h3>AMBUJACEM</h3>
                                  <h3>NSE EQ</h3>
                                  <div class="abbr-wrap row">
                                      <div class="smbl text-left col-xs-6">MTM</div>
                                      <div class="smbl text-right col-xs-6">228.65</div>
                                  </div>
                                  <div class="abbr-wrap row">
                                      <div class="smbl text-left col-xs-6">0</div>
                                      <div class="smbl text-right col-xs-6">0</div>
                                  </div>  
                                  <div class="buy-sell row">
                                      <div class="value-blk"> 1</div>
                                      <div class="grey-sell text-left col-xs-6">
                                          <div style="margin-top: 10px;">0</div>
                                          <div class="sell-itm">Sell</div>
                                      </div>
                                      <div class="green-buy text-right col-xs-6">
                                          <div style="margin-top: 10px;">228.65</div>
                                          <div class="buy-itm">Buy</div>
                                      </div>
                                  </div>                                                                               
                                  <a class='delete' href='#'>&times;</a>
                                </div>
                              
                              </div>
                              <p class='actions'>
                                <a class='add button' href='#'>Add</a>
                              </p>
                      </section>
                      </div>
                  </div>                
                  <div class="handle-resize"></div>
              </div>
          </li>
          <li data-sizey="1" data-sizex="2" data-col="3" data-row="2" class="col-sm-5">
              <div class="gridster-box">
                  
                  <div class="theme-widget balance-widget open">
                      <div class="widget-header">
                          <h5 class="acr1"> <i class="fa fa-chevron-right" aria-hidden="true"></i>Order Book  <span class="text-muted" id="order_coin">BTC/AED</span></h5>
                      </div>
                      <div class="widget-body cstmscroll">
                          <div class="">
                              <div class="scroll-table">
                                  <div class="row">
                                      <div class="col-md-6 table-scroll">
                                          <div class="table-responsive">
                                              <table id="data-tab_bids" class="table-striped theme-dt bg-success tableclass scroll" style="width:100%">
                                                  <thead style="position: initial;">
                                                  <tr>
                                                      <th ><span class="coinnames"></span> Amount</th>
                                                      <th >Price / <span class="coinnames"></span></th>
                                                      <th >Total: (AED)</th>
                                                  </tr>
                                                  </thead>
                                                  <tbody id="bids_placeholder"></tbody>
                                              </table>

                                          </div>
                                      </div>
                                      <div class="col-md-6 table-scroll">
                                          <table id="data-tab_asks" class="theme-dt bg-danger tableclass scroll" style="width:100%">
                                              <thead style="position: initial;">
                                              <tr>
                                                  <th  ><span class="coinnames"></span> Amount</th>
                                                  <th  >Price / <span class="coinnames"></span></th>
                                                  <th  >Total: (AED)</th>
                                              </tr>
                                              </thead>
                                              <tbody id="asks_placeholder"></tbody>
                                          </table>

                                      </div>
                                  </div>
                              </div>



                          </div>
                          <div class="full-book mt-10">
                              <!--<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal">Full Book</button>-->
                          </div>

                      </div>
                  </div>                
                  <div class="handle-resize"></div>
              </div>
          </li>
          <li data-sizey="1" data-sizex="2" data-col="5" data-row="2" class="col-sm-3">
              <div class="gridster-box">
                  <input type="hidden" value="<?php echo e($btc_balance ? $btc_balance:0); ?>" id="bal_users_btc">
                  <input type="hidden" value="<?php echo e($eth_balance ? $eth_balance:0); ?>" id="bal_users_eth">
                  <input type="hidden" value="<?php echo e($ltc_balance ? $ltc_balance:0); ?>" id="bal_users_ltc">
                  <input type="hidden" value="<?php echo e($xrp_balance ? $xrp_balance:0); ?>" id="bal_users_xrp">
                  <input type="hidden" value="<?php echo e($aed_balance ? $aed_balance:0); ?>" id="bal_users_aed">


                  <div class="theme-widget balance-widget open">
                      <div class="widget-header acr">
                          <h5> <i class="fa fa-chevron-right" aria-hidden="true"></i>Trades Block <span class="text-muted"><samp class="coinnames"></samp>/AED</span></h5>
                      </div>
                      <div class="widget-body cstmscroll">
                          <div id="typecall" class="table-responsive">
                              <table class="display theme-dt tableclass scroll table-striped" style="width:100%;overflow: hidden;">
                                  <thead style="position: initial;">
                                  <tr style="background: #f8f8f8;">
                                      <th >TYPE</th>
                                      <th >Amount</th>
                                      <th >PRICE</th>
                                  </tr>
                                  </thead>
                                  <tbody id="live_trade">

                                  </tbody>
                              </table>
                              <div></div>
                          </div>
                      </div>
                  </div>

                  <div class="handle-resize"></div>
              </div>
          </li> 
        </ul>
      </div>
    </div>
  </div>
</div>


    </div>

</div>






 </div>
<div class="bottomtabbar">
<button class="tablink" onclick="openPage('quotesTab', this, '#0046bb', '#fff')" id="defaultOpen">Quotes</button>
<button class="tablink" onclick="openPage('Home', this, '#0046bb', '#fff')">Books</button>
<button class="tablink" onclick="openPage('chartsTab', this, '#0046bb', '#fff')">Charts</button>


</div>

<!--
<div class="testcontainer">.testcontainer</div>
 <button id="btn2" class="btn btn-primary" type="button">Example 2</button>
 <button id="btn1" class="btn btn-primary" type="button">Example 1</button>
 <button id="btn4" class="btn btn-primary" type="button">Example 4</button>
<button id="btn3" class="btn btn-primary" type="button">Example 3</button>-->

<script>
function openPage(pageName,elmnt,color,textcolor) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.height = "0px";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].style.backgroundColor = "";
        tablinks[i].style.color = "";
        tablinks[i].classList.remove("tabSelected");
        
    }
    document.getElementById(pageName).style.height = "100%";
    elmnt.style.backgroundColor = color;
    elmnt.style.color = textcolor;
    elmnt.classList.add("tabSelected");

}
// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>




        <!-- jQuery 2.2.3 -->
        <script src="<?php echo e(asset('assets/plugins/jQuery/jquery-2.2.3.min.js')); ?>"></script>

        <script src="<?php echo e(asset('assets/plugins/bootstrap-toastr/toastr.min.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/plugins/jqueryui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo e(asset('assets/bootstrap/js/bootstrap.min.js')); ?>"></script>
    <!--<script src="<?php echo e(asset('assets/plugins/datepicker/bootstrap-datepicker.min.js')); ?>"
            type="text/javascript"></script>-->
        <script src="<?php echo e(asset('assets/bootstrap/js/jquery.mCustomScrollbar.js')); ?>"></script>
        
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <!-- /page container -->
        <script src="<?php echo e(asset('assets/plugins/sweetalert2/sweetalert2.min.js')); ?>"></script>
        <script src="<?php echo e(asset('assets/plugins/toastr/toastr.min.js')); ?>"></script>
        <script src="<?php echo e(asset('assets/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js')); ?>"
                type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/plugins/jquery-validation/jquery.validate.min.js')); ?>"
                type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/plugins/jquery-validation/additional-methods.min.js')); ?>"
                type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/plugins/moment/js/moment.min.js')); ?>"
                type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')); ?>"
                type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/plugins/bootstrap-touchspin/bootstrap.touchspin.min.js')); ?>"
                type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/plugins/tinymce/tinymce.min.js')); ?>"
                type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/plugins/fancybox/jquery.fancybox.js')); ?>"
                type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/plugins/jquery.numeric.js')); ?>"></script>

        
        
        <script src="<?php echo e(asset('assets/themes/limitless/js/core/app.js')); ?>"></script>
        <script src="<?php echo e(asset('assets/themes/limitless/js/plugins/ui/ripple.min.js')); ?>"></script>
        <script src="<?php echo e(asset('assets/themes/limitless/js/plugins/forms/styling/uniform.min.js')); ?>"></script>
        <script src="<?php echo e(asset('assets/plugins/select2/select2.min.js')); ?>"></script>
      

        <script src="<?php echo e(asset('assets/themes/limitless/js/plugins/gridly/jquery.gridly.js')); ?>"></script>
        <script src="<?php echo e(asset('assets/themes/limitless/js/plugins/gridly/jquery.gridster.min.js')); ?>"></script>

        
        
        <!-- SlimScroll 1.3.0 -->
        <script src="<?php echo e(asset('assets/themes/limitless/js/plugins/tables/datatables/datatables.min.js')); ?>"></script>
       

        <?php echo $__env->yieldContent('footer-scripts'); ?>
    <!-- ChartJS 1.0.1 -->
        <script src="<?php echo e(asset('assets/themes/limitless/js/custom.js')); ?>"></script>


        <script src="https://d3dy5gmtp8yhk7.cloudfront.net/2.1/pusher.min.js"></script>
  


   
        <script>


            var j = 0;
            var aed_price_admin =<?php echo e($aed_rate); ?>;


            function callshoket(currentcoin) {
                if (currentcoin == "BTC") {
                    currrent = "order_book";
                }
                else if (currentcoin == "ETH") {
                    currrent = "order_book_ethusd";
                }
                else if (currentcoin == "XRP") {
                    currrent = "order_book_xrpusd";
                }
                else if (currentcoin == "LTC") {
                    currrent = "order_book_ltcusd";
                }


                var bidsPlaceholder = document.getElementById("bids_placeholder");
                var asksPlaceholder = document.getElementById("asks_placeholder");
                var btc_buy = document.getElementById("btc_buy_quote");
                var btc_sell = document.getElementById("btc_sell_quote");
                var orderBookChannel = "";

                pusher = new Pusher('de504dc5763aeef9ff52');
                orderBookChannel = '';
                orderBookChannel = pusher.subscribe(currrent);
                i = 0;
                orderBookChannel.bind('data', function (data) {

                    bidsPlaceholder.innerHTML = '';
                    asksPlaceholder.innerHTML = '';
                    btc_buy.innerHTML = (data.asks[0][0] * aed_price_admin).toFixed(2);
                    btc_sell.innerHTML = (data.bids[0][0] * aed_price_admin).toFixed(2);
                   
                    for (i = 0; i < data.bids.length; i += 1) {

                        var bidsrate = 'onclick="setvalue(' + data.bids[i][0] * aed_price_admin + ',' + data.bids[i][1] + ')"';

                        bidsPlaceholder.innerHTML += '<tr ' + bidsrate + ' ><td>' + parseFloat(data.bids[i][1]).toFixed(4) + '</td><td>' + (data.bids[i][0] * aed_price_admin).toFixed(4) + ' AED' + '</td><td>' + (data.bids[i][1] * data.bids[i][0] * aed_price_admin).toFixed(4) + '</td></tr>';
                    }
                    for (i = 0; i < data.asks.length; i += 1) {
                        var asksrate = 'onclick="setvalue(' + data.asks[i][0] * aed_price_admin + ',' + data.asks[i][1] + ')"';

                        asksPlaceholder.innerHTML += '<tr ' + asksrate + ' ><td>' + parseFloat(data.asks[i][1]).toFixed(4) + '</td><td>' + (data.asks[i][0] * aed_price_admin).toFixed(4) + ' AED' + '</td><td>' + (data.asks[i][1] * data.asks[i][0] * aed_price_admin).toFixed(4) + '</td></tr>';

                        
                    }
                });
            }
        </script>


        <script>

            $(".acr").click(function(){
                $(this).parent().toggleClass("open");
            });

            $(".acr1").click(function(){
                $(this).parent().parent().toggleClass("open");
                $(this).parent().toggleClass("acr1");
            });

            $('#order-tbl').DataTable();
            $('#order-data-table3').DataTable();
            $('#balance-tbl').DataTable();
            $('#block-1-tbl').DataTable();
            $('#block-2-tbl').DataTable();

            $('#order-his-tbl').DataTable( {
                "order": [[ 0, "desc" ]],
                "bInfo": false,
                "searching": false,
                "bLengthChange": false,
                "language": {
                "emptyTable":     "No previous orders"
        }
        });
            
            $('#example12').DataTable( {
                "order": [[ 0, "desc" ]],
                "bInfo": false,
                "searching": false,
                "bLengthChange": false,
                "language": {
                "emptyTable":     "No open orders"
                }
        });
            


            $('[data-toggle="tooltip"]').tooltip();


        </script>
        <script type="text/javascript">
            $(window).load(function() {
                $(".loader").fadeOut("slow");
            });

            $('#balance-check').hover(
              function(){
                   $('#balance-show').css('display', 'block')
               }, 
               function() { 
                   $('#balance-show').css('display', 'none')
            });

        </script>
        <script>

            //BTC
            function call_btc_aed_new()
            {
                $('#image_change').empty(); $('#coin_name').empty(); $('#coin_volume').empty(); $('#coin_low').empty();
                $('#coin_high').empty(); $('#coin_price').empty(); $('#coin_24hr').empty();

                $('#image_change').append("<img style='height:50px;width:50px;' src='<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/btc.png'>");
                $('#coin_name').append('BTC/AED');
                $('#coin_volume').append('<?php echo number_format($eth_vol_btc,2); ?>');
                $('#coin_low').append('<?php echo number_format($btc_aed_low,2); ?>');
                $('#coin_high').append('<?php echo number_format($btc_aed_high,2); ?>');
                $('#coin_price').append('<?php echo number_format($btc_aed_last,2); ?> ETH');
                $('#coin_24hr').append('<?php echo number_format($eth_24hr_btc,2); ?> %');
            }

            function call_ltc_btc()
            {
                $('#image_change').empty(); $('#coin_name').empty(); $('#coin_volume').empty(); $('#coin_low').empty();
                $('#coin_high').empty(); $('#coin_price').empty(); $('#coin_24hr').empty();
                $('#image_change').append("<img style='height:50px;width:50px;' src='<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/btc.png'>");
                $('#coin_name').append('BTC/LTC');
                $('#coin_volume').append('<?php echo number_format($ltc_vol_btc,2); ?>');
                $('#coin_low').append('<?php echo number_format($ltc_low_btc,2); ?>');
                $('#coin_high').append('<?php echo number_format($ltc_high_btc,2); ?>');
                $('#coin_price').append('<?php echo number_format($ltc_last_btc,2); ?> LTC');
                $('#coin_24hr').append('<?php echo number_format($ltc_24hr_btc,2); ?> %');
            }

            function call_xrp_btc()
            {
                $('#image_change').empty(); $('#coin_name').empty(); $('#coin_volume').empty(); $('#coin_low').empty();
                $('#coin_high').empty(); $('#coin_price').empty(); $('#coin_24hr').empty();
                $('#image_change').append("<img style='height:50px;width:50px;' src='<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/btc.png'>");
                $('#coin_name').append('BTC/XRP');
                $('#coin_volume').append('<?php echo number_format($xrp_vol_btc,2); ?>');
                $('#coin_low').append('<?php echo number_format($xrp_low_btc,2); ?>');
                $('#coin_high').append('<?php echo number_format($xrp_high_btc,2); ?>');
                $('#coin_price').append('<?php echo number_format($xrp_last_btc,2); ?> XRP');
                $('#coin_24hr').append('<?php echo number_format($xrp_24hr_btc,2); ?> %');
            }


            function call_aed_btc()
            {
                $('#image_change').empty(); $('#coin_name').empty(); $('#coin_volume').empty(); $('#coin_low').empty();
                $('#coin_high').empty(); $('#coin_price').empty(); $('#coin_24hr').empty();
                $('#image_change').append("<img style='height:50px;width:50px;' src='<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/btc.png'>");
                $('#coin_name').append('BTC/AED');
                $('#coin_volume').append('<?php echo number_format($aud_vol_btc,2); ?>');
                $('#coin_low').append('<?php echo number_format($aud_low_btc,2); ?>');
                $('#coin_high').append('<?php echo number_format($aud_high_btc,2); ?>');
                $('#coin_price').append('<?php echo number_format($aud_last_btc,2); ?> AED');
                $('#coin_24hr').append('<?php echo number_format($aud_24hr_btc,2); ?> %');
            }

            //LTC
            function call_eth_ltc()
            {
                $('#image_change').empty(); $('#coin_name').empty(); $('#coin_volume').empty(); $('#coin_low').empty();
                $('#coin_high').empty(); $('#coin_price').empty(); $('#coin_24hr').empty();
                $('#image_change').append("<img style='height:50px;width:50px;' src='<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/ltc.png'>");
                $('#coin_name').append('LTC/ETH');
                $('#coin_volume').append('<?php echo number_format($eth_vol_ltc,2); ?>');
                $('#coin_low').append('<?php echo number_format($eth_low_ltc,2); ?>');
                $('#coin_high').append('<?php echo number_format($eth_high_ltc,2); ?>');
                $('#coin_price').append('<?php echo number_format($eth_last_ltc,2); ?> ETH');
                $('#coin_24hr').append('<?php echo number_format($eth_24hr_ltc,2); ?> %');
            }

            function call_btc_ltc()
            {
                $('#image_change').empty(); $('#coin_name').empty(); $('#coin_volume').empty(); $('#coin_low').empty();
                $('#coin_high').empty(); $('#coin_price').empty(); $('#coin_24hr').empty();
                $('#image_change').append("<img style='height:50px;width:50px;' src='<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/ltc.png'>");
                $('#coin_name').append('LTC/BTC');
                $('#coin_volume').append('<?php echo number_format($btc_vol_ltc,2); ?>');
                $('#coin_low').append('<?php echo number_format($btc_low_ltc,2); ?>');
                $('#coin_high').append('<?php echo number_format($btc_high_ltc,2); ?>');
                $('#coin_price').append('<?php echo number_format($btc_last_ltc,2); ?> BTC');
                $('#coin_24hr').append('<?php echo number_format($btc_24hr_ltc,2); ?> %');
            }
            function call_xrp_ltc()
            {
                $('#image_change').empty(); $('#coin_name').empty(); $('#coin_volume').empty(); $('#coin_low').empty();
                $('#coin_high').empty(); $('#coin_price').empty(); $('#coin_24hr').empty();
                $('#image_change').append("<img style='height:50px;width:50px;' src='<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/ltc.png'>");
                $('#coin_name').append('LTC/XRP');
                $('#coin_volume').append('<?php echo number_format($xrp_vol_ltc,2); ?>');
                $('#coin_low').append('<?php echo number_format($xrp_low_ltc,2); ?>');
                $('#coin_high').append('<?php echo number_format($xrp_high_ltc,2); ?>');
                $('#coin_price').append('<?php echo number_format($xrp_last_ltc,2); ?> XRP');
                $('#coin_24hr').append('<?php echo number_format($xrp_24hr_ltc,2); ?> %');
            }

            function call_aed_ltc()
            {
                $('#image_change').empty(); $('#coin_name').empty(); $('#coin_volume').empty(); $('#coin_low').empty();
                $('#coin_high').empty(); $('#coin_price').empty(); $('#coin_24hr').empty();
                $('#image_change').append("<img style='height:50px;width:50px;' src='<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/ltc.png'>");
                $('#coin_name').append('LTC/AED');
                $('#coin_volume').append('<?php echo number_format($aud_vol_ltc,2); ?>');
                $('#coin_low').append('<?php echo number_format($aud_low_ltc,2); ?>');
                $('#coin_high').append('<?php echo number_format($aud_high_ltc,2); ?>');
                $('#coin_price').append('<?php echo number_format($aud_last_ltc,2); ?> AED');
                $('#coin_24hr').append('<?php echo number_format($aud_24hr_ltc,2); ?> %');
            }

            //XRP
            function call_eth_xrp()
            {
                $('#image_change').empty(); $('#coin_name').empty(); $('#coin_volume').empty(); $('#coin_low').empty();
                $('#coin_high').empty(); $('#coin_price').empty(); $('#coin_24hr').empty();
                $('#image_change').append("<img style='height:50px;width:50px;' src='<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/xrp.png'>");
                $('#coin_name').append('XRP/ETH');
                $('#coin_volume').append('<?php echo number_format($eth_vol_xrp,2); ?>');
                $('#coin_low').append('<?php echo number_format($eth_low_xrp,2); ?>');
                $('#coin_high').append('<?php echo number_format($eth_high_xrp,2); ?>');
                $('#coin_price').append('<?php echo number_format($eth_last_xrp,2); ?> ETH');
                $('#coin_24hr').append('<?php echo number_format($eth_24hr_xrp,2); ?> %');
            }

            function call_btc_xrp()
            {
                $('#image_change').empty(); $('#coin_name').empty(); $('#coin_volume').empty(); $('#coin_low').empty();
                $('#coin_high').empty(); $('#coin_price').empty(); $('#coin_24hr').empty();
                $('#image_change').append("<img style='height:50px;width:50px;' src='<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/xrp.png'>");
                $('#coin_name').append('XRP/BTC');
                $('#coin_volume').append('<?php echo number_format($btc_vol_xrp,2); ?>');
                $('#coin_low').append('<?php echo number_format($btc_low_xrp,2); ?>');
                $('#coin_high').append('<?php echo number_format($btc_high_xrp,2); ?>');
                $('#coin_price').append('<?php echo number_format($btc_last_xrp,2); ?> BTC');
                $('#coin_24hr').append('<?php echo number_format($btc_24hr_xrp,2); ?> %');
            }

            function call_ltc_xrp()
            {
                $('#image_change').empty(); $('#coin_name').empty(); $('#coin_volume').empty(); $('#coin_low').empty();
                $('#coin_high').empty(); $('#coin_price').empty(); $('#coin_24hr').empty();
                $('#image_change').append("<img style='height:50px;width:50px;' src='<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/xrp.png'>");
                $('#coin_name').append('XRP/LTC');
                $('#coin_volume').append('<?php echo number_format($ltc_vol_xrp,2); ?>');
                $('#coin_low').append('<?php echo number_format($ltc_low_xrp,2); ?>');
                $('#coin_high').append('<?php echo number_format($ltc_high_xrp,2); ?>');
                $('#coin_price').append('<?php echo number_format($ltc_last_xrp,2); ?> LTC');
                $('#coin_24hr').append('<?php echo number_format($ltc_24hr_xrp,2); ?> %');
            }

            function call_aed_xrp()
            {
                $('#image_change').empty(); $('#coin_name').empty(); $('#coin_volume').empty(); $('#coin_low').empty();
                $('#coin_high').empty(); $('#coin_price').empty(); $('#coin_24hr').empty();
                $('#image_change').append("<img style='height:50px;width:50px;' src='<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/xrp.png'>");
                $('#coin_name').append('XRP/AED');
                $('#coin_volume').append('<?php echo number_format($aud_vol_xrp,2); ?>');
                $('#coin_low').append('<?php echo number_format($aud_low_xrp,2); ?>');
                $('#coin_high').append('<?php echo number_format($aud_high_xrp,2); ?>');
                $('#coin_price').append('<?php echo number_format($aud_last_xrp,2); ?> AED');
                $('#coin_24hr').append('<?php echo number_format($aud_24hr_xrp,2); ?> %');
            }


            //ETH
            function call_xrp_eth()
            {
                $('#image_change').empty(); $('#coin_name').empty(); $('#coin_volume').empty(); $('#coin_low').empty();
                $('#coin_high').empty(); $('#coin_price').empty(); $('#coin_24hr').empty();
                $('#image_change').append("<img style='height:50px;width:50px;' src='<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/eth.png'>");
                $('#coin_name').append('ETH/XRP');
                $('#coin_volume').append('<?php echo number_format($xrp_vol_eth,2); ?>');
                $('#coin_low').append('<?php echo number_format($xrp_low_eth,2); ?>');
                $('#coin_high').append('<?php echo number_format($xrp_high_eth,2); ?>');
                $('#coin_price').append('<?php echo number_format($xrp_last_eth,2); ?> XRP');
                $('#coin_24hr').append('<?php echo number_format($xrp_24hr_eth,2); ?> %');
            }

            function call_btc_eth()
            {
                $('#image_change').empty(); $('#coin_name').empty(); $('#coin_volume').empty(); $('#coin_low').empty();
                $('#coin_high').empty(); $('#coin_price').empty(); $('#coin_24hr').empty();
                $('#image_change').empty();  $('#image_change').append("<img style='height:50px;width:50px;' src='<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/eth.png'>");
                $('#coin_name').empty(); $('#coin_name').append('ETH/BTC');
                $('#coin_volume').empty();$('#coin_volume').append('<?php echo number_format($btc_vol_eth,2); ?>');
                $('#coin_low').empty();$('#coin_low').append('<?php echo number_format($btc_low_eth,2); ?>');
                $('#coin_high').empty();$('#coin_high').append('<?php echo number_format($btc_high_eth,2); ?>');
                $('#coin_price').empty();$('#coin_price').append('<?php echo number_format($btc_last_eth,2); ?> BTC');
                $('#coin_24hr').empty();$('#coin_24hr').append('<?php echo number_format($btc_24hr_eth,2); ?> %');
            }

            function call_ltc_eth()
            {
                $('#image_change').empty(); $('#coin_name').empty(); $('#coin_volume').empty(); $('#coin_low').empty();
                $('#coin_high').empty(); $('#coin_price').empty(); $('#coin_24hr').empty();
                $('#image_change').append("<img style='height:50px;width:50px;' src='<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/eth.png'>");
                $('#coin_name').append('ETH/LTC');
                $('#coin_volume').append('<?php echo number_format($ltc_vol_eth,2); ?>');
                $('#coin_low').append('<?php echo number_format($ltc_low_eth,2); ?>');
                $('#coin_high').append('<?php echo number_format($ltc_high_eth,2); ?>');
                $('#coin_price').append('<?php echo number_format($ltc_last_eth,2); ?> LTC');
                $('#coin_24hr').append('<?php echo number_format($ltc_24hr_eth,2); ?> %');
            }

            function call_aed_eth()
            {
                $('#image_change').empty(); $('#coin_name').empty(); $('#coin_volume').empty(); $('#coin_low').empty();
                $('#coin_high').empty(); $('#coin_price').empty(); $('#coin_24hr').empty();
                $('#image_change').append("<img style='height:50px;width:50px;' src='<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/eth.png'>");
                $('#coin_name').append('ETH/AED');
                $('#coin_volume').append('<?php echo number_format($aud_vol_eth,2); ?>');
                $('#coin_low').append('<?php echo number_format($aud_low_eth,2); ?>');
                $('#coin_high').append('<?php echo number_format($aud_high_eth,2); ?>');
                $('#coin_price').append('<?php echo number_format($aud_last_eth,2); ?> AED');
                $('#coin_24hr').append('<?php echo number_format($aud_24hr_eth,2); ?> %');
            }


            //AED
            function call_eth_aed()
            {
                $('#image_change').empty(); $('#coin_name').empty(); $('#coin_volume').empty(); $('#coin_low').empty();
                $('#coin_high').empty(); $('#coin_price').empty(); $('#coin_24hr').empty();
                $('#image_change').append("<img style='height:50px;width:50px;' src='<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/aed.png'>");
                $('#coin_name').append('AED/ETH');
                $('#coin_volume').append('-');
                $('#coin_low').append('<?php echo number_format($eth_low_aed,2); ?>');
                $('#coin_high').append('<?php echo number_format($eth_high_aed,2); ?>');
                $('#coin_price').append('<?php echo number_format($eth_last_aed,2); ?> ETH');
                $('#coin_24hr').append('<?php echo number_format($eth_24hr_aed,2); ?> %');
            }

            function call_btc_aed()
            {
                $('#image_change').empty(); $('#coin_name').empty(); $('#coin_volume').empty(); $('#coin_low').empty();
                $('#coin_high').empty(); $('#coin_price').empty(); $('#coin_24hr').empty();
                $('#image_change').append("<img style='height:50px;width:50px;' src='<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/aed.png'>");
                $('#coin_name').append('XRP/BTC');
                $('#coin_volume').append('-');
                $('#coin_low').append('<?php echo number_format($btc_low_aed,2); ?>');
                $('#coin_high').append('<?php echo number_format($btc_high_aed,2); ?>');
                $('#coin_price').append('<?php echo number_format($btc_last_aed,2); ?> BTC');
                $('#coin_24hr').append('<?php echo number_format($btc_24hr_aed,2); ?> %');
            }

            function call_ltc_aed()
            {
                $('#image_change').empty(); $('#coin_name').empty(); $('#coin_volume').empty(); $('#coin_low').empty();
                $('#coin_high').empty(); $('#coin_price').empty(); $('#coin_24hr').empty();
                $('#image_change').append("<img style='height:50px;width:50px;' src='<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/aed.png'>");
                $('#coin_name').append('XRP/LTC');
                $('#coin_volume').append('-');
                $('#coin_low').append('<?php echo number_format($ltc_low_aed,2); ?>');
                $('#coin_high').append('<?php echo number_format($ltc_high_aed,2); ?>');
                $('#coin_price').append('<?php echo number_format($ltc_last_aed,2); ?> LTC');
                $('#coin_24hr').append('<?php echo number_format($ltc_24hr_aed,2); ?> %');
            }

            function call_xrp_aed()
            {
                $('#image_change').empty(); $('#coin_name').empty(); $('#coin_volume').empty(); $('#coin_low').empty();
                $('#coin_high').empty(); $('#coin_price').empty(); $('#coin_24hr').empty();
                $('#image_change').append("<img style='height:50px;width:50px;' src='<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/aed.png'>");
                $('#coin_name').append('XRP/AED');
                $('#coin_volume').append('-');
                $('#coin_low').append('<?php echo number_format($xrp_low_aed,2); ?>');
                $('#coin_high').append('<?php echo number_format($xrp_high_aed,2); ?>');
                $('#coin_price').append('<?php echo number_format($xrp_last_aed,2); ?> XRP');
                $('#coin_24hr').append('<?php echo number_format($xrp_24hr_aed,2); ?> %');
            }

        </script>

        <script src="<?php echo e(asset('candle/js/highstock.js')); ?>"></script>
        <script src="<?php echo e(asset('candle/js/exporting.js')); ?>"></script>
        <script src="<?php echo e(asset('candle/js/btc_chart_head.js')); ?>"></script>

        <script type="text/javascript">

            $(document).ready( function(){
                call_all('<?php echo e($coinuse); ?>');
            });

            function loadcharts(coin, cur ,widthc) {
                $('#coin').val(coin);
                $('#currency').val(cur);
                $('#loader').show();


                $.getJSON("chart/"+coin+'/'+cur, function(data) {
                    //console.log(data);
                    Highcharts.setOptions({
                        global: {
                            timezoneOffset: -2 * 60
                        }
                    });
                    // split the data set into ohlc and volume
                    var ohlc = [],
                        volume = [],
                        dataLength = data.length;

                    for (i = 0; i < dataLength; i++) {
                        ohlc.push([
                            data[i]['time']*1000, // the date
                            data[i]['open'], // open
                            data[i]['high'], // high
                            data[i]['low'], // low
                            data[i]['close'] // close
                        ]);

                        volume.push([
                            data[i]['time']*1000, // the date
                            data[i]['volumefrom'] // the volume
                        ])
                    }

                    // create the chart
                    $('#loader').hide();

                    $('#chartcontainer').highcharts('StockChart', {

                        rangeSelector: {
                            inputEnabled: $('#chartcontainer').width() > 480,
                            selected: 0,
                            enabled: false

                        },
                        //Rads added START
                        navigator: { //Rads says not sure why this one is purple!
                            enabled: false
                        },
                        chart: {
                            marginBottom: 3,
                            height: widthc,
                            backgroundColor: '#FFFFFF'
                        },

                        scrollbar: {
                            enabled: false
                        },

                        credits: {
                            enabled: false
                        },
                        exporting: {
                            enabled: false
                        },
                        yAxis: [{

                            labels: {
                                enabled: true,
                                align: 'left',
                            },

                            title: {
                                text: 'OHLC',
                                x: 11
                            },
                            height: '10%',
                            height: '100%',
                            lineWidth: 2
                        }, {
                            labels: {
                                align: 'right',
                                x: -3,
                                style: { "color": "#000000", "fontWeight": "none" }
                            },
                            title: {
                                text: 'Volume',
                                y: 70,
                                style: { "color": "#000000", "fontWeight": "bold" }
                            },
                            height: '100%',
                            offset: 0,
                            lineWidth: 2,
                            opposite: false

                        }],

                        rangeSelector: {
                            buttons: [{
                                type: 'hour',
                                count: 1,
                                text: '1H'
                            }, {
                                type: 'day',
                                count: 1,
                                text: '1D'
                            }, {
                                type: 'week',
                                count: 1,
                                text: '1W'
                            }, {
                                type: 'month',
                                count: 1,
                                text: '1M'
                            }, {
                                type: 'all',
                                count: 1,
                                text: 'All'
                            }],
                            selected: 1,
                            inputEnabled: true
                        },

                        series: [{
                            type: 'column',
                            //color: '#FF9900',//This changes the bar chart to orange Also changes the little dot...
                            color: '#4ebc91',
                            name: 'Volume',
                            data: volume,
                            yAxis: 1,

                        },
                            {
                                type: 'candlestick',
                                name: coin+'/'+cur,
                                data: ohlc,
                                color: '#EB4D5C',
                                upColor: '#1f87e6',
                                lineColor: '#EB4D5C',
                                upLineColor: 'green' // docs
                            }]
                    });


                });
            }


            //  setInterval(coindeta, 1000000000000000)


            function call_all(coin) {

                callshoket(coin);
                liveorders(coin);
                tradehistory(coin)
                loadcharts(coin, 'AED', 500);

                $.ajax({
                    url: '<?php echo e(url('')); ?>/exchange/call_order_middle',
                    type: 'post',
                    data: {
                        option: coin,
                        _token: "<?php echo e(csrf_token()); ?>"
                    },
                    success: function(response) {
                        var buy = response.data;
                        //Get the total rows
                        $.each(buy, function(index, value) {
                            var t = $('#example12').DataTable();
                            var btn = "<lable onclick='cancel_order_user("+value.id+","+value.order_id+")' class='btn btn-info'>cancel</lable>";
                            var counter = 1;

                            if (value.status == 0) {

                                var row = '<label class="label label-info buycount "> Pending</label>';
                            }
                            else if (value.status == 1) {
                                var row = '<label class="label label-success buycount ">  Success </label>';
                            }
                            else if (value.status == 2) {
                                var row = '<label class="label label-warning buycount ">  Partially </label>';
                            }
                            else if (value.status == 3) {
                                var row = '<label class="label label-danger buycount ">  Cancel </label>';
                            }


                            if (value.side == 'buy') {

                                var rowside = '<label class="label label-info buycount "> Buy</label>';
                            }
                            else if (value.side == 'sell') {
                                var rowside = '<label class="label label-success buycount ">  Sell </label>';
                            }

                            t.row.add([
                                value.id,
                                rowside,
                                value.order_id,
                                value.symbol,
                                value.same_price,
                                value.price,
                                value.buy_fees,
                                value.final_aed,
                                btn,
                                row
                            ]).draw(false);
                        });
                    }
                });

                $.ajax({
                    url: '<?php echo e(url('')); ?>/exchange/call_order_full',
                    type: 'post',
                    data:{ option:coin,_token:"<?php echo e(csrf_token()); ?>"},
                    success: function(response) {
                        var buy = response.data;
                        //Get the total rows
                        $.each(buy, function(index, value) {
                            var t = $('#order-his-tbl').DataTable();
                            var counter = 1;

                            if (value.status == 0) {

                                var row = '<label class="label label-info buycount "> Pending</label>';
                            }
                            else if (value.status == 1) {
                                var row = '<label class="label label-success buycount ">  Success </label>';
                            }
                            else if (value.status == 2) {
                                var row = '<label class="label label-warning buycount ">  Partially </label>';
                            }
                            else if (value.status == 3) {
                                var row = '<label class="label label-danger buycount ">  Cancel </label>';
                            }


                            if (value.side == 'buy') {

                                var rowside = '<label class="label label-info buycount "> Buy</label>';
                            }
                            else if (value.side == 'sell') {
                                var rowside = '<label class="label label-success buycount ">  Sell </label>';
                            }

                            t.row.add([
                                value.id,
                                rowside,
                                value.order_id,
                                value.symbol,
                                value.same_price,
                                value.price,
                                value.buy_fees,
                                value.final_aed,
                                row
                            ]).draw(false);
                        });

                    }

                });


                /* var type_coin = $('#coin').val();
                 if (type_coin == "LTC") {
                 swal('alert?', 'LTC will be add in exchange later , for now it when disabled', 'question');
                 }*/

                //Add currency for dynamic charts
                $('#coin').val(coin);
                $('#currency').val('AED');
                $('.coinnames').html(coin);

                $('#message').empty();

                $('#slash_coin').empty();
                $('#slash_coin').append(coin+'/AED');

                $('#fees_coin').empty();
                $('#fees_coin').append('<b>'+coin+'</b>');

                $('#fees_coin1').empty();
                $('#fees_coin1').append('<b>'+coin+'</b>');



                $('#coinname').empty();
                $('#coinname').append('Amount '+coin);

                $('#coin_use').val(coin);
                $('#chartcontainer').empty();

                $('#order_coin').empty();
                $('#order_coin').append(coin+' /AED');

                var coinx = coin.toLowerCase();

                $('#image_change').empty();$('#image_change').append("<img style='height:50px;width:50px;' src='<?php echo e(url('/')); ?>/assets/themes/limitless/images/icon/"+coinx+".png'>");

                if(coinx == 'btc')
                {   call_aed_btc();  }
                else if(coinx == 'eth')
                {   call_aed_eth(); }
                else if(coinx == 'ltc')
                {   call_aed_ltc(); }
                else if(coinx == 'xrp')
                {   call_aed_xrp(); }
                else
                {  call_aed_btc();  }

                // data();
                // loadcharts(coin, 'AED');

                $('#order_full').empty();
                // $('#order_middle').empty();
                $('#full_block').empty();
                $('#sell_full_coin').empty();
                $('#buy_full_coin').empty();

                // $.ajax({ url:'<?php echo e(url('')); ?>/exchange/order_middle1/'+coin,  type: 'get',
                //     success: function(response) {  $('#order_middle').append(response);  }  });

                // $.ajax({ url:'<?php echo e(url('')); ?>/exchange/call_order_full/'+coin, type: 'get',
                //     success: function(response) { console.log(response); $('#order_full').append(response);  }   });

                // $.ajax({ url:'<?php echo e(url('')); ?>/exchange/order_full_block1/'+coin, type: 'get',
                //     success: function(response) {  $('#full_block').append(response);  }  });

                // $.ajax({ url:'<?php echo e(url('')); ?>/exchange/buy_full_coin1/'+coin, type: 'get',
                //     success: function(response) {  $('#buy_full_coin').append(response);  }  });

                // $.ajax({ url:'<?php echo e(url('')); ?>/exchange/sell_full_coin1/'+coin, type: 'get',
                //     success: function(response) {  $('#sell_full_coin').append(response);  }  });

            }




            function order_tr_call(count,amount,total,price)
            {
                $('#message').empty();
                document.getElementById("buy_button").disabled = false;
                document.getElementById("sell_button").disabled = false;

                $('#usd_amount').val(price);
                $('#coin_amount').val(total);

                var usd_amount = 0;
                var coin_amount = 0;

                usd_amount = $('#usd_amount').val();
                coin_amount = $('#coin_amount').val();

                var multiply = parseFloat(usd_amount) * parseFloat(coin_amount); if(isNaN(multiply)){  multiply=0; }
                $('#approx_usd').empty();
                $('#approx_usd').append('≈ '+multiply);
                $('#final_aed').val(multiply);

                var bfees = '<?php echo e($buy_fees); ?>';
                var sfees = '<?php echo e($sell_fees); ?>';

                var aed_amount = $('#final_aed').val();

                var final_buy_fees = parseFloat(aed_amount) * parseFloat(bfees) / 100;  if(isNaN(final_buy_fees)){  final_buy_fees=0; }
                $('#buy_fees').val(final_buy_fees);

                var final_sell_fees = parseFloat(aed_amount) * parseFloat(sfees) / 100; if(isNaN(final_sell_fees)){  final_sell_fees=0; }
                $('#sell_fees').val(final_sell_fees);
                var coin_name = $('#coin_use').val();

                var pay_total = parseFloat(aed_amount)+parseFloat(final_buy_fees); if(isNaN(pay_total)){  pay_total=0; }
                var rec_total = parseFloat(aed_amount)-parseFloat(final_buy_fees); if(isNaN(rec_total)){  rec_total=0; }

                $("#pay_div").empty(); $("#pay_div").append(pay_total.toFixed(2));
                $("#rec_div").empty(); $("#rec_div").append(rec_total.toFixed(2));

                var bbalance = 0;
                var sbalance = 0;

                bbalance = "<?php echo e($aed_balance); ?>";
                if(coin_name == 'BTC')
                {    sbalance = $('#bal_users_btc').val();  }
                else if(coin_name == 'LTC')
                {    sbalance = $('#bal_users_ltc').val();   }
                else if(coin_name == 'XRP')
                {    sbalance =  $('#bal_users_xrp').val();  }
                else if(coin_name == 'ETH')
                {    sbalance = $('#bal_users_eth').val();   }
                else
                {  }

                //00
                if(parseFloat(aed_amount) < parseFloat(bbalance) && parseFloat(coin_amount) < parseFloat(sbalance))
                {
                    $('#message').empty();
                    document.getElementById("buy_button").disabled = false;
                    document.getElementById("sell_button").disabled = false;
                }//01
                else if(parseFloat(aed_amount) < parseFloat(bbalance) && parseFloat(coin_amount) > parseFloat(sbalance))
                {
                    $('#message').append("<div class='alert alert-danger' role='alert'> Insufficient "+coin_name+" Balance.</div>");
                    document.getElementById("buy_button").disabled = false;
                    document.getElementById("sell_button").disabled = true;
                }//10
                else if(parseFloat(aed_amount) > parseFloat(bbalance) && parseFloat(coin_amount) < parseFloat(sbalance))
                {
                    $('#message').append("<div class='alert alert-danger' role='alert'> Insufficient AED Balance.</div>");
                    document.getElementById("buy_button").disabled = true;
                    document.getElementById("sell_button").disabled = false;
                }//11
                else if(parseFloat(aed_amount) > parseFloat(bbalance) && parseFloat(coin_amount) > parseFloat(sbalance))
                {
                    $('#message').append("<div class='alert alert-danger' role='alert'> Insufficient AED Balance.</div>");
                    $('#message').append("<div class='alert alert-danger' role='alert'> Insufficient "+coin_name+" Balance.</div>");
                    document.getElementById("buy_button").disabled = true;
                    document.getElementById("sell_button").disabled = true;
                }
                else  {  }


            }

            function create_exchange(type)
            {
                $('#message').empty();

                var usd_amount = $('#usd_amount').val();
                var coin_amount = $('#coin_amount').val();
                var order_type = $('#order_type').val();
                var coin_name = $('#coin_use').val();
                var buy_fees = $('#buy_fees').val();
                var sell_fees = $('#sell_fees').val();
                var final_aed = $('#final_aed').val();
                var balance = 0;

                $.ajax({
                    url: '/create_exg',
                    type: 'post',
                    data:{ final_aed:final_aed, buy_fees:buy_fees, sell_fees:sell_fees, type:type, usd_amount:usd_amount, coin_amount:coin_amount, order_type:order_type,coin_name:coin_name,_token:"<?php echo e(csrf_token()); ?>"},
                    success: function(response)
                    {
                        if(response == 1)
                        {    $('#message').append("<div class='alert alert-success' role='alert'> Create "+ type +" Successfully</div>");  }
                        else if(response == 0)
                        {    $('#message').append("<div class='alert alert-danger' role='alert'> Some Problem of create "+type+" </div>");  }
                        else
                        {  }
                    }
                });
            }

            function call_order(option) {
                var t = $('#example12').DataTable();
                t.rows()
                    .remove()
                    .draw();

                $.ajax({
                    url: '<?php echo e(url('')); ?>/exchange/call_order_middle',
                    type: 'post',
                    data: {
                        option: option,
                        _token: "<?php echo e(csrf_token()); ?>"
                    },
                    success: function(response) {
                        var buy = response.data;
                        //Get the total rows
                        $.each(buy, function(index, value) {
                            var t = $('#example12').DataTable();
                            var btn = "<lable onclick='cancel_order_user("+value.id+","+value.order_id+")' class='btn btn-info'>cancel</lable>";
                            var counter = 1;

                            if (value.status == 0) {

                                var row = '<label class="label label-info buycount "> Pending</label>';
                            }
                            else if (value.status == 1) {
                                var row = '<label class="label label-success buycount ">  Success </label>';
                            }
                            else if (value.status == 2) {
                                var row = '<label class="label label-warning buycount ">  Partially </label>';
                            }
                            else if (value.status == 3) {
                                var row = '<label class="label label-danger buycount ">  Cancel </label>';
                            }

                            if (value.side == 'buy') {

                                var rowside = '<label class="label label-info buycount "> Buy</label>';
                            }
                            else if (value.side == 'sell') {
                                var rowside = '<label class="label label-success buycount ">  Sell </label>';
                            }

                            t.row.add([
                                value.id,
                                rowside,
                                value.order_id,
                                value.symbol,
                                value.same_price,
                                value.price,
                                value.buy_fees,
                                value.final_aed,
                                btn,
                                row
                            ]).draw(false);
                        });

                    }

                });
            }

            function call_order_full(option)
            {
                var t = $('#order-his-tbl').DataTable();
                t.rows()
                    .remove()
                    .draw();

                $.ajax({
                    url: '<?php echo e(url('')); ?>/exchange/call_order_full',
                    type: 'post',
                    data:{ option:option,_token:"<?php echo e(csrf_token()); ?>"},
                    success: function(response) {
                        var buy = response.data;
                        //Get the total rows
                        $.each(buy, function(index, value) {
                            var t = $('#order-his-tbl').DataTable();
                            var counter = 1;

                            if (value.status == 0) {

                                var row = '<label class="label label-info buycount "> Pending</label>';
                            }
                            else if (value.status == 1) {
                                var row = '<label class="label label-success buycount ">  Success </label>';
                            }
                            else if (value.status == 2) {
                                var row = '<label class="label label-warning buycount ">  Partially </label>';
                            }
                            else if (value.status == 3) {
                                var row = '<label class="label label-danger buycount ">  Cancel </label>';
                            }

                            if (value.side == 'buy') {

                                var rowside = '<label class="label label-info buycount "> Buy</label>';
                            }
                            else if (value.side == 'sell') {
                                var rowside = '<label class="label label-success buycount ">  Sell </label>';
                            }

                            t.row.add([
                                value.id,
                                rowside,
                                value.order_id,
                                value.symbol,
                                value.same_price,
                                value.price,
                                value.buy_fees,
                                value.final_aed,
                                row
                            ]).draw(false);
                        });

                    }

                });
            }

            function order_full(option)
            {

                var t = $('#order-his-tbl').DataTable();
                t.rows()
                    .remove()
                    .draw();

                $.ajax({
                    url: '<?php echo e(url('')); ?>/exchange/order_full',
                    type: 'post',
                    data:{ option:option,_token:"<?php echo e(csrf_token()); ?>"},
                    success: function(response) {
                        var buy = response.data;
                        //Get the total rows
                        $.each(buy, function(index, value) {
                            var t = $('#order-his-tbl').DataTable();
                            var counter = 1;

                            if (value.status == 0) {

                                var row = '<label class="label label-info buycount "> Pending</label>';
                            }
                            else if (value.status == 1) {
                                var row = '<label class="label label-success buycount ">  Success </label>';
                            }
                            else if (value.status == 2) {
                                var row = '<label class="label label-warning buycount ">  Partially </label>';
                            }
                            else if (value.status == 3) {
                                var row = '<label class="label label-danger buycount ">  Cancel </label>';
                            }

                            if (value.side == 'buy') {

                                var rowside = '<label class="label label-info buycount "> Buy</label>';
                            }
                            else if (value.side == 'sell') {
                                var rowside = '<label class="label label-success buycount ">  Sell </label>';
                            }

                            t.row.add([
                                value.id,
                                rowside,
                                value.order_id,
                                value.symbol,
                                value.same_price,
                                value.price,
                                value.buy_fees,
                                value.final_aed,
                                row
                            ]).draw(false);
                        });

                    }
                });
            }


            function order_middle(option)
            {
                var t = $('#example12').DataTable();
                t.rows()
                    .remove()
                    .draw();

                $('#order_middle').empty();
                $.ajax({
                    url: '<?php echo e(url('')); ?>/exchange/order_middle',
                    type: 'post',
                    data:{ option:option,_token:"<?php echo e(csrf_token()); ?>"},
                    success: function(response) {
                        var buy = response.data;
                        //Get the total rows
                        $.each(buy, function(index, value) {
                            var t = $('#example12').DataTable();
                            var btn = "<lable onclick='cancel_order_user("+value.id+","+value.order_id+")' class='btn btn-info'>cancel</lable>";
                            var counter = 1;

                            if (value.status == 0) {

                                var row = '<label class="label label-info buycount "> Pending</label>';
                            }
                            else if (value.status == 1) {
                                var row = '<label class="label label-success buycount ">  Success </label>';
                            }
                            else if (value.status == 2) {
                                var row = '<label class="label label-warning buycount ">  Partially </label>';
                            }
                            else if (value.status == 3) {
                                var row = '<label class="label label-danger buycount ">  Cancel </label>';
                            }

                            if (value.side == 'buy') {

                                var rowside = '<label class="label label-info buycount "> Buy</label>';
                            }
                            else if (value.side == 'sell') {
                                var rowside = '<label class="label label-success buycount ">  Sell </label>';
                            }

                            t.row.add([
                                value.id,
                                rowside,
                                value.order_id,
                                value.symbol,
                                value.same_price,
                                value.price,
                                value.buy_fees,
                                value.final_aed,
                                btn,
                                row
                            ]).draw(false);
                        });

                    }

                });

            }


            function calculate_price()
            {
                $('#message').empty();
                document.getElementById("buy_button").disabled = false;
                document.getElementById("sell_button").disabled = false;

                var usd_amount = 0;
                var coin_amount = 0;

                usd_amount = $('#usd_amount').val();
                coin_amount = $('#coin_amount').val();

                var multiply = parseFloat(usd_amount) * parseFloat(coin_amount); if(isNaN(multiply)){  multiply=0; }
                $('#approx_usd').empty();
                $('#approx_usd').append('≈ '+multiply.toFixed(4));
                $('#final_aed').val(multiply.toFixed(4));

                var bfees = '<?php echo e($buy_fees); ?>';
                var sfees = '<?php echo e($sell_fees); ?>';
                var aed_amount = $('#final_aed').val();

                var final_buy_fees = parseFloat(aed_amount) * parseFloat(bfees) / 100;  if(isNaN(final_buy_fees)){  final_buy_fees=0; }
                $('#buy_fees').val(final_buy_fees);

                var final_sell_fees = parseFloat(aed_amount) * parseFloat(sfees) / 100; if(isNaN(final_sell_fees)){  final_sell_fees=0; }
                $('#sell_fees').val(final_sell_fees);
                var coin_name = $('#coin_use').val();

                var pay_total = parseFloat(aed_amount)+parseFloat(final_buy_fees); if(isNaN(pay_total)){  pay_total=0; }
                var rec_total = parseFloat(aed_amount)-parseFloat(final_sell_fees); if(isNaN(rec_total)){  rec_total=0; }

                $("#pay_div").empty(); $("#pay_div").append(pay_total.toFixed(2));
                $("#rec_div").empty(); $("#rec_div").append(rec_total.toFixed(2));

                var bbalance = 0;
                var sbalance = 0;

                bbalance = "<?php echo e($aed_balance); ?>";
                if(coin_name == 'BTC')
                {    sbalance = $('#bal_users_btc').val();  }
                else if(coin_name == 'LTC')
                {    sbalance = $('#bal_users_ltc').val();   }
                else if(coin_name == 'XRP')
                {    sbalance =  $('#bal_users_xrp').val();  }
                else if(coin_name == 'ETH')
                {    sbalance = $('#bal_users_eth').val();   }
                else
                {  }

                //00
                if(parseFloat(aed_amount) < parseFloat(bbalance) && parseFloat(coin_amount) < parseFloat(sbalance))
                {
                    $('#message').empty();
                    document.getElementById("buy_button").disabled = false;
                    document.getElementById("sell_button").disabled = false;
                }//01
                else if(parseFloat(aed_amount) < parseFloat(bbalance) && parseFloat(coin_amount) > parseFloat(sbalance))
                {
                    $('#message').append("<div class='alert alert-danger' role='alert'> Insufficient "+coin_name+" Balance.</div>");
                    document.getElementById("buy_button").disabled = false;
                    document.getElementById("sell_button").disabled = true;
                }//10
                else if(parseFloat(aed_amount) > parseFloat(bbalance) && parseFloat(coin_amount) < parseFloat(sbalance))
                {
                    $('#message').append("<div class='alert alert-danger' role='alert'> Insufficient AED Balance.</div>");
                    document.getElementById("buy_button").disabled = true;
                    document.getElementById("sell_button").disabled = false;
                }//11
                else if(parseFloat(aed_amount) > parseFloat(bbalance) && parseFloat(coin_amount) > parseFloat(sbalance))
                {
                    $('#message').append("<div class='alert alert-danger' role='alert'> Insufficient AED Balance.</div>");
                    $('#message').append("<div class='alert alert-danger' role='alert'> Insufficient "+coin_name+" Balance.</div>");
                    document.getElementById("buy_button").disabled = true;
                    document.getElementById("sell_button").disabled = true;
                }
                else  {  }
            }


            $(".card-header-right .full-card").on('click', function() {
                var $this = $(this);
                var port = $($this.parents('.card'));
                port.toggleClass("full-card");
                $(this).toggleClass("icofont-resize");
            });
        </script>


        <!-- expand script js start here -->
        <script>
            function toggleFullScreen() {
                if ((document.fullScreenElement && document.fullScreenElement !== null) ||
                    (!document.mozFullScreen && !document.webkitIsFullScreen)) {
                    if (document.documentElement.requestFullScreen) {
                        document.documentElement.requestFullScreen();
                    } else if (document.documentElement.mozRequestFullScreen) {
                        document.documentElement.mozRequestFullScreen();
                    } else if (document.documentElement.webkitRequestFullScreen) {
                        document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
                    }
                } else {
                    if (document.cancelFullScreen) {
                        document.cancelFullScreen();
                    } else if (document.mozCancelFullScreen) {
                        document.mozCancelFullScreen();
                    } else if (document.webkitCancelFullScreen) {
                        document.webkitCancelFullScreen();
                    }
                }
                /*$(".expand-chart").hide();
                 $(".expand-chart").toggleClass("expand-full");
                 $(".expand-chart-button").html("Minimize");
                 var btntext = $(".expand-chart-button").text();
                 if(btntext == "Minimize"){
                 $(".expand-chart-button").html("Expand");
                 }
                 data();
                 */
            }
            $(".expand-chart-button").on('click', function() {
                var heightw = $( window ).height();
                if ($(this).text() == "Expand"){
                    $(this).text("Minimize");
                    $(".market-depth-sec").children().removeClass("container");
                    $(".market-depth-sec").children().addClass("container-fluid");
                    loadcharts($('#coin_use').val(), 'AED' , heightw);
                }
                else{
                    $(this).text("Expand");
                    $(".market-depth-sec").children().addClass("container");
                    $(".market-depth-sec").children().removeClass("container-fluid");
                    loadcharts($('#coin_use').val(), 'AED' ,500);
                }

            });


        </script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.js"></script>

        


        <script>

            function user_bal()
            {
                $.ajax({
                    /* the route pointing to the post function */
                    url: '<?php echo e(url('exchange/balanceuser')); ?>',
                    type: 'get',
                    /* send the csrf-token and the input to the controller */
                    data: {_token: '<?php echo e(csrf_token()); ?>'},
                    dataType: 'JSON',
                    /* remind that 'data' is the response of the AjaxController */
                    success: function (data) {

                        $('#btc_bal_set').html(parseFloat(data.BTC).toFixed(6));
                        $('#eth_bal_set').html(parseFloat(data.ETH).toFixed(4));
                        $('#ltc_bal_set').html(parseFloat(data.LTC).toFixed(4));
                        $('#xrp_bal_set').html(parseFloat(data.XRP).toFixed(4));
                        $('#aed_bal_set').html(parseFloat(data.AED).toFixed(4));

                        $('#bal_users_btc').val(parseFloat(data.BTC).toFixed(6));
                        $('#bal_users_eth').val(parseFloat(data.ETH).toFixed(4));
                        $('#bal_users_ltc').val(parseFloat(data.LTC).toFixed(4));
                        $('#bal_users_xrp').val(parseFloat(data.XRP).toFixed(4));
                        $('#bal_users_aed').val(parseFloat(data.AED).toFixed(4));

                    }
                });
                var coin = $('#coin_use').val();
                // alert(coin);
            }

            function setvalue(aed_amount,coin_amount){
                $('#usd_amount').val(aed_amount);
                $('#coin_amount').val(coin_amount);

                document.getElementById("buy_button").disabled = false;
                document.getElementById("sell_button").disabled = false;


                var buy_fees =<?php echo e($buy_fees); ?>;
                var sell_fees =<?php echo e($sell_fees); ?>;
                $('#counter').val(aed_amount);
                $('#counter1').val(coin_amount);
                $('#buy_fees ').val((((aed_amount*coin_amount)*buy_fees)/100).toFixed(4));
                $('#sell_fees ').val((((aed_amount*coin_amount)*sell_fees)/100).toFixed(4));
                $('#approx_usd').html('≈ '+(aed_amount*coin_amount).toFixed(4));
                $('#pay_div').html((((aed_amount*coin_amount)*buy_fees/100)+(aed_amount*coin_amount)).toFixed(4))
                $('#rec_div').html((((aed_amount*coin_amount)*sell_fees/100)-(aed_amount*coin_amount)).toFixed(4))
            }

          function cancel_order_user(id,order_id) {
        $.ajax({
            /* the route pointing to the post function */
            url: '<?php echo e(url('cancelorder')); ?>',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {_token: '<?php echo e(csrf_token()); ?>',id:id,order_id:order_id},
            dataType: 'JSON',
            /* remind that 'data' is the response of the AjaxController */
            success: function (data) {

                if(data.reason == 'invalid action')
                {
                    toastr.success('invalid action');
                }
                else{
                    toastr.success('Order cancelled');
                }
            }
        });
        var coin = $('#coin_use').val();
        // alert(coin);
        call_order_full(coin);
        call_order(coin);
        user_bal();
    }


            function buy_order(){

                var USD = $('#usd_amount').val();
                var BTC = $('#coin_amount').val();
                var type = $('#coin').val();
                var order_type = $('#order_type').val();

                $.ajax({
                    /* the route pointing to the post function */
                    url: '<?php echo e(url(('placeordersbuy'))); ?>',
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    data: {_token: '<?php echo e(csrf_token()); ?>',USD: USD, COIN: BTC ,type:type,order_type:order_type},
                    dataType: 'JSON',
                    async:true,
                    /* remind that 'data' is the response of the AjaxController */
                    success: function (data) {

                        if(data.reason == "order place successfully")
                        {
                            toastr.success('Order placed successfully');
                            var coin = $('#coin_use').val();
                            // alert(coin);
                            call_order_full(coin);
                            call_order(coin);
                            user_bal();
                        }
                        if(data.reason == "insufficient balance")
                        {
                            toastr.warning('Insufficient balance');
                            var coin = $('#coin_use').val();
                            // alert(coin);
                            call_order_full(coin);
                            call_order(coin);
                            user_bal();
                        }
                        if (data.reason.__all__.includes("account balance")) {

                            toastr.warning('Invalid amount entered');
                        }
                        else if (data.reason.__all__) {
                            var error = data.reason.__all__;
                            if(error.indexOf('account balance') >= -1)
                                toastr.warning('Invalid amount entered');
                            else
                                toastr.error('An error occured. Please try again');
                        }
                        else {
                            toastr.error('An error occured. Please try again');
                            var coin = $('#coin_use').val();
                            // alert(coin);
                            call_order_full(coin);
                            call_order(coin);
                            user_bal();

                        }

                    }
                });
            }
            function sell_order(){

                var USD = $('#usd_amount').val();
                var BTC = $('#coin_amount').val();
                var type = $('#coin').val();
                var order_type = $('#order_type').val();


                $.ajax({
                    /* the route pointing to the post function */
                    url: '<?php echo e(url(('placeordersell'))); ?>',
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    data: {_token: '<?php echo e(csrf_token()); ?>',USD: USD, COIN: BTC ,type:type,order_type:order_type},
                    dataType: 'JSON',
                    async:true,

                    /* remind that 'data' is the response of the AjaxController */
                    success: function (data) {

                        if(data.reason == "order place successfully")
                        {toastr.success('Order placed successfully');
                            var coin = $('#coin_use').val();
                            // alert(coin);
                            call_order_full(coin);
                            call_order(coin);
                            user_bal();
                        }
                        if(data.reason == "insufficient balance")
                        {
                            toastr.warning('Insufficient balance');
                            var coin = $('#coin_use').val();
                            // alert(coin);
                            call_order_full(coin);
                            call_order(coin);
                            user_bal();
                        }

                        else if (data.reason.__all__) {
                            toastr.error('1', data.reason.__all__);
                            var coin = $('#coin_use').val();
                            // alert(coin);
                            call_order_full(coin);
                            call_order(coin);
                            user_bal();
                        }
                        else if (data.reason.limit_price) {
                            toastr.error(data.reason.limit_price);
                            var coin = $('#coin_use').val();
                            // alert(coin);
                            call_order_full(coin);
                            call_order(coin);
                            user_bal();
                        }
                        else {
                            toastr.error('An error occured. Please try again', data.reason.__all__);
                            var coin = $('#coin_use').val();
                            // alert(coin);
                            call_order_full(coin);
                            call_order(coin);
                            user_bal();

                        }
                    }
                });
            }
            document.getElementById("buy_button").disabled = true;
            document.getElementById("sell_button").disabled = true;
            $('.coinnames').html('BTC');

        </script>

        <script>
            function tradehistory(coin) {
                $.get('<?php echo e(url('treadehistory')); ?>'+'/'+coin, function(response){
                    $("#Trades_Block").html(response);

                    var $el = $("#typecall");
                    function anim() {
                        var st = $el.scrollTop();
                        var sb = $el.prop("scrollHeight")-$el.innerHeight();
                        $el.animate({scrollTop: st<sb/2 ? sb : 0}, 4000, anim);
                    }
                    function stop(){
                        $el.stop();
                    }
                    anim();
                    $el.hover(stop, anim);
                });
            }

            function liveorders(currentcoin) {
                if (currentcoin == "BTC") {
                    currrent = "live_trades";
                }
                else if (currentcoin == "ETH") {
                    currrent = "live_trades_ethusd";
                }
                else if (currentcoin == "XRP") {
                    currrent = "live_trades_xrpusd";
                }
                else if (currentcoin == "LTC") {
                    currrent = "live_trades_ltcusd";
                }



                var placeholder = $('#live_trade');
                var pusher = new Pusher('de504dc5763aeef9ff52');
                // console.log(pusher);
                ordersChannelnew = pusher.subscribe(currrent);
                $.each(['trade'], function (eventIndex, eventName) {

                    ordersChannelnew.bind(eventName, function (data) {
                        if ($('#live_trade tr').length > 21) {
                            $('#live_trade tr:first-child').remove();
                        }
                        placeholder.append('<tr><td>' + ((data.type == 0) ? 'BUY' : 'SELL') + '</td><td> ' + data.amount + '  </td><td> ' + parseFloat((data.price * <?php echo e($aed_rate); ?>)).toFixed(4) + ' AED ' + '</td></tr>');
                    });
                });

            };

            setInterval(function () {
                getMessage();
            }, 60 *60);

            function getMessage() {
                var table = $('#example12').dataTable();
                var numItems = table.fnGetData().length;
                $.ajax({
                    type: 'post',
                    url: '<?php echo e(url('buycountuser')); ?>',
                    data: {_token: '<?php echo e(csrf_token()); ?>',number:numItems},
                    success: function (data) {

                        if(data.error == 'true')
                        {
                            var coin = $('#coin_use').val();
                            // alert(coin);
                            call_order_full(coin);
                            call_order(coin);
                            user_bal();
                        }
                    }

                });
            }


            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (  charCode > 31 && ( charCode < 46 || charCode > 57)) {
                    document.getElementById("buy_button").disabled = true;
                    document.getElementById("sell_button").disabled = true;
                    return false;
                }
                else {
                    if(charCode == 47){
                        document.getElementById("buy_button").disabled = true;
                        document.getElementById("sell_button").disabled = true;
                        return false;
                    }
                    return true;

                }
            }

        </script>
<script type="text/javascript">
// Generated by CoffeeScript 1.9.3
(function() {
  $(function() {
    var brick;
    brick = "<div class='brick small'><h3>AMBUJACEM</h3><h3>NSE EQ</h3><div class='abbr-wrap row'><div class='smbl text-left col-xs-6'>MTM</div><div class='smbl text-right col-xs-6'>228.65</div></div><div class='abbr-wrap row'><div class='smbl text-left col-xs-6'>0</div><div class='smbl text-right col-xs-6'>0</div></div><div class='buy-sell row'><div class='value-blk'> 1</div><div class='grey-sell text-left col-xs-6'><div style='margin-top: 10px;'>0</div><div class='sell-itm'>Sell</div></div><div class='green-buy text-right col-xs-6'><div style='margin-top: 10px;'>228.65</div><div class='buy-itm'>Buy</div></div></div><a class='delete' href='#'>&times;</a></div>";
    $(document).on("click touchend", ".gridly .brick", function(event) {
      var $this, size;
      event.preventDefault();
      event.stopPropagation();
      $this = $(this);
      $this.toggleClass('small');
      $this.toggleClass('large');
      if ($this.hasClass('small')) {
        size = 180;
      }
      if ($this.hasClass('large')) {
        size = 180;
      }
      $this.data('width', size);
      $this.data('height', size);
      return $('.gridly').gridly('layout');
    });
    $(document).on("click", ".gridly .delete", function(event) {
      var $this;
      event.preventDefault();
      event.stopPropagation();
      $this = $(this);
      $this.closest('.brick').remove();
      return $('.gridly').gridly('layout');
    });
    $(document).on("click", ".add", function(event) {
      event.preventDefault();
      event.stopPropagation();
      $('.gridly').append(brick);
      return $('.gridly').gridly();
    });
    return $('.gridly').gridly();
  });

}).call(this);
</script>
 <script type="text/javascript">
      
      /*  var gridster1
        var gridster2
        var gridster3
        $(document).ready(function () {
            
             gridster1 = $("#books ul").gridster({
                namespace: '#books',
                widget_base_dimensions: ['auto', 305],
                autogenerate_stylesheet: true,
                min_cols: 1,
                max_cols: 6,
               
                widget_margins: [10, 10],
                resize: {
                    enabled: true
                }
            }).data('gridster');

            gridster2 = $("#chartsContent ul").gridster({
                namespace: '#chartsContent',
                widget_base_dimensions: ['auto', 305],
                autogenerate_stylesheet: true,
                min_cols: 1,
                max_cols: 6,
               
                widget_margins: [10, 10],
                resize: {
                    enabled: true
                }
            }).data('gridster');

            gridster3 = $("#quotesContent ul").gridster({
                namespace: '#quotesContent',
                widget_base_dimensions: ['auto', 305],
                autogenerate_stylesheet: true,
                min_cols: 1,
                max_cols: 6,
               
                widget_margins: [10, 10],
                resize: {
                    enabled: false
                }
            }).data('gridster');            

          $('.gridster  ul').css({'padding': '0'});
            $(document).on( "click", "#addWidgetButton", function(e) {
                 e.preventDefault(); 
                 gridster.add_widget.apply(gridster, ['<li><div class="theme-widget balance-widget open"><div class="widget-header acr"><h5>New Widget</h5></div><div class="widget-body "><div class="table-responsive"></div></div></div></li>', 1, 2]);
            });
            $(".removeWidget").click(function(e){
                var $div=$(e.target).parent();
                //removed the selected widget
                gridster.remove_widget($div);
              });

        });*/



$('#grayButton').on('click', function () {
   
    if ($('body').hasClass('gray')) {
        $('body').attr('class', 'dashboard overFlowH');
        $('img.logo-white').attr('src','<?php echo e(asset("assets/themes/limitless/images/bitex-logo.png")); ?>');
        $('.fa-sun-o').attr('class', 'fa fa-moon-o');
    } else {
       
         $('body').attr('class', 'gray dashboard overFlowH');
         $('img.logo-white').attr('src','<?php echo e(asset("assets/themes/limitless/images/bitex-white-2.png")); ?>');
         $('.fa-moon-o').attr('class', 'fa fa-sun-o');
    }

    //$("#header").animate({ "marginTop": mTop });
});

  
$('#myModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var hiddenVal = button.data('hidden')
  //alert(hiddenVal);
  var modal = $(this)
  //modal.find('.modal-body #image').attr('src',recipient)
})
 
   
    </script>

</body>
</html>