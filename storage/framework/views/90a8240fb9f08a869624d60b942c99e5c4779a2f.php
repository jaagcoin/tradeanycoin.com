
<?php $__env->startSection('title'); ?>
    <?php echo e(trans_choice('general.trade',1)); ?> <?php echo e(trans_choice('general.currency',2)); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="pageContent">
<div class="container">
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title"> <?php echo e(trans_choice('general.trade',1)); ?> <?php echo e(trans_choice('general.currency',2)); ?></h6>

            <div class="heading-elements">

            </div>
        </div>
        <div class="panel-body ">
            <div class="table-responsive">
                <table id="data-table" class="table table-striped table-condensed table-hover">
                    <thead>
                    <tr>
                        <th><?php echo e(trans_choice('general.id',1)); ?></th>
                        <th><?php echo e(trans_choice('general.logo',1)); ?></th>
                        <th><?php echo e(trans_choice('general.currency',1)); ?></th>
                        <th><?php echo e(trans_choice('general.type',1)); ?></th>
                        <th><?php echo e(trans_choice('general.fee',1)); ?></th>
                        <th><?php echo e(trans_choice('general.status',1)); ?></th>
                        <th><?php echo e(trans_choice('general.action',1)); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($key->id); ?></td>
                            <td>
                                <?php if(empty($key->logo)): ?>
                                    <img src="<?php echo e(asset('uploads/no_image.png')); ?>" width="33">
                                <?php else: ?>
                                    <img src="<?php echo e(asset('uploads/'.$key->logo)); ?>" width="33">
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php echo e($key->name); ?>

                                <?php if($key->default_currency==1): ?>
                                    <span class="label label-success"><?php echo e(trans_choice('general.default',1)); ?></span>
                                <?php endif; ?>
                            </td>
                            <td><?php echo e($key->xml_code); ?></td>
                            <td>
                                <?php if($key->fee_method=="fixed"): ?>
                                    <b><?php echo e(trans_choice('general.deposit_fixed_fee',1)); ?>

                                        :</b>   <?php echo e(round($key->deposit_fixed_fee,$key->decimals)); ?><br>
                                    <b><?php echo e(trans_choice('general.withdrawal_fixed_fee',1)); ?>

                                        :</b>   <?php echo e(round($key->withdrawal_fixed_fee,$key->decimals)); ?><br>
                                    <b><?php echo e(trans_choice('general.commission_fixed_fee',1)); ?>

                                        :</b>   <?php echo e(round($key->commission_fixed_fee,$key->decimals)); ?><br>
                                <?php endif; ?>
                                <?php if($key->fee_method=="percentage"): ?>
                                    <b><?php echo e(trans_choice('general.deposit_percentage_fee',1)); ?>

                                        :</b>   <?php echo e(round($key->deposit_percentage_fee,$key->decimals)); ?>%<br>
                                    <b><?php echo e(trans_choice('general.withdrawal_percentage_fee',1)); ?>

                                        :</b>   <?php echo e(round($key->withdrawal_percentage_fee,$key->decimals)); ?>%<br>
                                    <b><?php echo e(trans_choice('general.commission_percentage_fee',1)); ?>

                                        :</b>   <?php echo e(round($key->commission_percentage_fee,$key->decimals)); ?>%<br>
                                <?php endif; ?>
                                <?php if($key->fee_method=="both"): ?>
                                    <b><?php echo e(trans_choice('general.deposit_fee',1)); ?>

                                        :</b><?php echo e(round($key->deposit_fixed_fee,$key->decimals)); ?>

                                    +   <?php echo e(round($key->deposit_percentage_fee,$key->decimals)); ?>%<br>
                                    <b><?php echo e(trans_choice('general.withdrawal_fee',1)); ?>

                                        :</b> <?php echo e(round($key->withdrawal_fixed_fee,$key->decimals)); ?>

                                    +  <?php echo e(round($key->withdrawal_percentage_fee,$key->decimals)); ?>%<br>
                                    <b><?php echo e(trans_choice('general.commission_fee',1)); ?>

                                        :</b> <?php echo e(round($key->commission_fixed_fee,$key->decimals)); ?>

                                    +   <?php echo e(round($key->commission_percentage_fee,$key->decimals)); ?>%<br>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if($key->active==1): ?>
                                    <span class="label label-success"><?php echo e(trans_choice('general.active',1)); ?></span>
                                <?php endif; ?>
                                <?php if($key->active==0): ?>
                                    <span class="label label-warning"><?php echo e(trans_choice('general.inactive',1)); ?></span>
                                <?php endif; ?>
                            </td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                            <?php if(Sentinel::hasAccess('currencies.update')): ?>
                                                <li><a href="<?php echo e(url('trade_currency/'.$key->id.'/edit')); ?>"><i
                                                                class="fa fa-edit"></i> <?php echo e(trans('general.edit')); ?> </a>
                                                </li>
                                            <?php endif; ?>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.panel-body -->
    </div>
</div>
</div>
    <!-- /.box -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer-scripts'); ?>
    <script>
        $('#data-table').DataTable({
            "order": [[0, "asc"]],
            "columnDefs": [
                {"orderable": false, "targets": [6]}
            ],
            "language": {
                "lengthMenu": "<?php echo e(trans('general.lengthMenu')); ?>",
                "zeroRecords": "<?php echo e(trans('general.zeroRecords')); ?>",
                "info": "<?php echo e(trans('general.info')); ?>",
                "infoEmpty": "<?php echo e(trans('general.infoEmpty')); ?>",
                "search": "<?php echo e(trans('general.search')); ?>",
                "infoFiltered": "<?php echo e(trans('general.infoFiltered')); ?>",
                "paginate": {
                    "first": "<?php echo e(trans('general.first')); ?>",
                    "last": "<?php echo e(trans('general.last')); ?>",
                    "next": "<?php echo e(trans('general.next')); ?>",
                    "previous": "<?php echo e(trans('general.previous')); ?>"
                }
            },
            responsive: false
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>