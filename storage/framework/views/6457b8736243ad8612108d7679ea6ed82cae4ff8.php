
<?php $__env->startSection('title'); ?><?php echo e(trans_choice('general.sent',1)); ?> <?php echo e(trans_choice('general.email',2)); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
        <!-- Default box -->
<div class="pageContent">
    <div class="container">
<div class="panel panel-white">
    <div class="panel-heading">
        <h6 class="panel-title"><?php echo e(trans_choice('general.sent',1)); ?> <?php echo e(trans_choice('general.email',2)); ?></h6>

        <div class="heading-elements">
            <?php if(Sentinel::hasAccess('communication.create')): ?>
                <a href="<?php echo e(url('communication/email/create')); ?>"
                   class="btn btn-info btn-sm"><?php echo e(trans_choice('general.send',1)); ?> <?php echo e(trans_choice('general.email',1)); ?></a>
            <?php endif; ?>
        </div>
    </div>
    <div class="panel-body table-responsive">
        <table id="data-table" class="table table-striped table-condensed table-hover">
            <thead>
            <tr>
                <th><?php echo e(trans_choice('general.send_by',1)); ?></th>
                <th><?php echo e(trans_choice('general.to',1)); ?></th>
                <th><?php echo e(trans_choice('general.recipient',2)); ?></th>
                <th><?php echo e(trans_choice('general.message',1)); ?></th>
                <th><?php echo e(trans_choice('general.date',1)); ?></th>
                <th><?php echo e(trans_choice('general.action',1)); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td>
                        <?php if(!empty($key->user)): ?>
                            <a href="<?php echo e(url('user/'.$key->user_id.'/show')); ?>"><?php echo e($key->user->first_name); ?> <?php echo e($key->user->last_name); ?></a>
                        <?php else: ?>

                        <?php endif; ?>
                    </td>
                    <td><?php echo e($key->send_to); ?></td>
                    <td><?php echo e($key->recipients); ?></td>
                    <td><?php echo $key->message; ?></td>
                    <td><?php echo e($key->created_at); ?></td>
                    <td>
                        <?php if(Sentinel::hasAccess('communication.delete')): ?>
                            <a href="<?php echo e(url('communication/email/'.$key->id.'/delete')); ?>"
                               class="delete"><i
                                        class="fa fa-trash"></i> </a>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
    <!-- /.panel-body -->
</div>
</div>
</div>
<!-- /.box -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer-scripts'); ?>
    <script>

        $('#data-table').DataTable({
            "order": [[4, "desc"]],
            "columnDefs": [
                {"orderable": false, "targets": [5]}
            ],
            "language": {
                "lengthMenu": "<?php echo e(trans('general.lengthMenu')); ?>",
                "zeroRecords": "<?php echo e(trans('general.zeroRecords')); ?>",
                "info": "<?php echo e(trans('general.info')); ?>",
                "infoEmpty": "<?php echo e(trans('general.infoEmpty')); ?>",
                "search": "<?php echo e(trans('general.search')); ?>",
                "infoFiltered": "<?php echo e(trans('general.infoFiltered')); ?>",
                "paginate": {
                    "first": "<?php echo e(trans('general.first')); ?>",
                    "last": "<?php echo e(trans('general.last')); ?>",
                    "next": "<?php echo e(trans('general.next')); ?>",
                    "previous": "<?php echo e(trans('general.previous')); ?>"
                }
            }
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>