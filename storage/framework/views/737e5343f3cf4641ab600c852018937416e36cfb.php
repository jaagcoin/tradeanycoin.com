
<?php $__env->startSection('title'); ?>
    <?php echo e(trans_choice('general.bank',1)); ?> <?php echo e(trans_choice('general.account',2)); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title"> <?php echo e(trans_choice('general.bank',1)); ?> <?php echo e(trans_choice('general.account',2)); ?></h6>

            <div class="heading-elements">
                <a href="<?php echo e(url('user_bank_account/create')); ?>" class="btn btn-info btn-xs">
                    <?php echo e(trans_choice('general.add',1)); ?> <?php echo e(trans_choice('general.account',1)); ?>

                </a>
            </div>
        </div>
        <div class="panel-body ">
            <div class="table-responsive">
                <table id="data-table" class="table table-striped table-condensed table-hover">
                    <thead>
                    <tr>
                        <th><?php echo e(trans_choice('general.bank',1)); ?></th>
                        <th><?php echo e(trans_choice('general.account',1)); ?> <?php echo e(trans_choice('general.name',1)); ?></th>
                        <th><?php echo e(trans_choice('general.account',1)); ?> <?php echo e(trans_choice('general.number',1)); ?></th>
                        <th><?php echo e(trans_choice('general.agency_number',1)); ?></th>
                        <th><?php echo e(trans_choice('general.cpf_number',1)); ?></th>
                        <th><?php echo e(trans_choice('general.status',1)); ?></th>
                        <th><?php echo e(trans_choice('general.action',1)); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td>
                                <?php if(!empty($key->withdrawal_method)): ?>
                                    <?php echo e($key->withdrawal_method->name); ?>

                                <?php endif; ?>
                            </td>
                            <td><?php echo e($key->account_name); ?></td>
                            <td>
                                <?php echo e($key->account_number); ?>

                                <?php if($key->default_account==1): ?>
                                    <span class="label label-success" data-toggle="tooltip" title="Default"><i
                                                class="fa fa-check"></i> </span>
                                <?php endif; ?>
                            </td>
                            <td><?php echo e($key->agency_number); ?></td>
                            <td><?php echo e($key->cpf_number); ?></td>
                            <td>
                                <?php if($key->active==1): ?>
                                    <span class="label label-success"><?php echo e(trans_choice('general.active',1)); ?></span>
                                <?php endif; ?>
                                <?php if($key->active==0): ?>
                                    <span class="label label-warning"><?php echo e(trans_choice('general.inactive',1)); ?></span>
                                <?php endif; ?>
                            </td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right" role="menu">

                                            <li><a href="<?php echo e(url('user_bank_account/'.$key->id.'/edit')); ?>"><i
                                                            class="fa fa-edit"></i> <?php echo e(trans('general.edit')); ?> </a>
                                            </li>
                                            <li><a href="<?php echo e(url('user_bank_account/'.$key->id.'/delete')); ?>"
                                                   class="delete"><i
                                                            class="fa fa-trash"></i> <?php echo e(trans('general.delete')); ?>

                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.box -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer-scripts'); ?>
    <script>
        $('#data-table').DataTable({
            "order": [[0, "asc"]],
            "columnDefs": [
                {"orderable": false, "targets": [6]}
            ],
            "language": {
                "lengthMenu": "<?php echo e(trans('general.lengthMenu')); ?>",
                "zeroRecords": "<?php echo e(trans('general.zeroRecords')); ?>",
                "info": "<?php echo e(trans('general.info')); ?>",
                "infoEmpty": "<?php echo e(trans('general.infoEmpty')); ?>",
                "search": "<?php echo e(trans('general.search')); ?>",
                "infoFiltered": "<?php echo e(trans('general.infoFiltered')); ?>",
                "paginate": {
                    "first": "<?php echo e(trans('general.first')); ?>",
                    "last": "<?php echo e(trans('general.last')); ?>",
                    "next": "<?php echo e(trans('general.next')); ?>",
                    "previous": "<?php echo e(trans('general.previous')); ?>"
                }
            },
            responsive: false
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>