
<?php $__env->startSection('title'); ?>
    <?php echo e(trans_choice('general.verify',1)); ?> <?php echo e(trans_choice('general.document',2)); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title"> <?php echo e(trans_choice('general.verify',1)); ?> <?php echo e(trans_choice('general.document',2)); ?></h6>

            <div class="heading-elements">

            </div>
        </div>

        <div class="panel-body">

            <?php if(Sentinel::getUser()->documents_verified==1): ?>
                <div class="alert bg-success alert-bordered">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                class="sr-only">Close</span></button>
                    <?php echo e(trans('general.documents_verified')); ?>

                </div>
            <?php elseif(Sentinel::getUser()->proof_of_residence_picture == ""): ?>
                <div class="alert alert-primary alert-bordered">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                class="sr-only">Close</span></button>
                    <?php echo e(trans('general.verify_documents_dob_mag')); ?>

                </div>
            <?php else: ?>
                <div class="alert alert-primary alert-bordered">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                class="sr-only">Close</span></button>
                    <?php echo e(trans('general.verify_documents_msg')); ?>

                </div>
            <?php endif; ?>

            <?php echo Form::open(array('url' => 'user/verify_documents/check_documents','class'=>'verify-documents',"enctype" => "multipart/form-data")); ?>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group" id="">
                        <?php echo Form::label('first_name',trans('general.first_name'),array('class'=>'control-label')); ?>

                        <?php echo Form::text('first_name',$user->first_name,array('class'=>'form-control','required'=>'required','id'=>'first_name')); ?>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" id="">
                        <?php echo Form::label('last_name',trans('general.last_name'),array('class'=>'control-label')); ?>

                        <?php echo Form::text('last_name',$user->last_name,array('class'=>'form-control','required'=>'required','id'=>'last_name')); ?>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group" id="">
                        <?php echo Form::label('gender',trans('general.gender'),array('class'=>'control-label')); ?>

                        <?php echo Form::select('gender', array('male' =>trans('general.male'), 'female' => trans('general.female')),$user->gender,array('class'=>'form-control')); ?>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" id="">
                        <?php echo Form::label('dob',trans('general.dob'),array('class'=>'control-label')); ?>

                        <?php echo Form::text('dob',$user->dob,array('class'=>'form-control date-picker','required'=>'required','id'=>'dob')); ?>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group" id="">
                        <?php echo Form::label('country_id',trans('general.country'),array('class'=>'control-label')); ?>

                        <?php echo Form::select('country_id',$countries,$user->country_id,array('class'=>'form-control select2','required'=>'required','id'=>'country')); ?>

                    </div>
                </div>
            </div>

                <?php   $street=''; $street_2 = ''; $city=''; $state=''; $postcode = ''; $country=''; ?>
                <?php if(Sentinel::getUser()->address): ?>
                  <?php
                    $address = explode("|", Sentinel::getUser()->address);
                    $street = $address[0];
                    $street_2 = $address[1];
                    $city = $address[2];
                    $state = $address[3];
                    $postcode = $address[4];
                  ?>
                <?php endif; ?>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group" id="">
                        <?php echo Form::label('city',trans('general.city'),array('class'=>'control-label')); ?>

                        <?php echo Form::text('city',$city,array('class'=>'form-control','required'=>'required','id'=>'city')); ?>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" id="">
                        <?php echo Form::label('address',trans('general.address'),array('class'=>'control-label')); ?>

                        <?php echo Form::text('address',$street,array('class'=>'form-control','required'=>'required','id'=>'address','rows'=>'2')); ?>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group" id="">
                        <?php echo Form::label('zip',trans('general.zip'),array('class'=>'control-label')); ?>

                        <?php echo Form::text('zip',$postcode,array('class'=>'form-control','id'=>'zip')); ?>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" id="">
                        <?php echo Form::label('id_type',trans_choice('general.id',1)." ".trans_choice('general.type',1),array('class'=>'control-label')); ?>

                        <?php echo Form::select('id_type', array('id_card' =>trans('general.id_card'), 'passport' => trans('general.passport'), 'driver_license' => trans('general.driver_license')),$user->id_type,array('class'=>'form-control','required'=>'required','id'=>'id_type')); ?>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group" id="">
                        <?php echo Form::label('id_number',trans('general.id_number'),array('class'=>'control-label')); ?>

                        <?php echo Form::text('id_number',$user->id_number,array('class'=>'form-control','required'=>'required','id'=>'id_number')); ?>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" id="">

                        <?php echo Form::label('id_picture',trans_choice('general.id',1)." ".trans_choice('general.picture',1),array('class'=>'control-label')); ?>

                        <?php if(!empty($user->id_picture)): ?>
                            <p>Current picture: <a href="<?php echo e(asset('uploads/'.$user->id_picture)); ?>"
                                                   target="_blank"><?php echo e($user->id_picture); ?></a></p>
                            <?php echo Form::file('id_picture',array('class'=>'form-control','id'=>'id_picture')); ?>

                        <?php else: ?>
                            <?php echo Form::file('id_picture',array('class'=>'form-control','required'=>'required','id'=>'id_picture')); ?>

                        <?php endif; ?>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group" id="">
                        <?php echo Form::label('proof_of_residence_type',trans_choice('general.proof_of_residence_type',1),array('class'=>'control-label')); ?>

                        <?php echo Form::select('proof_of_residence_type', array('bank_statement' =>trans('general.bank_statement'), 'utility_bill' => trans('general.utility_bill')),$user->proof_of_residence_type,array('class'=>'form-control','required'=>'required','id'=>'proof_of_residence_type')); ?>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" id="">
                        <?php echo Form::label('proof_of_residence_picture',trans_choice('general.proof_of_residence_picture',1),array('class'=>'control-label')); ?>

                        <?php if(!empty($user->proof_of_residence_picture)): ?>
                            <p>Current picture: <a href="<?php echo e(asset('uploads/'.$user->proof_of_residence_picture)); ?>"
                                                   target="_blank"><?php echo e($user->proof_of_residence_picture); ?></a></p>
                            <?php echo Form::file('proof_of_residence_picture',array('class'=>'form-control','id'=>'proof_of_residence_picture')); ?>

                        <?php else: ?>
                            <?php echo Form::file('proof_of_residence_picture',array('class'=>'form-control','required'=>'required','id'=>'proof_of_residence_picture')); ?>

                        <?php endif; ?>

                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary pull-right"><?php echo e(trans_choice('general.verify',1)); ?></button>
            <?php echo Form::close(); ?>



        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer-scripts'); ?>
    <script>

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>