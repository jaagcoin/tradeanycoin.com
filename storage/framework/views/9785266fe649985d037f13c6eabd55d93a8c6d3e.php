
<?php $__env->startSection('title'); ?>
    <?php echo e(trans_choice('general.add',1)); ?> <?php echo e(trans_choice('general.account',1)); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title"><?php echo e(trans_choice('general.add',1)); ?> <?php echo e(trans_choice('general.account',1)); ?></h6>
            <div class="heading-elements">

            </div>
        </div>
        <?php echo Form::open(array('url' => url('user_bank_account/store'), 'method' => 'post', 'name' => 'form')); ?>

        <div class="panel-body">
            <div class="form-group">
                <?php echo Form::label('withdrawal_method_id',trans_choice('general.bank',1).' '.trans_choice('general.account',1),array('class'=>'')); ?>

                <?php echo Form::select('withdrawal_method_id',$withdrawal_methods,null, array('class' => 'form-control',null,'required'=>'required','placeholder'=>'')); ?>

            </div>
            <div class="form-group">
                <?php echo Form::label('account_name',trans_choice('general.account',1)." ".trans_choice('general.name',1),array('class'=>'')); ?>

                <?php echo Form::text('account_name',null, array('class' => 'form-control', 'placeholder'=>'','required'=>'required')); ?>

            </div>
            <div class="form-group">
                <?php echo Form::label('account_number',trans_choice('general.account',1)." ".trans_choice('general.number',1),array('class'=>'')); ?>

                <?php echo Form::text('account_number',null, array('class' => 'form-control', 'placeholder'=>'','required'=>'required')); ?>

            </div>
            <div class="form-group">
                <?php echo Form::label('agency_number',trans_choice('general.agency_number',1),array('class'=>'')); ?>

                <?php echo Form::text('agency_number',null, array('class' => 'form-control', 'placeholder'=>'')); ?>

            </div>
            <div class="form-group">
                <?php echo Form::label('cpf_number',trans_choice('general.cpf_number',1),array('class'=>'')); ?>

                <?php echo Form::text('cpf_number',null, array('class' => 'form-control', 'placeholder'=>'')); ?>

            </div>
            <div class="form-group">
                <?php echo Form::label('default_account',trans_choice('general.default',1),array('class'=>'')); ?>

                <?php echo Form::select('default_account',array('1'=>trans_choice('general.yes',1),'0'=>trans_choice('general.no',1)),1, array('class' => 'form-control',)); ?>

            </div>
        </div>
        <!-- /.panel-body -->
        <div class="panel-footer">
            <div class="heading-elements">
                <button type="submit" class="btn btn-primary pull-right"><?php echo e(trans_choice('general.save',1)); ?></button>
            </div>
        </div>
        <?php echo Form::close(); ?>

    </div>
    <!-- /.box -->
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>