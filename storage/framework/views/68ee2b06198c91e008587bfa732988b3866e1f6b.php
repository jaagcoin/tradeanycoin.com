<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trade any Coin - Exchange</title>

<!-- Bootstrap CSS CDN -->
<link rel="stylesheet" href="http://localhost:8000/assets/bootstrap/css/bootstrap.min.css" >
<!-- Our Custom CSS -->
<link rel="stylesheet" href="http://localhost:8000/assets/css/style.css">
<!-- Scrollbar Custom CSS -->
<link rel="stylesheet" href="http://localhost:8000/assets/css/jquery.mCustomScrollbar.min.css">

<!-- Font Awesome JS -->
<script defer src="http://localhost:8000/assets/js/solid.js"></script>
<script defer src="http://localhost:8000/assets/js/fontawesome.js"></script>
</head>

<body  class="login">
<div class="page-wrapper flex-row align-items-center">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-5 no-padding-right">
        <div class="card p-4">
          <div class="card-body ">
            <form class="login-form" action="#" method="post">
              <div id="forgetpassword">
                <div class="h2"> Reset your account password</div>
                <?php if(Session::has('flash_notification.message')): ?>
                        <script>toastr.<?php echo e(Session::get('flash_notification.level')); ?>('<?php echo e(Session::get("flash_notification.message")); ?>', 'Response Status')</script>
                    <?php endif; ?>
                    <?php if(isset($msg)): ?>
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo e($msg); ?>

                        </div>
                    <?php endif; ?>
                    <?php if(isset($error)): ?>
                        <div class="alert alert-error">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo e($error); ?>

                        </div>
                    <?php endif; ?>
                    <?php if(count($errors) > 0): ?>
                        <div class="alert alert-danger">
                            <ul>
                                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><?php echo e($error); ?></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                    <?php echo Form::open(array('url' => url('reset'), 'method' => 'post', 'name' => 'form','class'=>'f-login-form')); ?>

                <div class="form-group ">
                  <label class="form-control-label">Email</label>
                  <?php echo Form::email('email', null, array('class' => 'form-control', 'placeholder'=>trans_choice('general.email',1),'required'=>'required')); ?>

                </div>
                <div class="row">
                  <div class="col-12">
                    <button type="submit" name="resetpassword" id="resetpassword" class="btn btn-orange btn-block  ">Reset Password</button>
                  </div>
                  <?php echo Form::close(); ?>

                </div>
                <div class="row mt-3">
                  <div class="col-12"> Back to login <a href="<?php echo e(url('login')); ?>" class="btn btn-link"> Click here</a> </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-md-5 no-padding-left bg-blue">
        <div class="bg-blue "> </div>
      </div>
    </div>
  </div>
</div>
<!-- jQuery CDN - Slim version (=without AJAX) --> 
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> 
<!-- Bootstrap JS --> 
<script src="http://localhost:8000/assets/bootstrap/js/bootstrap.min.js"></script> 
<!-- jQuery Custom Scroller CDN --> 
<script src="http://localhost:8000/assets/js/jquery.mCustomScrollbar.concat.min.js"></script> 
<script src="http://localhost:8000/assets/js/custom.js"></script>
</body>
</html>
