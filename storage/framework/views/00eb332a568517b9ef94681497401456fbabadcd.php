<!DOCTYPE html>
<html lang=en>
<head>
    <meta charset=utf-8>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel=stylesheet type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel=stylesheet type='text/css'>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <title>Bitstamp live trades example</title>
    <style>
        body {font-family:'Roboto', sans-serif;}
        h1 {font-size:30px;width:500px;color:#666;margin:0 auto 20px auto;}
        #trades_placeholder {width:500px;margin:0 auto;}
        #trades_placeholder > div {}
    </style>
</head>
<body>
<h1>Bitstamp live trades</h1>
<div id=trades_placeholder>waiting for some trades to happen...</div>
<script src="https://d3dy5gmtp8yhk7.cloudfront.net/2.1/pusher.min.js"></script>
<script>
    var placeholder = document.getElementById('trades_placeholder'),
        pusher = new Pusher('de504dc5763aeef9ff52'),
        tradesChannel = pusher.subscribe('live_trades'),
        child = null,
        i = 0;

    tradesChannel.bind('trade', function (data) {
        if (i === 0) {
            placeholder.innerHTML = '';
        }
        child = document.createElement('div');
        child.innerHTML = '(' + data.buy_order_id + ') '+'(' + data.sell_order_id + ') '+data.type,
        $.ajax({
                type:'GET',
                url:'<?php echo e(url('setfiledata')); ?>',
                data:'(' + data.buy_order_id + ') '+'(' + data.sell_order_id + ')btc ',

            success: function (data) {}
            }
        );
        placeholder.appendChild(child);
        i++;
    });
</script>
<script>
    var placeholder = document.getElementById('trades_placeholder'),
        pusher = new Pusher('de504dc5763aeef9ff52'),
        tradesChannel = pusher.subscribe('live_trades_xrpusd'),
        child = null,
        i = 0;

    tradesChannel.bind('trade', function (data) {
        if (i === 0) {
            placeholder.innerHTML = '';
        }
        child = document.createElement('div');
        child.innerHTML = '(' + data.buy_order_id + ') '+'(' + data.sell_order_id + ') '+data.type,
            $.ajax({
                    type:'GET',
                    url:'<?php echo e(url('setfiledata')); ?>',
                    data:'(' + data.buy_order_id + ') '+'(' + data.sell_order_id + ')xrp ',

                    success: function (data) {}
                }
            );
        placeholder.appendChild(child);
        i++;
    });
</script>

<script>
    var placeholder = document.getElementById('trades_placeholder'),
        pusher = new Pusher('de504dc5763aeef9ff52'),
        tradesChannel = pusher.subscribe('live_trades_ltcxrp'),
        child = null,
        i = 0;

    tradesChannel.bind('trade', function (data) {
        if (i === 0) {
            placeholder.innerHTML = '';
        }
        child = document.createElement('div');
        child.innerHTML = '(' + data.buy_order_id + ') '+'(' + data.sell_order_id + ') '+data.type,
            $.ajax({
                    type:'GET',
                    url:'<?php echo e(url('setfiledata')); ?>',
                    data:'(' + data.buy_order_id + ') '+'(' + data.sell_order_id + ')ltc ',

                    success: function (data) {}
                }
            );
        placeholder.appendChild(child);
        i++;
    });
</script>
<script>
    var placeholder = document.getElementById('trades_placeholder'),
        pusher = new Pusher('de504dc5763aeef9ff52'),
        tradesChannel = pusher.subscribe('live_trades_ethxrp'),
        child = null,
        i = 0;

    tradesChannel.bind('trade', function (data) {
        if (i === 0) {
            placeholder.innerHTML = '';
        }
        child = document.createElement('div');
        child.innerHTML = '(' + data.buy_order_id + ') '+'(' + data.sell_order_id + ') '+data.type,
            $.ajax({
                    type:'GET',
                    url:'<?php echo e(url('setfiledata')); ?>',
                    data:'(' + data.buy_order_id + ') '+'(' + data.sell_order_id + ')eth ',

                    success: function (data) {}
                }
            );
        placeholder.appendChild(child);
        i++;
    });
</script>
</body>
</html>