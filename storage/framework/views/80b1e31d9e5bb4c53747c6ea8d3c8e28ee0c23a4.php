<?php $__env->startSection('title'); ?>
    <?php echo e(trans('general.dashboard')); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php if(Sentinel::inRole('client')): ?>

<link rel="stylesheet" href="<?php echo e(asset('assets/themes/limitless/css/style.css')); ?>">
<section>
      <div class="pageContent">
        <div class="container">
          

         
          <div class="topInfoCards">
            <?php
                $usd = \App\Models\TradeCurrency::where('default_currency', 1)->first();
                $btc = \App\Models\TradeCurrency::where('network', "bitcoin")->first();
                $dogecoin = \App\Models\TradeCurrency::where('network', "dogecoin")->first();
                $ltc = \App\Models\TradeCurrency::where('network', "litecoin")->first();
                $xrp = \App\Models\TradeCurrency::where('network', "ripple")->first();
                $eth = \App\Models\TradeCurrency::where('network', "ethereum")->first();
            ?>

            

        


           
        <div id="main-content-container" class="bars-above"><div id="dashboard-page">

        

            <!-- Waluty -->
            <!--<div id="currencies"></div>-->

            <div class="row page-margin">

                <div class="row ">
                    <div class="row ">
                        

                        <!-- Portfele -->
                        <div class="col-xs-12 col-md-4 col-lg-4">
                            <div id="wallets" class="card"><div class="wallets-container">
    <div class="small-header horizontal layout center">
    <div class="left">
        <h4>Wallets</h4>
    </div>

    <div class="right flex">
        <value>
            <number id="wallets-value">≈ <?php echo e(number_format(\App\Helpers\GeneralHelper::user_aed_available(Sentinel::getUser()->id),4)); ?></number>
            <currency id="wallets-currency">
                <span class="currency-name-holder">AED</span>
                
            </currency>

        </value>
    </div>
</div>

<div class="items layout vertical">
    



    <div class="item layout horizontal center waves-effect" data-currency="btc">
            <div class="left layout horizontal center">
                <i class="currency-icon big mdi-currency-bitcoin" style="background: #6d5170;"></i>
                <span class="currency-name" style="color:#6d5170;">glc</span>
            </div>

            <div class="right flex">
                <div class="row">
                    <div class="col xs12 s12 m6 l6 xl6 offset-xs0 offset-s0 offset-m0 offset-l0 offset-xl0 wallet-value">
                        <value>
                            <number style="color: #6d5170;">12.00</number>
                        </value>

                        <div class="blocked layout horizontal center hidden">
                            <i class="mdi-lock"></i>
                            <number>0.00000000</number>
                        </div>
                    </div>

                    
                        <div class="col xs12 s12 m6 l6 xl6 about-value layout horizontal center hidden">-</div>
                    
                </div>
            </div>
        </div>


        <div class="item layout horizontal center waves-effect" data-currency="btc">
            <div class="left layout horizontal center">
                <i class="currency-icon big mdi-currency-bitcoin" style="background: #ffb400;"></i>
                <span class="currency-name" style="color:#ffb400;">btc</span>
            </div>

            <div class="right flex">
                <div class="row">
                    <div class="col xs12 s12 m6 l6 xl6 offset-xs0 offset-s0 offset-m0 offset-l0 offset-xl0 wallet-value">
                        <value>
                            <number style="color: #ffb400;"><?php echo number_format(Sentinel::getuser()->bitcoin_balance,4); ?></number>
                        </value>

                        <div class="blocked layout horizontal center hidden">
                            <i class="mdi-lock"></i>
                            <number>0.00000000</number>
                        </div>
                    </div>

                    
                        <div class="col xs12 s12 m6 l6 xl6 about-value layout horizontal center hidden">-</div>
                    
                </div>
            </div>
        </div>
    
        <div class="item layout horizontal center waves-effect" data-currency="usd">
            <div class="left layout horizontal center">
                <i class="currency-icon big mdi-currency-dollar" style="background: #37c866;"></i>
                <span class="currency-name" style="color:#37c866;">aed</span>
            </div>

            <div class="right flex">
                <div class="row">
                    <div class="col xs12 s12 m6 l6 xl6 offset-xs0 offset-s0 offset-m0 offset-l0 offset-xl0 wallet-value">
                        <value>
                            <number style="color: #37c866;"><?php echo e(number_format(\App\Helpers\GeneralHelper::user_aed_available(Sentinel::getUser()->id),4)); ?></number>
                        </value>

                        <div class="blocked layout horizontal center hidden">
                            <i class="mdi-lock"></i>
                            <number>0.00</number>
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
    
        <div class="item layout horizontal center waves-effect" data-currency="game">
            <div class="left layout horizontal center">
                <i class="currency-icon big mdi-currency-gamecredit" style="background: #98c01f;"></i>
                <span class="currency-name" style="color:#98c01f;">ltc</span>
            </div>

            <div class="right flex">
                <div class="row">
                    <div class="col xs12 s12 m6 l6 xl6 offset-xs0 offset-s0 offset-m0 offset-l0 offset-xl0 wallet-value">
                        <value>
                            <number style="color: #98c01f;"><?php echo number_format(Sentinel::getuser()->litecoin_balance,4); ?></number>
                        </value>

                        <div class="blocked layout horizontal center hidden">
                            <i class="mdi-lock"></i>
                            <number>0.00000000</number>
                        </div>
                    </div>

                    
                        <div class="col xs12 s12 m6 l6 xl6 about-value layout horizontal center hidden">-</div>
                    
                </div>
            </div>
        </div>
    
        <div class="item layout horizontal center waves-effect" data-currency="eth">
            <div class="left layout horizontal center">
                <i class="currency-icon big mdi-currency-ethereum" style="background: #4a90e2;"></i>
                <span class="currency-name" style="color:#4a90e2;">eth</span>
            </div>

            <div class="right flex">
                <div class="row">
                    <div class="col xs12 s12 m6 l6 xl6 offset-xs0 offset-s0 offset-m0 offset-l0 offset-xl0 wallet-value">
                        <value>
                            <number style="color: #4a90e2;"><?php echo number_format(Sentinel::getuser()->ethereum_balance,4); ?></number>
                        </value>

                        <div class="blocked layout horizontal center hidden">
                            <i class="mdi-lock"></i>
                            <number>0.00000000</number>
                        </div>
                    </div>

                    
                        <div class="col xs12 s12 m6 l6 xl6 about-value layout horizontal center hidden">-</div>
                    
                </div>
            </div>
        </div>
    
        <div class="item layout horizontal center waves-effect" data-currency="bcc">
            <div class="left layout horizontal center">
                <i class="currency-icon big mdi-currency-bitcoin-cash" style="background: #ff9639;"></i>
                <span class="currency-name" style="color:#ff9639;">xrp</span>
            </div>

            <div class="right flex">
                <div class="row">
                    <div class="col xs12 s12 m6 l6 xl6 offset-xs0 offset-s0 offset-m0 offset-l0 offset-xl0 wallet-value">
                        <value>
                            <number style="color: #ff9639;"><?php echo number_format(Sentinel::getuser()->ripple_balance,4); ?></number>
                        </value>

                        <div class="blocked layout horizontal center hidden">
                            <i class="mdi-lock"></i>
                            <number>0.00000000</number>
                        </div>
                    </div>

                    
                        <div class="col xs12 s12 m6 l6 xl6 about-value layout horizontal center hidden">-</div>
                    
                </div>
            </div>
        </div>
    
        
    


    <!-- karta jako ostatni - 5ty item -->
    <!--<div class="item layout horizontal center" id="card-item"></div>-->
</div>

                      <div class="layout horizontal center-justified goto-link-container">
                            <a href="<?php echo e(url('wallet/btc')); ?>" title="" id="go-to-history" class="layout horizontal center">
                                <h6 style="color: #6d5170">See Wallets</h6>
                                <i class="mdi-chevron-right"></i>
                            </a>
                        </div>


</div></div>
                        </div>

                        <!-- Rynki -->
                        <div class="col-xs-12 col-md-4 col-lg-5">
                            <div id="markets" class="card"><div class="markets-container">

 <div class="small-header horizontal layout center">
    <div class="left"><br>
        <h4>AED Markets</h4><hr>
    </div>

    
</div>

<?php
                        //BTC
                        $full_btc_usd  = json_decode($full_btc_usd = App\Models\Liveprice::where('key','full_btc_usd')->first()->value,true);
                        $full_eth_usd  = json_decode($full_eth_usd = App\Models\Liveprice::where('key','full_eth_usd')->first()->value,true);
                        $full_ltc_usd  = json_decode($full_ltc_usd = App\Models\Liveprice::where('key','full_ltc_usd')->first()->value,true);
                        $full_xrp_usd  = json_decode($full_xrp_usd = App\Models\Liveprice::where('key','full_xrp_usd')->first()->value,true);


                        $btc_aed_last = $full_btc_usd['last'] * 3.74;
                        $btc_aed_high = $full_btc_usd['high'] * 3.74;
                        $btc_aed_low =  $full_btc_usd['low']* 3.74;


                        $eth_aed_last = $full_eth_usd['last'] * 3.74;
                        $eth_aed_high = $full_eth_usd['high'] * 3.74;
                        $eth_aed_low =  $full_eth_usd['low']* 3.74;


                        $ltc_aed_last = $full_ltc_usd['last'] * 3.74;
                        $ltc_aed_high = $full_ltc_usd['high'] * 3.74;
                        $ltc_aed_low =  $full_ltc_usd['low']* 3.74;


                        $xrp_aed_last = $full_xrp_usd['last'] * 3.74;
                        $xrp_aed_high = $full_xrp_usd['high'] * 3.74;
                        $xrp_aed_low =  $full_xrp_usd['low']* 3.74;

?>

<div id="markets-list">
    <div id="header" class="layout horizontal center">
        <div class="space"></div>
        <div class="header row no-padding-vertical">
            <div class="col s6 xxxl7 layout horizontal center">
                <div>Rate</div>
            </div>

            <div class="col s6 xxxl5 layout horizontal center">
                <div>Volume</div>
            </div>
        </div>

        <div class="empty"></div>
    </div>

    <div class="items">
        <!-- tutaj pojawia sie rynki z danej waluty glownej -->



    <div class="item layout horizontal center active" data-market="BTC-USD" data-first-currency="BTC" data-second-currency="USD"><!-- klasa "active" -->
    <div class="layout horizontal center currency-pair">
        <i class="currency-icon big mdi-currency-bitcoin" style="background-color: #6d5170;"></i>
        <span>GLC</span>
    </div>

    <div class="overlay" style="background-color: #6d5170;"></div>

    <div class="horizontal layout center row no-margin">
        <div class="col xxs12 s6 xxxl7 value-and-change">
            <div class="layout horizontal center rate-value">
                <value>3.70</value>
            </div>

            <div class="layout horizontal center change-value">
                <value>
                    <difference class="layout horizontal center up" style="display: flex;">
                        <i class="mdi-chevron-up"></i>
                        <i class="mdi-equal"></i>
                        <span id="difference-value">0.00%</span>
                    </difference>
                </value>
                <!--<div class="transaction-text">Sell</div>-->
            </div>
        </div>

        <div class="col xxs12 s6 xxxl5 layout horizontal center">
            <div class="layout horizontal center volume-value">
                <value>100.479195</value>
            </div>
        </div>
    </div>

    <button class="btn btn-flat btn-floating btn-transparent waves-effect go-to-market">
        <i class="mdi-chevron-right"></i>
    </button>
</div>



    <div class="item layout horizontal center active" data-market="BTC-USD" data-first-currency="BTC" data-second-currency="USD"><!-- klasa "active" -->
    <div class="layout horizontal center currency-pair">
        <i class="currency-icon big mdi-currency-bitcoin" style="background-color: #ffb400;"></i>
        <span>BTC</span>
    </div>

    <div class="overlay" style="background-color: #ffb400;"></div>

    <div class="horizontal layout center row no-margin">
        <div class="col xxs12 s6 xxxl7 value-and-change">
            <div class="layout horizontal center rate-value">
                <value><?php  echo number_format($btc_aed_last,2); ?></value>
            </div>

            <div class="layout horizontal center change-value">
                <value>
                    <difference class="layout horizontal center up" style="display: flex;">
                        <i class="mdi-chevron-up"></i>
                        <i class="mdi-equal"></i>
                        <span id="difference-value">1.56%</span>
                    </difference>
                </value>
                <!--<div class="transaction-text">Sell</div>-->
            </div>
        </div>

        <div class="col xxs12 s6 xxxl5 layout horizontal center">
            <div class="layout horizontal center volume-value">
                <value>1.47919519</value>
            </div>
        </div>
    </div>

    <button class="btn btn-flat btn-floating btn-transparent waves-effect go-to-market">
        <i class="mdi-chevron-right"></i>
    </button>
</div><div class="item layout horizontal center active" data-market="BCC-USD" data-first-currency="BCC" data-second-currency="USD"><!-- klasa "active" -->
    <div class="layout horizontal center currency-pair">
        <i class="currency-icon big mdi-currency-bitcoin-cash" style="background-color: #ff9639;"></i>
        <span>LTC</span>
    </div>

    <div class="overlay" style="background-color: #ff9639;"></div>

    <div class="horizontal layout center row no-margin">
        <div class="col xxs12 s6 xxxl7 value-and-change">
            <div class="layout horizontal center rate-value">
                <value><?php  echo number_format($ltc_aed_last,2); ?></value>
            </div>

            <div class="layout horizontal center change-value">
                <value>
                    <difference class="layout horizontal center down" style="display: flex;">
                        <i class="mdi-chevron-up"></i>
                        <i class="mdi-equal"></i>
                        <span id="difference-value">5.32%</span>
                    </difference>
                </value>
                <!--<div class="transaction-text">Sell</div>-->
            </div>
        </div>

        <div class="col xxs12 s6 xxxl5 layout horizontal center">
            <div class="layout horizontal center volume-value">
                <value>0.18262453</value>
            </div>
        </div>
    </div>

    <button class="btn btn-flat btn-floating btn-transparent waves-effect go-to-market">
        <i class="mdi-chevron-right"></i>
    </button>
</div><div class="item layout horizontal center active" data-market="ETH-USD" data-first-currency="ETH" data-second-currency="USD"><!-- klasa "active" -->
    <div class="layout horizontal center currency-pair">
        <i class="currency-icon big mdi-currency-ethereum" style="background-color: #4a90e2;"></i>
        <span>ETH</span>
    </div>

    <div class="overlay" style="background-color: #4a90e2;"></div>

    <div class="horizontal layout center row no-margin">
        <div class="col xxs12 s6 xxxl7 value-and-change">
            <div class="layout horizontal center rate-value">
                <value><?php  echo number_format($eth_aed_last,2); ?></value>
            </div>

            <div class="layout horizontal center change-value">
                <value>
                    <difference class="layout horizontal center up" style="display: flex;">
                        <i class="mdi-chevron-up"></i>
                        <i class="mdi-equal"></i>
                        <span id="difference-value">0.08%</span>
                    </difference>
                </value>
                <!--<div class="transaction-text">Sell</div>-->
            </div>
        </div>

        <div class="col xxs12 s6 xxxl5 layout horizontal center">
            <div class="layout horizontal center volume-value">
                <value>0.61517164</value>
            </div>
        </div>
    </div>

    <button class="btn btn-flat btn-floating btn-transparent waves-effect go-to-market">
        <i class="mdi-chevron-right"></i>
    </button>
</div><div style="border-bottom: 0px;" class="item layout horizontal center active" data-market="LSK-USD" data-first-currency="LSK" data-second-currency="USD"><!-- klasa "active" -->
    <div class="layout horizontal center currency-pair">
        <i class="currency-icon big mdi-currency-lisk" style="background-color: #1170a4;"></i>
        <span>XRP</span>
    </div>

    <div class="overlay" style="background-color: #1170a4;"></div>

    <div class="horizontal layout center row no-margin">
        <div class="col xxs12 s6 xxxl7 value-and-change">
            <div class="layout horizontal center rate-value">
                <value><?php  echo number_format($xrp_aed_last,2); ?></value>
            </div>

            <div class="layout horizontal center change-value">
                <value>
                    <difference class="layout horizontal center up" style="display: flex;">
                        <i class="mdi-chevron-up"></i>
                        <i class="mdi-equal"></i>
                        <span id="difference-value">10.96%</span>
                    </difference>
                </value>
                <!--<div class="transaction-text">Sell</div>-->
            </div>
        </div>

        <div class="col xxs12 s6 xxxl5 layout horizontal center">
            <div class="layout horizontal center volume-value">
                <value>14.87150040</value>
            </div>
        </div>
    </div>

    <button class="btn btn-flat btn-floating btn-transparent waves-effect go-to-market">
        <i class="mdi-chevron-right"></i>
    </button>
</div></div>

<div class="layout horizontal center-justified goto-link-container">
                            <a href="<?php echo e(url('wallet/btc')); ?>" title="" id="go-to-history" class="layout horizontal center">
                                <h6 style="color: #6d5170">See Market</h6>
                                <i class="mdi-chevron-right"></i>
                            </a>
                        </div>
</div>


<div class="layout horizontal center-justified goto-link-container">
    
</div>

    <!--div class="loader-container" style="background: #fff">
        <paper-spinner-lite active></paper-spinner-lite>
    </div-->
</div></div>
                        </div>

                       
            <div class="col-xs-12 col-md-4 col-lg-3">
            <div id="wallets" class="card"><div class="wallets-container">
              <div class="small-header horizontal layout center">
                  <div class="left"><br>
                      <h4>Account Status</h4><hr>
                  </div>

                  
              </div>
                <?php if(Sentinel::getUser()->email_verified==1): ?>
                <div class="item layout horizontal center waves-effect">
                <div class="left layout horizontal center">
                      
                      <h5 class="currency-name">1. Email Verified</h5>

                </div>

                <div class="right flex" style="font-size: 3rem; color: green; margin-left: 53%">
                <i class="mdi-verified"></i>
                </div></div>
                <?php else: ?>
                  <div class="item layout horizontal center waves-effect">
                    <div class="left layout horizontal center">
                      
                      <h5 class="currency-name"><a style="color:black;" href="<?php echo e(url('setting/data')); ?>">1. Email Verified</a></h5>
                    </div>

                    <div class="right flex" style="font-size: 3rem; color: red; margin-left: 53%">
                    <i class="mdi-close-circle"></i>
                    </div></div>
                <?php endif; ?>

                <?php if(Sentinel::getUser()->phone_verified==1): ?>
                <div class="item layout horizontal center waves-effect">
                <div class="left layout horizontal center">
                      
                      <h5 class="currency-name">2. Phone Verified</h5>

                </div>

                <div class="right flex" style="font-size: 3rem; color: green; margin-left: 51%">
                <i class="mdi-verified"></i>
                </div>
                </div>
                <?php else: ?>
                  <div class="item layout horizontal center waves-effect">
                    <div class="left layout horizontal center">
                      
                      <h5 class="currency-name"><a style="color:black;" href="<?php echo e(url('setting/data')); ?>">2. Phone Verified</a></h5>
                    </div>

                    <div class="right flex" style="font-size: 3rem; color: red; margin-left: 51%">
                    <i class="mdi-close-circle"></i>
                    </div></div>
                <?php endif; ?>

                <?php if(Sentinel::getUser()->documents_verified==1): ?>
                <div class="item layout horizontal center waves-effect">
                <div class="left layout horizontal center">
                      
                      <h5 class="currency-name">3. Documents Verified</h5>

                </div>

                <div class=" flex" style="font-size: 3rem; color: green; margin-left: 40%">
                <i class="mdi-verified"></i>
                </div></div>
                <?php else: ?>
                  <div class="item layout horizontal center waves-effect">
                    <div class="left layout horizontal center">
                      
                      <h5 class="currency-name"><a style="color:black;" href="<?php echo e(url('setting/data')); ?>">3. Documents Verified</a></h5>
                    </div>

                    <div class="right flex" style="font-size: 3rem; color: red; margin-left: 40%">
                    <i class="mdi-close-circle"></i>
                    </div></div>
                <?php endif; ?>

              </div><br>
                        <div class="layout horizontal center-justified goto-link-container">
                            <a href="<?php echo e(url('setting/data')); ?>" title="" id="go-to-history" class="layout horizontal center">
                                <h6 style="color: #6d5170">Go to Settings</h6>
                                <i class="mdi-chevron-right"></i>
                            </a>
                        </div>
                    </div>
                    </div>
                </div>

</ul>
</div>
</div>


            </div>



        
          <div class="row">

          <div class="col-lg-7 col-md-6 col-sm-12 col-xs-12">
          <div class="panel panel-body panelStyle" style="border: 2px solid #edeff1;">
          <div class="small-header horizontal layout center ">
            <div class="left ">
                <h4 style="font-size: 24px; color: #284274">History</h4><hr>
            </div>

        </div>
            <div class="table-responsive">

              <table id="order_history" class="table tableStyle">
                <thead>
                  <tr>
                    <th><?php echo e(trans_choice('general.type',1)); ?></th>
                    <th><?php echo e(trans_choice('general.status',1)); ?></th>
                    <th><?php echo e(trans_choice('general.market',1)); ?></th>
                    <th><?php echo e(trans_choice('general.price',1)); ?></th>
                    <th><?php echo e(trans_choice('general.volume',1)); ?></th>
                    <th><?php echo e(trans_choice('general.time',1)); ?></th>
                  </tr>
                </thead>
                <tbody>
                  <?php $__currentLoopData = \App\Models\OrderBook::where('user_id', Sentinel::getUser()->id)->orderBy('created_at','desc')->limit(5)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php
                    $trade_currency = \App\Models\TradeCurrency::find($key->trade_currency_id);
                    $default = \App\Models\TradeCurrency::where('default_currency', 1)->first();
                  ?>

                  
                  <tr>
                    <td class="typeTd">
                      <?php if($key->order_type=="ask"): ?>
                        <?php echo e(trans_choice('general.ask',1)); ?>

                        <?php endif; ?>
                        <?php if($key->order_type=="bid"): ?>
                        <?php echo e(trans_choice('general.bid',1)); ?>

                        <?php endif; ?>
                    </td>
                    <td>
                      <?php if($key->status=="pending"): ?>
                         <?php echo e(trans_choice('general.pending',1)); ?>

                      <?php endif; ?>
                      <?php if($key->status=="processing"): ?>
                         <?php echo e(trans_choice('general.processing',1)); ?>

                      <?php endif; ?>
                      <?php if($key->status=="cancelled"): ?>
                         <?php echo e(trans_choice('general.cancelled',1)); ?>

                      <?php endif; ?>
                      <?php if($key->status=="done"): ?>
                         <?php echo e(trans_choice('general.done',1)); ?>

                      <?php endif; ?>
                      <?php if($key->status=="accepted"): ?>
                         <?php echo e(trans_choice('general.accepted',1)); ?>

                      <?php endif; ?>
                    </td>
                    <td>
                      <?php if(!empty($trade_currency)): ?>
                         <?php echo e($trade_currency->xml_code); ?>

                         <?php endif; ?>
                         <?php if(!empty($default)): ?>
                         <?php echo e($default->xml_code); ?>


                      <?php endif; ?>
                    </td>
                    <td><?php echo e(round($key->amount,6)); ?></td>
                    <td><?php echo e(round( $key->volume,6)); ?></td>
                    <td><?php echo e($key->created_at); ?></td>
                  </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
              </table>
            </div>
          </div>

        </div>
        <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12" style="padding-bottom: 20px;">
        <div class="panel panel-body panelStyle" style="border: 2px solid #edeff1;">
        <div class="small-header horizontal layout center">
            <div class="left">
                <h4 style="font-size: 24px; color: #284274">How does it work?</h4><hr>
                <div class="left flex" style="font-size: 3rem; color: #1170a4;">
                    <i class="mdi-numeric-1-box"></i>   
                </div>
                <div class=" layout horizontal center">
                      
                      <h5 class="currency-name">Deposit AED via <b>Wire Transfer</b>, <b>Credit/Debit card</b> or <b>Cash pick-up service</b>.</h5>
                    </div><br>

                <div class="left flex" style="font-size: 3rem; color: #37c866;">
                    <i class="mdi-numeric-2-box"></i>   
                </div>
                <div class=" layout horizontal center">
                      
                      <h5 class="currency-name">Buy digital currencies like <b>Bitcoin, Litecoin, Ethereum and Ripple</b> using AED (United Arab Emirates Dirham).</h5>
                    </div><br>

                <div class="left flex" style="font-size: 3rem; color: #6d5170;">
                    <i class="mdi-numeric-3-box"></i>   
                </div>
                <div class=" layout horizontal center">
                      
                      <h5 class="currency-name">Start trading cryptocurrencies on our Professional <b>Trading platform</b>.</h5>
                    </div><br>
                <div class="layout horizontal center-justified goto-link-container">
                            <a href="<?php echo e(url('setting/data')); ?>" title="" id="go-to-history" class="layout horizontal center">
                                <h6 style="color: #6d5170">Go to FAQs</h6>
                                <i class="mdi-chevron-right"></i>
                            </a>
                        </div>
                

        </div>
      


      </div>
      </div>
      </div>
      </div>
    
      </div>

      
    </section>
    <?php else: ?>
    <?php
        $usd = \App\Models\TradeCurrency::where('default_currency', 1)->first();
        $btc = \App\Models\TradeCurrency::where('network', "bitcoin")->first();
        $dogecoin = \App\Models\TradeCurrency::where('network', "dogecoin")->first();
        $ltc = \App\Models\TradeCurrency::where('network', "litecoin")->first();
        $xrp = \App\Models\TradeCurrency::where('network', "ripple")->first();
        $eth = \App\Models\TradeCurrency::where('network', "ethereum")->first();
        ?>
        <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-3 col-xs-12">
                <div class="panel panel-body bg-green has-bg-image">
                    <div class="media no-margin">
                        <div class="media-body">
                            <h3 class="no-margin"><?php echo e(number_format(\App\Helpers\GeneralHelper::total_usd_fees(),2)); ?></h3>
                            <span class="text-uppercase text-size-mini"><?php echo e($usd->xml_code); ?> <?php echo e(trans_choice('general.fee',2)); ?></span>
                        </div>

                        <div class="media-right media-middle">
                            <i class="icon-wallet icon-3x opacity-75"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-12">
                <div class="panel panel-body bg-blue-400 has-bg-image">
                    <div class="media no-margin">
                        <div class="media-body">
                            <h3 class="no-margin"><?php echo e(round(\App\Helpers\GeneralHelper::total_currency_fees($btc->id),$btc->decimals)); ?></h3>
                            <span class="text-uppercase text-size-mini"><?php echo e($btc->xml_code); ?> <?php echo e(trans_choice('general.fee',2)); ?></span>
                        </div>

                        <div class="media-right media-middle">
                            <i class="icon-wallet icon-3x opacity-75"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-12">
                <div class="panel panel-body bg-orange-400 has-bg-image">
                    <div class="media no-margin">
                        <div class="media-body">
                            <h3 class="no-margin"><?php echo e(round(\App\Helpers\GeneralHelper::total_currency_fees($dogecoin->id),$dogecoin->decimals)); ?></h3>
                            <span class="text-uppercase text-size-mini"><?php echo e($dogecoin->xml_code); ?> <?php echo e(trans_choice('general.fee',2)); ?></span>
                        </div>

                        <div class="media-right media-middle">
                            <i class="icon-wallet icon-3x opacity-75"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-12">
                <div class="panel panel-body bg-blue-400 has-bg-image">
                    <div class="media no-margin">
                        <div class="media-body">
                            <h3 class="no-margin"> <?php echo e(round(\App\Helpers\GeneralHelper::total_currency_fees($ltc->id),$ltc->decimals)); ?></h3>
                            <span class="text-uppercase text-size-mini"><?php echo e($ltc->xml_code); ?> <?php echo e(trans_choice('general.fee',2)); ?></span>
                        </div>
                        <div class="media-right media-middle">
                            <i class="icon-wallet icon-3x opacity-75"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-12">
                <div class="panel panel-body bg-orange-400 has-bg-image">
                    <div class="media no-margin">
                        <div class="media-body">
                            <h3 class="no-margin"><?php echo e(round(\App\Helpers\GeneralHelper::total_currency_fees($eth->id),$eth->decimals)); ?></h3>
                            <span class="text-uppercase text-size-mini"><?php echo e($eth->xml_code); ?> <?php echo e(trans_choice('general.fee',2)); ?></span>
                        </div>

                        <div class="media-right media-middle">
                            <i class="icon-wallet icon-3x opacity-75"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-12">
                <div class="panel panel-body bg-blue-400 has-bg-image">
                    <div class="media no-margin">
                        <div class="media-body">
                            <h3 class="no-margin"> <?php echo e(round(\App\Helpers\GeneralHelper::total_currency_fees($xrp->id),$xrp->decimals)); ?></h3>
                            <span class="text-uppercase text-size-mini"><?php echo e($ltc->xml_code); ?> <?php echo e(trans_choice('general.fee',2)); ?></span>
                        </div>
                        <div class="media-right media-middle">
                            <i class="icon-wallet icon-3x opacity-75"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2 col-sm-3 col-xs-12">
                <div class="panel panel-body bg-green has-bg-image">
                    <div class="media no-margin">
                        <div class="media-body">
                            <h3 class="no-margin"><?php echo e(number_format(\App\Helpers\GeneralHelper::total_usd_deposits(),2)); ?></h3>
                            <span class="text-uppercase text-size-mini"><?php echo e($usd->xml_code); ?> <?php echo e(trans_choice('general.deposit',2)); ?></span>
                        </div>

                        <div class="media-right media-middle">
                            <i class="icon-wallet icon-3x opacity-75"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-12">
                <div class="panel panel-body bg-blue-400 has-bg-image">
                    <div class="media no-margin">
                        <div class="media-body">
                            <h3 class="no-margin"><?php echo e(round(\App\Helpers\GeneralHelper::total_currency_deposits($btc->id),$btc->decimals)); ?></h3>
                            <span class="text-uppercase text-size-mini"><?php echo e($btc->xml_code); ?> <?php echo e(trans_choice('general.deposit',2)); ?></span>
                        </div>

                        <div class="media-right media-middle">
                            <i class="icon-wallet icon-3x opacity-75"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-12">
                <div class="panel panel-body bg-orange-400 has-bg-image">
                    <div class="media no-margin">
                        <div class="media-body">
                            <h3 class="no-margin"><?php echo e(round(\App\Helpers\GeneralHelper::total_currency_deposits($dogecoin->id),$dogecoin->decimals)); ?></h3>
                            <span class="text-uppercase text-size-mini"><?php echo e($dogecoin->xml_code); ?> <?php echo e(trans_choice('general.deposit',2)); ?></span>
                        </div>

                        <div class="media-right media-middle">
                            <i class="icon-wallet icon-3x opacity-75"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-12">
                <div class="panel panel-body bg-blue-400 has-bg-image">
                    <div class="media no-margin">
                        <div class="media-body">
                            <h3 class="no-margin"> <?php echo e(round(\App\Helpers\GeneralHelper::total_currency_deposits($ltc->id),$ltc->decimals)); ?></h3>
                            <span class="text-uppercase text-size-mini"><?php echo e($ltc->xml_code); ?> <?php echo e(trans_choice('general.deposit',2)); ?></span>
                        </div>
                        <div class="media-right media-middle">
                            <i class="icon-wallet icon-3x opacity-75"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-12">
                <div class="panel panel-body bg-orange-400 has-bg-image">
                    <div class="media no-margin">
                        <div class="media-body">
                            <h3 class="no-margin"><?php echo e(round(\App\Helpers\GeneralHelper::total_currency_deposits($eth->id),$eth->decimals)); ?></h3>
                            <span class="text-uppercase text-size-mini"><?php echo e($eth->xml_code); ?> <?php echo e(trans_choice('general.deposit',2)); ?></span>
                        </div>

                        <div class="media-right media-middle">
                            <i class="icon-wallet icon-3x opacity-75"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-12">
                <div class="panel panel-body bg-blue-400 has-bg-image">
                    <div class="media no-margin">
                        <div class="media-body">
                            <h3 class="no-margin"> <?php echo e(round(\App\Helpers\GeneralHelper::total_currency_deposits($xrp->id),$xrp->decimals)); ?></h3>
                            <span class="text-uppercase text-size-mini"><?php echo e($xrp->xml_code); ?> <?php echo e(trans_choice('general.deposit',2)); ?></span>
                        </div>
                        <div class="media-right media-middle">
                            <i class="icon-wallet icon-3x opacity-75"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 col-sm-3 col-xs-12">
                <div class="panel panel-body bg-green has-bg-image">
                    <div class="media no-margin">
                        <div class="media-body">
                            <h3 class="no-margin"><?php echo e(number_format(\App\Helpers\GeneralHelper::total_usd_withdrawals(),2)); ?></h3>
                            <span class="text-uppercase text-size-mini"><?php echo e($usd->xml_code); ?> <?php echo e(trans_choice('general.withdrawal',2)); ?></span>
                        </div>

                        <div class="media-right media-middle">
                            <i class="icon-wallet icon-3x opacity-75"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-12">
                <div class="panel panel-body bg-blue-400 has-bg-image">
                    <div class="media no-margin">
                        <div class="media-body">
                            <h3 class="no-margin"><?php echo e(round(\App\Helpers\GeneralHelper::total_currency_withdrawals($btc->id),$btc->decimals)); ?></h3>
                            <span class="text-uppercase text-size-mini"><?php echo e($btc->xml_code); ?> <?php echo e(trans_choice('general.withdrawal',2)); ?></span>
                        </div>

                        <div class="media-right media-middle">
                            <i class="icon-wallet icon-3x opacity-75"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-12">
                <div class="panel panel-body bg-orange-400 has-bg-image">
                    <div class="media no-margin">
                        <div class="media-body">
                            <h3 class="no-margin"><?php echo e(round(\App\Helpers\GeneralHelper::total_currency_withdrawals($dogecoin->id),$dogecoin->decimals)); ?></h3>
                            <span class="text-uppercase text-size-mini"><?php echo e($dogecoin->xml_code); ?> <?php echo e(trans_choice('general.withdrawal',2)); ?></span>
                        </div>

                        <div class="media-right media-middle">
                            <i class="icon-wallet icon-3x opacity-75"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-12">
                <div class="panel panel-body bg-blue-400 has-bg-image">
                    <div class="media no-margin">
                        <div class="media-body">
                            <h3 class="no-margin"> <?php echo e(round(\App\Helpers\GeneralHelper::total_currency_withdrawals($ltc->id),$ltc->decimals)); ?></h3>
                            <span class="text-uppercase text-size-mini"><?php echo e($ltc->xml_code); ?> <?php echo e(trans_choice('general.withdrawal',2)); ?></span>
                        </div>
                        <div class="media-right media-middle">
                            <i class="icon-wallet icon-3x opacity-75"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-12">
                <div class="panel panel-body bg-orange-400 has-bg-image">
                    <div class="media no-margin">
                        <div class="media-body">
                            <h3 class="no-margin"><?php echo e(round(\App\Helpers\GeneralHelper::total_currency_withdrawals($eth->id),$eth->decimals)); ?></h3>
                            <span class="text-uppercase text-size-mini"><?php echo e($eth->xml_code); ?> <?php echo e(trans_choice('general.withdrawal',2)); ?></span>
                        </div>

                        <div class="media-right media-middle">
                            <i class="icon-wallet icon-3x opacity-75"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-12">
                <div class="panel panel-body bg-blue-400 has-bg-image">
                    <div class="media no-margin">
                        <div class="media-body">
                            <h3 class="no-margin"> <?php echo e(round(\App\Helpers\GeneralHelper::total_currency_withdrawals($xrp->id),$xrp->decimals)); ?></h3>
                            <span class="text-uppercase text-size-mini"><?php echo e($xrp->xml_code); ?> <?php echo e(trans_choice('general.withdrawal',2)); ?></span>
                        </div>
                        <div class="media-right media-middle">
                            <i class="icon-wallet icon-3x opacity-75"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>

        
        <script src="<?php echo e(asset('assets/plugins/amcharts/amcharts.js')); ?>"
                type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/plugins/amcharts/serial.js')); ?>"
                type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/plugins/amcharts/pie.js')); ?>"
                type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/plugins/amcharts/themes/light.js')); ?>"
                type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/plugins/amcharts/plugins/export/export.min.js')); ?>"
                type="text/javascript"></script>

        <script>
        $('#order_history').DataTable({
            
            "language": {
            "emptyTable":     "No deposts yet"
        }
    });
        
        
      
    </script>
    <?php endif; ?>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>