<!doctype html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
    <head>
        <title>Verify - Bitex UAE</title>

        <meta charset="utf-8">
        <meta name="description" content="Login to Bitex UAE to start trading digital assets like Bitcoin, Ethereum, Litecoin and Ripple using AED (Dirham) currency in the United Arab Emirates.">
        <meta name="keywords" content="login, create an account, register, bitex uae, bitex, bitcoin, ethereum, ripple, litecoin, uae, united arab emirates, cryptocurrency, exchange, trading"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"/>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

        
        <!-- Loading Lato font here, optimize this-->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">

        <!-- First fold CSS -->
        <style type="text/css">*{box-sizing:border-box}body{font-family:'Lato',sans-serif;margin:0}header.demat-header{width:100%}header.demat-header .upstox-logo{margin:50px;display:inline-block}header.demat-header .demat-help-button{display:inline-block;position:absolute;right:0;margin:50px;height:50px;width:80px;cursor:pointer;color:#0046bb}header.demat-header .demat-help-button span{}header.demat-header .demat-help-button span.demat-help-bubble{background:url(https://cf.upstox.com/open-demat-account/images/xhelp_bubble.png.pagespeed.ic.vY5wtg7fNg.png) no-repeat;background-size:contain;margin-right:10px;display:inline-block;width:20px;height:20px;position:relative;top:7px}main.first-fold{background:url(https://tradeanycoin.com/assets/themes/limitless/images/register-background.png) no-repeat;background-size:85% 100%;width:100%;padding-bottom:60px}@media  screen and (max-width:1370px){main.first-fold{background-size:1000px}.account-container section.demat-details{margin-top:70px}}.first-fold .account-container{display:flex;flex-direction:row;justify-content:space-evenly}.account-container .demat-details{color:#fffefe;flex:0 0 auto;margin-top:100px}.account-container .demat-details h1{font-size:50px;font-weight:300}.account-container .demat-details p{font-size:22px;font-weight:300}.account-container .demat-form,.account-container .signinbox{width:400px;background-color:#fff;box-shadow:0 0 19.3px 15.8px rgba(0,0,0,.05);margin-bottom:20px;margin-top:20px;padding:25px}.account-container .demat-form p{font-size:18px;letter-spacing:.8px;text-align:left;font-weight:bold;color:#aaa;margin-top:0;margin-bottom:20px;text-transform:uppercase;letter-spacing:1.5px}.account-container .demat-form input{opacity:.75;height:50px;width:100%;background-color:#fff;border:solid 1px #ccc;border-radius:0;padding:10px 20px;font-size:16px;margin-bottom:20px;font-family:Lato;font-size:16px;font-weight:400;letter-spacing:.8px;text-align:left}.demat-form #txtcontact{width:calc(100% - 49px)}.account-container .demat-form .input-group-addon{display:inline-block;line-height:50px;height:50px;position:relative;top:1px;color:#000;text-align:center;border:1px solid #ced4da;border-right:none;padding:0 10px}.demat-form #submitsignup{width:100%;height:50px;border:none;background:#17294e;color:#fff;font-size:16px;font-weight:700;letter-spacing:1px;text-transform:uppercase;cursor:pointer}.demat-form .form-divider{position:relative;margin-top:30px;left:-25px;width: calc(100% + 50px);border-bottom:1px solid rgba(204,204,204,.75)}.demat-form #referral_section{margin-top:20px}.demat-form #referral_section label{font-size:14px;display:block;color:#666}.demat-form #referral_section .ref-wrapper{display:flex;flex-direction:row;margin-top:20px;margin-bottom:20px}button:focus{outline:0}.demat-form #referral_section .ref-wrapper button{border-radius:4px;border:solid 1px #ccc;padding:10px!important;background:transparent;font-size:16px;letter-spacing:.6px;text-align:center;color:#ccc;height:40px;line-height:14px;width:48%}.ref-yes{margin-right:20px}.demat-form #referral_section .ref-wrapper button.selected{border:solid 1px #0046bb;background:#e8f1ff;color:#0046bb}.account-container .signinbox p{margin-top:0;font-size:14px;text-transform:uppercase;color:#666;letter-spacing:1.5px}.account-container .signinbox a{font-size:18px;font-weight:700;color:#0046bb;letter-spacing:1.5px;border-bottom:2px solid #0046bb;padding-bottom:3px;text-decoration:none!important}small{display:none}@media  screen and (max-width:1150px){header.demat-header .demat-help-button{color:#fff}.first-fold .account-container{flex-direction:column}.account-container section.demat-details{margin-top:0}.account-container .demat-details h1{text-align:center;font-size:36px;margin-top:0}.account-container .demat-details div{display:none}.account-container .form-container{display:flex;align-items:center;flex-direction:column}.account-container .demat-form .input-group-addon{top:0}}@media  screen and (max-width:450px){.account-container .demat-details h1{font-size:22px}.account-container .demat-form p{font-size:14px}header.demat-header .demat-help-button{margin:15px 20px;font-size:12px;width:62px}header.demat-header .upstox-logo{margin:20px}header.demat-header .upstox-logo img{height:100%}.account-container .demat-form,.account-container .signinbox{width:calc(100% - 30px);min-width:200px;margin-bottom:20px;margin-top:20px;padding:15px}.account-container .demat-form input{height:40px;font-size:14px;padding:15px}.account-container .demat-form .input-group-addon{line-height:40px;height:40px;top:1px;font-size:14px}.account-container .signinbox{padding-bottom:20px;margin-top:5px}.account-container .signinbox p{font-size:12px;margin-bottom:5px}.account-container .signinbox a{font-size:14px;margin-bottom:5px}main.first-fold{padding-bottom:20px;margin-bottom:20px}.demat-form .form-divider{width: calc(100% + 30px);left:-15px}}</style>
        <!-- End First fold CSS -->
    </head>


    <body>

        <main class="first-fold">
            <header class="demat-header">
                <div class="upstox-logo">
                    <a href="https://www.bitexuae.com"><img src="<?php echo e(asset('assets/themes/limitless/images/bitex-white-2.png')); ?>" width="200"></a>
                </div>
                <div class="demat-help-button"><a href="<?php echo e(url('logout')); ?>" style="text-decoration: none; color: #17294e; border: 1px solid #d0d0d0; padding: 10px;">LOGOUT</a></div>
            </header>
            <div class="account-container">
                <section class="demat-details">
                    <h1>Complete verification</h1>
                    <div>
                        <p style="line-height: 40px; letter-spacing: 0.8px;">
                            To Buy/Sell Digital Assets<br> 
                            like Bitcoin, Ethereum, Litecoin and Ripple.<br> Use the professional trading platform today.
                        </p>
                    </div>
                    
                </section>

                <div class="form-container">
                    <section class="demat-form">
                    <?php if(empty(Sentinel::getUser()->phone)): ?>
                        <div class="alert alert-primary alert-bordered">
                            <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                        class="sr-only">Close</span></button>
                            <?php echo e(trans('general.verify_phone_msg')); ?>

                        </div>
                        <div class="alert bg-danger alert-bordered">
                            <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                        class="sr-only">Close</span></button>
                            <?php echo e(trans('general.phone_not_set')); ?>

                        </div>
                    <?php else: ?>
                        <?php if(Sentinel::getUser()->phone_verified==1): ?>
                            <div class="alert bg-success alert-bordered">
                                <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                            class="sr-only">Close</span></button>
                                <?php echo e(trans('general.phone_verified')); ?>

                            </div>
                        <?php else: ?>
                        <div style="text-align: center;">
                        <span style="color: #d0d0d0">1. Email</span><div style="display: inline-block;width: 80px;height: 2px;margin: 9px 10px 2px;background-color: #d0d0d0;"></div>
                        <span>2. Phone</span>
                        </div><br><br>
                        
                        <div style="text-align: center;">
                            <button type="submit" class="btn btn-success btn-sm" style="width: 50%;height: 50px;border: none;background: #8296a5;color: #fff;font-size: 16px;font-weight: 500;letter-spacing: 1px;cursor: pointer;" 
                                    id="requestOtp"><?php echo e(trans_choice('general.request',1)); ?> <?php echo e(trans_choice('general.otp',1)); ?></button><br>
                                </div>
                        

                        
                        <?php echo Form::open(array('url' => 'user/verify_phone/check_otp','class'=>'',"enctype" => "multipart/form-data")); ?>

                            
                            

                            <div id="emailGroup" class="form-group">
                                <?php echo Form::label('otp',trans('general.otp'),array('class'=>'control-label')); ?>

                                <?php echo Form::number('otp','',array('class'=>'form-control','required'=>'required','id'=>'otp')); ?>

                                
                            </div>

                            <div class="form-group">
                                <button id="submitsignup" type="submit" class="btn-primary"><?php echo e(trans_choice('general.verify',1)); ?></button>
                            </div>
                            <?php echo Form::close(); ?>

                        <?php endif; ?>
            <?php endif; ?>
                            
                        </form>
                    </section>

                    <section class="signinbox">
                        <p>Don't have an account?</p>
                                                    <a href="<?php echo e(url('register')); ?>" id="ac-sign-in" clevertapevent="Sign In">REGISTER</a>
                                            </section>
                </div>
            </div>          
        </main>
        <!-- End of First-Fold -->
        <script>
        $("#requestOtp").click(function (e) {
            $.ajax({
                type: 'GET',
                url: '<?php echo e(url('user/verify_phone/send_otp/'.Sentinel::getUser()->id)); ?>',
                dataType: 'json',
                success: function (data) {
                    if (data.success === 1) {
                        swal({
                            title: '<?php echo e(trans_choice('general.success',1)); ?>',
                            html: '<?php echo e(trans_choice('general.sms_send_success',1)); ?>',
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: '<?php echo e(trans_choice('general.ok',1)); ?>',
                        });
                        $('#requestOtp').hide();
                    } else {
                        swal({
                            title: '<?php echo e(trans_choice('general.error',1)); ?>',
                            text: '<?php echo e(trans_choice('general.error_occurred',1)); ?>',
                            type: 'warning',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: '<?php echo e(trans_choice('general.ok',1)); ?>',
                            cancelButtonText: '<?php echo e(trans_choice('general.cancel',1)); ?>'
                        })
                    }
                }
                ,
                error: function (e) {
                    swal({
                        title: '<?php echo e(trans_choice('general.error',1)); ?>',
                        text: '<?php echo e(trans_choice('general.error_occurred',1)); ?>',
                        type: 'warning',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: '<?php echo e(trans_choice('general.ok',1)); ?>',
                        cancelButtonText: '<?php echo e(trans_choice('general.cancel',1)); ?>'
                    })
                }
            });
        })
    </script>
        <!-- JS File -->
        <script data-pagespeed-orig-type="text/javascript" src="https://cf.upstox.com/open-demat-account/app.js.pagespeed.jm.pLWohfiWgz.js" defer type="text/psajs" data-pagespeed-orig-index="2"></script>

        <style>.no-select{-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-o-user-select:none;user-select:none}section.demat-help{position:fixed;height:100vh;width:100vw;top:0;overflow:hidden;z-index:2}.demat-help .help-overlay{background-color:rgba(0,0,0,.5);width:100%;height:100%}.help-sidebar{width:430px;height:100%;position:absolute;right:-400px;top:0;display:block;background-color:#fff}.help-sidebar h6{font-size:18px;margin:25px 30px}.help-sidebar h6 span{position:absolute;right:50px;cursor:pointer;font-size:12px;top:30px}section.demat-help{}.demat-help .help-overlay{}.demat-help .help-sidebar{transition:all .2s ease-out}.help-sidebar h2{font-size:18px}.help-sidebar .help-topics-container{padding:0 30px;height:80%;overflow-y:auto}.help-sidebar .help-topic{border:1px solid rgba(0,0,0,.125);border-radius:.25rem;margin-bottom:30px}.help-topic .topic-name{background:#ececec;padding:20px;font-weight:700;font-size:16px;cursor:pointer;margin:0}.help-topic .topic-dropdown{font-size:14px;line-height:1.43;letter-spacing:.7px;text-align:left;color:#4c4c4c;max-height:0;overflow:hidden;-webkit-transition:max-height .2s ease-out;-moz-transition:max-height .2s ease-out;-o-transition:max-height .2s ease-out;transition:max-height .2s ease-out}.help-topic .topic-dropdown.show-faq{max-height:300px;padding:20px}.topic-dropdown div{}.topic-dropdown ul{padding:15px;margin-top:0}.topic-dropdown li{}main.second-fold{background:url(https://cf.upstox.com/open-demat-account/images/grey-bottom.svg) no-repeat;width:100%;background-position:right top 6px}section.demat-benefits{width:100%;margin-bottom:60px}section.demat-benefits .benefits-container{display:flex;width:900px;flex-wrap:wrap;justify-content:space-between;margin:0 auto}.benefits-container .benefits{width:320px;text-align:center;margin-bottom:50px}.benefits-container .benefits h2{font-size:22px;margin:20px 0}.benefits-container .benefits p{font-size:18px;line-height:35px;letter-spacing:.5px}.benefits .benefits-img{margin:0 auto;height:65px;width:65px;background:url(https://cf.upstox.com/open-demat-account/images/xdemat_sprite.png.pagespeed.ic._cYAE8glE8.png) no-repeat}.benefits-img.cross-image{background-position:-3px 5px}.benefits-img.tech-image{background-position:-73px 6px}.benefits-img.cost-image{background-position:-142px 5px}.benefits-img.paper-image{background-position:-207px -2px}section.demat-howto .howto-container{display:flex;width:1000px;flex-wrap:wrap;justify-content:space-between;margin:0 auto}.howto-container .howto{width:320px}.howto-container .howto p{display:inline-block;width:220px;margin:0;margin-left:20px;font-size:16px;line-height:30px;letter-spacing:.5px}.howto .howto-img{margin:0 auto;height:180px;width:235px;background:url(https://cf.upstox.com/open-demat-account/images/xdemat_sprite.png.pagespeed.ic._cYAE8glE8.png) no-repeat;margin-bottom:30px}.howto .howto-num{height:80px;width:50px;display:inline-block;background:url(https://cf.upstox.com/open-demat-account/images/xdemat_sprite.png.pagespeed.ic._cYAE8glE8.png) no-repeat}.howto-img.form-image{background-position:-2px -94px;margin-top:30px}.howto-img.scan-image{width:200px;height:210px;background-position:-250px -78px}.howto-img.welcome-image{height:205px;width:250px;background-position:-460px -79px}.howto-num.one-image{background-position:-328px 0}.howto-num.two-image{background-position:-462px 0}.howto-num.three-image{background-position:-662px 0}section.demat-faq{background-color:#0046bb;margin-top:80px;padding-top:5px}section.demat-faq .faq-container{display:flex;flex-wrap:wrap;width:1000px;margin:0 auto;justify-content:space-between;padding-bottom:60px}section.demat-faq .faq{flex:0 1 460px}section.demat-faq .faq a{color:#fff}section.demat-faq .faq h2.show-on-desktop{display:block}section.demat-faq .faq h2.show-on-mobile{display:none}.carousal-container .carousal-left{display:none}.carousal-container .carousal-right{display:none}@media  screen and (max-width:1150px){body{background:#efefef}main.second-fold{background:#efefef}.carousal-container{display:flex;flex-direction:row;width:400px;margin:0 auto}.carousal-container .carousal-window{overflow:hidden;box-shadow:0 0 20px 0 rgba(0,0,0,.1)}.carousal-container .carousal{display:flex;height:100%;overflow:hidden;position:relative;-webkit-transition:left .2s ease-out;-moz-transition:left .2s ease-out;-o-transition:left .2s ease-out;transition:left .2s ease-out}.carousal-container .carousal-left{flex:0 0 40px;display:block;cursor:pointer;font-size:50px;text-align:center;line-height:340px}.carousal-container .carousal-right{flex:0 0 40px;display:block;cursor:pointer;font-size:50px;text-align:center;line-height:340px}.demat-benefits .carousal-container .carousal-window{height:320px}section.demat-benefits .benefits-container{width:1280px}.benefits-container .benefits{width:320px;height:100%;text-align:center;margin-bottom:50px;display:inline-block;background-color:#fff;padding:20px}section.demat-howto .howto-container{display:flex;width:100%;flex-direction:column;align-items:center}.howto-container .howto{margin-bottom:50px}section.demat-faq .faq-container{display:flex;flex-wrap:wrap;width:100%;padding:0 60px 60px 60px}section.demat-faq .faq{flex:0 1 100%}section.demat-faq .faq h2{cursor:pointer;-webkit-touch-callout:none;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}section.demat-faq .faq p{max-height:0;overflow:hidden;-webkit-transition:max-height .2s ease-out;-moz-transition:max-height .2s ease-out;-o-transition:max-height .2s ease-out;transition:max-height .2s ease-out}section.demat-faq .faq h2.show-on-desktop{display:none}section.demat-faq .faq h2.show-on-mobile{display:block}section.demat-feedback .carousal-container .feedback-container{font-size:16px;justify-content:left;width:1060px}section.demat-feedback .feedback-container .user-feedback{font-size:14px}section.demat-feedback .feedback-container .feedback{flex:0 0 300px;background:#fff;padding:20px}.demat-feedback .carousal-container{width:380px}.demat-feedback .carousal-container .carousal-window{height:340px}.second-fold section.demat-feedback h6{width:86%}.second-fold section.demat-account-link button{width:100%;padding:30px}body footer{padding:10px}}section.demat-faq h2{font-size:22px;font-weight:900;letter-spacing:1.1px;text-align:left;color:#fff;margin-bottom:20px}.faq p{font-size:16px;font-weight:300;text-align:left;color:#e0e0e0;letter-spacing:.6px;line-height:30px}section.demat-feedback{margin-top:60px;margin-bottom:60px}section.demat-feedback h6{font-size:22px;line-height:1.36;letter-spacing:.5px;text-align:center;color:#4c4c4c;width:500px;margin:0 auto;font-weight:400;margin-bottom:50px}section.demat-feedback .feedback-container{display:flex;flex-wrap:wrap;justify-content:space-around;width:1000px;margin:0 auto}.feedback-container .feedback{flex:1 1;text-align:center;padding:30px;position:relative}.feedback-container .feedback:hover{box-shadow:0 0 20px 0 rgba(0,0,0,.1)}.feedback .user-img{width:115px;height:115px;margin-bottom:30px;margin:0 auto;background-color:#d4d4d4;border-radius:50%;line-height:113px;font-size:70px;font-weight:900;color:#fff}.feedback .user-img.one{}.feedback .user-feedback{font-size:18px;line-height:1.39;letter-spacing:.4px;text-align:center;color:#2a2a2a;margin-bottom:30px;margin-top:40px}.feedback .user-name{font-weight:900;position:absolute;bottom:0;width:calc(100% - 60px)}section.demat-account-link{background-color:#0046bb;display:flex;flex-direction:column;align-items:center}section.demat-account-link h6{text-align:center;margin:0;font-size:22px;color:#fff;font-weight:400;margin-bottom:80px}section.demat-account-link button{background-color:#fff;box-shadow:0 14px 21px 0 rgba(0,0,0,.28);padding:27px 90px;font-size:18px;font-weight:700;letter-spacing:.9px;width:auto;text-align:center;color:#0046bb;text-transform:uppercase;border:none;margin-bottom:80px;cursor:pointer}footer{background:#000;font-size:20px;line-height:2;letter-spacing:.3px;color:#445a70!important;padding:60px 150px 30px 150px;display:flex;flex-direction:column}footer p,footer #footer_readmore{font-size:12px}footer p:not(.made-in-india){margin-top:0}footer h2{font-size:14px}footer h3{font-size:13px;margin-bottom:0}.custom-container{padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}@media (min-width:768px){.custom-container{width:750px}}@media (min-width:992px){.custom-container{width:970px}}@media (min-width:1200px){.custom-container{width:1000px}}footer #footer_readmore{color:#0046bb;cursor:pointer}footer a{text-decoration:none;color:#0046bb}.validation-text{display:none}#iccodeGroup .validation-text{bottom:-26px}.validation-text.invalid_email,.validation-text.invalid_mobile,.validation-text.zero_start{display:none}.validation-text.duplicate_email,.validation-text.duplicate_mobile,.validation-text.empty_password,.validation-text.short_password{display:none}.benefit-demat-ac{font-size:50px;font-weight:300;text-align:center;color:#001a46;width:100%;margin-bottom:50px}.subheading{font-size:22px;font-weight:900;line-height:1;letter-spacing:1.1px;text-align:center;color:#000}.how-to-open-demat-ac{font-size:50px;font-weight:300;text-align:center;color:#001a46;width:100%}.faq-title{font-size:50px;font-weight:300;text-align:center;color:#fff;width:100%;margin-top:60px;margin-bottom:60px}.testimonial-title{font-size:50px;font-weight:300;line-height:1;text-align:center;color:#001a46;width:100%;margin:60px 0 40px 0}.c2a-title{font-size:50px;font-weight:300;line-height:1;text-align:center;width:100%;margin-bottom:50px;margin-top:50px;color:#fff}@media  screen and (max-width:450px){.help-sidebar{width:calc(100% - 40px)}.help-topic .topic-dropdown.show-faq{max-height:340px}.carousal-container{width:300px}.carousal-container .carousal-left,.carousal-container .carousal-right{flex:0 0 20px;line-height:300px}.demat-benefits .carousal-container .carousal-window{height:300px}.benefits-container .benefits{width:260px}.demat-feedback .carousal-container{width:300px}section.demat-feedback .feedback-container .feedback{flex:0 0 260px}.benefits-container .benefits p{font-size:14px;line-height:20px}.howto .howto-num{margin-left:23px}section.demat-faq .faq h2{font-size:16px}section.demat-faq .faq-container{padding:0 30px 20px}section.demat-faq .faq h2.show-on-mobile{text-align:center}section.demat-benefits .benefits-container{width:1160px}.help-topic .topic-name{overflow:hidden;word-wrap:break-word}.benefit-demat-ac,.how-to-open-demat-ac,.faq-title,.testimonial-title,.c2a-title{font-size:30px;font-weight:400}}</style>

        </main>
        
        
    </body>
</html>