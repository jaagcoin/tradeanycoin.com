<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trade any Coin - Exchange Portal</title>

<!-- Bootstrap CSS CDN -->
<link rel="stylesheet" href="http://localhost:8000/assets/bootstrap/css/bootstrap.min.css" >
<!-- Our Custom CSS -->
<link rel="stylesheet" href="http://localhost:8000/assets/css/style.css">
<!-- <link rel="stylesheet" href="http://localhost:8000/assets/style2.css"> -->
<!-- Scrollbar Custom CSS -->
<link rel="stylesheet" href="http://localhost:8000/assets/css/jquery.mCustomScrollbar.min.css">
<link rel="stylesheet" href="<?php echo e(asset('assets/font/materialdesignicons-webfont.woff')); ?>">

<!-- Font Awesome JS -->
<script defer src="http://localhost:8000/assets/js/solid.js"></script>
<script defer src="http://localhost:8000/assets/js/fontawesome.js"></script>
</head>

<body>
<div class="wrapper"> 
    <?php
        $usd = \App\Models\TradeCurrency::where('default_currency', 1)->first();
        $btc = \App\Models\TradeCurrency::where('network', "bitcoin")->first();
        $bch = \App\Models\TradeCurrency::where('network', "bitcash")->first();
        $jaagcoin = \App\Models\TradeCurrency::where('network', "jaagcoin")->first();
        $ltc = \App\Models\TradeCurrency::where('network', "litecoin")->first();
        $neo = \App\Models\TradeCurrency::where('network', "neo")->first();
        $eth = \App\Models\TradeCurrency::where('network', "ethereum")->first();
    ?>
  <!-- Sidebar  -->
  <nav id="sidebar">
    <div class="sidebar-header">
      <h3><img src="https://tradeanycoin.com/assets/images/logo.jpg" alt="logo" /></h3>
    </div>
    <ul class="list-unstyled components">
      <li class="dashboard active"><a href="<?php echo e(url('dashboard')); ?>">Dashboard</a></li>
      <li class="wallet"> <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Wallet</a>
        <ul class="collapse list-unstyled" id="homeSubmenu">
          <li><a href="deposits-withdrawals.html">Deposit & Withdraws</a></li>
          <li><a href="history.html">History</a></li>
        </ul>
      </li>
      <li class="exchange"> <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Exchange</a>
        <ul class="collapse list-unstyled" id="pageSubmenu">
          <li> <a href="exchange.html">Exchange</a> </li>
          <li> <a href="my-open-orders.html">My Open Orders</a> </li>
          <li> <a href="my-trade-history.html">My Trade History</a> </li>
        </ul>
      </li>
      <li class="transaction"><a href="transactions.htm"l>Transaction</a></li>
      <li class="settings"> <a href="#settingsSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Settings</a>
        <ul class="collapse list-unstyled" id="settingsSubmenu">
          <li> <a href="profile.html">My Profile</a> </li>
          <li> <a href="security.html">Security</a> </li>
        </ul>
      </li>
      <li class="affiliate"> <a href="#affiliateSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Affiliate</a>
        <ul class="collapse list-unstyled" id="affiliateSubmenu">
          <li> <a href="member.html">Members</a> </li>
        </ul>
      </li>
      <li class="support"> <a href="support-ticket.html">Support and Ticket</a> </li>
    </ul>
  </nav>
  
  <!-- Page Content  -->
  <div id="content">
    <nav id="headnev" class="navbar navbar-expand-lg navbar-light">
      <div class="container-fluid">
        <button type="button" id="sidebarCollapse" class="btn btn-info"> <i class="fas fa-align-left"></i> </button>
        <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <i class="fas fa-align-justify"></i> </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="nav navbar-nav ml-auto">
            <li class="nav-item active"> 1 BTC = 8180.28 </li>
            <li class="nav-item"> 1 ETC = 469.07 </li>
            <li class="nav-item"> NAPIERIRF </li>
            <li class="nav-item"> <a class="nav-link" href="#">Sign out</a> </li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="inner-content">
      <link rel="stylesheet" href="<?php echo e(asset('assets/themes/limitless/css/style.css')); ?>">
<section>
      <div class="pageContent">
        <div class="container">
          

         
          <div class="topInfoCards">
            <?php
                $usd = \App\Models\TradeCurrency::where('default_currency', 1)->first();
                $btc = \App\Models\TradeCurrency::where('network', "bitcoin")->first();
                $bch = \App\Models\TradeCurrency::where('network', "bitcash")->first();
                $jaagcoin = \App\Models\TradeCurrency::where('network', "jaagcoin")->first();
                $ltc = \App\Models\TradeCurrency::where('network', "litecoin")->first();
                $neo = \App\Models\TradeCurrency::where('network', "neo")->first();
                $eth = \App\Models\TradeCurrency::where('network', "ethereum")->first();
            ?>

            

        


           
        <div id="main-content-container" class="bars-above"><div id="dashboard-page">

        

            <!-- Waluty -->
            <!--<div id="currencies"></div>-->

            <div class="row page-margin">

                <div class="row ">
                    <div class="row ">
                        

                        <!-- Portfele -->
                        <div class="col-xs-12 col-md-4 col-lg-4">
                            <div id="wallets" class="card"><div class="wallets-container">
    <div class="small-header horizontal layout center">
    <div class="left">
        <h4>Wallets</h4>
    </div>

    <div class="right flex">
        <value>
            <number id="wallets-value">≈ <?php echo e(number_format(\App\Helpers\GeneralHelper::user_aed_available(Sentinel::getUser()->id),4)); ?></number>
            <currency id="wallets-currency">
                <span class="currency-name-holder">AED</span>
                
            </currency>

        </value>
    </div>
</div>

<div class="items layout vertical">
    



    <div class="item layout horizontal center waves-effect" data-currency="btc">
            <div class="left layout horizontal center">
                <i class="currency-icon big mdi-currency-bitcoin" style="background: #6d5170;"></i>
                <span class="currency-name" style="color:#6d5170;">glc</span>
            </div>

            <div class="right flex">
                <div class="row">
                    <div class="col xs12 s12 m6 l6 xl6 offset-xs0 offset-s0 offset-m0 offset-l0 offset-xl0 wallet-value">
                        <value>
                            <number style="color: #6d5170;">12.00</number>
                        </value>

                        <div class="blocked layout horizontal center hidden">
                            <i class="mdi-lock"></i>
                            <number>0.00000000</number>
                        </div>
                    </div>

                    
                        <div class="col xs12 s12 m6 l6 xl6 about-value layout horizontal center hidden">-</div>
                    
                </div>
            </div>
        </div>


        <div class="item layout horizontal center waves-effect" data-currency="btc">
            <div class="left layout horizontal center">
                <i class="currency-icon big mdi-currency-bitcoin" style="background: #ffb400;"></i>
                <span class="currency-name" style="color:#ffb400;">btc</span>
            </div>

            <div class="right flex">
                <div class="row">
                    <div class="col xs12 s12 m6 l6 xl6 offset-xs0 offset-s0 offset-m0 offset-l0 offset-xl0 wallet-value">
                        <value>
                            <number style="color: #ffb400;"><?php echo number_format(Sentinel::getuser()->bitcoin_balance,4); ?></number>
                        </value>

                        <div class="blocked layout horizontal center hidden">
                            <i class="mdi-lock"></i>
                            <number>0.00000000</number>
                        </div>
                    </div>

                    
                        <div class="col xs12 s12 m6 l6 xl6 about-value layout horizontal center hidden">-</div>
                    
                </div>
            </div>
        </div>
    
        <div class="item layout horizontal center waves-effect" data-currency="usd">
            <div class="left layout horizontal center">
                <i class="currency-icon big mdi-currency-dollar" style="background: #37c866;"></i>
                <span class="currency-name" style="color:#37c866;">aed</span>
            </div>

            <div class="right flex">
                <div class="row">
                    <div class="col xs12 s12 m6 l6 xl6 offset-xs0 offset-s0 offset-m0 offset-l0 offset-xl0 wallet-value">
                        <value>
                            <number style="color: #37c866;"><?php echo e(number_format(\App\Helpers\GeneralHelper::user_aed_available(Sentinel::getUser()->id),4)); ?></number>
                        </value>

                        <div class="blocked layout horizontal center hidden">
                            <i class="mdi-lock"></i>
                            <number>0.00</number>
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
    
        <div class="item layout horizontal center waves-effect" data-currency="game">
            <div class="left layout horizontal center">
                <i class="currency-icon big mdi-currency-gamecredit" style="background: #98c01f;"></i>
                <span class="currency-name" style="color:#98c01f;">ltc</span>
            </div>

            <div class="right flex">
                <div class="row">
                    <div class="col xs12 s12 m6 l6 xl6 offset-xs0 offset-s0 offset-m0 offset-l0 offset-xl0 wallet-value">
                        <value>
                            <number style="color: #98c01f;"><?php echo number_format(Sentinel::getuser()->litecoin_balance,4); ?></number>
                        </value>

                        <div class="blocked layout horizontal center hidden">
                            <i class="mdi-lock"></i>
                            <number>0.00000000</number>
                        </div>
                    </div>

                    
                        <div class="col xs12 s12 m6 l6 xl6 about-value layout horizontal center hidden">-</div>
                    
                </div>
            </div>
        </div>
    
        <div class="item layout horizontal center waves-effect" data-currency="eth">
            <div class="left layout horizontal center">
                <i class="currency-icon big mdi-currency-ethereum" style="background: #4a90e2;"></i>
                <span class="currency-name" style="color:#4a90e2;">eth</span>
            </div>

            <div class="right flex">
                <div class="row">
                    <div class="col xs12 s12 m6 l6 xl6 offset-xs0 offset-s0 offset-m0 offset-l0 offset-xl0 wallet-value">
                        <value>
                            <number style="color: #4a90e2;"><?php echo number_format(Sentinel::getuser()->ethereum_balance,4); ?></number>
                        </value>

                        <div class="blocked layout horizontal center hidden">
                            <i class="mdi-lock"></i>
                            <number>0.00000000</number>
                        </div>
                    </div>

                    
                        <div class="col xs12 s12 m6 l6 xl6 about-value layout horizontal center hidden">-</div>
                    
                </div>
            </div>
        </div>
    
        <div class="item layout horizontal center waves-effect" data-currency="bcc">
            <div class="left layout horizontal center">
                <i class="currency-icon big mdi-currency-bitcoin-cash" style="background: #ff9639;"></i>
                <span class="currency-name" style="color:#ff9639;">neo</span>
            </div>

            <div class="right flex">
                <div class="row">
                    <div class="col xs12 s12 m6 l6 xl6 offset-xs0 offset-s0 offset-m0 offset-l0 offset-xl0 wallet-value">
                        <value>
                            <number style="color: #ff9639;"><?php echo number_format(Sentinel::getuser()->ripple_balance,4); ?></number>
                        </value>

                        <div class="blocked layout horizontal center hidden">
                            <i class="mdi-lock"></i>
                            <number>0.00000000</number>
                        </div>
                    </div>

                    
                        <div class="col xs12 s12 m6 l6 xl6 about-value layout horizontal center hidden">-</div>
                    
                </div>
            </div>
        </div>
    
        
    


    <!-- karta jako ostatni - 5ty item -->
    <!--<div class="item layout horizontal center" id="card-item"></div>-->
</div>

                      <div class="layout horizontal center-justified goto-link-container">
                            <a href="<?php echo e(url('wallet/btc')); ?>" title="" id="go-to-history" class="layout horizontal center">
                                <h6 style="color: #6d5170">See Wallets</h6>
                                <i class="mdi-chevron-right"></i>
                            </a>
                        </div>


</div></div>
                        </div>

                        <!-- Rynki -->
                        <div class="col-xs-12 col-md-4 col-lg-5">
                            <div id="markets" class="card"><div class="markets-container">

 <div class="small-header horizontal layout center">
    <div class="left"><br>
        <h4>AED Markets</h4><hr>
    </div>

    
</div>



<div id="markets-list">
    <div id="header" class="layout horizontal center">
        <div class="space"></div>
        <div class="header row no-padding-vertical">
            <div class="col s6 xxxl7 layout horizontal center">
                <div>Rate</div>
            </div>

            <div class="col s6 xxxl5 layout horizontal center">
                <div>Volume</div>
            </div>
        </div>

        <div class="empty"></div>
    </div>

    <div class="items">
        <!-- tutaj pojawia sie rynki z danej waluty glownej -->



    <div class="item layout horizontal center active" data-market="BTC-USD" data-first-currency="BTC" data-second-currency="USD"><!-- klasa "active" -->
    <div class="layout horizontal center currency-pair">
        <i class="currency-icon big mdi-currency-bitcoin" style="background-color: #6d5170;"></i>
        <span>GLC</span>
    </div>

    <div class="overlay" style="background-color: #6d5170;"></div>

    <div class="horizontal layout center row no-margin">
        <div class="col xxs12 s6 xxxl7 value-and-change">
            <div class="layout horizontal center rate-value">
                <value>3.70</value>
            </div>

            <div class="layout horizontal center change-value">
                <value>
                    <difference class="layout horizontal center up" style="display: flex;">
                        <i class="mdi-chevron-up"></i>
                        <i class="mdi-equal"></i>
                        <span id="difference-value">0.00%</span>
                    </difference>
                </value>
                <!--<div class="transaction-text">Sell</div>-->
            </div>
        </div>

        <div class="col xxs12 s6 xxxl5 layout horizontal center">
            <div class="layout horizontal center volume-value">
                <value>100.479195</value>
            </div>
        </div>
    </div>

    <button class="btn btn-flat btn-floating btn-transparent waves-effect go-to-market">
        <i class="mdi-chevron-right"></i>
    </button>
</div>



    <div class="item layout horizontal center active" data-market="BTC-USD" data-first-currency="BTC" data-second-currency="USD"><!-- klasa "active" -->
    <div class="layout horizontal center currency-pair">
        <i class="currency-icon big mdi-currency-bitcoin" style="background-color: #ffb400;"></i>
        <span>BTC</span>
    </div>

    <div class="overlay" style="background-color: #ffb400;"></div>

    <div class="horizontal layout center row no-margin">
        <div class="col xxs12 s6 xxxl7 value-and-change">
            <div class="layout horizontal center rate-value">
                
            </div>

            <div class="layout horizontal center change-value">
                <value>
                    <difference class="layout horizontal center up" style="display: flex;">
                        <i class="mdi-chevron-up"></i>
                        <i class="mdi-equal"></i>
                        <span id="difference-value">1.56%</span>
                    </difference>
                </value>
                <!--<div class="transaction-text">Sell</div>-->
            </div>
        </div>

        <div class="col xxs12 s6 xxxl5 layout horizontal center">
            <div class="layout horizontal center volume-value">
                <value>1.47919519</value>
            </div>
        </div>
    </div>

    <button class="btn btn-flat btn-floating btn-transparent waves-effect go-to-market">
        <i class="mdi-chevron-right"></i>
    </button>
</div><div class="item layout horizontal center active" data-market="BCC-USD" data-first-currency="BCC" data-second-currency="USD"><!-- klasa "active" -->
    <div class="layout horizontal center currency-pair">
        <i class="currency-icon big mdi-currency-bitcoin-cash" style="background-color: #ff9639;"></i>
        <span>LTC</span>
    </div>

    <div class="overlay" style="background-color: #ff9639;"></div>

    <div class="horizontal layout center row no-margin">
        <div class="col xxs12 s6 xxxl7 value-and-change">
            <div class="layout horizontal center rate-value">
                
            </div>

            <div class="layout horizontal center change-value">
                <value>
                    <difference class="layout horizontal center down" style="display: flex;">
                        <i class="mdi-chevron-up"></i>
                        <i class="mdi-equal"></i>
                        <span id="difference-value">5.32%</span>
                    </difference>
                </value>
                <!--<div class="transaction-text">Sell</div>-->
            </div>
        </div>

        <div class="col xxs12 s6 xxxl5 layout horizontal center">
            <div class="layout horizontal center volume-value">
                <value>0.18262453</value>
            </div>
        </div>
    </div>

    <button class="btn btn-flat btn-floating btn-transparent waves-effect go-to-market">
        <i class="mdi-chevron-right"></i>
    </button>
</div><div class="item layout horizontal center active" data-market="ETH-USD" data-first-currency="ETH" data-second-currency="USD"><!-- klasa "active" -->
    <div class="layout horizontal center currency-pair">
        <i class="currency-icon big mdi-currency-ethereum" style="background-color: #4a90e2;"></i>
        <span>ETH</span>
    </div>

    <div class="overlay" style="background-color: #4a90e2;"></div>

    <div class="horizontal layout center row no-margin">
        <div class="col xxs12 s6 xxxl7 value-and-change">
            <div class="layout horizontal center rate-value">
                
            </div>

            <div class="layout horizontal center change-value">
                <value>
                    <difference class="layout horizontal center up" style="display: flex;">
                        <i class="mdi-chevron-up"></i>
                        <i class="mdi-equal"></i>
                        <span id="difference-value">0.08%</span>
                    </difference>
                </value>
                <!--<div class="transaction-text">Sell</div>-->
            </div>
        </div>

        <div class="col xxs12 s6 xxxl5 layout horizontal center">
            <div class="layout horizontal center volume-value">
                <value>0.61517164</value>
            </div>
        </div>
    </div>

    <button class="btn btn-flat btn-floating btn-transparent waves-effect go-to-market">
        <i class="mdi-chevron-right"></i>
    </button>
</div><div style="border-bottom: 0px;" class="item layout horizontal center active" data-market="LSK-USD" data-first-currency="LSK" data-second-currency="USD"><!-- klasa "active" -->
    <div class="layout horizontal center currency-pair">
        <i class="currency-icon big mdi-currency-lisk" style="background-color: #1170a4;"></i>
        <span>NEO</span>
    </div>

    <div class="overlay" style="background-color: #1170a4;"></div>

    <div class="horizontal layout center row no-margin">
        <div class="col xxs12 s6 xxxl7 value-and-change">
            <div class="layout horizontal center rate-value">
                
            </div>

            <div class="layout horizontal center change-value">
                <value>
                    <difference class="layout horizontal center up" style="display: flex;">
                        <i class="mdi-chevron-up"></i>
                        <i class="mdi-equal"></i>
                        <span id="difference-value">10.96%</span>
                    </difference>
                </value>
                <!--<div class="transaction-text">Sell</div>-->
            </div>
        </div>

        <div class="col xxs12 s6 xxxl5 layout horizontal center">
            <div class="layout horizontal center volume-value">
                <value>14.87150040</value>
            </div>
        </div>
    </div>

    <button class="btn btn-flat btn-floating btn-transparent waves-effect go-to-market">
        <i class="mdi-chevron-right"></i>
    </button>
</div></div>

<div class="layout horizontal center-justified goto-link-container">
                            <a href="<?php echo e(url('wallet/btc')); ?>" title="" id="go-to-history" class="layout horizontal center">
                                <h6 style="color: #6d5170">See Market</h6>
                                <i class="mdi-chevron-right"></i>
                            </a>
                        </div>
</div>


<div class="layout horizontal center-justified goto-link-container">
    
</div>

    <!--div class="loader-container" style="background: #fff">
        <paper-spinner-lite active></paper-spinner-lite>
    </div-->
</div></div>
                        </div>

                       
            <div class="col-xs-12 col-md-4 col-lg-3">
            <div id="wallets" class="card"><div class="wallets-container">
              <div class="small-header horizontal layout center">
                  <div class="left"><br>
                      <h4>Account Status</h4><hr>
                  </div>

                  
              </div>
                <?php if(Sentinel::getUser()->email_verified==1): ?>
                <div class="item layout horizontal center waves-effect">
                <div class="left layout horizontal center">
                      
                      <h5 class="currency-name">1. Email Verified</h5>

                </div>

                <div class="right flex" style="font-size: 3rem; color: green; margin-left: 53%">
                <i class="mdi-verified"></i>
                </div></div>
                <?php else: ?>
                  <div class="item layout horizontal center waves-effect">
                    <div class="left layout horizontal center">
                      
                      <h5 class="currency-name"><a style="color:black;" href="<?php echo e(url('setting/data')); ?>">1. Email Verified</a></h5>
                    </div>

                    <div class="right flex" style="font-size: 3rem; color: red; margin-left: 53%">
                    <i class="mdi-close-circle"></i>
                    </div></div>
                <?php endif; ?>

                <?php if(Sentinel::getUser()->phone_verified==1): ?>
                <div class="item layout horizontal center waves-effect">
                <div class="left layout horizontal center">
                      
                      <h5 class="currency-name">2. Phone Verified</h5>

                </div>

                <div class="right flex" style="font-size: 3rem; color: green; margin-left: 51%">
                <i class="mdi-verified"></i>
                </div>
                </div>
                <?php else: ?>
                  <div class="item layout horizontal center waves-effect">
                    <div class="left layout horizontal center">
                      
                      <h5 class="currency-name"><a style="color:black;" href="<?php echo e(url('setting/data')); ?>">2. Phone Verified</a></h5>
                    </div>

                    <div class="right flex" style="font-size: 3rem; color: red; margin-left: 51%">
                    <i class="mdi-close-circle"></i>
                    </div></div>
                <?php endif; ?>

                <?php if(Sentinel::getUser()->documents_verified==1): ?>
                <div class="item layout horizontal center waves-effect">
                <div class="left layout horizontal center">
                      
                      <h5 class="currency-name">3. Documents Verified</h5>

                </div>

                <div class=" flex" style="font-size: 3rem; color: green; margin-left: 40%">
                <i class="mdi-verified"></i>
                </div></div>
                <?php else: ?>
                  <div class="item layout horizontal center waves-effect">
                    <div class="left layout horizontal center">
                      
                      <h5 class="currency-name"><a style="color:black;" href="<?php echo e(url('setting/data')); ?>">3. Documents Verified</a></h5>
                    </div>

                    <div class="right flex" style="font-size: 3rem; color: red; margin-left: 40%">
                    <i class="mdi-close-circle"></i>
                    </div></div>
                <?php endif; ?>

              </div><br>
                        <div class="layout horizontal center-justified goto-link-container">
                            <a href="<?php echo e(url('setting/data')); ?>" title="" id="go-to-history" class="layout horizontal center">
                                <h6 style="color: #6d5170">Go to Settings</h6>
                                <i class="mdi-chevron-right"></i>
                            </a>
                        </div>
                    </div>
                    </div>
                </div>

</ul>
</div>
</div>


            </div>



        
          <div class="row">

          <div class="col-lg-7 col-md-6 col-sm-12 col-xs-12">
          <div class="panel panel-body panelStyle" style="border: 2px solid #edeff1;">
          <div class="small-header horizontal layout center ">
            <div class="left ">
                <h4 style="font-size: 24px; color: #284274">History</h4><hr>
            </div>

        </div>
            <div class="table-responsive">

              <table id="order_history" class="table tableStyle">
                <thead>
                  <tr>
                    <th><?php echo e(trans_choice('general.type',1)); ?></th>
                    <th><?php echo e(trans_choice('general.status',1)); ?></th>
                    <th><?php echo e(trans_choice('general.market',1)); ?></th>
                    <th><?php echo e(trans_choice('general.price',1)); ?></th>
                    <th><?php echo e(trans_choice('general.volume',1)); ?></th>
                    <th><?php echo e(trans_choice('general.time',1)); ?></th>
                  </tr>
                </thead>
                <tbody>
                  <?php $__currentLoopData = \App\Models\OrderBook::where('user_id', Sentinel::getUser()->id)->orderBy('created_at','desc')->limit(5)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php
                    $trade_currency = \App\Models\TradeCurrency::find($key->trade_currency_id);
                    $default = \App\Models\TradeCurrency::where('default_currency', 1)->first();
                  ?>

                  
                  <tr>
                    <td class="typeTd">
                      <?php if($key->order_type=="ask"): ?>
                        <?php echo e(trans_choice('general.ask',1)); ?>

                        <?php endif; ?>
                        <?php if($key->order_type=="bid"): ?>
                        <?php echo e(trans_choice('general.bid',1)); ?>

                        <?php endif; ?>
                    </td>
                    <td>
                      <?php if($key->status=="pending"): ?>
                         <?php echo e(trans_choice('general.pending',1)); ?>

                      <?php endif; ?>
                      <?php if($key->status=="processing"): ?>
                         <?php echo e(trans_choice('general.processing',1)); ?>

                      <?php endif; ?>
                      <?php if($key->status=="cancelled"): ?>
                         <?php echo e(trans_choice('general.cancelled',1)); ?>

                      <?php endif; ?>
                      <?php if($key->status=="done"): ?>
                         <?php echo e(trans_choice('general.done',1)); ?>

                      <?php endif; ?>
                      <?php if($key->status=="accepted"): ?>
                         <?php echo e(trans_choice('general.accepted',1)); ?>

                      <?php endif; ?>
                    </td>
                    <td>
                      <?php if(!empty($trade_currency)): ?>
                         <?php echo e($trade_currency->xml_code); ?>

                         <?php endif; ?>
                         <?php if(!empty($default)): ?>
                         <?php echo e($default->xml_code); ?>


                      <?php endif; ?>
                    </td>
                    <td><?php echo e(round($key->amount,6)); ?></td>
                    <td><?php echo e(round( $key->volume,6)); ?></td>
                    <td><?php echo e($key->created_at); ?></td>
                  </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
              </table>
            </div>
          </div>

        </div>
        <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12" style="padding-bottom: 20px;">
        <div class="panel panel-body panelStyle" style="border: 2px solid #edeff1;">
        <div class="small-header horizontal layout center">
            <div class="left">
                <h4 style="font-size: 24px; color: #284274">How does it work?</h4><hr>
                <div class="left flex" style="font-size: 3rem; color: #1170a4;">
                    <i class="mdi-numeric-1-box"></i>   
                </div>
                <div class=" layout horizontal center">
                      
                      <h5 class="currency-name">Deposit AED via <b>Wire Transfer</b>, <b>Credit/Debit card</b> or <b>Cash pick-up service</b>.</h5>
                    </div><br>

                <div class="left flex" style="font-size: 3rem; color: #37c866;">
                    <i class="mdi-numeric-2-box"></i>   
                </div>
                <div class=" layout horizontal center">
                      
                      <h5 class="currency-name">Buy digital currencies like <b>Bitcoin, Litecoin, Ethereum and Ripple</b> using AED (United Arab Emirates Dirham).</h5>
                    </div><br>

                <div class="left flex" style="font-size: 3rem; color: #6d5170;">
                    <i class="mdi-numeric-3-box"></i>   
                </div>
                <div class=" layout horizontal center">
                      
                      <h5 class="currency-name">Start trading cryptocurrencies on our Professional <b>Trading platform</b>.</h5>
                    </div><br>
                <div class="layout horizontal center-justified goto-link-container">
                            <a href="<?php echo e(url('setting/data')); ?>" title="" id="go-to-history" class="layout horizontal center">
                                <h6 style="color: #6d5170">Go to FAQs</h6>
                                <i class="mdi-chevron-right"></i>
                            </a>
                        </div>
                

        </div>
      


      </div>
      </div>
      </div>
      </div>
    
      </div>

      
    </section>

        <script src="<?php echo e(asset('assets/plugins/amcharts/amcharts.js')); ?>"
                type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/plugins/amcharts/serial.js')); ?>"
                type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/plugins/amcharts/pie.js')); ?>"
                type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/plugins/amcharts/themes/light.js')); ?>"
                type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/plugins/amcharts/plugins/export/export.min.js')); ?>"
                type="text/javascript"></script>

        <script>
        $('#order_history').DataTable({
            
            "language": {
            "emptyTable":     "No deposts yet"
        }
    });
        
        
      
    </script>
    </div>
  </div>
</div>
<!-- jQuery CDN - Slim version (=without AJAX) --> 
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> 
<!-- Bootstrap JS --> 
<script src="http://localhost:8000/assets/bootstrap/js/bootstrap.min.js"></script> 
<!-- jQuery Custom Scroller CDN --> 
<script src="http://localhost:8000/assets/js/jquery.mCustomScrollbar.concat.min.js"></script> 
<script src="http://localhost:8000/assets/js/custom.js"></script>
</body>
</html>