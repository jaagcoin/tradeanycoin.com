
<?php $__env->startSection('title'); ?>
    <?php echo e(trans_choice('general.deposit',2)); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="pageContent">
    <div class="container">
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title"> <?php echo e(trans_choice('general.deposit',2)); ?></h6>

            <div class="heading-elements">

            </div>
        </div>
        <div class="panel-body ">
            <div class="table-responsive">
                <table id="data-table" class="table table-striped table-condensed table-hover">
                    <thead>
                    <tr>
                        <th><?php echo e(trans_choice('general.id',1)); ?></th>
                        <th><?php echo e(trans_choice('general.user',1)); ?></th>
                        <th><?php echo e(trans_choice('general.currency',1)); ?></th>
                        <th><?php echo e(trans_choice('general.amount',1)); ?></th>
                        <th><?php echo e(trans_choice('general.account',1)); ?></th>
                        <th><?php echo e(trans_choice('general.status',1)); ?></th>
                        <th><?php echo e(trans_choice('general.time',1)); ?></th>
                        <th><?php echo e(trans_choice('general.action',1)); ?></th>
                    </tr>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php
                        $trade_currency = \App\Models\TradeCurrency::find($key->trade_currency_id);
                        ?>
                        <tr>
                            <td><?php echo e($key->id); ?></td>
                            <td>
                                <?php if(!empty($key->user)): ?>
                                    <a href="<?php echo e(url('user/'.$key->user_id.'/show')); ?>"> <?php echo e($key->user->first_name); ?>  <?php echo e($key->user->last_name); ?></a>
                                <?php endif; ?>
                            </td>

                            <td>
                                <?php if(!empty($trade_currency)): ?>
                                    <?php echo e($trade_currency->xml_code); ?>

                                <?php endif; ?>
                            </td>
                            <td><?php echo e(number_format($key->amount,2)); ?></td>
                            <td>
                                <?php if($key->network!="usd"): ?>
                                    <?php echo e($key->receiver_address); ?>

                                <?php else: ?>
                                    <?php if($key->bank_account): ?>
                                        <?php if(!empty($key->bank_account->withdrawal_method)): ?>
                                            <?php echo e($key->bank_account->withdrawal_method->name); ?>

                                        <?php endif; ?>
                                         (<?php echo e($key->bank_account->account_name); ?>- <?php echo e($key->bank_account->account_number); ?>)
                                    <?php else: ?>
                                        <?php echo e($key->account_name); ?>- <?php echo e($key->account_number); ?>

                                    <?php endif; ?>


                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if($key->status=="pending"): ?>
                                    <?php echo e(trans_choice('general.pending',1)); ?>

                                <?php endif; ?>
                                <?php if($key->status=="processing"): ?>
                                    <?php echo e(trans_choice('general.processing',1)); ?>

                                <?php endif; ?>
                                <?php if($key->status=="cancelled"): ?>
                                    <?php echo e(trans_choice('general.cancelled',1)); ?>

                                <?php endif; ?>
                                <?php if($key->status=="done"): ?>
                                    <?php echo e(trans_choice('general.done',1)); ?>

                                <?php endif; ?>
                                <?php if($key->status=="accepted"): ?>
                                    <?php echo e(trans_choice('general.accepted',1)); ?>

                                <?php endif; ?>
                            </td>
                            <td><?php echo e($key->created_at); ?></td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>
                                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                                <?php if(Sentinel::hasAccess('deposits.update')): ?>
                                                    <li><a href="<?php echo e(url('deposit/'.$key->id.'/cancel')); ?>" class="delete">
                                                            <?php echo e(trans('general.cancel')); ?> </a>
                                                    </li>
                                                    <li><a href="<?php echo e(url('deposit/'.$key->id.'/pending')); ?>" class="delete">
                                                            <?php echo e(trans('general.pending')); ?> </a>
                                                    </li>
                                                    <li><a href="<?php echo e(url('deposit/'.$key->id.'/processing')); ?>" class="delete">
                                                            <?php echo e(trans('general.processing')); ?> </a>
                                                    </li>
                                                    <li><a href="<?php echo e(url('deposit/'.$key->id.'/done')); ?>" class="delete">
                                                            <?php echo e(trans('general.done')); ?> </a>
                                                    </li>
                                                <?php endif; ?>
                                                <?php if(Sentinel::hasAccess('deposits.delete')): ?>
                                                    <li><a href="<?php echo e(url('deposit/'.$key->id.'/delete')); ?>"
                                                           class="delete"> <?php echo e(trans('general.delete')); ?>

                                                        </a>
                                                    </li>
                                                <?php endif; ?>
                                            </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
                <?php
                $total_done=0; 
                $total_pending = 0; 
                $total_cancelled=0; 
                $total_processing=0;
                ?>

                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php
                        $trade_currency = \App\Models\TradeCurrency::find($key->trade_currency_id);
                        ?>
                <?php if($trade_currency->xml_code == "AED"): ?>
                    <?php if($key->status == "done"): ?>
                        <?php ($total_done += $key->amount); ?>
                    <?php endif; ?>
                    <?php if($key->status == "pending"): ?>
                        <?php ($total_pending += $key->amount); ?>
                    <?php endif; ?>
                    <?php if($key->status == "cancelled"): ?>
                        <?php ($total_cancelled += $key->amount); ?>
                    <?php endif; ?>
                    <?php if($key->status == "processing"): ?>
                        <?php ($total_processing += $key->amount); ?>
                    <?php endif; ?>  

                <?php endif; ?>
                <h6><?php echo e($key); ?></h6>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        
            </div>
        </div>
        <!-- /.panel-body -->
    </div>
</div>
</div>
    <!-- /.box -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer-scripts'); ?>
    <script>
        $('#data-table').DataTable({
            "order": [[6, "desc"]],
            "columnDefs": [
                {"orderable": false, "targets": [7]}
            ],
            "language": {
                "lengthMenu": "<?php echo e(trans('general.lengthMenu')); ?>",
                "zeroRecords": "<?php echo e(trans('general.zeroRecords')); ?>",
                "info": "<?php echo e(trans('general.info')); ?>",
                "infoEmpty": "<?php echo e(trans('general.infoEmpty')); ?>",
                "search": "<?php echo e(trans('general.search')); ?>",
                "infoFiltered": "<?php echo e(trans('general.infoFiltered')); ?>",
                "paginate": {
                    "first": "<?php echo e(trans('general.first')); ?>",
                    "last": "<?php echo e(trans('general.last')); ?>",
                    "next": "<?php echo e(trans('general.next')); ?>",
                    "previous": "<?php echo e(trans('general.previous')); ?>"
                }
            },
            responsive: false
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>