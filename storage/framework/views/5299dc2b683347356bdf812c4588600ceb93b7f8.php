
<?php $__env->startSection('title'); ?>
    <?php echo e(trans_choice('general.trade',2)); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>


    <div class="dashboard-main">
                
                
                    
                        <div class="row">
                        
                            <div class="col-sm-12 col-xs-12">

                                


                                <div class="theme-widget balance-widget open">
                                    <div class="widget-header">
                                        <h5 class="acr1"> <i class="fa fa-chevron-right" aria-hidden="true"></i> Open Orders </h5>
                                        <span style="float: right;margin-top: -25px;">
                                            <button onclick="order_middle('bid')" class="btn btn-sm btn-default">Bid</button>
                                             <button onclick="order_middle('ask')"  class="btn btn-sm btn-default">Ask</button>
                                                <select id="both_side" onchange="call_order(this.value)">
                                                    <!-- <option value="ALL">All</option> -->
                                                    <option <?php if('BTC' == 'BTC'): ?> selected=" " <?php endif; ?> value="BTC">BTC/AED</option>
                                                    <option <?php if('LTC' == 'LTC'): ?> selected=" " <?php endif; ?> value="LTC">LTC/AED</option>
                                                    <option <?php if('ETH' == 'ETH'): ?> selected=" " <?php endif; ?> value="ETH">ETH/AED</option>
                                                    <option <?php if('XRP' == 'XRP'): ?> selected=" " <?php endif; ?> value="XRP">XRP/AED</option>
                                                </select>
                                        </span>
                                    </div>


                                    <div class="widget-body">
                                        <div class="table-responsive">
                                            <table id="example12" class="display" style="width:100%">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Side</th>
                                                    <th>order_id</th>
                                                    <th>Pair</th>
                                                    <th>Price</th>
                                                    <th>Amount</th>
                                                    <th>Fee</th>
                                                    <th>TOTAL</th>
                                                    <th>Action</th>
                                                    <th>Status</th>
                                                </tr>
                                                </thead>
                                                <tbody id="order_middle">
                                                <?php $i=1; $exchange_data = array(); ?>
                                                <?php $__currentLoopData = $exchange_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $exg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if($exg->status == 0 || $exg->status == 2): ?>
                                                        <?php  if($exg->side == 'buy') { $color='#fbe9e7'; }
                                                        elseif($exg->side == 'sell') {  $color='#e8f5e9'; }
                                                        else {}  ?>
                                                        <?php $total = ($exg->price) * ($exg->same_price);  ?>
                                                        <tr id="<?php echo e("order_cancal".$exg->id); ?>" style="background-color: <?php echo $color; ?>">
                                                            <td><b><?php echo e($i++); ?></b></td>
                                                            <td><b><?php echo e($exg->side); ?></b></td>
                                                            <td><?php echo e($exg->order_id); ?></td>
                                                            <td><?php echo e($exg->symbol); ?></td>
                                                            <td><?php echo e(number_format($exg->same_price, 2)); ?></td>
                                                            <td><?php echo e(number_format($exg->price, 2)); ?></td>

                                                            <td><?php echo e(number_format($exg->buy_fees, 2)); ?></td>
                                                            <td><?php echo e(number_format($exg->final_aed,2)); ?></td>
                                                            <td >
                                                                <?php if($exg->status == 0): ?>
                                                                    <lable onclick="cancel_order_user('<?php echo e($exg->id); ?>','<?php echo e($exg->order_id); ?>')" class="btn btn-info">cancel</lable>
                                                                <?php endif; ?>,
                                                            </td>
                                                            <td>
                                                                <?php if($exg->status == 0): ?>
                                                                    <label class="label label-warning"> Pending </label>
                                                                <?php elseif($exg->status == 2): ?>
                                                                    <label class="label label-info"> Partially Pay </label>
                                                                <?php else: ?>
                                                                <?php endif; ?>
                                                            </td>
                                                        </tr>
                                                    <?php else: ?>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="theme-widget open">
                                    <div class="widget-header">
                                        <h5 class="acr1"> <i class="fa fa-chevron-right" aria-hidden="true"></i>Order history</h5>
                                        <span style="float: right;margin-top: -25px;">
                                            <button onclick="order_full('bid')" class="btn btn-sm btn-default">Bid</button>
                                             <button onclick="order_full('ask')"  class="btn btn-sm btn-default">Ask</button>
                                                <select onchange="call_order_full(this.value)">
                                                    <!-- <option value="ALL">All</option> -->
                                                    <option <?php if('BTC' == 'BTC'): ?> selected=" " <?php endif; ?> value="BTC">BTC/AED</option>
                                                    <option <?php if('LTC' == 'LTC'): ?> selected=" " <?php endif; ?> value="LTC">LTC/AED</option>
                                                    <option <?php if('ETH' == 'ETH'): ?> selected=" " <?php endif; ?> value="ETH">ETH/AED</option>
                                                    <option <?php if('XRP' == 'XRP'): ?> selected=" " <?php endif; ?> value="XRP">XRP/AED</option>
                                                </select>
                                        </span>
                                       

                                    </div>
                                    <div class="widget-body">
                                        <div class="table-responsive">
                                        <table id="order-his-tbl" class="display" style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Side</th>
                                                <th>order_id</th>
                                                <th>Pair</th>
                                                <th>Price</th>
                                                <th>Amount</th>
                                                <th>Fee</th>
                                                <th>TOTAL</th>
                                                <th>Status</th>
                                            </tr>
                                            </thead>
                                            <tbody id="order_full">
                                            <?php $i=1; $exchange_data = array(); ?>
                                            <?php $__currentLoopData = $exchange_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $exg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            
                                                <?php if($exg->status == 3 || $exg->status == 1): ?>
                                                    <?php  if($exg->side == 'buy') { $color='#fbe9e7'; }
                                                    elseif($exg->side == 'sell') {  $color='#e8f5e9'; }
                                                    else {}  ?>
                                                    <?php $total = ($exg->price) * ($exg->same_price);  ?>
                                                    <tr onclick="setvalue('<?php echo e($exg->same_price); ?>','<?php echo e($exg->price); ?>')" style="background-color: <?php echo $color; ?>">
                                                        <td><b><?php echo e($i++); ?></b></td>
                                                        <td><b><?php echo e($exg->side); ?></b></td>
                                                        <td><?php echo e($exg->order_id); ?></td>
                                                        <td><?php echo e($exg->symbol); ?></td>
                                                        <td><?php echo e(number_format($exg->same_price, 2)); ?></td>
                                                        <td><?php echo e(number_format($exg->price, 2)); ?></td>
                                                        <td><?php echo e(number_format($exg->buy_fees, 2)); ?></td>
                                                        <td><?php echo e(number_format($exg->final_aed, 2)); ?></td>

                                                        <td>
                                                            <?php if($exg->status == 1): ?>
                                                                <label class="label label-success"> Success </label>
                                                            <?php elseif($exg->status == 3): ?>
                                                                <label class="label label-danger"> Cancel </label>
                                                            <?php else: ?>
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>
                                                <?php else: ?>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            

                            
                        </div>
                    
                
            </div>



        <!-- jQuery 2.2.3 -->
        <script src="<?php echo e(asset('assets/plugins/jQuery/jquery-2.2.3.min.js')); ?>"></script>

        <script src="<?php echo e(asset('assets/plugins/bootstrap-toastr/toastr.min.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/plugins/jqueryui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo e(asset('assets/bootstrap/js/bootstrap.min.js')); ?>"></script>
    <!--<script src="<?php echo e(asset('assets/plugins/datepicker/bootstrap-datepicker.min.js')); ?>"
            type="text/javascript"></script>-->
        <script src="<?php echo e(asset('assets/bootstrap/js/jquery.mCustomScrollbar.js')); ?>"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <!-- /page container -->
        <script src="<?php echo e(asset('assets/plugins/sweetalert2/sweetalert2.min.js')); ?>"></script>
        <script src="<?php echo e(asset('assets/plugins/toastr/toastr.min.js')); ?>"></script>
        <script src="<?php echo e(asset('assets/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js')); ?>"
                type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/plugins/jquery-validation/jquery.validate.min.js')); ?>"
                type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/plugins/jquery-validation/additional-methods.min.js')); ?>"
                type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/plugins/moment/js/moment.min.js')); ?>"
                type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')); ?>"
                type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/plugins/bootstrap-touchspin/bootstrap.touchspin.min.js')); ?>"
                type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/plugins/tinymce/tinymce.min.js')); ?>"
                type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/plugins/fancybox/jquery.fancybox.js')); ?>"
                type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/plugins/jquery.numeric.js')); ?>"></script>

        
        
        <script src="<?php echo e(asset('assets/themes/limitless/js/core/app.js')); ?>"></script>
        <script src="<?php echo e(asset('assets/themes/limitless/js/plugins/ui/ripple.min.js')); ?>"></script>
        <script src="<?php echo e(asset('assets/themes/limitless/js/plugins/forms/styling/uniform.min.js')); ?>"></script>
        <script src="<?php echo e(asset('assets/plugins/select2/select2.min.js')); ?>"></script>
        <!-- SlimScroll 1.3.0 -->
        <script src="<?php echo e(asset('assets/themes/limitless/js/plugins/tables/datatables/datatables.min.js')); ?>"></script>


<?php echo $__env->yieldContent('footer-scripts'); ?>
<!-- ChartJS 1.0.1 -->
<script src="<?php echo e(asset('assets/themes/limitless/js/custom.js')); ?>"></script>


<script src="https://d3dy5gmtp8yhk7.cloudfront.net/2.1/pusher.min.js"></script>


<script>

    $(".acr").click(function(){
        $(this).parent().toggleClass("open");
    });

    $(".acr1").click(function(){
        $(this).parent().parent().toggleClass("open");
        $(this).parent().toggleClass("acr1");
    });

    $('#order-tbl').DataTable();
    $('#order-data-table3').DataTable();
    $('#balance-tbl').DataTable();
    $('#block-1-tbl').DataTable();
    $('#block-2-tbl').DataTable();
    
     $('#order-his-tbl').DataTable( {
        "order": [[ 0, "desc" ]],
        "bInfo": false,
        "searching": false,
        "bLengthChange": false,
        "language": {
        "emptyTable":     "No previous orders"
    }
    } );

     $('#example12').DataTable( {
        "order": [[ 0, "desc" ]],
        "bInfo": false,
        "searching": false,
        "bLengthChange": false,
        "language": {
        "emptyTable":     "No open orders"
    }
    } );

   


    $('[data-toggle="tooltip"]').tooltip();


</script>
<script type="text/javascript">
    $(window).load(function() {
        $(".loader").fadeOut("slow");
    });



</script>


<script src="<?php echo e(asset('candle/js/highstock.js')); ?>"></script>
<script src="<?php echo e(asset('candle/js/exporting.js')); ?>"></script>
<script src="<?php echo e(asset('candle/js/btc_chart_head.js')); ?>"></script>

<script type="text/javascript">

    $(document).ready( function(){
        call_all('XRP');
    });

    

    function call_all(coin) {

        callshoket(coin);
        liveorders(coin);
        tradehistory(coin)
        loadcharts(coin, 'AED', 500);

        $.ajax({
            url: '<?php echo e(url('')); ?>/exchange/call_order_middle',
            type: 'post',
            data: {
                option: coin,
                _token: "<?php echo e(csrf_token()); ?>"
            },
            success: function(response) {
                var buy = response.data;
                //Get the total rows
                $.each(buy, function(index, value) {
                    var t = $('#example12').DataTable();
                    var btn = "<lable onclick='cancel_order_user("+value.id+","+value.order_id+")' class='btn btn-info'>cancel</lable>";
                    var counter = 1;
                    
                    if (value.status == 0) {

                        var row = '<label class="label label-info buycount "> Pending</label>';
                    }
                    else if (value.status == 1) {
                        var row = '<label class="label label-success buycount ">  Success </label>';
                    }
                    else if (value.status == 2) {
                        var row = '<label class="label label-warning buycount ">  Partially </label>';
                    }
                    else if (value.status == 3) {
                        var row = '<label class="label label-danger buycount ">  Cancel </label>';
                    }
                    
                    
                     if (value.side == 'buy') {

                        var rowside = '<label class="label label-info buycount "> Buy</label>';
                    }
                    else if (value.side == 'sell') {
                        var rowside = '<label class="label label-success buycount ">  Sell </label>';
                    }

                    t.row.add([
                        value.id,
                        rowside,
                        value.order_id,
                        value.symbol,
                        value.same_price,
                        value.price,
                        value.buy_fees,
                        value.final_aed,
                        btn,
                        row
                    ]).draw(false);
                });
            }
        });

        $.ajax({
            url: '<?php echo e(url('')); ?>/exchange/call_order_full',
            type: 'post',
            data:{ option:coin,_token:"<?php echo e(csrf_token()); ?>"},
            success: function(response) {
                var buy = response.data;
                //Get the total rows
                $.each(buy, function(index, value) {
                    var t = $('#order-his-tbl').DataTable();
                    var counter = 1;
                    
                    if (value.status == 0) {

                        var row = '<label class="label label-info buycount "> Pending</label>';
                    }
                    else if (value.status == 1) {
                        var row = '<label class="label label-success buycount ">  Success </label>';
                    }
                    else if (value.status == 2) {
                        var row = '<label class="label label-warning buycount ">  Partially </label>';
                    }
                    else if (value.status == 3) {
                        var row = '<label class="label label-danger buycount ">  Cancel </label>';
                    }
                    
                    
                     if (value.side == 'buy') {

                        var rowside = '<label class="label label-info buycount "> Buy</label>';
                    }
                    else if (value.side == 'sell') {
                        var rowside = '<label class="label label-success buycount ">  Sell </label>';
                    }

                    t.row.add([
                        value.id,
                        rowside,
                        value.order_id,
                        value.symbol,
                        value.same_price,
                        value.price,
                        value.buy_fees,
                        value.final_aed,
                        row
                    ]).draw(false);
                });
                
            }

        });
    


    function call_order(option) {
        var t = $('#example12').DataTable();
        t.rows()
        .remove()
        .draw();

        $.ajax({
            url: '<?php echo e(url('')); ?>/exchange/call_order_middle',
            type: 'post',
            data: {
                option: option,
                _token: "<?php echo e(csrf_token()); ?>"
            },
            success: function(response) {
                var buy = response.data;
                //Get the total rows
                $.each(buy, function(index, value) {
                    var t = $('#example12').DataTable();
                    var btn = "<lable onclick='cancel_order_user("+value.id+","+value.order_id+")' class='btn btn-info'>cancel</lable>";
                    var counter = 1;
                    
                    if (value.status == 0) {

                        var row = '<label class="label label-info buycount "> Pending</label>';
                    }
                    else if (value.status == 1) {
                        var row = '<label class="label label-success buycount ">  Success </label>';
                    }
                    else if (value.status == 2) {
                        var row = '<label class="label label-warning buycount ">  Partially </label>';
                    }
                    else if (value.status == 3) {
                        var row = '<label class="label label-danger buycount ">  Cancel </label>';
                    }
                    
                    if (value.side == 'buy') {

                        var rowside = '<label class="label label-info buycount "> Buy</label>';
                    }
                    else if (value.side == 'sell') {
                        var rowside = '<label class="label label-success buycount ">  Sell </label>';
                    }

                    t.row.add([
                        value.id,
                        rowside,
                        value.order_id,
                        value.symbol,
                        value.same_price,
                        value.price,
                        value.buy_fees,
                        value.final_aed,
                        btn,
                        row
                    ]).draw(false);
                });
                
            }

        });
    }

    function call_order_full(option)
    {
        var t = $('#order-his-tbl').DataTable();
        t.rows()
        .remove()
        .draw();
        
        $.ajax({
            url: '<?php echo e(url('')); ?>/exchange/call_order_full',
            type: 'post',
            data:{ option:option,_token:"<?php echo e(csrf_token()); ?>"},
            success: function(response) {
                var buy = response.data;
                //Get the total rows
                $.each(buy, function(index, value) {
                    var t = $('#order-his-tbl').DataTable();
                    var counter = 1;
                    
                    if (value.status == 0) {

                        var row = '<label class="label label-info buycount "> Pending</label>';
                    }
                    else if (value.status == 1) {
                        var row = '<label class="label label-success buycount ">  Success </label>';
                    }
                    else if (value.status == 2) {
                        var row = '<label class="label label-warning buycount ">  Partially </label>';
                    }
                    else if (value.status == 3) {
                        var row = '<label class="label label-danger buycount ">  Cancel </label>';
                    }
                    
                    if (value.side == 'buy') {

                        var rowside = '<label class="label label-info buycount "> Buy</label>';
                    }
                    else if (value.side == 'sell') {
                        var rowside = '<label class="label label-success buycount ">  Sell </label>';
                    }

                    t.row.add([
                        value.id,
                        rowside,
                        value.order_id,
                        value.symbol,
                        value.same_price,
                        value.price,
                        value.buy_fees,
                        value.final_aed,
                        row
                    ]).draw(false);
                });
                
            }

        });
    }

    function order_full(option)
    {
        
         var t = $('#order-his-tbl').DataTable();
        t.rows()
        .remove()
        .draw();

        $.ajax({
            url: '<?php echo e(url('')); ?>/exchange/order_full',
            type: 'post',
            data:{ option:option,_token:"<?php echo e(csrf_token()); ?>"},
            success: function(response) {
                var buy = response.data;
                //Get the total rows
                $.each(buy, function(index, value) {
                    var t = $('#order-his-tbl').DataTable();
                    var counter = 1;
                    
                    if (value.status == 0) {

                        var row = '<label class="label label-info buycount "> Pending</label>';
                    }
                    else if (value.status == 1) {
                        var row = '<label class="label label-success buycount ">  Success </label>';
                    }
                    else if (value.status == 2) {
                        var row = '<label class="label label-warning buycount ">  Partially </label>';
                    }
                    else if (value.status == 3) {
                        var row = '<label class="label label-danger buycount ">  Cancel </label>';
                    }
                    
                    if (value.side == 'buy') {

                        var rowside = '<label class="label label-info buycount "> Buy</label>';
                    }
                    else if (value.side == 'sell') {
                        var rowside = '<label class="label label-success buycount ">  Sell </label>';
                    }

                    t.row.add([
                        value.id,
                        rowside,
                        value.order_id,
                        value.symbol,
                        value.same_price,
                        value.price,
                        value.buy_fees,
                        value.final_aed,
                        row
                    ]).draw(false);
                });
                
            }
        });
    }


    function order_middle(option)
    {
         var t = $('#example12').DataTable();
        t.rows()
        .remove()
        .draw();

        $('#order_middle').empty();
        $.ajax({
            url: '<?php echo e(url('')); ?>/exchange/order_middle',
            type: 'post',
            data:{ option:option,_token:"<?php echo e(csrf_token()); ?>"},
            success: function(response) {
                var buy = response.data;
                //Get the total rows
                $.each(buy, function(index, value) {
                    var t = $('#example12').DataTable();
                    var btn = "<lable onclick='cancel_order_user("+value.id+","+value.order_id+")' class='btn btn-info'>cancel</lable>";
                    var counter = 1;
                    
                    if (value.status == 0) {

                        var row = '<label class="label label-info buycount "> Pending</label>';
                    }
                    else if (value.status == 1) {
                        var row = '<label class="label label-success buycount ">  Success </label>';
                    }
                    else if (value.status == 2) {
                        var row = '<label class="label label-warning buycount ">  Partially </label>';
                    }
                    else if (value.status == 3) {
                        var row = '<label class="label label-danger buycount ">  Cancel </label>';
                    }
                    
                    if (value.side == 'buy') {

                        var rowside = '<label class="label label-info buycount "> Buy</label>';
                    }
                    else if (value.side == 'sell') {
                        var rowside = '<label class="label label-success buycount ">  Sell </label>';
                    }

                    t.row.add([
                        value.id,
                        rowside,
                        value.order_id,
                        value.symbol,
                        value.same_price,
                        value.price,
                        value.buy_fees,
                        value.final_aed,
                        btn,
                        row
                    ]).draw(false);
                });
                
            }

        });
        
    }

    setInterval(function () {
                getMessage();
            }, 60 *60);

            function getMessage() {
                var table = $('#example12').dataTable();
                var numItems = table.fnGetData().length;
                $.ajax({
                    type: 'post',
                    url: '<?php echo e(url('buycountuser')); ?>',
                    data: {_token: '<?php echo e(csrf_token()); ?>',number:numItems},
                    success: function (data) {

                        if(data.error == 'true')
                        {
                            var coin = $('#coin_use').val();
                            // alert(coin);
                            call_order_full(coin);
                            call_order(coin);
                            
                        }
                    }

                });
            }


</script>



<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>